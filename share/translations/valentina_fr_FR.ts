<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AddBackgroundImage</name>
    <message>
        <source>add background image</source>
        <translation>Ajouter une image en arrière plan</translation>
    </message>
</context>
<context>
    <name>AddDet</name>
    <message>
        <source>add detail</source>
        <translation type="vanished">Ajouter une pièce</translation>
    </message>
</context>
<context>
    <name>AddGroup</name>
    <message>
        <source>add group</source>
        <translation>Ajouter groupe</translation>
    </message>
</context>
<context>
    <name>AddItemToGroup</name>
    <message>
        <source>Add item to group</source>
        <translation>Ajouter un objet au groupe</translation>
    </message>
</context>
<context>
    <name>AddPatternPiece</name>
    <message>
        <source>add pattern piece %1</source>
        <translation>Ajouter un élément de patron %1</translation>
    </message>
</context>
<context>
    <name>AddPiece</name>
    <message>
        <source>add detail</source>
        <translation>Ajouter une pièce</translation>
    </message>
</context>
<context>
    <name>AddToCalc</name>
    <message>
        <source>add object</source>
        <translation>Ajouter un objet</translation>
    </message>
</context>
<context>
    <name>AddUnionDetails</name>
    <message>
        <source>add union details</source>
        <translation type="vanished">Ajouter des pièces fusionnées</translation>
    </message>
</context>
<context>
    <name>ChangeGroupOptions</name>
    <message>
        <source>rename group</source>
        <translation>Renommer le groupe</translation>
    </message>
</context>
<context>
    <name>ChangeGroupVisibility</name>
    <message>
        <source>change group visibility</source>
        <translation>Modifier la visibilité du groupe</translation>
    </message>
</context>
<context>
    <name>ChangeMultipleGroupsVisibility</name>
    <message>
        <source>change multiple groups visibility</source>
        <translation>Modifier la visibilité de plusieurs groupes</translation>
    </message>
</context>
<context>
    <name>ColorPickerPopup</name>
    <message>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
</context>
<context>
    <name>CommunityPage</name>
    <message>
        <source>Server</source>
        <translation type="vanished">Serveur</translation>
    </message>
    <message>
        <source>Server name/IP</source>
        <translation type="vanished">Nom du serveur/IP</translation>
    </message>
    <message>
        <source>Secure connection</source>
        <translation type="vanished">Connexion séurisée</translation>
    </message>
    <message>
        <source>Proxy settings</source>
        <translation type="vanished">Réglages du proxy</translation>
    </message>
    <message>
        <source>Use Proxy</source>
        <translation type="vanished">Utiliser un proxy</translation>
    </message>
    <message>
        <source>Proxy address</source>
        <translation type="vanished">Adresse du proxy</translation>
    </message>
    <message>
        <source>Proxy port</source>
        <translation type="vanished">Port du proxy</translation>
    </message>
    <message>
        <source>Proxy user</source>
        <translation type="vanished">Nom d&apos;utilisateur du proxy</translation>
    </message>
    <message>
        <source>Proxy pass</source>
        <translation type="vanished">Mot de passe du proxy</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation type="vanished">Paramètres utilisateur</translation>
    </message>
    <message>
        <source>User Name</source>
        <translation type="vanished">Nom de l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Save password</source>
        <translation type="vanished">Enregistrer le mot de passe</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Mot de passe</translation>
    </message>
    <message>
        <source>Server name/IP:</source>
        <translation type="vanished">Nom du serveur/IP :</translation>
    </message>
    <message>
        <source>Proxy address:</source>
        <translation type="vanished">Adresse du proxy :</translation>
    </message>
    <message>
        <source>Proxy port:</source>
        <translation type="vanished">Port du proxy :</translation>
    </message>
    <message>
        <source>Proxy user:</source>
        <translation type="vanished">Nom d&apos;utilisateur du proxy :</translation>
    </message>
    <message>
        <source>Proxy pass:</source>
        <translation type="vanished">Mot de passe du proxy :</translation>
    </message>
    <message>
        <source>User Name:</source>
        <translation type="vanished">Nom de l&apos;utilisateur :</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">Mot de passe :</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>Apply</source>
        <translation type="vanished">Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="vanished">&amp;Ok</translation>
    </message>
    <message>
        <source>Config Dialog</source>
        <translation type="vanished">Boîte de dialogue de configuration</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">Configuration</translation>
    </message>
    <message>
        <source>Pattern</source>
        <translation type="vanished">Patron</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="vanished">Communauté</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation type="vanished">Répertoires</translation>
    </message>
</context>
<context>
    <name>ConfigurationPage</name>
    <message>
        <source>Setup user interface language updated and will be used the next time start</source>
        <translation type="vanished">La langue de l&apos;interface a été mise à jour et sera utilisée lors du prochain démarrage</translation>
    </message>
    <message>
        <source>Default unit updated and will be used the next pattern creation</source>
        <translation type="vanished">L&apos;unité par défaut a été mise à jour et sera utilisée lors de la  prochaine création de patron</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Sauvegarder</translation>
    </message>
    <message>
        <source>Auto-save modified pattern</source>
        <translation type="vanished">Sauvegarde automatique des changements du patron</translation>
    </message>
    <message>
        <source>min</source>
        <translation type="vanished">minimum</translation>
    </message>
    <message>
        <source>Interval:</source>
        <translation type="vanished">Intervalle :</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Langue</translation>
    </message>
    <message>
        <source>GUI language</source>
        <translation type="vanished">Langue de l&apos;interface</translation>
    </message>
    <message>
        <source>Decimal separator parts</source>
        <translation type="vanished">Séparateur de décimale</translation>
    </message>
    <message>
        <source>With OS options (%1)</source>
        <translation type="vanished">Utiliser les réglages système (%1)</translation>
    </message>
    <message>
        <source>Default unit</source>
        <translation type="vanished">Unité par défaut</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation type="vanished">Centimètres</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation type="vanished">Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation type="vanished">Pouces</translation>
    </message>
    <message>
        <source>Label language</source>
        <translation type="vanished">Langue des libellés</translation>
    </message>
    <message>
        <source>Send crash reports</source>
        <translation type="vanished">Envoyer les rapports de plantage</translation>
    </message>
    <message>
        <source>Send crash reports (recommended)</source>
        <translation type="vanished">Envoyer les rapports de plantage (recommandé)</translation>
    </message>
    <message>
        <source>After each crash Valentina collect information that may help us fix a problem. We do not collect any personal information. Find more about what &lt;a href=&quot;https://bitbucket.org/dismine/valentina/wiki/manual/Crash_reports&quot;&gt;kind of information&lt;/a&gt; we collect.</source>
        <translation type="vanished">Après chaque plantage, Valentina collecte des informations qui peuvent nous aider à résoudre le problème. Nous ne collectons pas d&apos;informations personnelles à cette occasion. En savoir plus sur &lt;a href=&quot;https://bitbucket.org/dismine/valentina/wiki/manual/Crash_reports&quot;&gt;le type d&apos;informations&lt;/a&gt; que nous collectons.</translation>
    </message>
    <message>
        <source>Pattern Editing</source>
        <translation type="vanished">Édition du Patron</translation>
    </message>
    <message>
        <source>Confirm item deletion</source>
        <translation type="vanished">Confirmez la suppression de l&apos;objet</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation type="vanished">Barre d&apos;outils</translation>
    </message>
    <message>
        <source>The text appears under the icon. (recommended for beginners.)</source>
        <translation type="vanished">Le texte apparait sous l’icône. (recommandé pour les débutants)</translation>
    </message>
    <message>
        <source>GUI language:</source>
        <translation type="vanished">Langue de l&apos;interface :</translation>
    </message>
    <message>
        <source>Decimal separator parts:</source>
        <translation type="vanished">Séparateur de décimale :</translation>
    </message>
    <message>
        <source>Default unit:</source>
        <translation type="vanished">Unité par défaut :</translation>
    </message>
    <message>
        <source>Label language:</source>
        <translation type="vanished">Langue des libellés :</translation>
    </message>
    <message>
        <source>Pattern making system</source>
        <translation type="vanished">Méthode de patronage</translation>
    </message>
    <message>
        <source>Pattern making system:</source>
        <translation type="vanished">Méthode de patronage :</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation type="vanished">Auteur :</translation>
    </message>
    <message>
        <source>Book:</source>
        <translation type="vanished">Livre :</translation>
    </message>
    <message>
        <source>The Default unit has been updated and will be used as the default for the next pattern you create.</source>
        <translation type="vanished">L&apos;unité a été mise à jour et sera utilisée par défaut lors de la prochaine création de patron.</translation>
    </message>
    <message>
        <source>After each crash Valentina collects information that may help us fix the problem. We do not collect any personal information. Find more about what &lt;a href=&quot;https://bitbucket.org/dismine/valentina/wiki/manual/Crash_reports&quot;&gt;kind of information&lt;/a&gt; we collect.</source>
        <translation type="vanished">Après chaque plantage, Valentina collecte des informations qui peuvent nous servir à régler le problème. Nous ne collectons aucune information personnelle. Pour en savoir plus : &lt;a href=&quot;https://bitbucket.org/dismine/valentina/wiki/manual/Crash_reports&quot;&gt;le genre d&apos;information que nous collectons&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>The text appears under the icon (recommended for beginners).</source>
        <translation type="vanished">Le texte apparait sous l&apos;icone (recommandé pour les débutants).</translation>
    </message>
    <message>
        <source>After each crash Valentina collects information that may help us fix the problem. We do not collect any personal information. Find more about what %1kind of information%2 we collect.</source>
        <translation type="vanished">Après chaque plantage, Valentina collecte des informations qui peuvent nous servir à régler le problème. Nous ne collectons aucune information personnelle. Pour en savoir plus : %1 le genre d&apos;information%2 que nous collectons.</translation>
    </message>
</context>
<context>
    <name>DelGroup</name>
    <message>
        <source>delete group</source>
        <translation>supprimer le groupe</translation>
    </message>
</context>
<context>
    <name>DelTool</name>
    <message>
        <source>delete tool</source>
        <translation>Outil de suppression</translation>
    </message>
</context>
<context>
    <name>DeleteBackgroundImage</name>
    <message>
        <source>delete background image</source>
        <translation>Supprimer l&apos;image en arrière-plan</translation>
    </message>
</context>
<context>
    <name>DeleteDetail</name>
    <message>
        <source>delete tool</source>
        <translation type="vanished">Outil de suppression</translation>
    </message>
</context>
<context>
    <name>DeletePatternPiece</name>
    <message>
        <source>delete pattern piece %1</source>
        <translation>Supprimer l&apos;élément de patron %1</translation>
    </message>
</context>
<context>
    <name>DeletePiece</name>
    <message>
        <source>delete tool</source>
        <translation>Outil de suppression</translation>
    </message>
</context>
<context>
    <name>Detail</name>
    <message>
        <source>Fabric</source>
        <translation type="vanished">Tissu</translation>
    </message>
    <message>
        <source>Lining</source>
        <translation type="vanished">Doublure</translation>
    </message>
    <message>
        <source>Interfacing</source>
        <translation type="vanished">Entoilage</translation>
    </message>
    <message>
        <source>Interlining</source>
        <translation type="vanished">Triplure</translation>
    </message>
</context>
<context>
    <name>DialogAboutApp</name>
    <message>
        <source>About Valentina</source>
        <translation>À propos de Valentina</translation>
    </message>
    <message>
        <source>Valentina version</source>
        <translation>Version de Valentina</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Contributeurs</translation>
    </message>
    <message>
        <source>Built on %3 at %4</source>
        <translation type="vanished">Compilé le %3 à %4</translation>
    </message>
    <message>
        <source>Web site : %1</source>
        <translation>Site web : %1</translation>
    </message>
    <message>
        <source>Cannot open your default browser</source>
        <translation>Impossible d&apos;ouvrir votre navigateur par défaut</translation>
    </message>
    <message>
        <source>Build revision:</source>
        <translation>Version compilée :</translation>
    </message>
    <message>
        <source>Built on %1 at %2</source>
        <translation>Compilé le %1 à %2</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation>Vérifier les Mises à Jour</translation>
    </message>
</context>
<context>
    <name>DialogAboutTape</name>
    <message>
        <source>About Tape</source>
        <translation>A propos de Tape</translation>
    </message>
    <message>
        <source>Tape version</source>
        <translation>Version de Tape</translation>
    </message>
    <message>
        <source>Build revision:</source>
        <translation>N° de version :</translation>
    </message>
    <message>
        <source>This program is part of Valentina project.</source>
        <translation>Ce programme fait partie du projet Valentina.</translation>
    </message>
    <message>
        <source>Build revision: %1</source>
        <translation>N° de version : %1</translation>
    </message>
    <message>
        <source>Built on %3 at %4</source>
        <translation type="vanished">Compilé le %3 à %4</translation>
    </message>
    <message>
        <source>Web site : %1</source>
        <translation>Site web : %1</translation>
    </message>
    <message>
        <source>Cannot open your default browser</source>
        <translation>Impossible d&apos;ouvrir votre navigateur par défaut</translation>
    </message>
    <message>
        <source>Built on %1 at %2</source>
        <translation>Compilé le %1 à %2</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation>Vérifier les Mises à Jour</translation>
    </message>
</context>
<context>
    <name>DialogAddBackgroundImage</name>
    <message>
        <source>Background image</source>
        <translation>Image en arrière-plan</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Determine should an image built in or added as path to the file.</source>
        <translation>Choisir si l&apos;image est intégrée au fichier ou ajoutée en tant que chemin d&apos;accès à un fichier.</translation>
    </message>
    <message>
        <source>Built in</source>
        <translation>Image intégrée</translation>
    </message>
</context>
<context>
    <name>DialogAlongLine</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation type="vanished">Premier point de la ligne</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Second point of line</source>
        <translation type="vanished">Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation type="vanished">Afficher la ligne du premier point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le deuxième point d&apos;une ligne</translation>
    </message>
    <message>
        <source>Point at distance along line</source>
        <translation>Point à distance dans l&apos;axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur du trait</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Editer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point :</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>First point of the line</source>
        <translation>Premier point de la ligne</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point :</translation>
    </message>
    <message>
        <source>Second point of the line</source>
        <translation>Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne :</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne :</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogArc</name>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>Value of radius</source>
        <translation type="vanished">Valeur du rayon</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>First angle</source>
        <translation type="vanished">Premier angle</translation>
    </message>
    <message>
        <source>Value of first angle</source>
        <translation type="vanished">Valeur du premier angle</translation>
    </message>
    <message>
        <source>Second angle</source>
        <translation type="vanished">Deuxième angle</translation>
    </message>
    <message>
        <source>Value of second angle</source>
        <translation type="vanished">Valeur du deuxième angle</translation>
    </message>
    <message>
        <source>Center point</source>
        <translation type="vanished">Point central</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation type="vanished">Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Radius can&apos;t be negative</source>
        <translation>La valeur en radians ne peut pas être négative</translation>
    </message>
    <message>
        <source>Angles equal</source>
        <translation type="vanished">Les angles valent</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Edit radius</source>
        <translation>Editer le rayon</translation>
    </message>
    <message>
        <source>Edit first angle</source>
        <translation>Editer le premier angle</translation>
    </message>
    <message>
        <source>Edit second angle</source>
        <translation>Editer le deuxième angle</translation>
    </message>
    <message>
        <source>Radius:</source>
        <translation>Rayon :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calulation</source>
        <translation type="vanished">Calcul</translation>
    </message>
    <message>
        <source>First angle:</source>
        <translation>Premier angle :</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Second angle:</source>
        <translation>Deuxième angle:</translation>
    </message>
    <message>
        <source>Center point:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Select center point of the arc</source>
        <translation>Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur:</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo :</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogArcWithLength</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>Value of radius</source>
        <translation type="vanished">Valeur du rayon</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>First angle</source>
        <translation type="vanished">Premier angle</translation>
    </message>
    <message>
        <source>Value of first angle</source>
        <translation type="vanished">Valeur du premier angle</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Arc length</source>
        <translation type="vanished">Longueur de l&apos;arc</translation>
    </message>
    <message>
        <source>Center point</source>
        <translation type="vanished">Point central</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation type="vanished">Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Edit radius</source>
        <translation>Editer le rayon</translation>
    </message>
    <message>
        <source>Edit the first angle</source>
        <translation>Editer le premier angle</translation>
    </message>
    <message>
        <source>Edit the arc length</source>
        <translation>Éditer la longueur de l&apos;arc</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Radius can&apos;t be negative</source>
        <translation type="vanished">Le rayon ne peut pas être négatif</translation>
    </message>
    <message>
        <source>Length can&apos;t be equal 0</source>
        <translation type="vanished">La longueur ne peut pas être égale à 0</translation>
    </message>
    <message>
        <source>Radius:</source>
        <translation>Rayon :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>First angle:</source>
        <translation>Premier angle :</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Center point:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo :</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogBisector</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>First point of angle</source>
        <translation type="vanished">Premier point de l&apos;angle</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Second point of angle</source>
        <translation type="vanished">Deuxième point de l&apos;angle</translation>
    </message>
    <message>
        <source>Third point</source>
        <translation type="vanished">Troisième point</translation>
    </message>
    <message>
        <source>Third point of angle</source>
        <translation type="vanished">Troisième point de l&apos;angle</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from second point to this point</source>
        <translation type="vanished">Afficher la ligne du deuxième point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Select second point of angle</source>
        <translation>Choisir le deuxième point de l&apos;angle</translation>
    </message>
    <message>
        <source>Select third point of angle</source>
        <translation>Choisir le troisième point de l&apos;angle</translation>
    </message>
    <message>
        <source>Point along bisector</source>
        <translation>Point sur bissectrice</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de la ligne</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Editer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point :</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point :</translation>
    </message>
    <message>
        <source>Third point:</source>
        <translation>Troisième point :</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne :</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne :</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogCubicBezier</name>
    <message>
        <source>Cubic bezier</source>
        <translation type="vanished">Courbe de Bezier Cubique</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point :</translation>
    </message>
    <message>
        <source>Third point:</source>
        <translation>Troisième point :</translation>
    </message>
    <message>
        <source>Fourth point:</source>
        <translation>Quatrième point :</translation>
    </message>
    <message>
        <source>Select the second point of curve</source>
        <translation>Choisir le deuxième point de la courbe</translation>
    </message>
    <message>
        <source>Select the third point of curve</source>
        <translation>Choisir le troisième point de la courbe</translation>
    </message>
    <message>
        <source>Select the fourth point of curve</source>
        <translation>Choisir le quatrième point de la courbe</translation>
    </message>
    <message>
        <source>Invalid spline</source>
        <translation>Spline invalide</translation>
    </message>
    <message>
        <source>Tool cubic bezier</source>
        <translation>Outil courbe de bezier cubique</translation>
    </message>
    <message>
        <source>Pen Style:</source>
        <translation>Style de stylo :</translation>
    </message>
    <message>
        <source>Approximation Scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogCubicBezierPath</name>
    <message>
        <source>Dialog cubic bezier path</source>
        <translation type="vanished">Dialogue trajectoire de Bezier Cubique</translation>
    </message>
    <message>
        <source>Point:</source>
        <translation>Point :</translation>
    </message>
    <message>
        <source>List of points</source>
        <translation>Liste de points</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Invalid spline path</source>
        <translation>Trajectoire de spline invalide</translation>
    </message>
    <message>
        <source>Tool cubic bezier path</source>
        <translation>Outil trajectoire de la courbe de bezier</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Cannot find point with id %1</source>
        <translation>Impossible de trouver le point %1</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogCurveIntersectAxis</name>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Value of angle</source>
        <translation type="vanished">Valeur de l&apos;angle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Montrer le calcul complet dans une boite de dioalogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Axis point</source>
        <translation type="vanished">Point sur l&apos;axe</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation type="vanished">Courbe</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation type="vanished">Montre la ligne depuis le premier point jusqu&apos;a ce point</translation>
    </message>
    <message>
        <source>Select axis point</source>
        <translation>Choisir un point d&apos;axe</translation>
    </message>
    <message>
        <source>Point intersect curve and axis</source>
        <translation>Point d&apos;intersection d&apos;une courbe et d&apos;un axe</translation>
    </message>
    <message>
        <source>Axis Point</source>
        <translation type="vanished">Origine de l&apos;Axe</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de la ligne</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Éditer l&apos;angle</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Axis point:</source>
        <translation>Point d&apos;axe :</translation>
    </message>
    <message>
        <source>Curve:</source>
        <translation>Courbe:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1 :</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
</context>
<context>
    <name>DialogCutArc</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Valeur de Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation type="vanished">Arc</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Segment an arc</source>
        <translation>Segmenter un arc</translation>
    </message>
    <message>
        <source>Selected arc</source>
        <translation type="vanished">Arc sélectionné</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Éditer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Arc:</source>
        <translation>Arc:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation type="vanished">Couleur:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1 :</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogCutSpline</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation type="vanished">Courbe</translation>
    </message>
    <message>
        <source>Selected curve</source>
        <translation type="vanished">Courbe sélectionnée</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Segmenting a simple curve</source>
        <translation>Segmenter une courbe simple</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Éditer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Curve:</source>
        <translation>Courbe:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation type="vanished">Couleur:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1 :</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogCutSplinePath</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation type="vanished">Courbe</translation>
    </message>
    <message>
        <source>Selected curve path</source>
        <translation type="vanished">Trajectoire de courbe sélectionné</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Segment a curved path</source>
        <translation>Segmenter une courbe complexe</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Éditer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Curve:</source>
        <translation>Courbe:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation type="vanished">Couleur:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1 :</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogDateTimeFormats</name>
    <message>
        <source>Label date time editor</source>
        <translation>Editeur d&apos;étiquette date et heure</translation>
    </message>
    <message>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <source>Insert a format</source>
        <translation>Insérer un format</translation>
    </message>
    <message>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
</context>
<context>
    <name>DialogDetail</name>
    <message>
        <source>Detail</source>
        <translation type="vanished">Pièces</translation>
    </message>
    <message>
        <source>Bias X</source>
        <translation type="vanished">Biais X</translation>
    </message>
    <message>
        <source>cm</source>
        <translation type="vanished">Cm</translation>
    </message>
    <message>
        <source>Bias Y</source>
        <translation type="vanished">Biais Y</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Options</translation>
    </message>
    <message>
        <source>Name of detail</source>
        <translation type="vanished">Nom de la pièce de patron</translation>
    </message>
    <message>
        <source>Seam allowance</source>
        <translation type="vanished">Marge de couture</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="vanished">Largeur</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation type="vanished">Fermé</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Supprimer</translation>
    </message>
    <message>
        <source>Got wrong scene object. Ignore.</source>
        <translation type="vanished">Récupération d&apos;un mauvais objet de scène. Ignorer.</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation type="vanished">Inverser</translation>
    </message>
    <message>
        <source>Seam allowance tool</source>
        <translation type="vanished">Outil marge de couture</translation>
    </message>
    <message>
        <source>All objects in path should follow in clockwise direction.</source>
        <translation type="vanished">Tous les objets du chemin doivent se suivre dans le sens des aiguilles d&apos;une montre.</translation>
    </message>
    <message>
        <source>Scroll down the list</source>
        <translation type="vanished">Faire défiler la liste</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Scroll up the list</source>
        <translation type="vanished">Faire défiler la liste</translation>
    </message>
    <message>
        <source>Ready!</source>
        <translation type="vanished">Prêt!</translation>
    </message>
    <message>
        <source>You need more points!</source>
        <translation type="vanished">Vous avez besoin de plus de points!</translation>
    </message>
    <message>
        <source>First point can not equal the last point!</source>
        <translation type="vanished">Le premier point ne peut être identique au premier!</translation>
    </message>
    <message>
        <source>You have double points!</source>
        <translation type="vanished">Vous avez des points en double!</translation>
    </message>
    <message>
        <source>You have to choose points in a clockwise direction!</source>
        <translation type="vanished">Vous devez choisir les points dans le sens des aiguilles d&apos;une montre!</translation>
    </message>
    <message>
        <source>Bias X:</source>
        <translation type="vanished">Biais X:</translation>
    </message>
    <message>
        <source>Bias Y:</source>
        <translation type="vanished">Biais Y:</translation>
    </message>
    <message>
        <source>Name of detail:</source>
        <translation type="vanished">Nom de la pièce de patron :</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="vanished">Largeur :</translation>
    </message>
    <message>
        <source>First point cannot be equal to the last point!</source>
        <translation type="vanished">Le premier point ne peut être identique au dernier!</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Général</translation>
    </message>
    <message>
        <source>Pattern piece data</source>
        <translation type="vanished">Données de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>Material/Cut number/Placement</source>
        <translation type="vanished">Matière/Numéro de coupe/Placement</translation>
    </message>
    <message>
        <source>Material type:</source>
        <translation type="vanished">Type de matière :</translation>
    </message>
    <message>
        <source>Cut number:</source>
        <translation type="vanished">Numéro de découpe :</translation>
    </message>
    <message>
        <source>Placement:</source>
        <translation type="vanished">Placement:</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Ajouter</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Retirer</translation>
    </message>
    <message>
        <source>Letter:</source>
        <translation type="vanished">Lettre :</translation>
    </message>
    <message>
        <source>Detail label visible</source>
        <translation type="vanished">Etiquette de pièce visible</translation>
    </message>
    <message>
        <source>Pattern label visible</source>
        <translation type="vanished">étiquette élément de patron visible</translation>
    </message>
    <message>
        <source>Fabric</source>
        <translation type="vanished">Tissu</translation>
    </message>
    <message>
        <source>Lining</source>
        <translation type="vanished">Lin</translation>
    </message>
    <message>
        <source>Interfacing</source>
        <translation type="vanished">Interfaçage</translation>
    </message>
    <message>
        <source>Interlining</source>
        <translation type="vanished">Interlignage</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Rien</translation>
    </message>
    <message>
        <source>Cut on fold</source>
        <translation type="vanished">Couper au pli</translation>
    </message>
    <message>
        <source>Cut %1 of %2%3</source>
        <translation type="vanished">Couper %1 de %2%3</translation>
    </message>
    <message>
        <source> on Fold</source>
        <translation type="vanished">au pli</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Mise à jour</translation>
    </message>
    <message>
        <source>on Fold</source>
        <translation type="vanished">au pli</translation>
    </message>
    <message>
        <source>You can choose one of the predefined materials or enter a new one</source>
        <translation type="vanished">Vous pouvez choisir un des matériaux prédéfinis ou en saisir un nouveau</translation>
    </message>
    <message>
        <source>Forbid piece be mirrored in a layout.</source>
        <translation type="vanished">Forbid piece be mirrored in a layout.</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation type="vanished">Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Letter of pattern piece</source>
        <translation type="vanished">Lettre de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>Name can&apos;t be empty</source>
        <translation type="vanished">La valeur Nom ne peut pas être vide</translation>
    </message>
    <message>
        <source>Grainline</source>
        <translation type="vanished">Droit-fil</translation>
    </message>
    <message>
        <source>Grainline visible</source>
        <translation type="vanished">Droit-fil visible</translation>
    </message>
    <message>
        <source>Rotation:</source>
        <translation type="vanished">Rotation:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation type="vanished">Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation type="vanished">Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation type="vanished">Longueur:</translation>
    </message>
    <message>
        <source>Infinite/undefined result</source>
        <translation type="vanished">Résultat infini ou non défini</translation>
    </message>
    <message>
        <source>Length should be positive</source>
        <translation type="vanished">La longueur doit être positive</translation>
    </message>
    <message>
        <source>Arrows:</source>
        <translation type="vanished">Fleches:</translation>
    </message>
    <message>
        <source>Both</source>
        <translation type="vanished">Les deux</translation>
    </message>
    <message>
        <source>Just front</source>
        <translation type="vanished">Juste le devant</translation>
    </message>
    <message>
        <source>Just rear</source>
        <translation type="vanished">Juste l&apos;arrière</translation>
    </message>
</context>
<context>
    <name>DialogDimensionCustomNames</name>
    <message>
        <source>Dimension custom names</source>
        <translation>Noms personnalisés dimensions</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Custom Name</source>
        <translation>Nom personnalisé</translation>
    </message>
</context>
<context>
    <name>DialogDimensionLabels</name>
    <message>
        <source>Dimension labels</source>
        <translation>Libellés des valeurs</translation>
    </message>
    <message>
        <source>Dimension:</source>
        <translation>Dimension :</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>DialogDuplicateDetail</name>
    <message>
        <source>Dialog duplicate detail</source>
        <translation>Boîte de dialogue duplication de pièce</translation>
    </message>
    <message>
        <source>Click to place duplicate</source>
        <translation>Clic gauche pour placer la copie</translation>
    </message>
</context>
<context>
    <name>DialogEditLabel</name>
    <message>
        <source>Edit label template</source>
        <translation>Editer le modèle d&apos;étiquette</translation>
    </message>
    <message>
        <source>Clear current and begin new label</source>
        <translation>Effacer l&apos;actuelle et commencer une nouvelle étiquette</translation>
    </message>
    <message>
        <source>Import from label template</source>
        <translation>Importer depuis un modèle d&apos;étiquette</translation>
    </message>
    <message>
        <source>Export label as template</source>
        <translation>Exporter comme modèle d&apos;étiquette</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Bold</source>
        <comment>Font formating</comment>
        <translation>Gras</translation>
    </message>
    <message>
        <source>Italic</source>
        <comment>Font formating</comment>
        <translation>Italique</translation>
    </message>
    <message>
        <source>Aligns with the left edge</source>
        <translation>Aligne avec le bord gauche</translation>
    </message>
    <message>
        <source>Centers horizontally in the available space</source>
        <translation>Centre horizontalement dans l&apos;espace disponible</translation>
    </message>
    <message>
        <source>Aligns with the right edge</source>
        <translation>Aligne avec le bord droit</translation>
    </message>
    <message>
        <source>Additional font size. Use to make a line bigger.</source>
        <translation>Taille supplémentaire de police d&apos;écriture.  Utiliser pour faire une plus grande ligne.</translation>
    </message>
    <message>
        <source>Text:</source>
        <translation>Texte:</translation>
    </message>
    <message>
        <source>Line of text</source>
        <translation>Ligne de texte</translation>
    </message>
    <message>
        <source>Insert placeholders</source>
        <translation>Insérer des espaces réservés</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Prévisualiser</translation>
    </message>
    <message>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
    <message>
        <source>Create new template</source>
        <translation>Créer un nouveau modèle</translation>
    </message>
    <message>
        <source>Creating new template will overwrite the current, do you want to continue?</source>
        <translation>Créer un nouveau patron écrasera le patron actuel, voulez-vous continuer?</translation>
    </message>
    <message>
        <source>Label template</source>
        <translation>Modèle d&apos;étiquette</translation>
    </message>
    <message>
        <source>Export label template</source>
        <translation>Exporter un modèle d&apos;étiquette</translation>
    </message>
    <message>
        <source>template</source>
        <translation>modèle</translation>
    </message>
    <message>
        <source>Could not save file</source>
        <translation>Impossible de sauvegarder le fichier</translation>
    </message>
    <message>
        <source>Import template</source>
        <translation>Importer un modèle</translation>
    </message>
    <message>
        <source>Import template will overwrite the current, do you want to continue?</source>
        <translation>Importer un patron écrasera le patron actuel, voulez-vous continuer?</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <source>Pattern name</source>
        <translation>Nom du patron</translation>
    </message>
    <message>
        <source>Pattern number</source>
        <translation>Numéro de patron</translation>
    </message>
    <message>
        <source>Company name or designer name</source>
        <translation>Nom de société ou du modéliste</translation>
    </message>
    <message>
        <source>Customer name</source>
        <translation>Nom du client</translation>
    </message>
    <message>
        <source>Pattern extension</source>
        <translation>Extension du patron</translation>
    </message>
    <message>
        <source>Pattern file name</source>
        <translation>Nom de fichier du patron</translation>
    </message>
    <message>
        <source>Measurments file name</source>
        <translation>Nom du fichier de mesures</translation>
    </message>
    <message>
        <source>Measurments extension</source>
        <translation>Extension de mesures</translation>
    </message>
    <message>
        <source>Piece letter</source>
        <translation>Lettre de la pièce</translation>
    </message>
    <message>
        <source>Piece annotation</source>
        <translation>Annotation de la pièce</translation>
    </message>
    <message>
        <source>Piece orientation</source>
        <translation>Orientation de la pièce</translation>
    </message>
    <message>
        <source>Piece rotation</source>
        <translation>Rotation de la pièce</translation>
    </message>
    <message>
        <source>Piece tilt</source>
        <translation>Inclinaison de la pièce</translation>
    </message>
    <message>
        <source>Piece fold position</source>
        <translation>Position du pli de la pièce</translation>
    </message>
    <message>
        <source>Piece name</source>
        <translation>Nom de la pièce du patron</translation>
    </message>
    <message>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <source>Material: Fabric</source>
        <translation>Matière: Tissu</translation>
    </message>
    <message>
        <source>Fabric</source>
        <translation>Tissu</translation>
    </message>
    <message>
        <source>Material: Lining</source>
        <translation>Matière: Doublure</translation>
    </message>
    <message>
        <source>Lining</source>
        <translation>Doublure</translation>
    </message>
    <message>
        <source>Material: Interfacing</source>
        <translation>Matière: Interfaçage</translation>
    </message>
    <message>
        <source>Interfacing</source>
        <translation>Interfaçage</translation>
    </message>
    <message>
        <source>Material: Interlining</source>
        <translation>Matière: Interligne</translation>
    </message>
    <message>
        <source>Interlining</source>
        <translation>Interligne</translation>
    </message>
    <message>
        <source>Word: Cut</source>
        <translation>Mot : Couper</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Coupure</translation>
    </message>
    <message>
        <source>Word: on fold</source>
        <translation>Mot : au pli</translation>
    </message>
    <message>
        <source>on fold</source>
        <translation>au pli</translation>
    </message>
    <message>
        <source>User material</source>
        <translation>Tissu de l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Insert…</source>
        <translation>Insérer…</translation>
    </message>
    <message>
        <source>Move on top</source>
        <translation>Déplacer au premier plan</translation>
    </message>
    <message>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <source>Move on bottom</source>
        <translation>Déplacer vers l&apos;arrière plan</translation>
    </message>
    <message>
        <source>Measurements units</source>
        <translation>Unités de mesure</translation>
    </message>
    <message>
        <source>Pattern units</source>
        <translation>Unités du patron</translation>
    </message>
    <message>
        <source>Size units</source>
        <translation>Unités de taille</translation>
    </message>
    <message>
        <source>Customer birth date</source>
        <translation>Date de naissance du client</translation>
    </message>
    <message>
        <source>Customer email</source>
        <translation>Adresse email du client</translation>
    </message>
    <message>
        <source>Height</source>
        <comment>dimension</comment>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>Size</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Hip</source>
        <comment>dimension</comment>
        <translation>Hanche</translation>
    </message>
    <message>
        <source>Waist</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Measurement: %1</source>
        <translation>Mensuration : %1</translation>
    </message>
    <message>
        <source>Height label</source>
        <comment>dimension</comment>
        <translation>Etiquette de hauteur</translation>
    </message>
    <message>
        <source>Size label</source>
        <comment>dimension</comment>
        <translation>Etiquette de taille</translation>
    </message>
    <message>
        <source>Hip label</source>
        <comment>dimension</comment>
        <translation>Etiquette de hanche</translation>
    </message>
    <message>
        <source>Waist label</source>
        <comment>dimension</comment>
        <translation>Etiquette de taille</translation>
    </message>
    <message>
        <source>Final measurement: %1</source>
        <translation>Dimension finale : %1</translation>
    </message>
    <message>
        <source>No data for the height dimension.</source>
        <translation>Pas de valeur pour la mesure de la hauteur.</translation>
    </message>
    <message>
        <source>No data for the size dimension.</source>
        <translation>Pas de valeur pour la mesure de la taille.</translation>
    </message>
    <message>
        <source>No data for the hip dimension.</source>
        <translation>Pas de valeur pour la mesure du tour de hanche.</translation>
    </message>
    <message>
        <source>No data for the waist dimension.</source>
        <translation>Pas de valeur pour la mesure du tour de taille.</translation>
    </message>
    <message>
        <source>Dimension X</source>
        <comment>dimension</comment>
        <translation>Mesure X en .vst (stature)</translation>
    </message>
    <message>
        <source>Dimension Y</source>
        <comment>dimension</comment>
        <translation>Mesure Y en .vst (taille commerciale)</translation>
    </message>
    <message>
        <source>Dimension Z</source>
        <comment>dimension</comment>
        <translation>Mesure Z en .vst (tour de taille)</translation>
    </message>
    <message>
        <source>Dimension W</source>
        <comment>dimension</comment>
        <translation>Mesure W en .vst (tour de hanches)</translation>
    </message>
    <message>
        <source>Dimension X label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension X</translation>
    </message>
    <message>
        <source>Dimension Y label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension Y</translation>
    </message>
    <message>
        <source>Dimension Z label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension Z</translation>
    </message>
    <message>
        <source>Dimension W label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension W</translation>
    </message>
    <message>
        <source>No data for the X dimension.</source>
        <translation>Pas de données pour la dimension X.</translation>
    </message>
    <message>
        <source>No data for the Y dimension.</source>
        <translation>Pas de données pour la dimension Y.</translation>
    </message>
    <message>
        <source>No data for the Z dimension.</source>
        <translation>Pas de données pour la dimension Z.</translation>
    </message>
    <message>
        <source>No data for the W dimension.</source>
        <translation>Pas de données pour la dimension W.</translation>
    </message>
</context>
<context>
    <name>DialogEditWrongFormula</name>
    <message>
        <source>Edit formula</source>
        <translation>Modifier la formule</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation type="vanished">Formules</translation>
    </message>
    <message>
        <source>Insert variable into formula</source>
        <translation>Insérer une variable dans la formule</translation>
    </message>
    <message>
        <source>Value of first angle</source>
        <translation type="vanished">Valeur du premier angle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Input data</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Size and height</source>
        <translation type="vanished">Taille et stature</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation>Mesures</translation>
    </message>
    <message>
        <source>Increments</source>
        <translation>Incréments</translation>
    </message>
    <message>
        <source>Length of lines</source>
        <translation>Longueur des lignes</translation>
    </message>
    <message>
        <source>Length of arcs</source>
        <translation type="vanished">Longueur des arcs</translation>
    </message>
    <message>
        <source>Length of curves</source>
        <translation>Longueur des courbes</translation>
    </message>
    <message>
        <source>Angle of lines</source>
        <translation>Angle des lignes</translation>
    </message>
    <message>
        <source>Hide empty measurements</source>
        <translation>Masquer les mesures vides</translation>
    </message>
    <message>
        <source>Double click for add to formula</source>
        <translation type="vanished">Double-cliquer pour ajouter à la formule</translation>
    </message>
    <message>
        <source>Height</source>
        <translation type="vanished">Hauteur</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Taille</translation>
    </message>
    <message>
        <source>Line length</source>
        <translation>Longueur de ligne</translation>
    </message>
    <message>
        <source>Arc length</source>
        <translation type="vanished">Longueur de l&apos;arc</translation>
    </message>
    <message>
        <source>Curve length</source>
        <translation>Longueur de courbe</translation>
    </message>
    <message>
        <source>Line Angle</source>
        <translation>Angle de Ligne</translation>
    </message>
    <message>
        <source>Radius of arcs</source>
        <translation>Rayons des arcs</translation>
    </message>
    <message>
        <source>Angles of arcs</source>
        <translation type="vanished">Angles des arcs</translation>
    </message>
    <message>
        <source>Angles of curves</source>
        <translation>Angles des courbes</translation>
    </message>
    <message>
        <source>Arc radius</source>
        <translation>Rayon de l&apos;arc</translation>
    </message>
    <message>
        <source>Arc angle</source>
        <translation type="vanished">Angle de l&apos;arc</translation>
    </message>
    <message>
        <source>Curve angle</source>
        <translation>Angle de la courbe</translation>
    </message>
    <message>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Full name</source>
        <translation>Nom complet</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation>Fonction</translation>
    </message>
    <message>
        <source>Lengths to control points</source>
        <translation>Longueur aux points de contrôle</translation>
    </message>
    <message>
        <source>Filter list by keyword</source>
        <translation>Filtrer la liste par mot-clefs</translation>
    </message>
    <message>
        <source>Preview calculations</source>
        <translation>Calculs préliminaires</translation>
    </message>
</context>
<context>
    <name>DialogEllipticalArc</name>
    <message>
        <source>Radius1:</source>
        <translation>Rayon1&#xa0;:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calulation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Radius2:</source>
        <translation>Rayon2&#xa0;:</translation>
    </message>
    <message>
        <source>First angle:</source>
        <translation>Premier angle :</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Second angle:</source>
        <translation>Deuxième angle :</translation>
    </message>
    <message>
        <source>Rotation angle:</source>
        <translation>Angle de rotation :</translation>
    </message>
    <message>
        <source>Center point:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Select center point of the arc</source>
        <translation>Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Edit radius1</source>
        <translation>Modifier rayon1</translation>
    </message>
    <message>
        <source>Edit radius2</source>
        <translation>Modifier rayon2</translation>
    </message>
    <message>
        <source>Edit first angle</source>
        <translation>Modifier le premier angle</translation>
    </message>
    <message>
        <source>Edit second angle</source>
        <translation>Modifier le deuxième angle</translation>
    </message>
    <message>
        <source>Edit rotation angle</source>
        <translation>Modifier l&apos;angle de rotation</translation>
    </message>
    <message>
        <source>Elliptical arc</source>
        <translation>Arc elliptique</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
</context>
<context>
    <name>DialogEndLine</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Value of angle</source>
        <translation type="vanished">Valeur de l&apos;angle</translation>
    </message>
    <message>
        <source>Base point</source>
        <translation type="vanished">Point de départ</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation type="vanished">Premier point de la ligne</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation type="vanished">Montrer la ligne du premier point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Point at distance and angle</source>
        <translation>Point à distance et angle</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur du trait</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Éditer l&apos;angle</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Éditer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Base point:</source>
        <translation>Point de départ:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogExportToCSV</name>
    <message>
        <source>Export options</source>
        <translation type="vanished">Options d&apos;exportation</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Exporter</translation>
    </message>
    <message>
        <source>With header</source>
        <translation>Avec en-tête</translation>
    </message>
    <message>
        <source>Codec:</source>
        <translation>Codec :</translation>
    </message>
    <message>
        <source>Separator</source>
        <translation>Séparateur</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation>Tabulation</translation>
    </message>
    <message>
        <source>Comma</source>
        <translation>Virgule</translation>
    </message>
    <message>
        <source>Semicolon</source>
        <translation>Point-virgule</translation>
    </message>
    <message>
        <source>Space</source>
        <translation>Espace</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Prévisualiser</translation>
    </message>
    <message>
        <source>Global</source>
        <comment>Options</comment>
        <translation>Global</translation>
    </message>
</context>
<context>
    <name>DialogFinalMeasurements</name>
    <message>
        <source>Final measurements</source>
        <translation>Mesures finales</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>The calculated value</source>
        <translation>La valeur calculée</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Pièces</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Calculated value:</source>
        <translation>Valeur calculée:</translation>
    </message>
    <message>
        <source>Formula:</source>
        <translation>Formule :</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Montrer le calcul complet dans une boite de dialogue &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant formule</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Description:</translation>
    </message>
    <message>
        <source>Move measurement up</source>
        <translation>Augmentez la dimension</translation>
    </message>
    <message>
        <source>Move measurement down</source>
        <translation>Diminuez la dimension</translation>
    </message>
    <message>
        <source>measurement</source>
        <translation>mesures</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Empty field.</source>
        <translation>Champ vide.</translation>
    </message>
    <message>
        <source>Edit measurement</source>
        <translation>Editer les mesures</translation>
    </message>
    <message>
        <source>Empty field</source>
        <translation>Champ vide</translation>
    </message>
    <message>
        <source>Invalid result. Value is infinite or NaN. Please, check your calculations.</source>
        <translation>Résultat  non valide. La valeur est infinie ou NaN. Veuillez revoir vos calculs.</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation>Erreur de l&apos;interpréteur : %1</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search history &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recherches précédentes &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+Down</source>
        <translation>Alt+Down</translation>
    </message>
    <message>
        <source>0 results</source>
        <translation>0 résultat trouvé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match Case &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient au moins &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match words &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient exactement &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match with regular expressions &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Avec expression régulière &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Previous &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Précédent &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Next %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat suivant %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
</context>
<context>
    <name>DialogFlippingByAxis</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialogue</translation>
    </message>
    <message>
        <source>Origin point:</source>
        <translation>Point d&apos;origine:</translation>
    </message>
    <message>
        <source>Suffix:</source>
        <translation>Suffixe :</translation>
    </message>
    <message>
        <source>Axis type:</source>
        <translation>Type d&apos;axe :</translation>
    </message>
    <message>
        <source>Select origin point</source>
        <translation>Selectionner le second point</translation>
    </message>
    <message>
        <source>Select origin point that is not part of the list of objects</source>
        <translation>Sélectionnez un point d&apos;origine qui n&apos;est pas dans la liste d&apos;objets</translation>
    </message>
    <message>
        <source>Vertical axis</source>
        <translation>Axes verticaux</translation>
    </message>
    <message>
        <source>Horizontal axis</source>
        <translation>Axes horizontaux</translation>
    </message>
    <message>
        <source>Flipping by axis</source>
        <translation>Reproduction des objets en miroir par rapport à un axe</translation>
    </message>
    <message>
        <source>Visibility Group</source>
        <translation>Groupe de visibilité</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Add tags</source>
        <translation>Ajouter des balises</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Invalid suffix</source>
        <translation>Suffixe invalide</translation>
    </message>
    <message>
        <source>Invalid group name</source>
        <translation>Nom de groupe non valide</translation>
    </message>
    <message>
        <source>Label:</source>
        <translation>Etiquette :</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Invalid point</source>
        <translation>Point invalide</translation>
    </message>
    <message>
        <source>Invalid label</source>
        <translation>Etiquette invalide</translation>
    </message>
    <message>
        <source>Invalid alias</source>
        <translation>Alias invalide</translation>
    </message>
    <message>
        <source>Enable to create a visibility group from original objects</source>
        <translation>Créer un groupe de visibilité comprenant les objets d&apos;origine</translation>
    </message>
</context>
<context>
    <name>DialogFlippingByLine</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialogue</translation>
    </message>
    <message>
        <source>First line point:</source>
        <translation>Point de la première ligne:</translation>
    </message>
    <message>
        <source>Suffix:</source>
        <translation>Suffixe :</translation>
    </message>
    <message>
        <source>Second line point:</source>
        <translation>Point de la deuxième ligne :</translation>
    </message>
    <message>
        <source>Select first line point</source>
        <translation>Selectionner le premier point de la ligne</translation>
    </message>
    <message>
        <source>Select first line point that is not part of the list of objects</source>
        <translation>Sélectionnez le premier point de la ligne qui n&apos;est pas dans la liste d&apos;objets</translation>
    </message>
    <message>
        <source>Select second line point</source>
        <translation>Choisir le deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Select second line point that is not part of the list of objects</source>
        <translation>Sélectionnez le deuxième point de la ligne qui n&apos;est pas dans la liste d&apos;objets</translation>
    </message>
    <message>
        <source>Flipping by line</source>
        <translation>Reproduction des objets en miroir par rapport à une ligne</translation>
    </message>
    <message>
        <source>Visibility Group</source>
        <translation>Groupe de visibilité</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Add tags</source>
        <translation>Ajouter des balises</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Invalid suffix</source>
        <translation>Suffixe invalide</translation>
    </message>
    <message>
        <source>Invalid group name</source>
        <translation>Nom de groupe non valide</translation>
    </message>
    <message>
        <source>Label:</source>
        <translation>Etiquette :</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Invalid line points</source>
        <translation>Points de la ligne non valides</translation>
    </message>
    <message>
        <source>Invalid first line point</source>
        <translation>Premier point de la ligne non valide</translation>
    </message>
    <message>
        <source>Invalid second line point</source>
        <translation>Second point de la ligne non valide</translation>
    </message>
    <message>
        <source>Invalid label</source>
        <translation>Etiquette invalide</translation>
    </message>
    <message>
        <source>Invalid alias</source>
        <translation>Alias invalide</translation>
    </message>
    <message>
        <source>Enable to create a visibility group from original objects</source>
        <translation>Créer un groupe de visibilité comprenant les objets d&apos;origine</translation>
    </message>
</context>
<context>
    <name>DialogGroup</name>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Group name:</source>
        <translation>Nom de Groupe :</translation>
    </message>
    <message>
        <source>Unique pattern piece name</source>
        <translation type="vanished">Nom unique d&apos;élément de patron</translation>
    </message>
    <message>
        <source>Choose group name</source>
        <translation>Choisir le nom de groupe</translation>
    </message>
    <message>
        <source>New group</source>
        <translation>Nouveau groupe</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Add tags</source>
        <translation>Ajouter des balises</translation>
    </message>
</context>
<context>
    <name>DialogHeight</name>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Base point</source>
        <translation type="vanished">Point de départ</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation type="vanished">Premier point de la ligne</translation>
    </message>
    <message>
        <source>Second point of line</source>
        <translation type="vanished">Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to our point</source>
        <translation type="vanished">Montrer la ligne du premier point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Select first point of line</source>
        <translation>Choisir le premier point de la ligne</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Perpendicular point along line</source>
        <translation>Point perpendiculaire sur axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Base Point</source>
        <translation type="vanished">Point de base</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur du trait</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Base point:</source>
        <translation>Point de départ:</translation>
    </message>
    <message>
        <source>First point of line:</source>
        <translation>Premier point de la ligne :</translation>
    </message>
    <message>
        <source>Second point of line:</source>
        <translation>Deuxième point de la ligne:</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogHistory</name>
    <message>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Can&apos;t create record.</source>
        <translation type="vanished">Ne peut pat créer d&apos;enregistrement.</translation>
    </message>
    <message>
        <source>%1 - Base point</source>
        <translation>%1 - Point de départ</translation>
    </message>
    <message>
        <source>%1_%2 - Line from point %1 to point %2</source>
        <translation>%1_%2 - Ligne du point %1 au point %2</translation>
    </message>
    <message>
        <source>%3 - Point along line %1_%2</source>
        <translation>%3 - Point le long d&apos;une ligne %1_%2</translation>
    </message>
    <message>
        <source>%1 - Point of shoulder</source>
        <translation>%1 - Point d&apos;épaule</translation>
    </message>
    <message>
        <source>%3 - normal to line %1_%2</source>
        <translation>%3 - normal à la ligne %1_%2</translation>
    </message>
    <message>
        <source>%4 - bisector of angle %1_%2_%3</source>
        <translation>%4 - bissectrice de l&apos;angle %1_%2_%3</translation>
    </message>
    <message>
        <source>%5 - intersection of lines %1_%2 and %3_%4</source>
        <translation>%5 - intersection des lignes %1_%2 et %3_%4</translation>
    </message>
    <message>
        <source>Curve %1_%2</source>
        <translation type="vanished">Courbe %1_%2</translation>
    </message>
    <message>
        <source>Arc with center in point %1</source>
        <translation type="vanished">Arc de centre %1</translation>
    </message>
    <message>
        <source>Curve point %1</source>
        <translation type="vanished">Point de courbe %1</translation>
    </message>
    <message>
        <source>%4 - point of contact of arc with the center in point %1 and line %2_%3</source>
        <translation>%4 - point de contact entre l&apos;arc de centre %1 et la ligne %2_%3</translation>
    </message>
    <message>
        <source>Point of perpendicular from point %1 to line %2_%3</source>
        <translation>Point de la perpendiculaire du point %1 à la ligne %2_%3</translation>
    </message>
    <message>
        <source>Triangle: axis %1_%2, points %3 and %4</source>
        <translation>Triangle : axe %1_%2, points %3 et %4</translation>
    </message>
    <message>
        <source>%1 - point of intersection %2 and %3</source>
        <translation>%1 - point d&apos;intersection %2 et %3</translation>
    </message>
    <message>
        <source>%1 - cut arc with center %2</source>
        <translation type="vanished">%1 - couper l&apos;arc avec le centre %2</translation>
    </message>
    <message>
        <source>%1 - cut curve %2_%3</source>
        <translation type="vanished">%1 - couper la courbe %2_%3</translation>
    </message>
    <message>
        <source>%1 - cut curve path %2</source>
        <translation type="vanished">%1 - coupe trajectoire de courbe %2</translation>
    </message>
    <message>
        <source>%1 - point of intersection line %2_%3 and axis through point %4</source>
        <translation>%1 - point d&apos;intersection entre la ligne %2_%3 et un axe passant par le point %4</translation>
    </message>
    <message>
        <source>%1 - point of intersection curve and axis through point %2</source>
        <translation>%1 - point d&apos;intersection entre courbe et axe passant par %2</translation>
    </message>
    <message>
        <source>Arc with center in point %1 and length %2</source>
        <translation type="vanished">Arc de centre %1 et de longueur %2</translation>
    </message>
    <message>
        <source>%1 - point of arcs intersection</source>
        <translation>%1 - Point à l&apos;intersection de 2 arcs</translation>
    </message>
    <message>
        <source>%1 - point of circles intersection</source>
        <translation>%1 - Point à l&apos;intersection de 2 cercles</translation>
    </message>
    <message>
        <source>%1 - point from circle and tangent</source>
        <translation>%1 - point à l&apos;intersection d&apos;un cercle et d&apos;une tangente</translation>
    </message>
    <message>
        <source>%1 - point from arc and tangent</source>
        <translation>%1 - point à l&apos;intersection d&apos;un arc et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Correction the dart %1_%2_%3</source>
        <translation>Corriger la pince %1_%2_%3</translation>
    </message>
    <message>
        <source>%1 - point of curves intersection</source>
        <translation>%1 - Point d&apos;intersection de courbes</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <source>Cubic bezier curve</source>
        <translation>Courbe de Bezier Cubique</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>%1 with length %2</source>
        <translation>%1 de longueur %2</translation>
    </message>
    <message>
        <source>Spline path</source>
        <translation>Trajectoire de spline</translation>
    </message>
    <message>
        <source>Cubic bezier curve path</source>
        <translation>Trajectoire de la courbe de bezier cubique</translation>
    </message>
    <message>
        <source>%1 - cut %2</source>
        <translation>%1 - coupe %2</translation>
    </message>
    <message>
        <source>arc</source>
        <translation>arc</translation>
    </message>
    <message>
        <source>curve</source>
        <translation>courbe</translation>
    </message>
    <message>
        <source>curve path</source>
        <translation>Trajectoire de courbe</translation>
    </message>
    <message>
        <source>Elliptical arc</source>
        <translation>Arc élliptique</translation>
    </message>
    <message>
        <source>Rotate objects around point %1. Suffix &apos;%2&apos;</source>
        <translation>Rotation des objets avec point %1 comme centre de rotation. Suffixe &apos;%2&apos;</translation>
    </message>
    <message>
        <source>Flipping by line %1_%2. Suffix &apos;%3&apos;</source>
        <translation>Miroir des objects par rapport à la ligne %1_%2. Suffixe &apos;%3&apos;</translation>
    </message>
    <message>
        <source>Flipping by axis through %1 point. Suffix &apos;%2&apos;</source>
        <translation>Miroir des objets par rapport à l&apos;axe %1 point. Suffixe &apos;%2&apos;</translation>
    </message>
    <message>
        <source>Move objects. Suffix &apos;%1&apos;</source>
        <translation>Déplacer des objets. Suffixe &apos;%1&apos;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search history &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recherches précédentes &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+Down</source>
        <translation>Alt+Down</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>0 results</source>
        <translation>0 résultat trouvé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match Case &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient au moins &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match words &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient exactement &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match with regular expressions &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Avec expression régulière &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Previous &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Précédent &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Next %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat suivant %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
</context>
<context>
    <name>DialogIncrements</name>
    <message>
        <source>Increments</source>
        <translation>Évolutions</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>The calculated value</source>
        <translation>Valeur calculée</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>Tables of Variables</source>
        <translation>Table des variables</translation>
    </message>
    <message>
        <source>Lines angles</source>
        <translation>Angles des lignes</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Lengths curves</source>
        <translation>Longueurs des courbes</translation>
    </message>
    <message>
        <source>Angles curves</source>
        <translation>Angles des courbes</translation>
    </message>
    <message>
        <source>Lengths arcs</source>
        <translation type="vanished">Longueurs des arcs</translation>
    </message>
    <message>
        <source>Radiuses arcs</source>
        <translation>Rayons des arcs</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>Angles arcs</source>
        <translation type="vanished">Angles des arcs</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détail</translation>
    </message>
    <message>
        <source>Move measurement up</source>
        <translation>Déplacer la variable d&apos;une ligne vers le haut</translation>
    </message>
    <message>
        <source>Move measurement down</source>
        <translation>Déplacer la variable d&apos;une ligne vers le bas</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Calculated value:</source>
        <translation>Valeur calculée :</translation>
    </message>
    <message>
        <source>Formula:</source>
        <translation>Formule :</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afficher le calcul complet &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Empty field.</source>
        <translation>Champ vide.</translation>
    </message>
    <message>
        <source>Empty field</source>
        <translation>Champ vide</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation>Erreur d&apos;analyse : %1</translation>
    </message>
    <message>
        <source>Increment_%1</source>
        <translation type="vanished">Increment_%1</translation>
    </message>
    <message>
        <source>Edit increment</source>
        <translation>Editer l&apos;incrément</translation>
    </message>
    <message>
        <source>Unique increment name</source>
        <translation>Nom unique d&apos;incrément</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant de création de formule</translation>
    </message>
    <message>
        <source>Invalid value</source>
        <translation type="vanished">Valeur non valide</translation>
    </message>
    <message>
        <source>Find:</source>
        <translation type="vanished">Trouver :</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Curves control point lengths</source>
        <translation>Longueurs des points de contrôle des courbes</translation>
    </message>
    <message>
        <source>Invalid result. Value is infinite or NaN. Please, check your calculations.</source>
        <translation>Résultat invalide. La valeur est infinie ou n&apos;est pas une valeur numérique. Veuillez vérifier vos calculs.</translation>
    </message>
    <message>
        <source>Refresh a pattern with all changes you made</source>
        <translation>Mise à jour du patron après modifications</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Actualiser</translation>
    </message>
    <message>
        <source>Preview calculations</source>
        <translation>Calculs préliminaires</translation>
    </message>
    <message>
        <source>Unique variable name</source>
        <translation>Nom unique de variable</translation>
    </message>
    <message>
        <source>Increment</source>
        <translation>Incrément</translation>
    </message>
    <message>
        <source>Separator</source>
        <translation>Séparateur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search history &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recherches précédentes &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+Down</source>
        <translation>Alt+Down</translation>
    </message>
    <message>
        <source>0 results</source>
        <translation>0 résultat trouvé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match Case &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient au moins &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match words &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient exactement &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match with regular expressions &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Avec expression régulière &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Previous &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat précédent &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Next %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat suivant %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <source>Units:</source>
        <translation>Unités :</translation>
    </message>
    <message>
        <source>Millimeters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Degrees</source>
        <translation>Degrés</translation>
    </message>
</context>
<context>
    <name>DialogInsertNode</name>
    <message>
        <source>Piece:</source>
        <translation>Pièce:</translation>
    </message>
    <message>
        <source>The list of pieces is empty. Please, first create at least one piece for current pattern piece.</source>
        <translation>La liste des pièces est vide. Créer d&apos;abord au moins une pièce pour le patron en cours.</translation>
    </message>
    <message>
        <source>Insert nodes</source>
        <translation>Insérer un noeud</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Number:</source>
        <translation>Nombre :</translation>
    </message>
</context>
<context>
    <name>DialogKnownMaterials</name>
    <message>
        <source>Known materials</source>
        <translation>Matériaux employés</translation>
    </message>
    <message>
        <source>Material:</source>
        <translation>Matière:</translation>
    </message>
    <message>
        <source>Name of material</source>
        <translation>Nom de matière</translation>
    </message>
    <message>
        <source>User material</source>
        <translation>Tissu de l&apos;utilisateur</translation>
    </message>
</context>
<context>
    <name>DialogLayoutProgress</name>
    <message>
        <source>Couldn&apos;t prepare data for creation layout</source>
        <translation type="vanished">Impossible de préparer les données pour la création du plan de coupe</translation>
    </message>
    <message>
        <source>Several workpieces left not arranged, but none of them match for paper</source>
        <translation type="vanished">Plusieurs pièces du patron n&apos;ont pas été arrangées, mais aucune ne va sur le papier</translation>
    </message>
    <message>
        <source>Create a Layout</source>
        <translation>Créer un plan de coupe</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Finding best position for worpieces. Please, wait.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recherche des positions optimales pour les pièces du patron. Un instant.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Arranged workpieces: %1 from %2</source>
        <translation type="vanished">Tri des pièces: %1 à %2</translation>
    </message>
    <message>
        <source>Nesting. Please, wait.</source>
        <translation>Mise en page, merci de patienter.</translation>
    </message>
    <message>
        <source>Time left:</source>
        <translation>Temps restant :</translation>
    </message>
    <message>
        <source>Time left: %1</source>
        <translation>Temps restant : %1</translation>
    </message>
    <message>
        <source>Efficiency coefficient: %1%</source>
        <translation>Coefficient d’efficacité: %1%</translation>
    </message>
</context>
<context>
    <name>DialogLayoutScale</name>
    <message>
        <source>Layout scale</source>
        <translation>Echelle de sortie</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Left:</source>
        <translation>Gauche :</translation>
    </message>
    <message>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <source>Right:</source>
        <translation>Droite :</translation>
    </message>
    <message>
        <source>Top:</source>
        <translation>Haut :</translation>
    </message>
    <message>
        <source>Bottom:</source>
        <translation>Bas :</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <source>Horizontal:</source>
        <translation>Horizontal :</translation>
    </message>
    <message>
        <source>Vertical:</source>
        <translation>Vertical :</translation>
    </message>
</context>
<context>
    <name>DialogLayoutSettings</name>
    <message>
        <source>Paper size</source>
        <translation type="vanished">Format du papier</translation>
    </message>
    <message>
        <source>Templates:</source>
        <translation>Modèles :</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <source>Rotate workpiece</source>
        <translation type="vanished">Tourner pièce en cours</translation>
    </message>
    <message>
        <source>Rotate by</source>
        <translation type="vanished">Tourner</translation>
    </message>
    <message>
        <source>degree</source>
        <translation type="vanished">degré</translation>
    </message>
    <message>
        <source>Creation options</source>
        <translation type="vanished">Options de création</translation>
    </message>
    <message>
        <source>Shift length:</source>
        <translation type="vanished">Changer longueur :</translation>
    </message>
    <message>
        <source>Principle of choosing the next workpiece</source>
        <translation type="vanished">Règle pour choisir l&apos;ordre des pièces</translation>
    </message>
    <message>
        <source>Three groups: big, middle, small</source>
        <translation>Trois groupes : grand, moyen, petit</translation>
    </message>
    <message>
        <source>Two groups: big, small</source>
        <translation>Deux groupes : grand, petit</translation>
    </message>
    <message>
        <source>Descending area</source>
        <translation>Surface décroissante</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Pixels</source>
        <translation>Pixels</translation>
    </message>
    <message>
        <source>Create a layout</source>
        <translation>Créer un plan de coupe</translation>
    </message>
    <message>
        <source>Auto crop unused length</source>
        <translation>Rogner automatiquement la longueur non utilisée</translation>
    </message>
    <message>
        <source>Unite pages (if possible)</source>
        <translation>Page unique (si possible)</translation>
    </message>
    <message>
        <source>Gap width:</source>
        <translation>Largeur d&apos;espacement :</translation>
    </message>
    <message>
        <source>Save length of the sheet</source>
        <translation>Sauvegarder la longueur de la feuille</translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="vanished">Lettre</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation type="vanished">Légal</translation>
    </message>
    <message>
        <source>Roll 24in</source>
        <translation type="vanished">Traceur 24 pouces</translation>
    </message>
    <message>
        <source>Roll 30in</source>
        <translation type="vanished">Traceur 30 pouces</translation>
    </message>
    <message>
        <source>Roll 36in</source>
        <translation type="vanished">Traceur 36 pouces</translation>
    </message>
    <message>
        <source>Roll 42in</source>
        <translation type="vanished">Traceur 42 pouces</translation>
    </message>
    <message>
        <source>Roll 44in</source>
        <translation type="vanished">Traceur 44 pouces</translation>
    </message>
    <message>
        <source>
	Three groups: big, middle, small = 0
	Two groups: big, small = 1
	Descending area = 2
</source>
        <translation type="vanished">
	Trois groupes : grand, moyen, petit = 0
	Deux groupes : grand, petit = 1
	Surface décroissante = 2
</translation>
    </message>
    <message>
        <source>Paper format</source>
        <translation>Format du plan</translation>
    </message>
    <message>
        <source>Fields</source>
        <translation type="vanished">Marges</translation>
    </message>
    <message>
        <source>Left:</source>
        <translation>Gauche :</translation>
    </message>
    <message>
        <source>Right:</source>
        <translation>Droite :</translation>
    </message>
    <message>
        <source>Top:</source>
        <translation>Haut :</translation>
    </message>
    <message>
        <source>Bottom:</source>
        <translation>Bas :</translation>
    </message>
    <message>
        <source>Ignore fileds</source>
        <translation type="vanished">Ignorer les marges</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Personnalisé</translation>
    </message>
    <message>
        <source>Wrong fields.</source>
        <translation>Marges incorrectes.</translation>
    </message>
    <message>
        <source>Fields go beyond printing. 

Apply settings anyway?</source>
        <translation type="vanished">Les marges s’étendent au-delà de la zone d’impression.. 

Appliquer quand même les réglages ?</translation>
    </message>
    <message>
        <source>
	Three groups: big, middle, small = 0;
	Two groups: big, small = 1;
	Descending area = 2</source>
        <translation>
	Trois groupes : grand, moyen, petit = 0
	Deux groupes : grand, petit = 1
	Surface décroissante = 2</translation>
    </message>
    <message>
        <source>Layout options</source>
        <translation>Options du plan de coupe</translation>
    </message>
    <message>
        <source>Shift/Offset length:</source>
        <translation type="vanished">Longueur de décalage:</translation>
    </message>
    <message>
        <source>Rule for choosing the next workpiece</source>
        <translation>Règle pour choisir l&apos;ordre des pièces</translation>
    </message>
    <message>
        <source>Enabling for sheets that have big height will speed up creating. </source>
        <translation type="vanished">Le choix d&apos;un grand format accélère la création du plan de coupe.</translation>
    </message>
    <message>
        <source>Divide into strips</source>
        <translation>Diviser en bandes</translation>
    </message>
    <message>
        <source>Multiplier</source>
        <translation>Multiplicateur</translation>
    </message>
    <message>
        <source>Set multiplier for length of the biggest workpiece in layout.</source>
        <translation>Régle le multiplicateur en référence à la longueur de la plus grande pièce dans le plan de coupe.</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="vanished">x</translation>
    </message>
    <message>
        <source>Enabling for sheets that have big height will speed up creating.</source>
        <translation>Le choix d&apos;un grand format accélère la création du plan de coupe.</translation>
    </message>
    <message>
        <source>Printer:</source>
        <translation>Imprimante :</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Printer</comment>
        <translation>Rien</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Text will be converted to paths</source>
        <translation>Les textes seront exportés en chemins</translation>
    </message>
    <message>
        <source>Export text as paths</source>
        <translation>Exporter les textes en chemins</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Ignore margins</source>
        <translation>Ignorer les marges</translation>
    </message>
    <message>
        <source>Margins go beyond printing. 

Apply settings anyway?</source>
        <translation>Les marges s’étendent au-delà de la zone d’impression.. 

Appliquer quand même les réglages ?</translation>
    </message>
    <message>
        <source>Follow grainline</source>
        <translation>Respecter le droit-fil</translation>
    </message>
    <message>
        <source>Time:</source>
        <translation>Temps de recherche :</translation>
    </message>
    <message>
        <source>Time given for the algorithm to find best layout.</source>
        <translation>Temps estimé pour que l’algorithme trouve la meilleure mise en page.</translation>
    </message>
    <message>
        <source>Efficiency:</source>
        <translation>Efficacité :</translation>
    </message>
    <message>
        <source>Manual priority</source>
        <translation>Priorité manuelle</translation>
    </message>
    <message>
        <source>Nest quantity of copies according to piece settings.</source>
        <translation>Regrouper le nombre de copies selon les paramètres de la pièce.</translation>
    </message>
    <message>
        <source>Nest quantity</source>
        <translation>Quantité à regrouper</translation>
    </message>
    <message>
        <source>Auto crop unused width</source>
        <translation>Rogner automatiquement la largeur non utilisée</translation>
    </message>
    <message>
        <source>Prefer one sheet solution</source>
        <translation>Préférer page unique (si possible)</translation>
    </message>
    <message>
        <source> min</source>
        <comment>minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <source>Enable this option to prefer getting one sheet solutions.</source>
        <translation>Cocher cette option si l&apos;on préfère un plan sur page unique (si possible).</translation>
    </message>
    <message>
        <source>Set layout efficiency coefficient. Layout efficiency coefficientt is the ratio of the area occupied by the pieces to the bounding rect of all pieces. If nesting reaches required level the process stops. If value is 0 no check will be made.</source>
        <translation>Configurez le coefficient d&apos;efficacité de création du plan de coupe. Ce coefficient constitue le ratio de l&apos;espace occupé par les pièces par rapport au rectangle englobant toutes les pièces. Quand le ratio configuré est atteint, la création est automatiquement validée même si la durée minimale de création configurée n&apos;est pas écoulée. Si le ratio est configuré à 0, le programme ne cherchera pas à disposer les pièces en fonction de ce critère.</translation>
    </message>
</context>
<context>
    <name>DialogLine</name>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation type="vanished">Visualiser la ligne du premier points à ce point</translation>
    </message>
    <message>
        <source>Select second point</source>
        <translation>Choisir le deuxième point</translation>
    </message>
    <message>
        <source>Line between points</source>
        <translation>Ligne entre 2 points</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de ligne</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point:</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogLineIntersect</name>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First line</source>
        <translation>Première ligne</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Second line</source>
        <translation>Deuxième ligne</translation>
    </message>
    <message>
        <source>Select second point of first line</source>
        <translation>Choisir le deuxième point de la première ligne</translation>
    </message>
    <message>
        <source>Select first point of second line</source>
        <translation>Choisir le premier point de la deuxième ligne</translation>
    </message>
    <message>
        <source>Select second point of second line</source>
        <translation>Choisir le deuxième point de la deuxième ligne</translation>
    </message>
    <message>
        <source>Point at line intersection</source>
        <translation>Point à l&apos;intersection de 2 lignes</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogLineIntersectAxis</name>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Value of angle</source>
        <translation type="vanished">Valeur de l&apos;angle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Axis point</source>
        <translation type="vanished">point d&apos;axe</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation>Premier point de la ligne</translation>
    </message>
    <message>
        <source>First line point</source>
        <translation type="vanished">Point de la première ligne</translation>
    </message>
    <message>
        <source>Second line point</source>
        <translation type="vanished">Point de deuxieme ligne</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation>Montre la ligne du premier point jusqu’à ce point</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le deuxième point d&apos;une ligne</translation>
    </message>
    <message>
        <source>Select axis point</source>
        <translation>Choisir origine de l&apos;axe</translation>
    </message>
    <message>
        <source>Point intersect line and axis</source>
        <translation>Point à l&apos;intersection d&apos;une ligne et d&apos;un axe</translation>
    </message>
    <message>
        <source>Axis Point</source>
        <translation>Origine de la droite</translation>
    </message>
    <message>
        <source>Second point of line</source>
        <translation>Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de la ligne</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Éditer l&apos;angle</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Axis point:</source>
        <translation>Point d&apos;axe :</translation>
    </message>
    <message>
        <source>First line point:</source>
        <translation>Point de la première ligne:</translation>
    </message>
    <message>
        <source>Second line point:</source>
        <translation>Point de la deuxième ligne:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogMDataBase</name>
    <message>
        <source>Measurement data base</source>
        <translation>Base de données de mesures</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation>Mesures</translation>
    </message>
    <message>
        <source>Direct Height</source>
        <comment>Measurement section</comment>
        <translation>Hauteur directe</translation>
    </message>
    <message>
        <source>Direct Width</source>
        <comment>Measurement section</comment>
        <translation>Largeur directe</translation>
    </message>
    <message>
        <source>Indentation</source>
        <comment>Measurement section</comment>
        <translation>Cambrure</translation>
    </message>
    <message>
        <source>Circumference and Arc</source>
        <comment>Measurement section</comment>
        <translation>Circonférence et arc</translation>
    </message>
    <message>
        <source>Vertical</source>
        <comment>Measurement section</comment>
        <translation>Vertical</translation>
    </message>
    <message>
        <source>Horizontal</source>
        <comment>Measurement section</comment>
        <translation>Horizontal</translation>
    </message>
    <message>
        <source>Bust</source>
        <comment>Measurement section</comment>
        <translation>Poitrine</translation>
    </message>
    <message>
        <source>Balance</source>
        <comment>Measurement section</comment>
        <translation>Équilibre</translation>
    </message>
    <message>
        <source>Arm</source>
        <comment>Measurement section</comment>
        <translation>Bras</translation>
    </message>
    <message>
        <source>Leg</source>
        <comment>Measurement section</comment>
        <translation>Jambe</translation>
    </message>
    <message>
        <source>Crotch and Rise</source>
        <comment>Measurement section</comment>
        <translation>Entrejambe et montant</translation>
    </message>
    <message>
        <source>Hand</source>
        <comment>Measurement section</comment>
        <translation>Main</translation>
    </message>
    <message>
        <source>Foot</source>
        <comment>Measurement section</comment>
        <translation>Pied</translation>
    </message>
    <message>
        <source>Head</source>
        <comment>Measurement section</comment>
        <translation>Tête</translation>
    </message>
    <message>
        <source>Men &amp; Tailoring</source>
        <comment>Measurement section</comment>
        <translation>Hommes &amp; Tailleurs</translation>
    </message>
    <message>
        <source>Historical &amp; Specialty</source>
        <comment>Measurement section</comment>
        <translation>Historiques &amp; Spécialisés</translation>
    </message>
    <message>
        <source>Patternmaking measurements</source>
        <comment>Measurement section</comment>
        <translation>Mesures de patronnage</translation>
    </message>
    <message>
        <source>Collapse All</source>
        <translation>Tout replier</translation>
    </message>
    <message>
        <source>Expand All</source>
        <translation>Tout déplier</translation>
    </message>
    <message>
        <source>Check all</source>
        <translation>Tout cocher</translation>
    </message>
    <message>
        <source>Uncheck all</source>
        <translation>Tout décocher</translation>
    </message>
    <message>
        <source>Search:</source>
        <translation>Rechercher:</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Select measurement</source>
        <translation>Selectionner la mensuration</translation>
    </message>
</context>
<context>
    <name>DialogMeasurementsCSVColumns</name>
    <message>
        <source>Setup columns</source>
        <translation>Paramétrage des colonnes</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Prévisualiser</translation>
    </message>
    <message>
        <source>Input</source>
        <translation>Format à l&apos;entrée</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <source>Full name:</source>
        <translation>Nom complet:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Description:</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Name</source>
        <comment>measurement column</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Value</source>
        <comment>measurement column</comment>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Full name</source>
        <comment>measurement column</comment>
        <translation>Nom complet</translation>
    </message>
    <message>
        <source>Description</source>
        <comment>measurement column</comment>
        <translation>Description du patron</translation>
    </message>
    <message>
        <source>Base value</source>
        <comment>measurement column</comment>
        <translation>Valeur de départ</translation>
    </message>
    <message>
        <source>Shift (%1):</source>
        <comment>measurement column</comment>
        <translation>Majuscule (%1):</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation>Passer</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Base value</source>
        <translation>Valeur de départ</translation>
    </message>
    <message>
        <source>Shift (%1)*:</source>
        <translation>Touche Majuscule (%1)*:</translation>
    </message>
    <message>
        <source>File path is empty</source>
        <translation>Le chemin d&apos;accès du ficihier est vide</translation>
    </message>
    <message>
        <source>Not enough columns</source>
        <translation>Nombre de colonnes insuffisant</translation>
    </message>
    <message>
        <source>Not enough data to import</source>
        <translation>Nombre de données à importer insuffisant</translation>
    </message>
    <message>
        <source>Please, select unique number for each column</source>
        <translation>Sélectionner pour chaque colonne un nombre unique</translation>
    </message>
</context>
<context>
    <name>DialogMove</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialogue</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Suffix:</source>
        <translation>Suffixe :</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Editer l&apos;angle</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Editer la longueur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <source>Rotation angle:</source>
        <translation>Angle de rotation :</translation>
    </message>
    <message>
        <source>Edit rotation angle</source>
        <translation>Modifier l&apos;angle de rotation</translation>
    </message>
    <message>
        <source>Rotation origin point:</source>
        <translation>Point d&apos;origine de la rotation:</translation>
    </message>
    <message>
        <source>Center point</source>
        <translation>Point central</translation>
    </message>
    <message>
        <source>Visibility Group</source>
        <translation>Groupe de visibilité</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Add tags</source>
        <translation>Ajouter des balises</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Invalid suffix</source>
        <translation>suffixe invalide</translation>
    </message>
    <message>
        <source>Invalid group name</source>
        <translation>Nom de groupe non valide</translation>
    </message>
    <message>
        <source>Label:</source>
        <translation>Etiquette :</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Invalid angle formula</source>
        <translation>La formule de l&apos;angle est invalide</translation>
    </message>
    <message>
        <source>Invalid rotation angle formula</source>
        <translation>Formule d’angle de rotation invalide</translation>
    </message>
    <message>
        <source>Invalid length formula</source>
        <translation>La formule de la longueur est invalide</translation>
    </message>
    <message>
        <source>Invalid label</source>
        <translation>Etiquette invalide</translation>
    </message>
    <message>
        <source>Invalid alias</source>
        <translation>Alias invalide</translation>
    </message>
    <message>
        <source>Enable to create a visibility group from original objects</source>
        <translation>Créer un groupe de visibilité comprenant les objets d&apos;origine</translation>
    </message>
</context>
<context>
    <name>DialogNewMeasurements</name>
    <message>
        <source>New measurement file</source>
        <translation>Nouveau fichier de mesures</translation>
    </message>
    <message>
        <source>Measurement type:</source>
        <translation>Type de mesures:</translation>
    </message>
    <message>
        <source>Unit:</source>
        <translation>Unité:</translation>
    </message>
    <message>
        <source>Base size:</source>
        <translation type="vanished">Taille de base:</translation>
    </message>
    <message>
        <source>Base height:</source>
        <translation type="vanished">Hauteur de base:</translation>
    </message>
    <message>
        <source>Individual</source>
        <translation>Individuel</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Multisize</source>
        <translation>Multi-tailles</translation>
    </message>
</context>
<context>
    <name>DialogNewPattern</name>
    <message>
        <source>Individual measurements</source>
        <translation type="vanished">Mesures individuelles</translation>
    </message>
    <message>
        <source>Pattern piece name</source>
        <translation type="vanished">Nom de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>Units:</source>
        <translation>Unités:</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Pattern piece name:</source>
        <translation>Nom de l&apos;élément de patron :</translation>
    </message>
    <message>
        <source>Unique pattern piece name</source>
        <translation>Nom unique de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>Choose unique pattern piece name.</source>
        <translation>Choisir un nom unique pour l&apos;élément de patron.</translation>
    </message>
    <message>
        <source>New pattern</source>
        <translation>Nouveau patron</translation>
    </message>
</context>
<context>
    <name>DialogNormal</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Additional angle degrees</source>
        <translation type="vanished">Angle supplémentaire</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to this point</source>
        <translation type="vanished">Montrer la ligne du premier point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le second point de la ligne</translation>
    </message>
    <message>
        <source>Point along perpendicular</source>
        <translation>Point sur perpendiculaire</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation type="vanished">Premier point de la ligne</translation>
    </message>
    <message>
        <source>Second point of line</source>
        <translation type="vanished">Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de la ligne</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Editer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point:</translation>
    </message>
    <message>
        <source>Additional angle degrees:</source>
        <translation>Angle additionnel en degré:</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPatternMaterials</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>Placeholder</source>
        <translation>Espace réservé</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>DialogPatternProperties</name>
    <message>
        <source>Pattern properties</source>
        <translation>Propriétés du patron</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Description du patron</translation>
    </message>
    <message>
        <source>Author name</source>
        <translation type="vanished">Nom de l&apos;auteur</translation>
    </message>
    <message>
        <source>Pattern description</source>
        <translation>Description du patron</translation>
    </message>
    <message>
        <source>For technical notes.</source>
        <translation type="vanished">Pour les notes techniques.</translation>
    </message>
    <message>
        <source>Heights and Sizes</source>
        <translation type="vanished">Longueurs et tailles</translation>
    </message>
    <message>
        <source>All heights (cm)</source>
        <translation type="vanished">Toutes les longueurs (cm)</translation>
    </message>
    <message>
        <source>All sizes (cm)</source>
        <translation type="vanished">Toutes les tailles (cm)</translation>
    </message>
    <message>
        <source>Default height and size</source>
        <translation type="vanished">Taille et stature par défaut</translation>
    </message>
    <message>
        <source>From standard measurements</source>
        <translation type="vanished">Issu de mesures standards</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Personnalisé</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation type="vanished">Stature:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="vanished">Taille:</translation>
    </message>
    <message>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <source>Open only for read</source>
        <translation>Ouverture en lecture seule</translation>
    </message>
    <message>
        <source>Call context menu for edit</source>
        <translation>Menu contextuel pour edition</translation>
    </message>
    <message>
        <source>No image</source>
        <translation>Image absente</translation>
    </message>
    <message>
        <source>Delete image</source>
        <translation>Effacer image</translation>
    </message>
    <message>
        <source>Change image</source>
        <translation>Changer d&apos;image</translation>
    </message>
    <message>
        <source>Save image to file</source>
        <translation>Enregistrer image</translation>
    </message>
    <message>
        <source>Show image</source>
        <translation>Voir image</translation>
    </message>
    <message>
        <source>Image for pattern</source>
        <translation>Image du patron</translation>
    </message>
    <message>
        <source>Images (*.png *.jpg *.jpeg *.bmp)</source>
        <translation type="vanished">Images (*.png *.jpg *.jpeg *.bmp)</translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="vanished">Images</translation>
    </message>
    <message>
        <source>Save File</source>
        <translation type="vanished">Enregistrer</translation>
    </message>
    <message>
        <source>untitled</source>
        <translation>sans titre</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation>Chemin:</translation>
    </message>
    <message>
        <source>Show in Explorer</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <source>&lt;Empty&gt;</source>
        <translation>&lt;vide&gt;</translation>
    </message>
    <message>
        <source>File was not saved yet.</source>
        <translation>Fichier non sauvegardé.</translation>
    </message>
    <message>
        <source>Show in Finder</source>
        <translation>Révéler dans le finder</translation>
    </message>
    <message>
        <source>General info</source>
        <translation type="vanished">Informations générales</translation>
    </message>
    <message>
        <source>Pattern name:</source>
        <translation type="vanished">Nom du patron :</translation>
    </message>
    <message>
        <source>Pattern number:</source>
        <translation type="vanished">Numéro de patron :</translation>
    </message>
    <message>
        <source>Company/Designer name:</source>
        <translation type="vanished">Nom de Société/Modéliste :</translation>
    </message>
    <message>
        <source>Customer name:</source>
        <translation type="vanished">Nom du client :</translation>
    </message>
    <message>
        <source>Created:</source>
        <translation type="vanished">Crée le :</translation>
    </message>
    <message>
        <source>Pattern size:</source>
        <translation type="vanished">Taille du patron :</translation>
    </message>
    <message>
        <source>Show measurements</source>
        <translation type="vanished">Voir les mensurations</translation>
    </message>
    <message>
        <source>Show date of creation</source>
        <translation type="vanished">Voir la date de création</translation>
    </message>
    <message>
        <source>Use %1 and %2 to insert pattern size and height</source>
        <translation type="vanished">Utilisez %1 et %2 pour définir la taille et la stature</translation>
    </message>
    <message>
        <source>Show date of layout creation (%1)</source>
        <translation type="vanished">Voir la date de création du plan de coupe (%1)</translation>
    </message>
    <message>
        <source>Show measurements file</source>
        <translation type="vanished">Voir le fichier de mesures</translation>
    </message>
    <message>
        <source>Pattern</source>
        <translation>Patron</translation>
    </message>
    <message>
        <source>For technical notes</source>
        <translation>Pour les notes techniques</translation>
    </message>
    <message>
        <source>Label language:</source>
        <translation>Langue:</translation>
    </message>
    <message>
        <source>Passmark length:</source>
        <translation>Longueur du cran d&apos;assemblage :</translation>
    </message>
    <message>
        <source>Invalid image. Error: %1</source>
        <translation>Image non valide. Erreur : %1</translation>
    </message>
    <message>
        <source>Unable to save image. Error: %1</source>
        <translation>Impossible d&apos;enregistrer l&apos;image. Erreur : %1</translation>
    </message>
    <message>
        <source>Save Image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <source>Unable to show image. Error: %1</source>
        <translation>Impossible d&apos;afficher l&apos;image. Erreur : %1</translation>
    </message>
    <message>
        <source>Unable to open temp file</source>
        <translation>Impossible d&apos;ouvrir le fichier temporaire</translation>
    </message>
</context>
<context>
    <name>DialogPatternXmlEdit</name>
    <message>
        <source>XML Editor</source>
        <translation type="vanished">Editeur XML</translation>
    </message>
    <message>
        <source>Value :</source>
        <translation type="vanished">Valeur:</translation>
    </message>
    <message>
        <source>Name :</source>
        <translation type="vanished">Nom : </translation>
    </message>
    <message>
        <source>&lt;No selection&gt;</source>
        <translation type="vanished">&lt;Pas de sélection&gt;</translation>
    </message>
    <message>
        <source>Type : </source>
        <translation type="vanished">Type :</translation>
    </message>
    <message>
        <source>Add  attribute</source>
        <translation type="vanished">Ajouter un attribut</translation>
    </message>
    <message>
        <source>Add  son</source>
        <translation type="vanished">Ajouter un fils</translation>
    </message>
    <message>
        <source>Remove attribute</source>
        <translation type="vanished">Retirer un attribut</translation>
    </message>
    <message>
        <source>Remove node</source>
        <translation type="vanished">Supprimer un nœud</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">Valider</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Apply changes</source>
        <translation type="vanished">Appliquer les changements</translation>
    </message>
    <message>
        <source>Undo last</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Immediate apply</source>
        <translation type="vanished">Appliquer immédiatement</translation>
    </message>
    <message>
        <source>Base selection</source>
        <translation type="vanished">Sélection de Base</translation>
    </message>
    <message>
        <source>All pattern pieces</source>
        <translation type="vanished">Toute les pièces du patron</translation>
    </message>
    <message>
        <source>No changes</source>
        <translation type="vanished">Pas de changements</translation>
    </message>
    <message>
        <source>Cannot delete previously created node</source>
        <translation type="vanished">Impossible de supprimer le noeud précédemment créé</translation>
    </message>
    <message>
        <source>No changes left</source>
        <translation type="vanished">Plus de changement restant</translation>
    </message>
    <message>
        <source>Cannot undo change</source>
        <translation type="vanished">Impossible d&apos;annuler le changement</translation>
    </message>
    <message>
        <source>&lt;no value&gt;</source>
        <translation type="vanished">&lt;sans valeur&gt;</translation>
    </message>
    <message>
        <source>Unchanged</source>
        <translation type="vanished">Inchangé</translation>
    </message>
    <message>
        <source>Cannot delete previously created attribute</source>
        <translation type="vanished">Impossible de supprimer l&apos;attribut précédemment crée</translation>
    </message>
    <message>
        <source>Node Name</source>
        <translation type="vanished">Nom du nœud</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">Nom:</translation>
    </message>
    <message>
        <source>Node Value (may be empty)</source>
        <translation type="vanished">Valeur de Nœud (peut être vide)</translation>
    </message>
    <message>
        <source>Value:</source>
        <translation type="vanished">Valeur:</translation>
    </message>
    <message>
        <source>Attribute Name</source>
        <translation type="vanished">Nom d&apos;attribut</translation>
    </message>
    <message>
        <source>Attribute Value</source>
        <translation type="vanished">Valeur d&apos;attribut</translation>
    </message>
    <message>
        <source>No selection</source>
        <translation type="vanished">Pas de sélection</translation>
    </message>
    <message>
        <source>Root node</source>
        <translation type="vanished">Noeud racine</translation>
    </message>
    <message>
        <source>Node</source>
        <translation type="vanished">Nœud</translation>
    </message>
    <message>
        <source>Attribute</source>
        <translation type="vanished">Attribut</translation>
    </message>
    <message>
        <source>Immediately apply</source>
        <translation type="vanished">Appliquer immédiatement</translation>
    </message>
    <message>
        <source>Type: </source>
        <translation type="vanished">Type : </translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">Type :</translation>
    </message>
</context>
<context>
    <name>DialogPiecePath</name>
    <message>
        <source>Piece path tool</source>
        <translation>Outil de création de pièce</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Unnamed path</source>
        <translation>Chemin sans nom</translation>
    </message>
    <message>
        <source>Create name for your path</source>
        <translation>Saisir le nom du chemin</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <source>Piece:</source>
        <translation>Pièce :</translation>
    </message>
    <message>
        <source>Type of pen:</source>
        <translation>Type de tracé :</translation>
    </message>
    <message>
        <source>Ready!</source>
        <translation>Prêt !</translation>
    </message>
    <message>
        <source>Seam allowance</source>
        <translation>Marge de couture</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant de création de formules</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afficher le calcul complet&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation>Nœuds</translation>
    </message>
    <message>
        <source>Node:</source>
        <translation>Nœud :</translation>
    </message>
    <message>
        <source>Before:</source>
        <translation>Avant :</translation>
    </message>
    <message>
        <source>Return to default width</source>
        <translation>Revenir à la largeur par défaut</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <source>After:</source>
        <translation>Après :</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle :</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation>Inverser</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Edit seam allowance width</source>
        <translation>Editer la largeur de la marge de couture</translation>
    </message>
    <message>
        <source>Edit seam allowance width before</source>
        <translation>Editer la largeur de la marge de couture avant</translation>
    </message>
    <message>
        <source>Edit seam allowance width after</source>
        <translation>Editer la largeur de la marge de couture après</translation>
    </message>
    <message>
        <source>Internal path</source>
        <translation>Chemin interne</translation>
    </message>
    <message>
        <source>Custom seam allowance</source>
        <translation>Marge de couture personnalisée</translation>
    </message>
    <message>
        <source>You need more points!</source>
        <translation>Il faut plus de points !</translation>
    </message>
    <message>
        <source>First point of &lt;b&gt;custom seam allowance&lt;/b&gt; cannot be equal to the last point!</source>
        <translation>Le premier point de la &lt;b&gt;marge de couture personnalisée&lt;/b&gt; ne peut être identique au dernier !</translation>
    </message>
    <message>
        <source>You have double points!</source>
        <translation>Vous avez des points en double !</translation>
    </message>
    <message>
        <source>Passmarks</source>
        <translation>Crans d&apos;assemblage</translation>
    </message>
    <message>
        <source>Passmark:</source>
        <translation>Crans d&apos;assemblage :</translation>
    </message>
    <message>
        <source>One line</source>
        <translation>Un trait</translation>
    </message>
    <message>
        <source>Two lines</source>
        <translation>Deux traits</translation>
    </message>
    <message>
        <source>Three lines</source>
        <translation>Trois traits</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Straightforward</source>
        <translation>Tout droit</translation>
    </message>
    <message>
        <source>Bisector</source>
        <translation>Bissectrice</translation>
    </message>
    <message>
        <source>Passmark</source>
        <translation>Cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Marks</source>
        <translation>Repères</translation>
    </message>
    <message>
        <source>T mark</source>
        <translation>Marquage en T</translation>
    </message>
    <message>
        <source>V mark</source>
        <translation>Marquage en V</translation>
    </message>
    <message>
        <source>Please, select a detail to insert into!</source>
        <translation>Merci de sélectionner au moins une pièce !</translation>
    </message>
    <message>
        <source>List of details is empty!</source>
        <translation>La liste des pièces est vide !</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Intersection</source>
        <translation>Intersection</translation>
    </message>
    <message>
        <source>Each point in the &lt;b&gt;custom seam allowance&lt;/b&gt; path must be unique!</source>
        <translation>Chaque point du chemin de la &lt;b&gt;marge de couture personnalisée&lt;/b&gt; doit être unique !</translation>
    </message>
    <message>
        <source>Cut on fabric</source>
        <translation>Couper le tissu</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark. Show only left passmark.</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage. N&apos;affiche que le cran d&apos;assemblage de gauche.</translation>
    </message>
    <message>
        <source>Intersection (only left)</source>
        <translation>Intersection (seulement à gauche)</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark. Show only right passmark.</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage. N&apos;affiche que le cran d&apos;assemblage de droite.</translation>
    </message>
    <message>
        <source>Intersection (only right)</source>
        <translation>Intersection (seulement à droite)</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Gestion</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <source>Create a formula that regulates visibility. Values different from &quot;0&quot; make a path visible.</source>
        <translation>Créer une formule pour gérer la visibilité. Toute autre valeur que &quot;0&quot; rend le tracé visible.</translation>
    </message>
    <message>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <source>Control visibility</source>
        <translation>Gestion de la visibilité</translation>
    </message>
    <message>
        <source>Intersection 2</source>
        <translation>Intersection 2</translation>
    </message>
    <message>
        <source>Intersection 2 (only left)</source>
        <translation>Intersection 2 (gauche seulement)</translation>
    </message>
    <message>
        <source>Intersection 2 (only right)</source>
        <translation>Intersection 2 (droite seulement)</translation>
    </message>
    <message>
        <source>Check uniqueness</source>
        <translation>Vérifier objet unique</translation>
    </message>
    <message>
        <source>Move on top</source>
        <translation>Déplacer au premier plan</translation>
    </message>
    <message>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <source>Move on bottom</source>
        <translation>Déplacer à l&apos;arrière plan</translation>
    </message>
    <message>
        <source>Continue the first point to the cutting contour</source>
        <translation>Prolonger le premier point jusqu&apos;au contour de coupe</translation>
    </message>
    <message>
        <source>Continue the last point to the cutting contour</source>
        <translation>Prolonger le dernier point jusqu&apos;au contour de coupe</translation>
    </message>
    <message>
        <source>The path is a cut contour. Use to control export to DXF-AAMA</source>
        <translation>Le chemin est le contour de coupe. A utiliser pour contrôler l&apos;export vers DXF-AAMA</translation>
    </message>
    <message>
        <source>The list of pieces is empty. Please, first create at least one piece for current pattern piece.</source>
        <translation>La liste des pièces est vide. Créer d&apos;abord au moins une pièce pour ce patron.</translation>
    </message>
    <message>
        <source>This option has effect only if the second passmark on seam line enabled in global preferences. The option helps disable the second passmark for this passmark only.</source>
        <translation>Cette option n&apos;a d&apos;effet que si la présence d&apos;un second cran d&apos;assemblage sur la ligne de couture, est activée dans les préférences de Valentina. Cette option ne désactive la présence du cran d&apos;assemblage que pour ce second repère.</translation>
    </message>
    <message>
        <source>Show the second passmark on seam line</source>
        <translation>Afficher le second cran d&apos;assemblage sur la ligne de couture</translation>
    </message>
    <message>
        <source>Excluded</source>
        <translation>Exclu</translation>
    </message>
    <message>
        <source>The same curve repeats twice!</source>
        <translation>La même courbe a été sélectionnée 2 fois !</translation>
    </message>
    <message>
        <source>&lt;Empty&gt;</source>
        <translation>&lt;Empty&gt;</translation>
    </message>
    <message>
        <source>Current seam allowance</source>
        <translation>Marge de couture par défaut</translation>
    </message>
    <message>
        <source>Acute angle that looks inside of piece</source>
        <translation>En forme d&apos;angle aigu orienté vers l&apos;intérieur de la pièce</translation>
    </message>
    <message>
        <source>Acute angle that looks outside of piece</source>
        <translation>En forme d&apos;angle aigu oreinté vers l&apos;extérieur de la pièce</translation>
    </message>
    <message>
        <source>V mark 2</source>
        <translation>Marquage en V (n° 2)</translation>
    </message>
    <message>
        <source>Select main path objects, &lt;b&gt;%1&lt;/b&gt; - reverse direction curve, &lt;b&gt;%2&lt;/b&gt; - finish creation</source>
        <translation>Sélectionner les objets du chemin principal, &lt;b&gt;%1&lt;/b&gt; - inverser la direction de la courbe, &lt;b&gt;%2&lt;/b&gt; - finir la création</translation>
    </message>
    <message>
        <source>Manual length</source>
        <translation>Longueur personnalisée</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>U mark</source>
        <translation>Marquage en U</translation>
    </message>
    <message>
        <source>Box mark</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <source>Edit passmark length</source>
        <translation>Editer la longueur du cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Invalid segment!</source>
        <translation>Segment invalide !</translation>
    </message>
</context>
<context>
    <name>DialogPin</name>
    <message>
        <source>Pin tool</source>
        <translation>Outil Epingle</translation>
    </message>
    <message>
        <source>Point:</source>
        <translation>Point :</translation>
    </message>
    <message>
        <source>Piece:</source>
        <translation>Element:</translation>
    </message>
    <message>
        <source>Pin</source>
        <translation>Epingle</translation>
    </message>
    <message>
        <source>The list of pieces is empty. Please, first create at least one piece for current pattern piece.</source>
        <translation>La liste des pièces est vide. Créer d&apos;abord au moins une pièce pour le patron en cours.</translation>
    </message>
</context>
<context>
    <name>DialogPlaceLabel</name>
    <message>
        <source>Width:</source>
        <translation>Largeur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant de création de formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afficher le calcul complet&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle :</translation>
    </message>
    <message>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>Point:</source>
        <translation>Point :</translation>
    </message>
    <message>
        <source>Piece:</source>
        <translation>Pièce :</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <source>Edit rectangle width</source>
        <translation>Modifier la largeur du rectangle</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Éditer l&apos;angle</translation>
    </message>
    <message>
        <source>Segment</source>
        <translation>Segment</translation>
    </message>
    <message>
        <source>Rectangle</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <source>Cross</source>
        <translation>Croix</translation>
    </message>
    <message>
        <source>T-shaped</source>
        <translation>En forme de T</translation>
    </message>
    <message>
        <source>Doubletree</source>
        <translation>Croix 2</translation>
    </message>
    <message>
        <source>Corner</source>
        <translation>En coin</translation>
    </message>
    <message>
        <source>Triangle</source>
        <translation>Triangle</translation>
    </message>
    <message>
        <source>H-shaped</source>
        <translation>En forme de H</translation>
    </message>
    <message>
        <source>Button</source>
        <translation>Bouton</translation>
    </message>
    <message>
        <source>Place label</source>
        <translation>Placer le repère</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Visibilité</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <source>Create a formula that regulates visibility. Values different from &quot;0&quot; make a path visible.</source>
        <translation>Créer une formule pour gérer la visibilité. Toute autre valeur que &quot;0&quot; rend le tracé visible.</translation>
    </message>
    <message>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <source>The list of pieces is empty. Please, first create at least one piece for current pattern piece.</source>
        <translation>La liste des pièces est vide. Créer d&apos;abord au moins une pièce pour ce patron.</translation>
    </message>
    <message>
        <source>Control visibility</source>
        <translation>Gestion de la visibilité</translation>
    </message>
    <message>
        <source>Circle</source>
        <translation>Cercle</translation>
    </message>
</context>
<context>
    <name>DialogPointFromArcAndTangent</name>
    <message>
        <source>Point from arc and tangent</source>
        <translation>Point à l&apos;intersection d&apos;un arc et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Tangent point</source>
        <translation type="vanished">Point de la tangente</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation type="vanished">Arc</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation type="vanished">Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Take</source>
        <translation type="vanished">Prendre</translation>
    </message>
    <message>
        <source>Select an arc</source>
        <translation>Choisir un arc</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Tangent point:</source>
        <translation>Point de la tangente:</translation>
    </message>
    <message>
        <source>Arc:</source>
        <translation>Arc:</translation>
    </message>
    <message>
        <source>Take:</source>
        <translation>Prendre:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointFromCircleAndTangent</name>
    <message>
        <source>Point from circle and tangent</source>
        <translation>Point à l&apos;intersection d&apos;un cercle et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Center of the circle</source>
        <translation type="vanished">Centre du cercle</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation type="vanished">Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Tangent point</source>
        <translation type="vanished">Point de la tangente</translation>
    </message>
    <message>
        <source>Take</source>
        <translation type="vanished">Prendre</translation>
    </message>
    <message>
        <source>Select a circle center</source>
        <translation>Choisir le centre du cercle</translation>
    </message>
    <message>
        <source>Edit radius</source>
        <translation>Editer le rayon</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Radius can&apos;t be negative</source>
        <translation>Le rayon ne peut pas être négatif</translation>
    </message>
    <message>
        <source>Radius:</source>
        <translation>Rayon:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Center of the circle:</source>
        <translation>Centre du cercle:</translation>
    </message>
    <message>
        <source>Tangent point:</source>
        <translation>Point de la tangente:</translation>
    </message>
    <message>
        <source>Take:</source>
        <translation>Prendre:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointOfContact</name>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>Value of radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Center of arc</source>
        <translation type="vanished">Centre de l&apos;arc</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation>Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Top of the line</source>
        <translation type="vanished">Sommet de la ligne</translation>
    </message>
    <message>
        <source>End of the line</source>
        <translation type="vanished">Fin de la ligne</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le second point d&apos;une ligne</translation>
    </message>
    <message>
        <source>Point at intersection of arc and line</source>
        <translation>Point à l&apos;intersection d&apos;un arc et d&apos;une ligne</translation>
    </message>
    <message>
        <source>Edit radius</source>
        <translation>Editer le rayon</translation>
    </message>
    <message>
        <source>Radius:</source>
        <translation>Rayon:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Center of arc:</source>
        <translation>Centre de l&apos;arc:</translation>
    </message>
    <message>
        <source>Top of the line:</source>
        <translation>Sommet de la ligne:</translation>
    </message>
    <message>
        <source>End of the line:</source>
        <translation>Fin de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointOfIntersection</name>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point of angle</source>
        <translation type="vanished">Premier point de l&apos;angle</translation>
    </message>
    <message>
        <source>Second point of angle</source>
        <translation type="vanished">Second point de l&apos;angle</translation>
    </message>
    <message>
        <source>Point from X and Y of two other points</source>
        <translation>Point à l&apos;X d&apos;un point et l&apos;Y d&apos;un autre</translation>
    </message>
    <message>
        <source>X: vertical point</source>
        <translation type="vanished">X : point à la verticale</translation>
    </message>
    <message>
        <source>Y: horizontal point</source>
        <translation type="vanished">Y : point à l&apos;horizontal</translation>
    </message>
    <message>
        <source>Select point for Y value (horizontal)</source>
        <translation>Choisir point pour la valeur de Y (horizontal)</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>X: vertical point:</source>
        <translation>X : point vertical:</translation>
    </message>
    <message>
        <source>Y: horizontal point:</source>
        <translation>Y : point horizontal:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointOfIntersectionArcs</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialogue</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First arc</source>
        <translation type="vanished">Premier arc</translation>
    </message>
    <message>
        <source>Selected arc</source>
        <translation type="vanished">Arc sélectionné</translation>
    </message>
    <message>
        <source>Second arc</source>
        <translation type="vanished">Deuxième arc</translation>
    </message>
    <message>
        <source>Take</source>
        <translation type="vanished">Prendre</translation>
    </message>
    <message>
        <source>Select second an arc</source>
        <translation>Choisir un deuxième arc</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First arc:</source>
        <translation>Premier arc:</translation>
    </message>
    <message>
        <source>Second arc:</source>
        <translation>Deuxième arc:</translation>
    </message>
    <message>
        <source>Take:</source>
        <translation>Prendre:</translation>
    </message>
    <message>
        <source>Tool point of intersetion arcs</source>
        <translation>Outil point d&apos;intersection des arcs</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointOfIntersectionCircles</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialogue</translation>
    </message>
    <message>
        <source>Radius of the first circle</source>
        <translation type="vanished">Rayon du premier cercle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Radius of the second circle</source>
        <translation type="vanished">Rayon du deuxième cercle</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Center of the first circle</source>
        <translation type="vanished">Centre du premier cercle</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation type="vanished">Choisir le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Center of the second circle</source>
        <translation type="vanished">Centre du deuxième cercle</translation>
    </message>
    <message>
        <source>Take</source>
        <translation type="vanished">Prendre</translation>
    </message>
    <message>
        <source>Select second circle center</source>
        <translation>Choisir le centre du deuxième cercle</translation>
    </message>
    <message>
        <source>Edit first circle radius</source>
        <translation>Editer le rayon du premier cercle</translation>
    </message>
    <message>
        <source>Edit second circle radius</source>
        <translation>Editer le rayon du deuxième cercle</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Radius can&apos;t be negative</source>
        <translation>Le rayon ne peut pas être négatif</translation>
    </message>
    <message>
        <source>Radius of the first circle:</source>
        <translation>Rayon du premier cercle:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Radius of the second circle:</source>
        <translation>Rayon du deuxième cercle:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Center of the first circle:</source>
        <translation>Centre du premier cercle:</translation>
    </message>
    <message>
        <source>Center of the second circle:</source>
        <translation>Centre du deuxième cercle:</translation>
    </message>
    <message>
        <source>Take:</source>
        <translation>Prendre:</translation>
    </message>
    <message>
        <source>Tool point of intersection circles</source>
        <translation>Outil Point d&apos;intersection de 2 cercles</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogPointOfIntersectionCurves</name>
    <message>
        <source>Tool point of intersection curves</source>
        <translation>Outil Point d&apos;intersection de 2 courbes</translation>
    </message>
    <message>
        <source>First curve:</source>
        <translation>Première courbe:</translation>
    </message>
    <message>
        <source>Second curve:</source>
        <translation>Seconde courbe:</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisir un nom unique.</translation>
    </message>
    <message>
        <source>Vertical correction:</source>
        <translation>Correction verticale :</translation>
    </message>
    <message>
        <source>Horizontal correction:</source>
        <translation>Correction horizontale :</translation>
    </message>
    <message>
        <source>Select second curve</source>
        <translation>Choisir une seconde courbe</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Curve 1</source>
        <translation>Courbe 1</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1:</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
    <message>
        <source>Curve 2</source>
        <translation>Courbe 2</translation>
    </message>
</context>
<context>
    <name>DialogPreferences</name>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <source>Pattern</source>
        <translation>Patron</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation>Répertoires</translation>
    </message>
    <message numerus="yes">
        <source>Followed %n option(s) require restart to take effect: %1.</source>
        <translation>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogPuzzlePreferences</name>
    <message>
        <source>Puzzle preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation>Répertoires</translation>
    </message>
    <message numerus="yes">
        <source>Followed %n option(s) require restart to take effect: %1.</source>
        <translation>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
        </translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Plan de coupe</translation>
    </message>
</context>
<context>
    <name>DialogRestrictDimension</name>
    <message>
        <source>Restrict dimension</source>
        <translation>Restreindre les dimensions</translation>
    </message>
    <message>
        <source>Dimension A:</source>
        <translation>Dimension A :</translation>
    </message>
    <message>
        <source>Restriction</source>
        <translation>Restriction</translation>
    </message>
    <message>
        <source>Min:</source>
        <translation>Minimum :</translation>
    </message>
    <message>
        <source>Max:</source>
        <translation>Maximum :</translation>
    </message>
    <message>
        <source>Exclude</source>
        <translation>Exclure</translation>
    </message>
    <message>
        <source>Include</source>
        <translation>Inclure</translation>
    </message>
</context>
<context>
    <name>DialogRotation</name>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calulation</source>
        <translation type="vanished">Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul complet dans une boite&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Origin Point:</source>
        <translation>Point d&apos;origine:</translation>
    </message>
    <message>
        <source>Suffix:</source>
        <translation>Suffixe:</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Editer l&apos;angle</translation>
    </message>
    <message>
        <source>Select origin point</source>
        <translation>Selectionner le second point</translation>
    </message>
    <message>
        <source>Select origin point that is not part of the list of objects</source>
        <translation type="vanished">Sélectionnez un point d&apos;origine qui n&apos;est pas dans la liste d&apos;objets</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>This point cannot be origin point. Please, select another origin point</source>
        <translation>Ce point ne peut pas être le point de départ. Sélectionner un autre point de départ</translation>
    </message>
    <message>
        <source>Visibility Group</source>
        <translation>Groupe de visibilité</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Add tags</source>
        <translation>Ajouter des balises</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias</source>
        <translation>Nom d&apos;affichage - Alias</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Invalid suffix</source>
        <translation>suffixe invalide</translation>
    </message>
    <message>
        <source>Invalid group name</source>
        <translation>Nom de groupe non valide</translation>
    </message>
    <message>
        <source>Invalid rotation point</source>
        <translation>Point de rotation invalide</translation>
    </message>
    <message>
        <source>Invalid label</source>
        <translation>Etiquette invalide</translation>
    </message>
    <message>
        <source>Invalid alias</source>
        <translation>Alias invalide</translation>
    </message>
    <message>
        <source>Invalid angle formula</source>
        <translation>La formule de l&apos;angle est invalide</translation>
    </message>
    <message>
        <source>Label:</source>
        <translation>Etiquette :</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Enable to create a visibility group from original objects</source>
        <translation>Créer un groupe de visibilité comprenant les objets d&apos;origine</translation>
    </message>
</context>
<context>
    <name>DialogSaveLAyout</name>
    <message>
        <source>Save Layout</source>
        <translation>Enregistrer le plan de coupe</translation>
    </message>
    <message>
        <source>File name:</source>
        <translation>Nom du fichier :</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation>Chemin d&apos;accès :</translation>
    </message>
    <message>
        <source>File format:</source>
        <translation>Format du fichier :</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation type="vanished">Parcourir …</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">EtiquetteTexte</translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation>Dossier de destination</translation>
    </message>
    <message>
        <source>Path to destination folder.</source>
        <translation type="vanished">Dossier de destination.</translation>
    </message>
    <message>
        <source>Select path to destination folder</source>
        <translation>Sélectionnez le dossier de destination</translation>
    </message>
    <message>
        <source>File base name</source>
        <translation>Nom du fichier de base</translation>
    </message>
    <message>
        <source>File base name. </source>
        <translation type="vanished">Nom du fichier de base.</translation>
    </message>
    <message>
        <source>File base name.</source>
        <translation type="vanished">Nom du fichier de base.</translation>
    </message>
    <message>
        <source>Path to destination folder</source>
        <translation>Dossier de destination</translation>
    </message>
    <message>
        <source>Binary form</source>
        <translation>Forme binaire</translation>
    </message>
    <message>
        <source>Text as paths</source>
        <translation>Texte en chemins</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Right:</source>
        <translation>Droit:</translation>
    </message>
    <message>
        <source>Left:</source>
        <translation>Gauche:</translation>
    </message>
    <message>
        <source>Top:</source>
        <translation>Haut :</translation>
    </message>
    <message>
        <source>Bottom:</source>
        <translation>Bas :</translation>
    </message>
    <message>
        <source>Paper format</source>
        <translation>Format du papier</translation>
    </message>
    <message>
        <source>Browse…</source>
        <translation>Parcourir…</translation>
    </message>
    <message>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <source>Templates:</source>
        <translation>Modèles:</translation>
    </message>
    <message>
        <source>Orientation:</source>
        <translation>Orientation :</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <source>Horizontal:</source>
        <translation>horizontal :</translation>
    </message>
    <message>
        <source>Vertical:</source>
        <translation>vertical :</translation>
    </message>
</context>
<context>
    <name>DialogSaveLayout</name>
    <message>
        <source>Name conflict</source>
        <translation>Conflit de nom</translation>
    </message>
    <message>
        <source>Folder already contain file with name %1. Rewrite all conflict file names?</source>
        <translation>Le dossier contient déjà un fichier avec le nom %1. Renommer tous les noms de fichiers en conflit?</translation>
    </message>
    <message>
        <source>Example:</source>
        <translation>Exemple:</translation>
    </message>
    <message>
        <source>Select folder</source>
        <translation>Sélectionner un dossier</translation>
    </message>
    <message>
        <source>Svg files (*.svg)</source>
        <translation type="vanished">Format SVG (*.svg)</translation>
    </message>
    <message>
        <source>PDF files (*.pdf)</source>
        <translation type="vanished">Format PDF (*.pdf)</translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation type="vanished">Format image (*.png)</translation>
    </message>
    <message>
        <source>Wavefront OBJ (*.obj)</source>
        <translation type="vanished">Format Wavefront OBJ (*.obj)</translation>
    </message>
    <message>
        <source>PS files (*.ps)</source>
        <translation type="vanished">Format Postscript (*.ps)</translation>
    </message>
    <message>
        <source>EPS files (*.eps)</source>
        <translation type="vanished">Format EPS (*.eps)</translation>
    </message>
    <message>
        <source>DXF files (*.dxf)</source>
        <translation type="vanished">Format DXF (*.dxf)</translation>
    </message>
    <message>
        <source>Tried to use out of range format number.</source>
        <translation>Nombre au delà de la plage de valeur.</translation>
    </message>
    <message>
        <source>Selected not present format.</source>
        <translation>Format sélectionné, absent.</translation>
    </message>
    <message>
        <source>The base filename has not match regular expression.</source>
        <translation type="vanished">Le nom du fichier de base ne correspond pas à une expression régulière.</translation>
    </message>
    <message>
        <source>The destination directory doesn&apos;t exists or is not readable.</source>
        <translation>Le répertoire de destination n&apos;existe pas ou est illisible.</translation>
    </message>
    <message>
        <source>The base filename does not match a regular expression.</source>
        <translation>Le nom du fichier de base ne correspond pas à une expression régulière.</translation>
    </message>
</context>
<context>
    <name>DialogSaveManualLayout</name>
    <message>
        <source>Path:</source>
        <translation>Chemin:</translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation>Le dossier de destination</translation>
    </message>
    <message>
        <source>Path to destination folder</source>
        <translation>Emplacement du dossier de destination</translation>
    </message>
    <message>
        <source>Select path to destination folder</source>
        <translation>Sélectionnez un emplacement pour le dossier de destination</translation>
    </message>
    <message>
        <source>Browse…</source>
        <translation>Parcourir…</translation>
    </message>
    <message>
        <source>File format:</source>
        <translation>Format du fichier :</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Binary form</source>
        <translation>Format binaire</translation>
    </message>
    <message>
        <source>Text as paths</source>
        <translation>Texte en chemins</translation>
    </message>
    <message>
        <source>Export unified</source>
        <translation>Exporter assemblé</translation>
    </message>
    <message>
        <source>Tiles scheme</source>
        <translation>Vue empilée</translation>
    </message>
    <message>
        <source>File name:</source>
        <translation>Nom du fichier:</translation>
    </message>
    <message>
        <source>File base name</source>
        <translation>Nom du fichier de base</translation>
    </message>
    <message>
        <source>The base filename does not match a regular expression.</source>
        <translation>Le nom du fichier de base ne correspond pas à une expression régulière.</translation>
    </message>
    <message>
        <source>Select folder</source>
        <translation>Sélectionner un dossier</translation>
    </message>
    <message>
        <source>Tried to use out of range format number.</source>
        <translation>Nombre au delà de la plage de valeur.</translation>
    </message>
    <message>
        <source>Selected not present format.</source>
        <translation>Format sélectionné, absent.</translation>
    </message>
    <message>
        <source>The destination directory doesn&apos;t exists or is not readable.</source>
        <translation>Le répertoire de destination n&apos;existe pas ou est illisible.</translation>
    </message>
    <message>
        <source>Name conflict</source>
        <translation>Conflit de nom</translation>
    </message>
    <message>
        <source>Folder already contain file with name %1. Rewrite all conflict file names?</source>
        <translation>Le dossier contient déjà un fichier avec le nom %1. Renommer tous les noms de fichiers en conflit?</translation>
    </message>
    <message>
        <source>Example:</source>
        <translation>Exemple :</translation>
    </message>
    <message>
        <source>Save manual layout</source>
        <translation>Sauvegarder le plan de coupe manuel</translation>
    </message>
</context>
<context>
    <name>DialogSeamAllowance</name>
    <message>
        <source>Ready!</source>
        <translation>Prêt!</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation>Pièce</translation>
    </message>
    <message>
        <source>Grainline</source>
        <translation>Droit-fil</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation>Inverser</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Error. Can&apos;t save piece path.</source>
        <translation>Erreur : le chemin d&apos;accès de la pièce ne peut pas être sauvegardé.</translation>
    </message>
    <message>
        <source>Infinite/undefined result</source>
        <translation>Résultat infini ou non défini</translation>
    </message>
    <message>
        <source>Length should be positive</source>
        <translation>La longueur indiquée doit être positive</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation>Erreur de l&apos;interpréteur : %1</translation>
    </message>
    <message>
        <source>Current seam allowance</source>
        <translation>Marge de couture actuelle</translation>
    </message>
    <message>
        <source>Edit seam allowance width</source>
        <translation>Editer la largeur de la marge de couture</translation>
    </message>
    <message>
        <source>Edit seam allowance width before</source>
        <translation>Editer la largeur de la marge de couture avant</translation>
    </message>
    <message>
        <source>Edit seam allowance width after</source>
        <translation>Editer la largeur de la marge de couture après</translation>
    </message>
    <message>
        <source>You need more points!</source>
        <translation>Vous avez besoin de plus de points!</translation>
    </message>
    <message>
        <source>You have to choose points in a clockwise direction!</source>
        <translation>Vous devez choisir les points dans le sens des aiguilles d&apos;une montre!</translation>
    </message>
    <message>
        <source>First point cannot be equal to the last point!</source>
        <translation>Le premier point ne peut être identique au dernier!</translation>
    </message>
    <message>
        <source>You have double points!</source>
        <translation>Vous avez des points en double!</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vide</translation>
    </message>
    <message>
        <source>main path</source>
        <translation>Contour principal</translation>
    </message>
    <message>
        <source>custom seam allowance</source>
        <translation>Personnaliser la marge de couture</translation>
    </message>
    <message>
        <source>Both</source>
        <translation>Les deux</translation>
    </message>
    <message>
        <source>Just front</source>
        <translation>Juste le devant</translation>
    </message>
    <message>
        <source>Just rear</source>
        <translation>Juste l&apos;arrière</translation>
    </message>
    <message>
        <source>Pins</source>
        <translation>Epingles</translation>
    </message>
    <message>
        <source>no pin</source>
        <translation>pas d&apos;épingle</translation>
    </message>
    <message>
        <source>Labels</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Éditer longueur</translation>
    </message>
    <message>
        <source>Edit angle</source>
        <translation>Éditer l&apos;angle</translation>
    </message>
    <message>
        <source>Edit height</source>
        <translation>Editer la stature</translation>
    </message>
    <message>
        <source>Edit width</source>
        <translation>Editer la largeur</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation>Chemins</translation>
    </message>
    <message>
        <source>Excluded</source>
        <translation>Exclus</translation>
    </message>
    <message>
        <source>Passmark</source>
        <translation>Cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Each point in the path must be unique!</source>
        <translation>Chaque point du contour doit être unique !</translation>
    </message>
    <message>
        <source>Passmarks</source>
        <translation>Crans d&apos;assemblage</translation>
    </message>
    <message>
        <source>Workpiece tool</source>
        <translation>Extraction de contour principal</translation>
    </message>
    <message>
        <source>Segment</source>
        <translation>Segment</translation>
    </message>
    <message>
        <source>Rectangle</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <source>Cross</source>
        <translation>Croix</translation>
    </message>
    <message>
        <source>T-shaped</source>
        <translation>En forme de T</translation>
    </message>
    <message>
        <source>Doubletree</source>
        <translation>Croix 2</translation>
    </message>
    <message>
        <source>Corner</source>
        <translation>Coin</translation>
    </message>
    <message>
        <source>Triangle</source>
        <translation>Triangle</translation>
    </message>
    <message>
        <source>H-shaped</source>
        <translation>En forme de H</translation>
    </message>
    <message>
        <source>Button</source>
        <translation>Bouton</translation>
    </message>
    <message>
        <source>Place label</source>
        <translation>Placer l&apos;étiquette</translation>
    </message>
    <message>
        <source>Check uniqueness</source>
        <translation>Vérifier la spécificité</translation>
    </message>
    <message>
        <source>To open all detail&apos;s features complete creating the main path. Please, press OK.</source>
        <translation>Pour avoir accès à toutes les options de la pièces, merci de finir la création du contour du contour principal. Cliquez sur OK.</translation>
    </message>
    <message>
        <source>The same curve repeats twice!</source>
        <translation>La même courbe a été sélectionnée 2 fois !</translation>
    </message>
    <message>
        <source>&lt;Empty&gt;</source>
        <translation>&lt;vide&gt;</translation>
    </message>
    <message>
        <source>Select main path objects clockwise, &lt;b&gt;%1&lt;/b&gt; - reverse direction curve, &lt;b&gt;%2&lt;/b&gt; - finish creation</source>
        <translation>Sélectionner les objets du chemin principal dans le sens horaire, &lt;b&gt;%1&lt;/b&gt; - inverser le sens de la courbe, &lt;b&gt;%2&lt;/b&gt; - finir la création</translation>
    </message>
    <message>
        <source>Circle</source>
        <translation>Cercle</translation>
    </message>
    <message>
        <source>Edit passmark length</source>
        <translation>Editer la longueur du cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>The customer name from individual measurements</source>
        <translation>Le nom du ou de la client.e depuis les mesures individuelles</translation>
    </message>
    <message>
        <source>The customer email from individual measurements</source>
        <translation>L&apos;Email du client lu depuis les mesures individuelles</translation>
    </message>
    <message>
        <source>The customer birth date from individual measurements</source>
        <translation>La date de naissance du client d&apos;après les mesures individuelles</translation>
    </message>
    <message>
        <source>Save label data.</source>
        <translation>Sauvegarder les données de l&apos;étiquette.</translation>
    </message>
    <message>
        <source>Label data were changed. Do you want to save them before editing label template?</source>
        <translation>Les données de l&apos;étiquette ont été modifiées. Souhaitez-vous les enregistrer avant de poursuivre ?</translation>
    </message>
    <message>
        <source>Height</source>
        <comment>dimension</comment>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>Size</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Hip</source>
        <comment>dimension</comment>
        <translation>Hanche</translation>
    </message>
    <message>
        <source>Waist</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Height label</source>
        <comment>dimension</comment>
        <translation>Etiquette de hauteur</translation>
    </message>
    <message>
        <source>Size label</source>
        <comment>dimension</comment>
        <translation>Etiquette de taille</translation>
    </message>
    <message>
        <source>Hip label</source>
        <comment>dimension</comment>
        <translation>Etiquette de hanche</translation>
    </message>
    <message>
        <source>Waist label</source>
        <comment>dimension</comment>
        <translation>Etiquette de taille</translation>
    </message>
    <message>
        <source>Measurement: %1</source>
        <translation>Mensuration : %1</translation>
    </message>
    <message>
        <source>Invalid segment!</source>
        <translation>Segment invalide !</translation>
    </message>
    <message>
        <source>Dimension X</source>
        <comment>dimension</comment>
        <translation>Mesure X (stature)</translation>
    </message>
    <message>
        <source>Dimension Y</source>
        <comment>dimension</comment>
        <translation>Mesure Y (taille commerciale)</translation>
    </message>
    <message>
        <source>Dimension Z</source>
        <comment>dimension</comment>
        <translation>Mesure Z (tour de taille)</translation>
    </message>
    <message>
        <source>Dimension W</source>
        <comment>dimension</comment>
        <translation>Mesure W en .vst (tour de hanches)</translation>
    </message>
    <message>
        <source>Dimension X label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension X</translation>
    </message>
    <message>
        <source>Dimension Y label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension Y</translation>
    </message>
    <message>
        <source>Dimension Z label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension Z</translation>
    </message>
    <message>
        <source>Dimension W label</source>
        <comment>dimension</comment>
        <translation>Etiquette de dimension W</translation>
    </message>
</context>
<context>
    <name>DialogSelectLanguage</name>
    <message>
        <source>Select language</source>
        <translation>Choix de la langue</translation>
    </message>
    <message>
        <source>Select user interface language</source>
        <translation>Choisissez la langue de l&apos;interface utilisateur</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
</context>
<context>
    <name>DialogSetupMultisize</name>
    <message>
        <source>Setup multisize measurements</source>
        <translation>Paramétrer les valeurs multi-tailles</translation>
    </message>
    <message>
        <source>Use full circumference</source>
        <translation>Avec circonférence entière</translation>
    </message>
    <message>
        <source>Min value:</source>
        <translation>Valeur minimale :</translation>
    </message>
    <message>
        <source>Minimal value described in the column</source>
        <translation>Valeur minimale pour cette mesure</translation>
    </message>
    <message>
        <source>Max value:</source>
        <translation>Valeur maximale :</translation>
    </message>
    <message>
        <source>Maximal value described in the column</source>
        <translation>Valeur maximale pour cette mesure</translation>
    </message>
    <message>
        <source>Step:</source>
        <translation>Intervalle :</translation>
    </message>
    <message>
        <source>Single-step between the column values</source>
        <translation>Intervalle de gradation pour cette mesure</translation>
    </message>
    <message>
        <source>Base:</source>
        <translation>Base :</translation>
    </message>
    <message>
        <source>The base value for the column</source>
        <translation>Valeur de départ pour cette mesure</translation>
    </message>
    <message>
        <source>Please, select at least one dimension</source>
        <translation>Merci de sélectionner au moins un type de mesure</translation>
    </message>
    <message>
        <source>No more than 3 dimensions allowed</source>
        <translation>Vous ne pouvez pas définir plus de 3 mesures</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Custom name</source>
        <translation>Libellé personnalisé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Libellé :</translation>
    </message>
    <message>
        <source>Please, provide correct data for dimension %1</source>
        <translation>Merci de renseigner une valeur valide pour la mesure %1</translation>
    </message>
    <message>
        <source>Please, provide custom name for dimension %1</source>
        <translation>Merci de remplir le libellé personnalisé de la mesure %1</translation>
    </message>
    <message>
        <source>A body measurement measured in units of length.</source>
        <translation>Mesure corporelle en unités de longueur.</translation>
    </message>
    <message>
        <source>Body Measurement</source>
        <translation>Mesure corporelle</translation>
    </message>
    <message>
        <source>A body measurement measured in units of length. Circumference.</source>
        <translation>Mesure corporelle en unités de longueur. Circonférence.</translation>
    </message>
</context>
<context>
    <name>DialogShoulderPoint</name>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Value of length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul entier dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Third point</source>
        <translation type="vanished">Troisième point</translation>
    </message>
    <message>
        <source>Type of line</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Show line from first point to our point</source>
        <translation type="vanished">Montrer la ligne du premier point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Select first point of line</source>
        <translation>Sélectionnez le premier point de la ligne</translation>
    </message>
    <message>
        <source>Select second point of line</source>
        <translation>Choisir le second point d&apos;une ligne</translation>
    </message>
    <message>
        <source>Special point on shoulder</source>
        <translation>Point épaule</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur le ligne</translation>
    </message>
    <message>
        <source>Edit length</source>
        <translation>Editer longueur</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point:</translation>
    </message>
    <message>
        <source>Third point:</source>
        <translation>Troisième point:</translation>
    </message>
    <message>
        <source>Type of line:</source>
        <translation>Type de ligne:</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogSinglePoint</name>
    <message>
        <source>Single point</source>
        <translation>Point unique</translation>
    </message>
    <message>
        <source>Coordinates on the sheet</source>
        <translation>Coordonnées sur la feuille</translation>
    </message>
    <message>
        <source>Coordinates</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <source>Y coordinate</source>
        <translation>Coordonnées Y</translation>
    </message>
    <message>
        <source>X coordinate</source>
        <translation>Coordonnées X</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation>Nom du point</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogSpline</name>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Length ratio of the first control point</source>
        <translation type="vanished">Ratio de longueur du premier point de contrôle</translation>
    </message>
    <message>
        <source>The angle of the first control point</source>
        <translation type="vanished">Angle du premier point de contrôle</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Length ratio of the second control point</source>
        <translation type="vanished">Ratio de longueur du deuxième point de contrôle</translation>
    </message>
    <message>
        <source>The angle of the second control point</source>
        <translation type="vanished">Angle du deuxième point de contrôle</translation>
    </message>
    <message>
        <source>Coefficient of curvature of the curve</source>
        <translation type="vanished">Coefficient de courbure</translation>
    </message>
    <message>
        <source>Select last point of curve</source>
        <translation>Choisir le dernier point de la courbe</translation>
    </message>
    <message>
        <source>Simple curve</source>
        <translation>Courbe cubique simple</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Coefficient of curvature of the curve:</source>
        <translation type="vanished">Coefficient de courbure:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur:</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>Control point</source>
        <translation>Point de contrôle</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Length ratio:</source>
        <translation type="vanished">Ratio de longueur:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point :</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Invalid spline</source>
        <translation>Spline invalide</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calulation</source>
        <translation type="vanished">Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul complet dans une boite&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Edit first control point angle</source>
        <translation>Editer le premier point de contrôle d&apos;angle</translation>
    </message>
    <message>
        <source>Edit second control point angle</source>
        <translation>Editer le second point de contrôle d&apos;angle</translation>
    </message>
    <message>
        <source>Edit first control point length</source>
        <translation>Editer le premier point de contrôle de longueur</translation>
    </message>
    <message>
        <source>Edit second control point length</source>
        <translation>Editer le second point de contrôle de longueur</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Length can&apos;t be negative</source>
        <translation type="vanished">Une longueur ne peut être négative</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogSplinePath</name>
    <message>
        <source>Curved path</source>
        <translation>Courbe cubique complexe</translation>
    </message>
    <message>
        <source>Point of curve</source>
        <translation type="vanished">Point de courbe</translation>
    </message>
    <message>
        <source>Length ratio of the first control point</source>
        <translation type="vanished">Ratio de longueur du premier point de contrôle</translation>
    </message>
    <message>
        <source>The angle of the first control point</source>
        <translation type="vanished">Angle du premier point de contrôle</translation>
    </message>
    <message>
        <source>Length ratio of the second control point</source>
        <translation type="vanished">Ratio de longueur du deuxième point de contrôle</translation>
    </message>
    <message>
        <source>The angle of the second control point</source>
        <translation type="vanished">Angle du deuxième point de contrôle</translation>
    </message>
    <message>
        <source>List of points</source>
        <translation>Liste des points</translation>
    </message>
    <message>
        <source>Coefficient of curvature of the curve</source>
        <translation type="vanished">Coefficient de courbure</translation>
    </message>
    <message>
        <source>Select point of curve path</source>
        <translation>Choisir un point sur la trajectoire de la courbe</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Coefficient of curvature of the curve:</source>
        <translation type="vanished">Coefficient de courbure:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur:</translation>
    </message>
    <message>
        <source>Point:</source>
        <translation>Point :</translation>
    </message>
    <message>
        <source>First control point</source>
        <translation>Premier point de contrôle</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Length ratio:</source>
        <translation type="vanished">Ratio de longueur:</translation>
    </message>
    <message>
        <source>Second control point</source>
        <translation>Second point de contrôle</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Invalid spline path</source>
        <translation>Trajectoire de spline invalide</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant Formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calulation</source>
        <translation type="vanished">Calcul</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul complet dans une boite&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Edit first control point angle</source>
        <translation>Editer le premier point de contrôle d&apos;angle</translation>
    </message>
    <message>
        <source>Edit second control point angle</source>
        <translation>Editer le premier point de contrôle d&apos;angle</translation>
    </message>
    <message>
        <source>Edit first control point length</source>
        <translation>Editer le premier point de contrôle de longueur</translation>
    </message>
    <message>
        <source>Edit second control point length</source>
        <translation>Editer le second point de contrôle de longueur</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Length can&apos;t be negative</source>
        <translation type="vanished">Une longueur ne peut être négative</translation>
    </message>
    <message>
        <source>Not used</source>
        <translation>Non utilisé</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Calcul</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Set approximation scale for this curve, 0 - use global value</source>
        <translation>Définir l&apos;approximation d&apos;échelle pour cette courbe, 0 - utiliser une valeur globale</translation>
    </message>
    <message>
        <source>Cannot find point with id %1</source>
        <translation>Impossible de trouver le point %1</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogTapePreferences</name>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation>Chemin d&apos;accès</translation>
    </message>
    <message numerus="yes">
        <source>Followed %n option(s) require restart to take effect: %1.</source>
        <translation>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
            <numerusform>Followed %n option(s) require restart to take effect: %1.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogTool</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Empty field</source>
        <translation type="vanished">Champ vide</translation>
    </message>
    <message>
        <source>Value can&apos;t be 0</source>
        <translation type="vanished">La valeur ne peut pas être égale à 0</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation type="vanished">Erreur d&apos;analyse : %1</translation>
    </message>
    <message>
        <source>First point</source>
        <translation>Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation>Deuxième point</translation>
    </message>
    <message>
        <source>Highest point</source>
        <translation>Point le plus haut</translation>
    </message>
    <message>
        <source>Lowest point</source>
        <translation>Point le plus bas</translation>
    </message>
    <message>
        <source>Leftmost point</source>
        <translation>Point le plus à gauche</translation>
    </message>
    <message>
        <source>Rightmost point</source>
        <translation>Point le plus à droite</translation>
    </message>
    <message>
        <source>Invalid value</source>
        <translation type="vanished">Valeur non valide</translation>
    </message>
    <message>
        <source>by length</source>
        <translation>Suivant longueur</translation>
    </message>
    <message>
        <source>by points intersetions</source>
        <translation>Suivant points d&apos;intersections</translation>
    </message>
    <message>
        <source>by first edge symmetry</source>
        <translation>Par symétrie au premier côté</translation>
    </message>
    <message>
        <source>by second edge symmetry</source>
        <translation>Par symétrie au second côté</translation>
    </message>
    <message>
        <source>by first edge right angle</source>
        <translation>Par angle droit au premier côté</translation>
    </message>
    <message>
        <source>by second edge right angle</source>
        <translation>Par angle droit au second côté</translation>
    </message>
</context>
<context>
    <name>DialogTriangle</name>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>First point of axis</source>
        <translation type="vanished">Premier point de l&apos;axe</translation>
    </message>
    <message>
        <source>First point of line</source>
        <translation type="vanished">Premier point de la ligne</translation>
    </message>
    <message>
        <source>Second point of axis</source>
        <translation type="vanished">Deuxième point de l&apos;axe</translation>
    </message>
    <message>
        <source>First point</source>
        <translation type="vanished">Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation type="vanished">Deuxième point</translation>
    </message>
    <message>
        <source>Select second point of axis</source>
        <translation>Choisir le deuxième point de l&apos;axe</translation>
    </message>
    <message>
        <source>Select first point</source>
        <translation>Choisir le premier point</translation>
    </message>
    <message>
        <source>Select second point</source>
        <translation>Choisir le deuxième point</translation>
    </message>
    <message>
        <source>Triangle tool</source>
        <translation>Outil Triangle</translation>
    </message>
    <message>
        <source>First point of the axis</source>
        <translation type="vanished">Premier point de la droite</translation>
    </message>
    <message>
        <source>Second point of the axis</source>
        <translation type="vanished">deuxième point de l&apos;axe</translation>
    </message>
    <message>
        <source>Second point of line</source>
        <translation type="vanished">Deuxième point de la ligne</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point:</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>First point of axis:</source>
        <translation>Premier point de l&apos;axe:</translation>
    </message>
    <message>
        <source>Second point of axis:</source>
        <translation>Deuxième point de l&apos;axe:</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point:</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point:</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogTrueDarts</name>
    <message>
        <source>True darts</source>
        <translation>Pince automatique</translation>
    </message>
    <message>
        <source>First base point</source>
        <translation type="vanished">Premier point de base</translation>
    </message>
    <message>
        <source>First point of angle</source>
        <translation type="vanished">Premier point de l&apos;angle</translation>
    </message>
    <message>
        <source>Second base point</source>
        <translation type="vanished">Deuxième point de base</translation>
    </message>
    <message>
        <source>Second point of angle</source>
        <translation type="vanished">Deuxième point de l&apos;angle</translation>
    </message>
    <message>
        <source>First dart point</source>
        <translation type="vanished">Premier point de la pince</translation>
    </message>
    <message>
        <source>Third point of angle</source>
        <translation type="vanished">Troisième point de l&apos;angle</translation>
    </message>
    <message>
        <source>Second dart point</source>
        <translation type="vanished">Deuxième point de la pince</translation>
    </message>
    <message>
        <source>Show line from second point to this point</source>
        <translation type="vanished">Afficher la ligne du deuxième point jusqu&apos;à ce point</translation>
    </message>
    <message>
        <source>Third dart point</source>
        <translation type="vanished">Troisième point de la pince</translation>
    </message>
    <message>
        <source>First new dart point</source>
        <translation type="vanished">Premier point de la nouvelle pince</translation>
    </message>
    <message>
        <source>Second new dart point</source>
        <translation type="vanished">Deuxième point de la nouvelle pince</translation>
    </message>
    <message>
        <source>Select the second base point</source>
        <translation>Choisir le deuxième point de base</translation>
    </message>
    <message>
        <source>Select the first dart point</source>
        <translation>Choisir le premier point de la pince</translation>
    </message>
    <message>
        <source>Select the second dart point</source>
        <translation>Choisir le deuxième point de la pince</translation>
    </message>
    <message>
        <source>Select the third dart point</source>
        <translation>Choisir le troisième point de la pince</translation>
    </message>
    <message>
        <source>First base point:</source>
        <translation>Premier point de base :</translation>
    </message>
    <message>
        <source>Second base point:</source>
        <translation>Second point de base :</translation>
    </message>
    <message>
        <source>First dart point:</source>
        <translation>Premier point de la pince :</translation>
    </message>
    <message>
        <source>Second dart point:</source>
        <translation>Deuxième point de la pince :</translation>
    </message>
    <message>
        <source>Third dart point:</source>
        <translation>Troisième point de la pince :</translation>
    </message>
    <message>
        <source>First new dart point:</source>
        <translation>Premier nouveau point de la pince :</translation>
    </message>
    <message>
        <source>Unique label</source>
        <translation>Nom unique</translation>
    </message>
    <message>
        <source>Choose unique label.</source>
        <translation>Choisissez un nom unique.</translation>
    </message>
    <message>
        <source>Second new dart point:</source>
        <translation>Second nouveau point de la pince :</translation>
    </message>
    <message>
        <source>Tool</source>
        <translation>Outil</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
<context>
    <name>DialogUndo</name>
    <message>
        <source>Broken formula</source>
        <translation>Formule incorecte</translation>
    </message>
    <message>
        <source>Error while calculation formula. You can try undo last operation or fix broken formula.</source>
        <translation type="vanished">Erreur lors du calcul de la formule. Essayez d&apos;annuler la dernière opération ou corrigez la formule.</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Fix formula</source>
        <translation>&amp;Corriger la formule</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Error while calculation formula. You can try to undo last operation or fix broken formula.</source>
        <translation>Erreur pendant le calcul. Essayer d&apos;annuler la précedente opération ou modifier la formule.</translation>
    </message>
</context>
<context>
    <name>DialogUnionDetails</name>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Do you really want union details?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voulez-vous vraiment fusionner les pièces de patron ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Union tool</source>
        <translation>Outil Fusion de pièces</translation>
    </message>
    <message>
        <source>Select a first point</source>
        <translation>Choisir un premier point</translation>
    </message>
    <message>
        <source>Workpiece should have at least two points and three objects</source>
        <translation>La pièce doit avoir au moins deux points et trois objets</translation>
    </message>
    <message>
        <source>Select a second point</source>
        <translation>Choisir un deuxième point</translation>
    </message>
    <message>
        <source>Select a unique point</source>
        <translation>Choisir point unique</translation>
    </message>
    <message>
        <source>Select a detail</source>
        <translation>Sélectionner une pièce</translation>
    </message>
    <message>
        <source>Select a point on edge</source>
        <translation>Choisir un point de bordure</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Do you really want to unite details?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voulez-vous vraiment fusionner les pièces de patron ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Retain original pieces</source>
        <translation>Mémoriser les pièces originales</translation>
    </message>
</context>
<context>
    <name>FvUpdateWindow</name>
    <message>
        <source>Software Update</source>
        <translation>Mise à jour logiciel</translation>
    </message>
    <message>
        <source>A new version of %1 is available!</source>
        <translation type="vanished">Une nouvelle version de %1 est disponible!</translation>
    </message>
    <message>
        <source>%1 %2 is now available - you have %3. Would you like to download it now?</source>
        <translation>%1 %2 sont disponibles - vous disposez de %3. Voulez-vous la télécharger?</translation>
    </message>
    <message>
        <source>Skip This Version</source>
        <translation>Passer cette version</translation>
    </message>
    <message>
        <source>Remind Me Later</source>
        <translation>Me le rappeler plus tard</translation>
    </message>
    <message>
        <source>Get Update</source>
        <translation>Obtenir la Màj</translation>
    </message>
    <message>
        <source>%1 update is available!</source>
        <translation>Mise à jour %1 disponible !</translation>
    </message>
    <message>
        <source>New %1 test version is now available. Would you like to download it now?</source>
        <translation>Un nouvelle version de test %1 est disponible. Voulez-vous la télécharger maintenant ?</translation>
    </message>
</context>
<context>
    <name>FvUpdater</name>
    <message>
        <source>Cannot open your default browser.</source>
        <translation>Impossible de lancer votre navigateur par défaut.</translation>
    </message>
    <message>
        <source>Feed download failed: %1.</source>
        <translation>Flux de téléchargement interrompu: %1.</translation>
    </message>
    <message>
        <source>Feed parsing failed: %1 %2.</source>
        <translation>Analyse du téléchargement interrompue: %1 %2.</translation>
    </message>
    <message>
        <source>No updates were found.</source>
        <translation>Votre logiciel est à jour.</translation>
    </message>
    <message>
        <source>Feed error: invalid &quot;enclosure&quot; with the download link</source>
        <translation>Erreur de téléchargement: problème d&apos;encapsulation de l&apos;URL</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">Information</translation>
    </message>
</context>
<context>
    <name>HideAllBackgroundImages</name>
    <message>
        <source>hide all background images</source>
        <translation>Cacher les images en arrière-plan</translation>
    </message>
    <message>
        <source>show all background images</source>
        <translation>Afficher les images en arrière-plan</translation>
    </message>
</context>
<context>
    <name>HideBackgroundImage</name>
    <message>
        <source>hide a background image</source>
        <translation>Cacher l&apos;image en arrière-plan</translation>
    </message>
    <message>
        <source>show a background image</source>
        <translation>Afficher l&apos;image en arrière-plan</translation>
    </message>
</context>
<context>
    <name>HoldAllBackgroundImages</name>
    <message>
        <source>hold all background images</source>
        <translation>Ancrer les images en arrière-plan</translation>
    </message>
    <message>
        <source>unhold background images</source>
        <translation>Ne pas ancrer les images en arrière-plan</translation>
    </message>
</context>
<context>
    <name>HoldBackgroundImage</name>
    <message>
        <source>hold background image</source>
        <translation>Ancrer une image en arrière plan</translation>
    </message>
    <message>
        <source>unhold background image</source>
        <translation>Ne pas ancrer l&apos;image en arrière-plan</translation>
    </message>
</context>
<context>
    <name>InternalStrings</name>
    <message>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>Ce programme est distribué dans l&apos;espoir qu&apos;il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIALITÉ ou DE CONFORMITÉ A UNE UTILISATION PARTICULIÈRE.</translation>
    </message>
</context>
<context>
    <name>MApplication</name>
    <message>
        <source>Error parsing file. Program will be terminated.</source>
        <translation>Erreur d&apos;interprétation du fichier. Fin du programme.</translation>
    </message>
    <message>
        <source>Error bad id. Program will be terminated.</source>
        <translation>Erreur d&apos;identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Error can&apos;t convert value. Program will be terminated.</source>
        <translation>Erreur : valeur non convertissable. Fin du programme.</translation>
    </message>
    <message>
        <source>Error empty parameter. Program will be terminated.</source>
        <translation>Erreur : paramètre vide. Fin du programme.</translation>
    </message>
    <message>
        <source>Error wrong id. Program will be terminated.</source>
        <translation>Erreur : mauvais identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Something&apos;s wrong!!</source>
        <translation>Quelque chose ne va pas!!</translation>
    </message>
    <message>
        <source>Parser error: %1. Program will be terminated.</source>
        <translation>Erreur dans l&apos;interprétation: le programme %1 va quitter.</translation>
    </message>
    <message>
        <source>Exception thrown: %1. Program will be terminated.</source>
        <translation>Erreur d&apos;exception : %1. Fin du programme.</translation>
    </message>
    <message>
        <source>Valentina&apos;s measurements editor.</source>
        <translation>L&apos;éditeur de mesures Valentina.</translation>
    </message>
    <message>
        <source>The measurement file.</source>
        <translation>Le fichier de mesures.</translation>
    </message>
    <message>
        <source>The base height</source>
        <translation type="vanished">La hauteur de base</translation>
    </message>
    <message>
        <source>The base size</source>
        <translation type="vanished">La taille de base</translation>
    </message>
    <message>
        <source>Set pattern file unit: cm, mm, inch.</source>
        <translation type="vanished">Choisir l&apos;unité du patron: cm, mm, pouces.</translation>
    </message>
    <message>
        <source>The pattern unit</source>
        <translation type="vanished">L&apos;unité du patron</translation>
    </message>
    <message>
        <source>Use for unit testing. Run the program and open a file without showing a window.</source>
        <translation type="vanished">À utiliser pour tests. Exécutez le programme et ouvrir un fichier sans fenêtre.</translation>
    </message>
    <message>
        <source>Invalid base size argument. Must be cm, mm or inch.</source>
        <translation>Unité de travail invalide: doit être cm, mm ou pouces.</translation>
    </message>
    <message>
        <source>Can&apos;t begin to listen for incoming connections on name &apos;%1&apos;</source>
        <translation>Ne peut être à l&apos;écoute des connexions entrantes de %1</translation>
    </message>
    <message>
        <source>Test mode doesn&apos;t support openning several files.</source>
        <translation type="vanished">Le mode test ne supporte pas l&apos;ouverture de plusieurs fichiers.</translation>
    </message>
    <message>
        <source>Please, provide one input file.</source>
        <translation>Merci de choisir un fichier.</translation>
    </message>
    <message>
        <source>Open with the base size. Valid values: %1cm.</source>
        <translation type="vanished">Ouverture avec la taille de base. Valeur attendue: %1cm.</translation>
    </message>
    <message>
        <source>Invalid base height argument. Must be %1cm.</source>
        <translation type="vanished">Stature de base incorrecte. Doit être: %1cm.</translation>
    </message>
    <message>
        <source>Invalid base size argument. Must be %1cm.</source>
        <translation type="vanished">Taille de base incorrecte. Doit être: %1cm.</translation>
    </message>
    <message>
        <source>Open with the base height. Valid values: %1cm.</source>
        <translation type="vanished">Ouverture avec la stature de base. Valeur attendue: %1cm.</translation>
    </message>
    <message>
        <source>Use for unit testing. Run the program and open a file without showing the main window.</source>
        <translation>À utiliser pour tests. Exécutez le programme et ouvrir un fichier sans le voir dans la fenêtre principale.</translation>
    </message>
    <message>
        <source>Disable high dpi scaling. Call this option if has problem with scaling (by default scaling enabled). Alternatively you can use the %1 environment variable.</source>
        <translation>Désactiver la mise à l&apos;échelle haute précision. Choisissez cette option si vous avez un problème avec la mise à l&apos;échelle (activée par défaut). Sinon vous pouvez aussi utiliser la variable d’environnement %1.</translation>
    </message>
    <message>
        <source>Set base for dimension A in the table units.</source>
        <translation>Définir la valeur de base pour la dimension A dans la table des unités.</translation>
    </message>
    <message>
        <source>The dimension A base</source>
        <translation>Valeur de base pour la dimension A</translation>
    </message>
    <message>
        <source>Set base for dimension B in the table units.</source>
        <translation>Définir la valeur de base pour la dimension B dans la table des unités.</translation>
    </message>
    <message>
        <source>The dimension B base</source>
        <translation>Valeur de base pour la dimension B</translation>
    </message>
    <message>
        <source>Set base for dimension C in the table units.</source>
        <translation>Définir la valeur de base pour la dimension C dans la table des unités.</translation>
    </message>
    <message>
        <source>The dimension C base</source>
        <translation>Valeur de base pour la dimension C</translation>
    </message>
    <message>
        <source>Set pattern file units: cm, mm, inch.</source>
        <translation>Définir les unités du plan de coupe : cm, mm, inch.</translation>
    </message>
    <message>
        <source>The pattern units</source>
        <translation>Unités du patron</translation>
    </message>
    <message>
        <source>Invalid dimension A base value.</source>
        <translation>Dimension invalide de base A.</translation>
    </message>
    <message>
        <source>Invalid dimension B base value.</source>
        <translation>Dimension invalide de base B.</translation>
    </message>
    <message>
        <source>Invalid dimension C base value.</source>
        <translation>Dimension invalide de base C.</translation>
    </message>
    <message>
        <source>Formula warning: %1. Program will be terminated.</source>
        <translation>Problème sur la formule : %1. Le programme va s&apos;arrêter.</translation>
    </message>
    <message>
        <source>Test mode doesn&apos;t support opening several files.</source>
        <translation>Le mode de test ne supporte pas l&apos;ouverture de plusieurs fichiers.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Valentina</source>
        <translation>Valentina</translation>
    </message>
    <message>
        <source>Tools for creating points.</source>
        <translation>Outils de création de points.</translation>
    </message>
    <message>
        <source>Point</source>
        <translation>Point</translation>
    </message>
    <message>
        <source>Point along perpendicular</source>
        <translation>Point sur perpendiculaire</translation>
    </message>
    <message>
        <source>Perpendicular point along line</source>
        <translation>Point perpendiculaire sur axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Point along bisector</source>
        <translation>Point sur bissectrice</translation>
    </message>
    <message>
        <source>Point at distance and angle</source>
        <translation>Point à distance et angle</translation>
    </message>
    <message>
        <source>Point at distance along line</source>
        <translation>Point à distance dans l&apos;axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Tools for creating lines.</source>
        <translation>Outils de création de lignes.</translation>
    </message>
    <message>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <source>Line between points</source>
        <translation>Ligne entre 2 points</translation>
    </message>
    <message>
        <source>Point at line intersection</source>
        <translation>Point à l&apos;intersection de 2 lignes</translation>
    </message>
    <message>
        <source>Tools for creating curves.</source>
        <translation>Outils de création de courbes.</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <source>Tools for creating arcs.</source>
        <translation>Outils arcs.</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>Tools for creating details.</source>
        <translation>Outils d&apos;extraction de pièces de patron.</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation>Pièce de patron</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Aid&amp;e</translation>
    </message>
    <message>
        <source>&amp;Pattern piece</source>
        <translation>Élément de &amp;patron</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation>Mesures</translation>
    </message>
    <message>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <source>Toolbar files</source>
        <translation>Barre d&apos;outil Fichier</translation>
    </message>
    <message>
        <source>ToolBar modes</source>
        <translation>Barre d&apos;outils Modes</translation>
    </message>
    <message>
        <source>Toolbar pattern</source>
        <translation>Barre d&apos;outil Elément de patron</translation>
    </message>
    <message>
        <source>Toolbar options</source>
        <translation>Barre d&apos;outils Options</translation>
    </message>
    <message>
        <source>Toolbar tools</source>
        <translation>Barre d&apos;outils Outils</translation>
    </message>
    <message>
        <source>Tool options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>Create a new pattern</source>
        <translation>Créer un nouveau patron</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>Open file with pattern</source>
        <translation>Ouvrir un fichier de patron</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Save pattern</source>
        <translation>Enregistrer le patron</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation type="vanished">Enregistrer &amp;sous ...</translation>
    </message>
    <message>
        <source>Save not yet saved pattern</source>
        <translation>Enregistrer le patron non sauvegardé</translation>
    </message>
    <message>
        <source>Draw</source>
        <translation>Traçage</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Pièces</translation>
    </message>
    <message>
        <source>Pointer tools</source>
        <translation>Pointeur</translation>
    </message>
    <message>
        <source>New pattern piece</source>
        <translation>Nouveau élément de patron</translation>
    </message>
    <message>
        <source>Add new pattern piece</source>
        <translation>Ajouter un nouveau élément de patron</translation>
    </message>
    <message>
        <source>Change the label of pattern piece</source>
        <translation>Renommer l&apos;élément de patron</translation>
    </message>
    <message>
        <source>Table of variables</source>
        <translation type="vanished">Table des variables</translation>
    </message>
    <message>
        <source>Tables of variables</source>
        <translation type="vanished">Tables des variables</translation>
    </message>
    <message>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <source>&amp;About Valentina</source>
        <translation>À propos de &amp;Valentina</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Pattern properties</source>
        <translation>Propriétés du patron</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <source>zoom in</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <source>Edit pattern XML code</source>
        <translation type="vanished">Editer le code XML du patron</translation>
    </message>
    <message>
        <source>Original zoom </source>
        <translation type="vanished">Zoom par défaut</translation>
    </message>
    <message>
        <source>Original Zoom </source>
        <translation type="vanished">Zoom par défaut</translation>
    </message>
    <message>
        <source>Zoom fit best</source>
        <translation>Zoom optimal</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stop</translation>
    </message>
    <message>
        <source>Stop using tool</source>
        <translation type="vanished">Arrêter d&apos;utiliser cet outil</translation>
    </message>
    <message>
        <source>Repot Bug...</source>
        <translation type="vanished">Rapporter un bug ...</translation>
    </message>
    <message>
        <source>Report bug</source>
        <translation>Rapporter un bug</translation>
    </message>
    <message>
        <source>Close window</source>
        <translation type="vanished">Fermer la fenêtre</translation>
    </message>
    <message>
        <source>Online help</source>
        <translation type="vanished">Aide en ligne</translation>
    </message>
    <message>
        <source>Show online help</source>
        <translation type="vanished">Afficher l&apos;aide en ligne</translation>
    </message>
    <message>
        <source>Pattern piece %1</source>
        <translation>Élément de patron %1</translation>
    </message>
    <message>
        <source>Select point</source>
        <translation>Sélectionner un point</translation>
    </message>
    <message>
        <source>Select first point</source>
        <translation>Sélectionner le premier point</translation>
    </message>
    <message>
        <source>Select first point of line</source>
        <translation>Sélectionner le premier point de la ligne</translation>
    </message>
    <message>
        <source>Select first point of angle</source>
        <translation>Sélectionner le premier point de l&apos;angle</translation>
    </message>
    <message>
        <source>Select first point of first line</source>
        <translation>Sélectionner le premier point de la première ligne</translation>
    </message>
    <message>
        <source>Select first point curve</source>
        <translation>Sélectionner le premier point de la courbe</translation>
    </message>
    <message>
        <source>Select simple curve</source>
        <translation>Sélectionner la courbe simple</translation>
    </message>
    <message>
        <source>Select point of center of arc</source>
        <translation>Sélectionner le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Select point of curve path</source>
        <translation>Sélectionner un point sur la courbe complexe</translation>
    </message>
    <message>
        <source>Select curve path</source>
        <translation>Sélectionner la courbe complexe</translation>
    </message>
    <message>
        <source>Select points, arcs, curves clockwise.</source>
        <translation type="vanished">Sélectionner les points, arcs et courbes, dans le sens horaire.</translation>
    </message>
    <message>
        <source>Select base point</source>
        <translation>Sélectionner le point de départ</translation>
    </message>
    <message>
        <source>Select first point of axis</source>
        <translation>Sélectionner le premier point de l&apos;axe</translation>
    </message>
    <message>
        <source>Select detail</source>
        <translation>Sélectionner la pièce de patron</translation>
    </message>
    <message>
        <source>Select arc</source>
        <translation>Sélectionner un arc</translation>
    </message>
    <message>
        <source>Select curve</source>
        <translation>Sélectionner la courbe</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <source>Height: </source>
        <translation type="vanished">Stature :</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="vanished">Taille :</translation>
    </message>
    <message>
        <source>Pattern Piece: </source>
        <translation type="vanished">Élément de patron :</translation>
    </message>
    <message>
        <source>Pattern files (*.val)</source>
        <translation type="vanished">Fichiers de patron (*.val)</translation>
    </message>
    <message>
        <source>pattern</source>
        <translation>patron</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <source>Could not save file</source>
        <translation>Impossible d&apos;enregistrer le patron</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <source>Error parsing file.</source>
        <translation>Erreur : impossible d&apos;analyser le fichier.</translation>
    </message>
    <message>
        <source>Error can&apos;t convert value.</source>
        <translation>Erreur : impossible de convertir la valeur.</translation>
    </message>
    <message>
        <source>Error empty parameter.</source>
        <translation>Erreur : paramètre vide.</translation>
    </message>
    <message>
        <source>Error wrong id.</source>
        <translation>Erreur : id non valide.</translation>
    </message>
    <message>
        <source>Error parsing file (std::bad_alloc).</source>
        <translation>Erreur : impossible d&apos;analyser le fichier (std::bad_alloc).</translation>
    </message>
    <message>
        <source>Bad id.</source>
        <translation>Id erroné.</translation>
    </message>
    <message>
        <source>File saved</source>
        <translation>Fichier sauvegardé</translation>
    </message>
    <message>
        <source>untitled.val</source>
        <translation>sanstitre.val</translation>
    </message>
    <message>
        <source>The pattern has been modified.
Do you want to save your changes?</source>
        <translation>Le patron a été modifié.
Voulez-vous sauvegarder vos modifications ?</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Rétablir</translation>
    </message>
    <message>
        <source>Pattern piece:</source>
        <translation>Élément de patron :</translation>
    </message>
    <message>
        <source>Enter a new label for the pattern piece.</source>
        <translation>Saisir un nouveau nom pour cet élément de patron.</translation>
    </message>
    <message>
        <source>This file already opened in another window.</source>
        <translation type="vanished">Ce fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>Wrong units.</source>
        <translation type="vanished">Unités de mesure erronées.</translation>
    </message>
    <message>
        <source>Application doesn&apos;t support standard table with inches.</source>
        <translation type="vanished">L&apos;application ne prend pas en charge la table de mesures standard en pouces.</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>File loaded</source>
        <translation type="vanished">Fichier chargé</translation>
    </message>
    <message>
        <source>Valentina didn&apos;t shut down correctly. Do you want reopen files (%1) you had open?</source>
        <translation>Valentina ne s&apos;est pas fermé correctement. Voulez vous ré-ouvrir le fichier %1 que vous aviez ouvert?</translation>
    </message>
    <message>
        <source>Reopen files.</source>
        <translation>Réouvrir les fichiers.</translation>
    </message>
    <message>
        <source>Standard measurements (*.vst)</source>
        <translation type="vanished">Mesures multi-tailles (*.vst)</translation>
    </message>
    <message>
        <source>Individual measurements (*.vit)</source>
        <translation type="vanished">Mesures individuelles (*.vit)</translation>
    </message>
    <message>
        <source>Special point on shoulder</source>
        <translation>Point épaule</translation>
    </message>
    <message>
        <source>Triangle tool</source>
        <translation>Outil Triangle</translation>
    </message>
    <message>
        <source>Point at intersection of arc and line</source>
        <translation type="vanished">Point à l&apos;intersection d&apos;un arc et d&apos;une ligne</translation>
    </message>
    <message>
        <source>Point from X and Y of two other points</source>
        <translation>Point à l&apos;X d&apos;un point et l&apos;Y d&apos;un autre</translation>
    </message>
    <message>
        <source>Point intersect line and axis</source>
        <translation>Point à l&apos;intersection d&apos;une ligne et d&apos;un axe</translation>
    </message>
    <message>
        <source>Simple curve</source>
        <translation>Courbe cubique simple</translation>
    </message>
    <message>
        <source>Curved path</source>
        <translation>Courbe cubique complexe</translation>
    </message>
    <message>
        <source>Segmenting a simple curve</source>
        <translation>Segmenter une courbe simple</translation>
    </message>
    <message>
        <source>Segment a curved path</source>
        <translation>Segmenter une courbe complexe</translation>
    </message>
    <message>
        <source>Point intersect curve and axis</source>
        <translation>Point d&apos;intersection d&apos;une courbe et d&apos;un axe</translation>
    </message>
    <message>
        <source>Segment an arc</source>
        <translation>Segmenter un arc</translation>
    </message>
    <message>
        <source>Point intersect arc and axis</source>
        <translation type="vanished">Point d&apos;&apos;intersection d&apos;un arc et d&apos;un axe</translation>
    </message>
    <message>
        <source>Seam allowance tool</source>
        <translation type="vanished">Outil marge de couture</translation>
    </message>
    <message>
        <source>Union tool</source>
        <translation>Outil Fusion de pièces</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="vanished">Barre d&apos;outils</translation>
    </message>
    <message>
        <source>Last Tool</source>
        <translation>Précédent outil</translation>
    </message>
    <message>
        <source>Activate last used tool again</source>
        <translation>Réactiver l&apos;outil précédent</translation>
    </message>
    <message>
        <source>Select point for X value (vertical)</source>
        <translation>Sélectionner le point de l&apos;axe X (vertical)</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <source>Pointer</source>
        <translation>Pointeur</translation>
    </message>
    <message>
        <source>Config pattern piece</source>
        <translation>Configurer la pièce de patron</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Plan de coupe</translation>
    </message>
    <message>
        <source>Show Curve Details</source>
        <translation>Montrer les détails de la courbe</translation>
    </message>
    <message>
        <source>Show/hide control points and curve direction</source>
        <translation>Montrer/cacher les points de contrôle et de direction de la courbe</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Outils</translation>
    </message>
    <message>
        <source>Point of intersection arcs</source>
        <translation>Point à l&apos;intersection de 2 arcs</translation>
    </message>
    <message>
        <source>Point of intersection circles</source>
        <translation>Point d&apos;intersection de 2 cercles</translation>
    </message>
    <message>
        <source>Point from circle and tangent</source>
        <translation>Point à l&apos;intersection d&apos;un cercle et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Point from arc and tangent</source>
        <translation>Point à l&apos;intersection d&apos;un arc et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Arc with given length</source>
        <translation>Arc de longueur donnée</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Layout pages</source>
        <translation>Pages de plan de coupe</translation>
    </message>
    <message>
        <source>Save as PDF</source>
        <translation type="vanished">Enregistrer au format PDF</translation>
    </message>
    <message>
        <source>Save original layout</source>
        <translation type="vanished">Enregistrer le plan de coupe original</translation>
    </message>
    <message>
        <source>Save as tiled PDF</source>
        <translation type="vanished">Enregistrer au format PDF empilé</translation>
    </message>
    <message>
        <source>Split and save a layout into smaller pages</source>
        <translation type="vanished">Découpe et sauvegarde le plan de coupe en plus petites pages</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <source>Print tiled PDF</source>
        <translation>Imprimer PDF empilé</translation>
    </message>
    <message>
        <source>Split and print a layout into smaller pages (for regular printers)</source>
        <translation>Découpe et imprime le plan de coupe en pages plus petites (imprimantes standard)</translation>
    </message>
    <message>
        <source>Print preview</source>
        <translation>Aperçu avant impression</translation>
    </message>
    <message>
        <source>Print preview original layout</source>
        <translation>Imprimer aperçu avant impression</translation>
    </message>
    <message>
        <source>Export As...</source>
        <translation type="vanished">Exporter sous ...</translation>
    </message>
    <message>
        <source>Export original layout</source>
        <translation>Exporter le plan de coupe original</translation>
    </message>
    <message>
        <source>Select first an arc</source>
        <translation>Sélectionner d&apos;abord un arc</translation>
    </message>
    <message>
        <source>Select first circle center </source>
        <translation type="vanished">Sélectionner d&apos;abord le centre du premier cercle</translation>
    </message>
    <message>
        <source>Select point on tangent </source>
        <translation type="vanished">Sélectionner un point sur la tangente</translation>
    </message>
    <message>
        <source>Select point of the center of the arc</source>
        <translation>Sélectionner le point central de l&apos;arc</translation>
    </message>
    <message>
        <source>Select the first base line point</source>
        <translation>Sélectionner le premier point de la ligne de base</translation>
    </message>
    <message>
        <source>Detail mode</source>
        <translation>Mode Pièces</translation>
    </message>
    <message>
        <source>You can&apos;t use now the Detail mode. Please, create at least one workpiece.</source>
        <translation type="vanished">Vous ne pouvez pas utiliser le mode Pièces à ce stade. Créez SVP au moins une pièce de patron en utilisant l&apos;outil &apos;Extraction du contour principal&apos; dans &apos;Pièce de patron&apos;</translation>
    </message>
    <message>
        <source>Layout mode</source>
        <translation>Mode Plan de coupe</translation>
    </message>
    <message>
        <source>You can&apos;t use now the Layout mode. Please, create at least one workpiece.</source>
        <translation type="vanished">Vous ne pouvez pas utiliser le mode Plan de coupe à ce stade. Créez SVP au moins une pièce de patron en utilisant l&apos;outil &apos;Extraction du contour principal&apos; dans &apos;Pièce de patron&apos;</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>Load Individual ...</source>
        <translation type="vanished">Charger mensurations individuelles ...</translation>
    </message>
    <message>
        <source>Load Standard ...</source>
        <translation type="vanished">Charger mensurations multi-tailles ...</translation>
    </message>
    <message>
        <source>Show ...</source>
        <translation type="vanished">Afficher ...</translation>
    </message>
    <message>
        <source>Show measurements</source>
        <translation type="vanished">Afficher les mesures</translation>
    </message>
    <message>
        <source>Sync measurements</source>
        <translation type="vanished">Synchroniser les mesures</translation>
    </message>
    <message>
        <source>Individual measurements (*.vit);;Standard measurements (*.vst)</source>
        <translation type="vanished">Mensurations individuelles (*.vit);;Mensurations multi-tailles (*.vst)</translation>
    </message>
    <message>
        <source>Measurements loaded</source>
        <translation>Mensurations chargées</translation>
    </message>
    <message>
        <source>Standard measurements (*.vst);;Individual measurements (*.vit)</source>
        <translation type="vanished">Mensurations multi-tailles (*.vst);;Mensurations individuelles (*.vit)</translation>
    </message>
    <message>
        <source>You can&apos;t export empty scene.</source>
        <translation>Impossible d&apos;exporter, l&apos;espace de travail est vide.</translation>
    </message>
    <message>
        <source>Create new Layout</source>
        <translation>Créer un nouveau plan de coupe</translation>
    </message>
    <message>
        <source>Create/Edit</source>
        <translation type="vanished">Créer/éditer</translation>
    </message>
    <message>
        <source>Create/edit measurements</source>
        <translation type="vanished">Créer/éditer les mensurations</translation>
    </message>
    <message>
        <source>%1, %2 (%3)</source>
        <extracomment>Coords in status line: &quot;X, Y (units)&quot;</extracomment>
        <translation type="vanished">%1, %2 (%3)</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window.</source>
        <translation>Verrouillage impossible. Le fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window. Expect collissions when run 2 copies of the program.</source>
        <translation>Verrouillage impossible car le fichier est déjà ouvert dans une autre fenêtre. Ceci se produit généralement quand 2 copies du programme sont ouvertes en même temps.</translation>
    </message>
    <message>
        <source>Measurement file contains invalid known measurement(s).</source>
        <translation>Le fichier contient une ou des mensurations invalides (de type &apos;connue&apos;).</translation>
    </message>
    <message>
        <source>Measurement file has unknown format.</source>
        <translation>La table de mensurations est de format inconnu.</translation>
    </message>
    <message>
        <source>Measurement file doesn&apos;t include all required measurements.</source>
        <translation type="vanished">Des mensurations requises manquent dans la table de mesures que vous voulez charger.</translation>
    </message>
    <message>
        <source>Please, additionaly provide: %1</source>
        <translation type="vanished">Merci de spécifier aussi: %1</translation>
    </message>
    <message>
        <source>Measurement files types have not match.</source>
        <translation>Les tableaux de mensurations n&apos;ont pas de correspondance.</translation>
    </message>
    <message>
        <source>Measurements was synced</source>
        <translation type="vanished">Mensurations synchronisées</translation>
    </message>
    <message>
        <source>Couldn&apos;t sync measurements.</source>
        <translation>Impossible de synchroniser les mensurations.</translation>
    </message>
    <message>
        <source>Couldn&apos;t update measurements.</source>
        <translation>Impossible de mettre à jour les mensurations.</translation>
    </message>
    <message>
        <source>The measurements file &apos;%1&apos; could not be found.</source>
        <translation>La table de mensurations &apos;%1&apos; est introuvable.</translation>
    </message>
    <message>
        <source>The measurements file &lt;br/&gt;&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; &lt;br/&gt;&lt;br/&gt; could not be found. Do you want to update the file location</source>
        <translation type="vanished">La table de mensurations &lt;br/&gt;&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; &lt;br/&gt;&lt;br/&gt;est introuvable. Voulez-vous la rechercher depuis un autre emplacement ?</translation>
    </message>
    <message>
        <source>Loading measurements file</source>
        <translation>Chargement de la table de mensurations</translation>
    </message>
    <message>
        <source>Not supported size value &apos;%1&apos; for this pattern file.</source>
        <translation type="vanished">Valeur de taille &apos;%1&apos; non prise en charge pour ce patron.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set size. Need a file with standard measurements.</source>
        <translation type="vanished">Impossible de définir une autre taille. Un tableau de mensurations multi-tailles est nécessaire.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set size. File wasn&apos;t opened.</source>
        <translation type="vanished">Impossible de définir une autre taille. Le fichier n&apos;a pas été ouvert.</translation>
    </message>
    <message>
        <source>The method %1 does nothing in GUI mode</source>
        <translation>La méthode %1 n&apos;a aucun effet en mode d&apos;interface</translation>
    </message>
    <message>
        <source>Not supported height value &apos;%1&apos; for this pattern file.</source>
        <translation type="vanished">Valeur de stature &apos;%1&apos; non prise en charge pour ce patron.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set height. Need a file with standard measurements.</source>
        <translation type="vanished">Impossible de définir une autre stature. Un tableau de mensurations multi-tailles est nécessaire.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set height. File wasn&apos;t opened.</source>
        <translation type="vanished">Impossible de définir une autre stature. Le fichier n&apos;a pas été ouvert.</translation>
    </message>
    <message>
        <source>Export error.</source>
        <translation>Erreur d&apos;exportation.</translation>
    </message>
    <message>
        <source>Please, provide one input file.</source>
        <translation>Merci de choisir un fichier.</translation>
    </message>
    <message>
        <source>Print an original layout</source>
        <translation>Imprimer le plan de coupe original</translation>
    </message>
    <message>
        <source>Preview tiled PDF</source>
        <translation>Aperçu avant impression du PDF empilé</translation>
    </message>
    <message>
        <source>Print preview tiled layout</source>
        <translation>Imprimer aperçu avant impression du PDF empilé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mode for working with pattern pieces. These pattern pieces are base for going to the next stage &amp;quot;Details mode&amp;quot;. Before you will be able to enable the &amp;quot;Details mode&amp;quot; need create at least one detail.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Phase de traçage et construction des éléments de patron. Ces éléments constituent l&apos;étape préalable pour passer à la suite : l&apos;extraction et la mise en forme des pièces en &amp;quot;mode Pièces&amp;quot;. Avant de pouvoir passer en &amp;quot;mode Pièces&amp;quot;, vous devrez avoir extrait au moins un contour principal de pièce depuis Pièce de patron.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mode for working with details. Before you will be able to enable the &amp;quot;Details mode&amp;quot; need create at least one detail on the stage &amp;quot;Draw mode&amp;quot;. Details created on this stage will be used for creating a layout. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Phase de mise en forme des pièces du patron. Avant de pouvoir passer en &amp;quot;mode Pièces&amp;quot;, vous devrez avoir extrait au moins un contour principal depuis Pièce de patron en &amp;quot;mode Traçage&amp;quot;. Les pièces mises en forme à cette étape seront ensuite intégrables au Plan de coupe. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mode for creating a layout of details. This mode avaliable if was created at least one detail on the stage &amp;quot;Details mode&amp;quot;. The layout can be exported to your prefered file format and saved to your harddirve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Phase de création du plan de coupe. Cette étape n&apos;est accessible que si vous avez créé au moins une pièce de patron en &amp;quot;mode Pièces&amp;quot;. Le plan de coupe peut ensuite être exporté dans le format de votre choix et sauvegardé dans votre système.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unload measurements</source>
        <translation type="vanished">Annuler le chargement des mensurations</translation>
    </message>
    <message>
        <source>Unload measurements if they was not used in a pattern file.</source>
        <translation type="vanished">Annuler le chargement des mensurations si elles ne sont pas utilisées dans le patron actuel.</translation>
    </message>
    <message>
        <source>Measurements unloaded</source>
        <translation>Annulation du chargement des mensurations</translation>
    </message>
    <message>
        <source>Couldn&apos;t unload measurements. Some of them are used in the pattern.</source>
        <translation>Impossible d&apos;annuler le chargement des mensurations. Certaines d&apos;entre elles sont utilisées dans le patron actuel.</translation>
    </message>
    <message>
        <source>True darts</source>
        <translation>Pince automatique</translation>
    </message>
    <message>
        <source>New pattern</source>
        <translation>Nouveau patron</translation>
    </message>
    <message>
        <source>Open pattern</source>
        <translation>Ouvrir le patron</translation>
    </message>
    <message>
        <source>Create/Edit measurements</source>
        <translation>Créer/éditer une table de mensurations</translation>
    </message>
    <message>
        <source>Save...</source>
        <translation type="vanished">Enregistrer ...</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>Ne pas sauvegarder</translation>
    </message>
    <message>
        <source>Locking file</source>
        <translation type="vanished">Verrouiller le fichier (lecture seule)</translation>
    </message>
    <message>
        <source>This file already opened in another window. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Ce fichier est déjà ouvert dans une autre fenêtre. Ignorer pour continuer quand même (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Le fichier sous lecture seule ne peut être créé car vous n&apos;avez pas les permissions nécessaires. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Une erreur inconnue s&apos;est produite, par exemple pour cause de partition pleine. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions.</source>
        <translation type="vanished">Impossible de créer le fichier en lecture seule, vous n&apos;avez pas les permissions nécessaires.</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file.</source>
        <translation type="vanished">Une erreur inconnue s&apos;est produite, il est possible qu&apos;une partition pleine empêche d&apos;écrire le fichier en lecture seule.</translation>
    </message>
    <message>
        <source>Report Bug...</source>
        <translation type="vanished">Rapport de bug ...</translation>
    </message>
    <message>
        <source>Point intersection curves</source>
        <translation>Point d&apos;intersection de 2 courbes</translation>
    </message>
    <message>
        <source>Select first curve</source>
        <translation>Sélectionner la première courbe</translation>
    </message>
    <message>
        <source>(read only)</source>
        <translation type="vanished">(lecture seule)</translation>
    </message>
    <message>
        <source>Measurements was changed. Do you want to sync measurements now?</source>
        <translation type="vanished">Les mensurations ont été modifiées. Voulez-vous les resynchroniser maintenant ?</translation>
    </message>
    <message>
        <source>Curve tool which uses point as control handle</source>
        <translation>Courbe simple utilisant les points comme poignées de contrôle</translation>
    </message>
    <message>
        <source>Select first curve point</source>
        <translation>Sélectionner le premier point de la courbe</translation>
    </message>
    <message>
        <source>Select point of cubic bezier path</source>
        <translation>Sélectionner un point sur la courbe</translation>
    </message>
    <message>
        <source>Toolbar pointer</source>
        <translation type="vanished">Pointeur</translation>
    </message>
    <message>
        <source>Operations</source>
        <translation>Opérations</translation>
    </message>
    <message>
        <source>Create new group</source>
        <translation type="vanished">Créer un nouveau groupe</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation type="vanished">Groupes</translation>
    </message>
    <message>
        <source>Select one or more objects, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">Sélectionner un ou plusieurs objets, puis cliquer sur &lt;b&gt;Entrée&lt;/b&gt; pour finir</translation>
    </message>
    <message>
        <source>Rotate objects</source>
        <translation>Rotation des objets</translation>
    </message>
    <message>
        <source>Close pattern</source>
        <translation>Fermer le patron</translation>
    </message>
    <message>
        <source>Select one or more objects, &lt;b&gt;Enter&lt;/b&gt; - confirm selection</source>
        <translation type="vanished">Sélectionner un ou plusieurs objets, puis cliquer sur &lt;b&gt;Entrée&lt;/b&gt; pour confirmer la sélection</translation>
    </message>
    <message>
        <source>Tool pointer</source>
        <translation>Pointeur</translation>
    </message>
    <message>
        <source>Midpoint between two points</source>
        <translation>Point à mi-distance entre deux points</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Contains all visibility groups</source>
        <translation>Contient tous les groupes de visibilité</translation>
    </message>
    <message>
        <source>Show which details will go in layout</source>
        <translation>Pièces de patrons qui seront intégrées au plan de coupe</translation>
    </message>
    <message>
        <source>You can&apos;t use now the Layout mode. Please, include at least one detail in layout.</source>
        <translation type="vanished">Vous ne pouvez pas utiliser le mode Plan de coupe à ce stade. Créez SVP au moins une pièce de patron en utilisant l&apos;outil &apos;Extraction du contour principal&apos; dans &apos;Pièce de patron&apos;</translation>
    </message>
    <message>
        <source>Original zoom</source>
        <translation>Zoom par défaut</translation>
    </message>
    <message>
        <source>Select first circle center</source>
        <translation>Sélectionner le point central du premier cercle</translation>
    </message>
    <message>
        <source>Select point on tangent</source>
        <translation>Sélectionner un point sur la tangente</translation>
    </message>
    <message>
        <source>Pattern Piece:</source>
        <translation>Élément de patron :</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation type="vanished">Stature:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="vanished">Taille :</translation>
    </message>
    <message>
        <source>The measurements file &lt;br/&gt;&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; &lt;br/&gt;&lt;br/&gt; could not be found. Do you want to update the file location?</source>
        <translation>La table de mensurations &lt;br/&gt;&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; &lt;br/&gt;&lt;br/&gt;est introuvable. Voulez-vous la rechercher depuis un autre emplacement ?</translation>
    </message>
    <message>
        <source>Flipping objects by line</source>
        <translation>Miroir des objets par rapport à une ligne</translation>
    </message>
    <message>
        <source>Flipping objects by axis</source>
        <translation>Miroir des objets par rapport à un axe</translation>
    </message>
    <message>
        <source>Move objects</source>
        <translation>Déplacer des objets</translation>
    </message>
    <message>
        <source>Measurements were changed. Do you want to sync measurements now?</source>
        <translation>Les mensurations ont été modifiées. Voulez-vous les resynchroniser maintenant ?</translation>
    </message>
    <message>
        <source>Gradation doesn&apos;t support inches</source>
        <translation type="vanished">Gradation en pouces non prise en charge</translation>
    </message>
    <message>
        <source>Measurements have been synced</source>
        <translation>Mensurations synchronisées</translation>
    </message>
    <message>
        <source>Tools for creating elliptical arcs.</source>
        <translation>Outils de création d&apos;arcs elliptiques.</translation>
    </message>
    <message>
        <source>Elliptical Arc</source>
        <translation>Arc elliptique</translation>
    </message>
    <message>
        <source>Select point of center of elliptical arc</source>
        <translation>Sélectionner le point central de l&apos;arc elliptique</translation>
    </message>
    <message>
        <source>Select main path objects clockwise.</source>
        <translation>Sélectionnez les objets du contour principal dans le sens horaire.</translation>
    </message>
    <message>
        <source>Could not save the file</source>
        <translation>Le fichier n&apos;a pas pu etre enregistré</translation>
    </message>
    <message>
        <source>read only</source>
        <translation>Lecture seule</translation>
    </message>
    <message>
        <source>Variables Table</source>
        <translation>Table des variables</translation>
    </message>
    <message>
        <source>Contains information about increments and internal variables</source>
        <translation>Contient les données des incréments et des variables internes au programme</translation>
    </message>
    <message>
        <source>Load Individual</source>
        <translation>Charger mensurations individuelles</translation>
    </message>
    <message>
        <source>Load Individual measurements file</source>
        <translation>Charger le fichier de mensurations individuelles</translation>
    </message>
    <message>
        <source>Load Multisize</source>
        <translation>Charger mensurations multi-tailles</translation>
    </message>
    <message>
        <source>Load multisize measurements file</source>
        <translation>Charger le fichier de mensurations multi-tailles</translation>
    </message>
    <message>
        <source>Open Tape</source>
        <translation>Ouvrir Tape</translation>
    </message>
    <message>
        <source>Edit Current</source>
        <translation>Editer les mesures en cours d&apos;utilisation</translation>
    </message>
    <message>
        <source>Edit linked to the pattern measurements</source>
        <translation>Editer les mesures liées au patron</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <source>Synchronize linked to the pattern measurements after change</source>
        <translation>Synchroniser les mesures liées au patron après un changement</translation>
    </message>
    <message>
        <source>Unload Current</source>
        <translation>Annuler le chargement du fichier de mensurations en cours d&apos;utilisation</translation>
    </message>
    <message>
        <source>Unload measurements if they were not used in a pattern file</source>
        <translation>Annuler le chargement des mensurations si elles ne sont pas utilisées dans le patron actuel</translation>
    </message>
    <message>
        <source>Individual measurements</source>
        <translation>Mensurations individuelles</translation>
    </message>
    <message>
        <source>Multisize measurements</source>
        <translation>Mensurations multi-tailles</translation>
    </message>
    <message>
        <source>Pattern files</source>
        <translation>Fichiers de patron</translation>
    </message>
    <message>
        <source>Pin tool</source>
        <translation>Outil Epingle</translation>
    </message>
    <message>
        <source>Select pin point</source>
        <translation>Sélectionner le point devant servir d&apos;épingle</translation>
    </message>
    <message>
        <source>Insert node tool</source>
        <translation>Outil Insérer un noeud</translation>
    </message>
    <message>
        <source>Open Tape app for creating or editing measurements file</source>
        <translation>Ouvrir Tape pour créer ou éditer un fichier de mensurations</translation>
    </message>
    <message>
        <source>Export increments to CSV</source>
        <translation>Exporter les incréments sous format CSV</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>The calculated value</source>
        <translation>Valeur calculée</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>You can&apos;t use Detail mode yet. Please, create at least one workpiece.</source>
        <translation>Vous ne pouvez pas utiliser le mode Pièces à ce stade. Créez SVP au moins une pièce de patron en utilisant l&apos;outil &apos;Extraction du contour principal&apos; dans &apos;Pièce de patron&apos;.</translation>
    </message>
    <message>
        <source>You can&apos;t use Layout mode yet. Please, create at least one workpiece.</source>
        <translation>Vous ne pouvez pas utiliser le mode Plan de coupe à ce stade. Créez SVP au moins une pièce de patron en utilisant l&apos;outil &apos;Extraction du contour principal&apos; dans &apos;Pièce de patron&apos;.</translation>
    </message>
    <message>
        <source>You can&apos;t use Layout mode yet. Please, include at least one detail in layout.</source>
        <translation>Vous ne pouvez pas utiliser le mode Plan de coupe à ce stade. Intégrez au moins une pièce pour transfert dans le plan de coupe, en la cochant en mode Pièces.</translation>
    </message>
    <message>
        <source>You can&apos;t use Layout mode yet.</source>
        <translation>Vous ne pouvez pas utiliser le mode Plan de coupe à ce stade.</translation>
    </message>
    <message>
        <source>Zoom fit best current</source>
        <translation>Zoom optimal</translation>
    </message>
    <message>
        <source>zoom fit best current pattern piece</source>
        <translation>Zoom optimal sur élément de patron actif</translation>
    </message>
    <message>
        <source>Export details skiping the Layout stage</source>
        <translation>Exporter les pièces en passant l&apos;étape Plan de coupe</translation>
    </message>
    <message>
        <source>You don&apos;t have enough details to export. Please, include at least one detail in layout.</source>
        <translation>Vous ne pouvez pas exporter, car aucune pièce n&apos;est intégrée pour transfert dans le plan de coupe.</translation>
    </message>
    <message>
        <source>Export details</source>
        <translation>Exporter les pièces</translation>
    </message>
    <message>
        <source>Can&apos;t export details.</source>
        <translation>Impossible d&apos;exporter les pièces.</translation>
    </message>
    <message>
        <source>Label template editor</source>
        <translation>Configurer le modèle d&apos;étiquette des mentions sur les pièces</translation>
    </message>
    <message>
        <source>Workpiece tool</source>
        <translation>Extraction de contour principal</translation>
    </message>
    <message>
        <source>Internal path tool</source>
        <translation>Extraction de chemin interne</translation>
    </message>
    <message>
        <source>Save &amp;As…</source>
        <translation>Enregistrer &amp;sous …</translation>
    </message>
    <message>
        <source>Report Bug…</source>
        <translation>Rapport de bug …</translation>
    </message>
    <message>
        <source>Export As…</source>
        <translation>Exporter sous …</translation>
    </message>
    <message>
        <source>Save…</source>
        <translation>Enregistrer…</translation>
    </message>
    <message>
        <source>Final measurements</source>
        <translation>Mesures finales</translation>
    </message>
    <message>
        <source>Export Final Measurements to CSV</source>
        <translation>Exporter les mesures finales au format CSV</translation>
    </message>
    <message>
        <source>You can&apos;t export empty scene. Please, include at least one detail in layout.</source>
        <translation>Vous ne pouvez pas exporter, car aucune pièce n&apos;est intégrée pour transfert dans le plan de coupe.</translation>
    </message>
    <message>
        <source>Export final measurements error.</source>
        <translation>Erreur d&apos;exportation des mesures finales.</translation>
    </message>
    <message>
        <source>Destination path is empty.</source>
        <translation>Le chemin de destination est vide.</translation>
    </message>
    <message>
        <source>Next pattern piece</source>
        <translation>Élément de patron suivant</translation>
    </message>
    <message>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <source>Previous pattern piece</source>
        <translation>Élément de patron précédent</translation>
    </message>
    <message>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <source>Place label tool</source>
        <translation>Outil Placer un repère</translation>
    </message>
    <message>
        <source>Duplicate detail tool</source>
        <translation>Outil Duplication de pièce</translation>
    </message>
    <message>
        <source>Select placelabel center point</source>
        <translation>Sélectionner le centre du repère</translation>
    </message>
    <message>
        <source>The measurements file &apos;%1&apos; could not be found or provides not enough information.</source>
        <translation>Le fichier de mesures &apos;%1&apos; n&apos;a pas été trouvé à l&apos;emplacement spécifié ou ne contient pas toutes les informations nécessaires.</translation>
    </message>
    <message>
        <source>Use these tools to create a pattern</source>
        <translation>Utilisez ces outils pour créer un patron</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation>Echelle :</translation>
    </message>
    <message>
        <source>Create new pattern piece to start working.</source>
        <translation>Créer un nouvel élément de patron pour commencer.</translation>
    </message>
    <message>
        <source>Changes applied.</source>
        <translation>Modifications appliquées.</translation>
    </message>
    <message>
        <source>Cannot save settings. Access denied.</source>
        <translation>Impossible d&apos;enregistrer les réglages. Accès refusé.</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>Cannot read settings from a malformed .INI file.</source>
        <translation>Lecture des réglages impossible, fichier .INI invalide.</translation>
    </message>
    <message>
        <source>Select path objects, &lt;b&gt;%1&lt;/b&gt; - reverse direction curve</source>
        <translation>Selectionnez les objets du contour principal, &lt;b&gt;%1&lt;/b&gt; - inversez la direction de la courbe</translation>
    </message>
    <message>
        <source>Select one or more objects, hold &lt;b&gt;%1&lt;/b&gt; - for multiple selection, &lt;b&gt;%2&lt;/b&gt; - finish creation</source>
        <translation>Sélectionner un ou plusieurs objets en maintenant la touche &lt;b&gt;%1&lt;/b&gt; enfoncée - ou la touche &lt;b&gt;%2&lt;/b&gt; pour une sélection multiple - et terminer</translation>
    </message>
    <message>
        <source>Select one or more objects, hold &lt;b&gt;%1&lt;/b&gt; - for multiple selection, &lt;b&gt;%2&lt;/b&gt; - confirm selection</source>
        <translation>Sélectionner un ou plusieurs objets en maintenant la touche &lt;b&gt;%1&lt;/b&gt; enfoncée - ou la touche &lt;b&gt;%2&lt;/b&gt; pour une sélection multiple - et terminer</translation>
    </message>
    <message>
        <source>Point of intersection circle and segment</source>
        <translation>Point à l&apos;intersection d&apos;un cercle et d&apos;un segment</translation>
    </message>
    <message>
        <source>Increase label font</source>
        <translation>Augmenter la taille des libellés des points</translation>
    </message>
    <message>
        <source>Decrease label font</source>
        <translation>Diminuer la taille des libellés des points</translation>
    </message>
    <message>
        <source>Original label font</source>
        <translation>Taille par défaut des libellés des points</translation>
    </message>
    <message>
        <source>Hide labels</source>
        <translation>Cacher les noms des points</translation>
    </message>
    <message>
        <source>Groups of visibility</source>
        <translation>Groupes de visibilité</translation>
    </message>
    <message>
        <source>Export recipe</source>
        <translation>Exporter les étapes de traçage</translation>
    </message>
    <message>
        <source>Recipe files</source>
        <translation>Fichiers des rapports de traçage</translation>
    </message>
    <message>
        <source>recipe</source>
        <translation>historique</translation>
    </message>
    <message>
        <source>Could not save recipe. %1</source>
        <translation>Impossible d&apos;exporter l&apos;historique. %1</translation>
    </message>
    <message>
        <source>Curved path tool which uses point as control handle</source>
        <translation>Courbe complexe utilisant les points comme poignées de contrôle</translation>
    </message>
    <message>
        <source>Point of intersection arc and axis</source>
        <translation>Point d&apos;intersection d&apos;un arc et d&apos;un axe</translation>
    </message>
    <message>
        <source>Show main path</source>
        <translation>Afficher le contour principal</translation>
    </message>
    <message>
        <source>Globally show pieces main path</source>
        <translation>Afficher le contour principal</translation>
    </message>
    <message>
        <source>Pattern messages</source>
        <translation>Messages d&apos;avertissement</translation>
    </message>
    <message>
        <source>Clear all messages</source>
        <translation>Supprimer tous les messages</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <source>DEBUG</source>
        <translation>DEBUGAGE</translation>
    </message>
    <message>
        <source>WARNING</source>
        <translation>AVERTISSEMENT</translation>
    </message>
    <message>
        <source>CRITICAL</source>
        <translation>CRITIQUE</translation>
    </message>
    <message>
        <source>FATAL</source>
        <translation>FATALE</translation>
    </message>
    <message>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <source>Auto refresh the list with each parse</source>
        <translation>Rafraîchir automatiquement la liste à chaque analyse</translation>
    </message>
    <message>
        <source>Watermark</source>
        <translation>Filigrane</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Retirer</translation>
    </message>
    <message>
        <source>Edit current</source>
        <translation>Editer le filigrane en cours d&apos;utilisation</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <source>Create or edit a watermark</source>
        <translation>Créer ou éditer un filigrane</translation>
    </message>
    <message>
        <source>Watermark files</source>
        <translation>Fichiers filigranes</translation>
    </message>
    <message>
        <source>Manual Layout</source>
        <translation>Plan de coupe manuel</translation>
    </message>
    <message>
        <source>Open Puzzle</source>
        <translation>Ouvrir Puzzle</translation>
    </message>
    <message>
        <source>Open the Puzzle app</source>
        <translation>Ouvrir Puzzle</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Create manual layout</source>
        <translation>Créer un plan de coupe manuel</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <source>Update manual layout</source>
        <translation>Mettre à jour le plan de coupe manuel</translation>
    </message>
    <message>
        <source>Unable to prepare raw layout data.</source>
        <translation>Impossible de préparer les données brutes de plan de coupe.</translation>
    </message>
    <message>
        <source>Manual layout files</source>
        <translation>Fichiers de plans de coupe manuels</translation>
    </message>
    <message>
        <source>Select manual layout</source>
        <translation>Sélectionnez le plan de coupe manuel</translation>
    </message>
    <message>
        <source>Not supported dimension A value &apos;%1&apos; for this pattern file.</source>
        <translation>Valeur &apos;%1&apos; de dimension A non prise en charge pour ce patron.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension A. Need a file with multisize measurements.</source>
        <translation>Impossible de définir la dimension A. Un tableau de mensurations multi-tailles est nécessaire.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension A. File wasn&apos;t opened.</source>
        <translation>Impossible de définir la dimension A. Le fichier n&apos;a pas été ouvert.</translation>
    </message>
    <message>
        <source>Not supported dimension B value &apos;%1&apos; for this pattern file.</source>
        <translation>Valeur &apos;%1&apos; de dimension B non prise en charge pour ce patron.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension B. Need a file with multisize measurements.</source>
        <translation>Impossible de définir la dimension B. Un tableau de mensurations multi-tailles est nécessaire.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension B. File wasn&apos;t opened.</source>
        <translation>Impossible de définir la dimension B. Le fichier n&apos;a pas été ouvert.</translation>
    </message>
    <message>
        <source>Not supported dimension C value &apos;%1&apos; for this pattern file.</source>
        <translation>Valeur &apos;%1&apos; de dimension C non prise en charge pour ce patron.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension C. Need a file with multisize measurements.</source>
        <translation>Impossible de définir la dimension C. Un tableau de mensurations multi-tailles est nécessaire.</translation>
    </message>
    <message>
        <source>Couldn&apos;t set dimension C. File wasn&apos;t opened.</source>
        <translation>Impossible de définir la dimension C. Le fichier n&apos;a pas été ouvert.</translation>
    </message>
    <message>
        <source>Could not create recipe file. %1</source>
        <translation>Impossible de créer le fichier d&apos;historique. %1</translation>
    </message>
    <message>
        <source>Shop</source>
        <translation>Boutique</translation>
    </message>
    <message>
        <source>Scalable Vector Graphics files</source>
        <translation>Fichiers SVG</translation>
    </message>
    <message>
        <source>Save draw</source>
        <translation>Enregistrer le dessin</translation>
    </message>
    <message>
        <source>Draw export</source>
        <translation>Export du dessin depuis mode Modélisme</translation>
    </message>
    <message>
        <source>Background images</source>
        <translation>Images en arrière-plan</translation>
    </message>
    <message>
        <source>Add background image</source>
        <translation>Ajouter une image en arrière-plan</translation>
    </message>
    <message>
        <source>Unable to add background image</source>
        <translation>Impossible d&apos;ajouter l&apos;image</translation>
    </message>
    <message>
        <source>Invalid image. Error: %1</source>
        <translation>Image non valide. Erreur : %1</translation>
    </message>
    <message>
        <source>Select background image</source>
        <translation>Sélection de l&apos;image</translation>
    </message>
    <message>
        <source>Unable to save image. Error: %1</source>
        <translation>Impossible d&apos;enregistrer l&apos;image. Erreur : %1</translation>
    </message>
    <message>
        <source>Unable to save image. No data.</source>
        <translation>Impossible d&apos;enregistrer l&apos;image. Aucune donnée.</translation>
    </message>
    <message>
        <source>untitled</source>
        <translation>sans nom</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <source>Save Image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <source>Visibility group</source>
        <translation>Groupe de visibilité</translation>
    </message>
</context>
<context>
    <name>MainWindowsNoGUI</name>
    <message>
        <source>For saving multypage document all sheet should have the same size. Use export function instead.</source>
        <translation type="vanished">Pour sauvegarder un document multipages, toutes les feuilles doivent être de la même taille. Utilisez plutôt la fonction Exporter.</translation>
    </message>
    <message>
        <source>For previewing multypage document all sheet should have the same size.</source>
        <translation type="vanished">Pour générer un aperçu avant impression d&apos;un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>For printing multypages document all sheet should have the same size.</source>
        <translation type="vanished">Pour imprimer un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>Creating file &apos;%1&apos; failed! %2</source>
        <translation type="vanished">La création du fichier &apos;%1&apos; a échoué ! %2</translation>
    </message>
    <message>
        <source>Critical error!</source>
        <translation type="vanished">Erreur critique !</translation>
    </message>
    <message>
        <source>Print to pdf</source>
        <translation type="vanished">Imprimer au format PDF</translation>
    </message>
    <message>
        <source>PDF file (*.pdf)</source>
        <translation type="vanished">Format PDF (*.pdf)</translation>
    </message>
    <message>
        <source>Print error</source>
        <translation type="vanished">Erreur d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot proceed because there are no available printers in your system.</source>
        <translation type="vanished">Impossible de poursuivre, aucune imprimante système détectée.</translation>
    </message>
    <message>
        <source>unnamed</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <source>The layout is stale.</source>
        <translation type="vanished">Le plan de coupe n&apos;est plus à jour.</translation>
    </message>
    <message>
        <source>The layout was not updated since last pattern modification. Do you want to continue?</source>
        <translation type="vanished">Le plan de coupe n&apos;a pas été mis à jour depuis la dernière modification du patron. Voulez-vous poursuivre quand même ?</translation>
    </message>
    <message>
        <source>Couldn&apos;t prepare data for creation layout</source>
        <translation>La préparation des données pour la création du plan de coupe a échoué</translation>
    </message>
    <message>
        <source>Several workpieces left not arranged, but none of them match for paper</source>
        <translation type="vanished">Plusieurs pièces du patron n&apos;ont pas pu être arrangées, car aucune ne correspond au format du plan de coupe</translation>
    </message>
    <message>
        <source>Can&apos;t open printer %1</source>
        <translation type="vanished">Impossible d&apos;utiliser l&apos;imprimante %1</translation>
    </message>
    <message>
        <source>Export error.</source>
        <translation type="vanished">Erreur d&apos;exportation.</translation>
    </message>
    <message>
        <source>For saving multipage document all sheet should have the same size. Use export function instead.</source>
        <translation type="vanished">Pour sauvegarder un document multipages, toutes les feuilles doivent être de la même taille. Utilisez plutôt la fonction Exporter.</translation>
    </message>
    <message>
        <source>For previewing multipage document all sheet should have the same size.</source>
        <translation type="vanished">Pour générer un aperçu avant impression d&apos;un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>For printing multipages document all sheet should have the same size.</source>
        <translation type="vanished">Pour imprimer un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>Pages will be cropped because they do not fit printer paper size.</source>
        <translation type="vanished">Les pages vont être tronquées, ne correspondent pas à la taille du papier de l&apos;imprimante.</translation>
    </message>
    <message>
        <source>Can&apos;t create path</source>
        <translation type="vanished">Impossible de créer l&apos;emplacement</translation>
    </message>
    <message>
        <source>Cannot set printer margins</source>
        <translation type="vanished">Impossible de définir les marges d&apos;impression de l&apos;imprimante</translation>
    </message>
    <message>
        <source>Can&apos;t create a path</source>
        <translation>Impossible de créer l&apos;emplacement</translation>
    </message>
    <message>
        <source>Pattern</source>
        <translation>Patron</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Export final measurements error.</source>
        <translation>Erreur d&apos;exportation des mesures finales.</translation>
    </message>
    <message>
        <source>Value in line %1 is infinite or NaN. Please, check your calculations.</source>
        <translation>Résultat invalide. La valeur %1 est infinie ou n&apos;est pas une valeur numérique. Veuillez vérifier vos calculs.</translation>
    </message>
    <message>
        <source>Parser error at line %1: %2.</source>
        <translation>Erreur d&apos;analyse à la ligne %1: %2.</translation>
    </message>
    <message>
        <source>File error %1.</source>
        <translation>Erreur de fichier %1.</translation>
    </message>
    <message>
        <source>One or more pattern pieces are bigger than the paper format you selected. Please, select a bigger paper format.</source>
        <translation>Une ou plusieurs pièces sont plus grandes que le format de plan sélectionné. Merci de choisir un format plus grand.</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Measurement file has unknown format.</source>
        <translation>La table de mensurations est de format inconnu.</translation>
    </message>
    <message>
        <source>Measurement file contains invalid known measurement(s).</source>
        <translation>Le fichier contient une ou des mensurations invalides (de type &apos;connue&apos;).</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>Measurement file doesn&apos;t include all required measurements.</source>
        <translation>Des mensurations requises manquent dans la table de mesures que vous voulez charger.</translation>
    </message>
    <message>
        <source>Please, additionally provide: %1</source>
        <translation>Merci de spécifier aussi: %1</translation>
    </message>
    <message>
        <source>Preparing details for layout</source>
        <translation>Préparation des pièces pour l&apos;édition du plan de coupe</translation>
    </message>
    <message>
        <source>Timeout.</source>
        <translation>Délai.</translation>
    </message>
    <message>
        <source>Process has been stoped because of exception.</source>
        <translation>L&apos;opération a été stoppée en raison d&apos;une exception.</translation>
    </message>
</context>
<context>
    <name>MoveBackgroundImage</name>
    <message>
        <source>move background image</source>
        <translation>Déplacer l&apos;image</translation>
    </message>
</context>
<context>
    <name>MoveDoubleLabel</name>
    <message>
        <source>move the first dart label</source>
        <translation>Déplacer la première étiquette de la pince</translation>
    </message>
    <message>
        <source>move the second dart label</source>
        <translation>Déplacer la seconde étiquette de la pince</translation>
    </message>
</context>
<context>
    <name>MoveLabel</name>
    <message>
        <source>move point label</source>
        <translation>Déplace l&apos;étiquette de point</translation>
    </message>
</context>
<context>
    <name>MoveSPoint</name>
    <message>
        <source>move single point</source>
        <translation>Déplacer un point</translation>
    </message>
</context>
<context>
    <name>MoveSpline</name>
    <message>
        <source>move spline</source>
        <translation>Déplacer la spline</translation>
    </message>
</context>
<context>
    <name>MoveSplinePath</name>
    <message>
        <source>move spline path</source>
        <translation>Déplacer le trajet de la spline</translation>
    </message>
</context>
<context>
    <name>OpaqueBackgroundImage</name>
    <message>
        <source>change a background image opacity</source>
        <translation>Modifier l&apos;opacité de l&apos;image</translation>
    </message>
</context>
<context>
    <name>OperationMoveLabel</name>
    <message>
        <source>move point label</source>
        <translation>Déplacer l&apos;étiquette du point</translation>
    </message>
</context>
<context>
    <name>OperationShowLabel</name>
    <message>
        <source>toggle label</source>
        <translation>Attacher l&apos;étiquette</translation>
    </message>
</context>
<context>
    <name>PathPage</name>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">Ouvrir un dossier</translation>
    </message>
    <message>
        <source>Path that use Valentina</source>
        <translation type="vanished">Dossiers utilisés par Valentina</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Défault</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Editer</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">Chemin</translation>
    </message>
    <message>
        <source>Individual measurements</source>
        <translation type="vanished">Mesures individuelles</translation>
    </message>
    <message>
        <source>Patterns</source>
        <translation type="vanished">Patrons</translation>
    </message>
    <message>
        <source>Standard measurements</source>
        <translation type="vanished">Mesures standard</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="vanished">Plan de coupe</translation>
    </message>
    <message>
        <source>Templates</source>
        <translation type="vanished">Modèles</translation>
    </message>
</context>
<context>
    <name>PatternPage</name>
    <message>
        <source>User</source>
        <translation type="vanished">Utilisateur</translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="vanished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Graphical output</source>
        <translation type="vanished">Sortie graphique</translation>
    </message>
    <message>
        <source>Use antialiasing</source>
        <translation type="vanished">Utiliser l&apos;antialiasing</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Count steps (0 - no limit)</source>
        <translation type="vanished">Nombre d&apos;actions pouvant être annulées (0 = pas de limite)</translation>
    </message>
    <message>
        <source>User name:</source>
        <translation type="vanished">Nom d&apos;utilisateur:</translation>
    </message>
    <message>
        <source>Count steps (0 - no limit):</source>
        <translation type="vanished">Comptage (0 -&gt; sans limite):</translation>
    </message>
    <message>
        <source>All user defined materials have been deleted!</source>
        <translation type="vanished">Tous les matériaux définis par l&apos;utilisateur ont été supprimés !</translation>
    </message>
    <message>
        <source>User defined materials</source>
        <translation type="vanished">Matériaux définis par l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Delete all</source>
        <translation type="vanished">Tout effacer</translation>
    </message>
    <message>
        <source>Workpiece</source>
        <translation type="vanished">Pièce en cours</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation type="vanished">Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>By default forbid flipping for all workpieces</source>
        <translation type="vanished">Interdire la reproduction en miroir des objets pour tous les éléments de patron par défaut</translation>
    </message>
    <message>
        <source>By default forbid flipping for all new created workpieces</source>
        <translation type="vanished">Interdire par défaut la reproduction en miroir des objets pour tous les nouveaux éléments de patron créés</translation>
    </message>
</context>
<context>
    <name>PreferencesConfigurationPage</name>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Auto-save modified pattern</source>
        <translation>Sauvegarde automatique des modifications du patron</translation>
    </message>
    <message>
        <source>Interval:</source>
        <translation>Intervalle :</translation>
    </message>
    <message>
        <source>min</source>
        <translation>minimum</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>GUI language:</source>
        <translation>Langue de l&apos;interface:</translation>
    </message>
    <message>
        <source>Decimal separator parts:</source>
        <translation>Séparateur de décimale:</translation>
    </message>
    <message>
        <source>Default unit:</source>
        <translation>Unité par défaut:</translation>
    </message>
    <message>
        <source>Label language:</source>
        <translation>Langue des libellés:</translation>
    </message>
    <message>
        <source>Pattern making system</source>
        <translation>Programme de réalisation de patrons</translation>
    </message>
    <message>
        <source>Pattern making system:</source>
        <translation>Programme de réalisation de patrons:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <source>Book:</source>
        <translation>Source:</translation>
    </message>
    <message>
        <source>Pattern editing</source>
        <translation>Édition du patron</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation>Afficher à nouveau les messages d&apos;avertissement</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <source>The text appears under the icon (recommended for beginners).</source>
        <translation>Un libellé accompagne les icônes (recommandé pour les débutants).</translation>
    </message>
    <message>
        <source>With OS options</source>
        <translation>Celui du système</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Update a pattern only after a curve release</source>
        <translation>Ne mettre à jour le patron qu&apos;une fois la courbe relâchée</translation>
    </message>
    <message>
        <source>Free curve mode</source>
        <translation>Mode &quot;courbe libre&quot;</translation>
    </message>
    <message>
        <source>default unit</source>
        <translation>Unité par défaut</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Double click calls Zoom fit best for current pattern piece</source>
        <translation>Double-clic = zoom optimal sur l&apos;élément de patron en cours</translation>
    </message>
    <message>
        <source>Scrolling</source>
        <translation>Défilement</translation>
    </message>
    <message>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <source>Scrolling animation duration</source>
        <translation>Durée de l&apos;animation de défilement</translation>
    </message>
    <message>
        <source>ms</source>
        <comment>milliseconds</comment>
        <translation>ms</translation>
    </message>
    <message>
        <source>Update interval:</source>
        <translation>Intervalle de rafraîchissement:</translation>
    </message>
    <message>
        <source>Time in milliseconds between each animation update</source>
        <translation>Durée en millisecondes entre chaque rafraîchissement de l&apos;animation</translation>
    </message>
    <message>
        <source>Mouse scale</source>
        <translation>Echelle de la souris</translation>
    </message>
    <message>
        <source>Sensor:</source>
        <comment>sensor mouse</comment>
        <translation>Capteur:</translation>
    </message>
    <message>
        <source>Scale scrolling sensitivity for mouse with sensor</source>
        <translation>Echelle de défilement pour souris optique</translation>
    </message>
    <message>
        <source>Wheel:</source>
        <comment>mouse wheel</comment>
        <translation>Molette:</translation>
    </message>
    <message>
        <source>Scale scrolling sensitivity for mouse with wheel</source>
        <translation>Echelle de défilement pour souris à molette</translation>
    </message>
    <message>
        <source>Acceleration:</source>
        <translation>Accélération:</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Thème de l&apos;interface</translation>
    </message>
    <message>
        <source>Activate dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>Change the position of the tool panel to optimize for big screen resolutions. By default, the tool panel will take all available space.</source>
        <translation>Change la position du panneau d&apos;outils pour s&apos;adapter aux hautes résolutions d&apos;écran. Par défaut, le panneau d&apos;outils prend toute la place disponible.</translation>
    </message>
    <message>
        <source>Tool panel scaling</source>
        <translation>Mise à l&apos;échelle du panneau d&apos;outils</translation>
    </message>
    <message>
        <source>Don&apos;t use the native file dialog</source>
        <translation>Ne pas utiliser la boîte de dialogue de fichier native</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <source>Automatically check for updates each time the application starts</source>
        <translation>Vérification des mises à jour automatique à chaque démarrage</translation>
    </message>
    <message>
        <source>Automatically check for updates</source>
        <translation>Vérification automatique des mises à jour</translation>
    </message>
</context>
<context>
    <name>PreferencesPathPage</name>
    <message>
        <source>Paths that Valentina uses</source>
        <translation>Chemins d&apos;accès que Valentina utilise</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ouvrir Dossier</translation>
    </message>
    <message>
        <source>My Individual Measurements</source>
        <translation>Mes Mensurations</translation>
    </message>
    <message>
        <source>My Multisize Measurements</source>
        <translation>Mes Mensurations Multi-tailles</translation>
    </message>
    <message>
        <source>My Patterns</source>
        <translation>Mes patrons</translation>
    </message>
    <message>
        <source>My Layouts</source>
        <translation>Mes plans de coupe</translation>
    </message>
    <message>
        <source>My Templates</source>
        <translation>Mes Modèles</translation>
    </message>
    <message>
        <source>My label templates</source>
        <translation>Mes modèles d&apos;étiquette</translation>
    </message>
    <message>
        <source>My manual layouts</source>
        <translation>Mes plans de coupe manuels</translation>
    </message>
</context>
<context>
    <name>PreferencesPatternPage</name>
    <message>
        <source>Graphical output</source>
        <translation>Sortie graphique</translation>
    </message>
    <message>
        <source>Use antialiasing</source>
        <translation>Utiliser l&apos;antialiasing</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Count steps (0 - no limit):</source>
        <translation>Pas de compte (0 - pas de limite) :</translation>
    </message>
    <message>
        <source>Workpiece</source>
        <translation>Pièce en cours</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation>Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Show a passmark both in the seam allowance and on the seam line.</source>
        <translation>Afficher un cran d&apos;assemblage à la fois sur la marge de couture et sur la ligne de couture.</translation>
    </message>
    <message>
        <source>Show second passmark on seam line</source>
        <translation>Afficher le second cran d&apos;assemblage sur la ligne de couture</translation>
    </message>
    <message>
        <source>By default forbid flipping for all new created workpieces</source>
        <translation>Interdire par défaut la reproduction en miroir des objets pour tous les nouveaux éléments de patron créés</translation>
    </message>
    <message>
        <source>By default hide the main path if the seam allowance was enabled</source>
        <translation>Cacher le chemin principal par défaut si la marge de couture était activée</translation>
    </message>
    <message>
        <source>Hide main path</source>
        <translation>Cacher le chemin principal</translation>
    </message>
    <message>
        <source>Label font:</source>
        <translation>Police de caractères d&apos;étiquette:</translation>
    </message>
    <message>
        <source>Seam allowance</source>
        <translation>Marge de couture</translation>
    </message>
    <message>
        <source>Default value:</source>
        <translation>Valeur par défaut:</translation>
    </message>
    <message>
        <source>Label data/time format</source>
        <translation>Etiquette de format de date et d&apos;heure</translation>
    </message>
    <message>
        <source>Date:</source>
        <translation>Date :</translation>
    </message>
    <message>
        <source>Edit formats</source>
        <translation>Editer les formats</translation>
    </message>
    <message>
        <source>Time:</source>
        <translation>Heure :</translation>
    </message>
    <message>
        <source>Materials</source>
        <translation>Matières</translation>
    </message>
    <message>
        <source>Known materials:</source>
        <translation>Matériaux employés :</translation>
    </message>
    <message>
        <source>Manage list of known materials</source>
        <translation>Gérer la liste des matériaux utilisés dans le patron</translation>
    </message>
    <message>
        <source>Manage</source>
        <translation>Gérer</translation>
    </message>
    <message>
        <source>When manage pattern materials save them to known materials list</source>
        <translation>Quand vous utilisez une nouvelle matière dans ce patron, enregistrez-la dans la liste des matériaux utilisés pour la réutiliser dans un autre patron</translation>
    </message>
    <message>
        <source>Remeber pattern materials</source>
        <translation>Mémoriser les matériaux du patron</translation>
    </message>
    <message>
        <source>Curve approximation:</source>
        <translation>Approximation des courbes :</translation>
    </message>
    <message>
        <source>Set default curve approximation scale</source>
        <translation>Définir l&apos;échelle par défaut de l&apos;approximation des courbes</translation>
    </message>
    <message>
        <source>Bold line width</source>
        <translation>Largeur de la ligne épaisse</translation>
    </message>
    <message>
        <source>Line width:</source>
        <translation>Largeur de la ligne :</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Use OpenGL to render a scene.
This option will take an affect after restart.</source>
        <translation>Utiliser le rendu OpenGL. Cette option sera active après le redémarrage.</translation>
    </message>
    <message>
        <source>Use OpenGL render</source>
        <translation>Utiliser le rendu OpenGL</translation>
    </message>
    <message>
        <source>antialiasing</source>
        <translation>antialiasing</translation>
    </message>
    <message>
        <source>scene render</source>
        <translation>Rendu de scène</translation>
    </message>
    <message>
        <source>Background image</source>
        <translation>Image en arrière-plan</translation>
    </message>
    <message>
        <source>Opacity:</source>
        <translation>Opacité :</translation>
    </message>
    <message>
        <source>Opacity value by default</source>
        <translation>Opacité par défaut</translation>
    </message>
</context>
<context>
    <name>Puzzle</name>
    <message>
        <source>Export options can be used with single input file only.</source>
        <translation>Les options d&apos;export ne peuvent être utilisées qu&apos;avec un fichier d&apos;entrée unique.</translation>
    </message>
    <message>
        <source>Valentina&apos;s manual layout editor.</source>
        <translation>Editeur de plan de coupe de Valentina.</translation>
    </message>
    <message>
        <source>The manual layout file.</source>
        <translation>Le fichier de plan de coupe manuel.</translation>
    </message>
</context>
<context>
    <name>PuzzlePreferencesConfigurationPage</name>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>GUI language:</source>
        <translation>Langue de l&apos;interface :</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <source>The text appears under the icon (recommended for beginners).</source>
        <translation>Un libellé accompagne les icônes (recommandé pour les débutants).</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Thème de l&apos;interface</translation>
    </message>
    <message>
        <source>Activate dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>Don&apos;t use the native file dialog</source>
        <translation>Ne pas utiliser la boîte de dialogue de fichier native</translation>
    </message>
    <message>
        <source>dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Graphical output</source>
        <translation>Sortie graphique</translation>
    </message>
    <message>
        <source>Use antialiasing</source>
        <translation>Utiliser l&apos;antialiasing</translation>
    </message>
    <message>
        <source>Use OpenGL to render a scene.
This option will take an affect after restart.</source>
        <translation>Utiliser le rendu OpenGL. Cette option sera active après le redémarrage.</translation>
    </message>
    <message>
        <source>Use OpenGL render</source>
        <translation>Utiliser le rendu OpenGL</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Count steps (0 - no limit):</source>
        <translation>Comptage (0 -&gt; sans limite):</translation>
    </message>
    <message>
        <source>Scrolling</source>
        <translation>Défilement</translation>
    </message>
    <message>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <source>Scrolling animation duration</source>
        <translation>Durée de l&apos;animation de défilement</translation>
    </message>
    <message>
        <source>ms</source>
        <comment>milliseconds</comment>
        <translation>ms</translation>
    </message>
    <message>
        <source>Update interval:</source>
        <translation>Intervalle de rafraîchissement:</translation>
    </message>
    <message>
        <source>Time in milliseconds between each animation update</source>
        <translation>Durée en millisecondes entre chaque rafraîchissement de l&apos;animation</translation>
    </message>
    <message>
        <source>Mouse scale</source>
        <translation>Echelle de la souris</translation>
    </message>
    <message>
        <source>Sensor:</source>
        <comment>sensor mouse</comment>
        <translation>Capteur:</translation>
    </message>
    <message>
        <source>Scale scrolling sensitivity for mouse with sensor</source>
        <translation>Echelle de défilement pour souris optique</translation>
    </message>
    <message>
        <source>Wheel:</source>
        <comment>mouse wheel</comment>
        <translation>Molette:</translation>
    </message>
    <message>
        <source>Scale scrolling sensitivity for mouse with wheel</source>
        <translation>Echelle de défilement pour souris à molette</translation>
    </message>
    <message>
        <source>Acceleration:</source>
        <translation>Accélération:</translation>
    </message>
    <message>
        <source>undo limit</source>
        <translation>Nombre d&apos;&quot;Annuler&quot; possible</translation>
    </message>
    <message>
        <source>antialiasing</source>
        <translation>antialiasing</translation>
    </message>
    <message>
        <source>scene render</source>
        <translation>Rendu de scène</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <source>Automatically check for updates each time the application starts</source>
        <translation>Vérification automatique des mises à jour à chaque démarrage</translation>
    </message>
    <message>
        <source>Automatically check for updates</source>
        <translation>Vérification des mises à jour automatique</translation>
    </message>
</context>
<context>
    <name>PuzzlePreferencesLayoutPage</name>
    <message>
        <source>Sheet</source>
        <translation>Feuille</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Unit:</source>
        <translation>Unité:</translation>
    </message>
    <message>
        <source>Templates:</source>
        <translation>Modèles:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Stature:</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Right:</source>
        <translation>Droit:</translation>
    </message>
    <message>
        <source>Top:</source>
        <translation>Haut :</translation>
    </message>
    <message>
        <source>Left:</source>
        <translation>Gauche:</translation>
    </message>
    <message>
        <source>Bottom:</source>
        <translation>Bas :</translation>
    </message>
    <message>
        <source>Ignore margins</source>
        <translation>Ignorer les marges</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <source>Warning superposition of pieces</source>
        <translation>Avertir de la superposition des pièces</translation>
    </message>
    <message>
        <source>Warning pieces out of bound</source>
        <translation>Avertir quand le plan excède la zone imprimable de la feuille</translation>
    </message>
    <message>
        <source>Sticky edges</source>
        <translation>Forcer l&apos;espacement minimum entre pièces</translation>
    </message>
    <message>
        <source>Follow grainline</source>
        <translation>Suivre le droit-fil</translation>
    </message>
    <message>
        <source>Pieces gap</source>
        <translation>Ecart des pièces</translation>
    </message>
    <message>
        <source>Tiles</source>
        <translation>Empilement</translation>
    </message>
    <message>
        <source>Show Tiles on sheet</source>
        <translation>Afficher la vue empilée sur la feuille</translation>
    </message>
    <message>
        <source>Show watermark preview</source>
        <translation>Afficher aperçu du filigrane</translation>
    </message>
    <message>
        <source>Show watermark</source>
        <translation>Afficher le filigrane</translation>
    </message>
    <message>
        <source>default layout settings</source>
        <translation>Réglages par défaut du plan de coupe</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Pixels</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Line width:</source>
        <translation>Largeur de la ligne :</translation>
    </message>
</context>
<context>
    <name>PuzzlePreferencesPathPage</name>
    <message>
        <source>Paths that Valentina uses</source>
        <translation>Chemins d&apos;accès que Valentina utilise</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ouvrir Dossier</translation>
    </message>
    <message>
        <source>My Individual Measurements</source>
        <translation>Mes Mensurations</translation>
    </message>
    <message>
        <source>My Multisize Measurements</source>
        <translation>Mes Mensurations Multi-tailles</translation>
    </message>
    <message>
        <source>My Patterns</source>
        <translation>Mes patrons</translation>
    </message>
    <message>
        <source>My Templates</source>
        <translation>Mes Modèles</translation>
    </message>
    <message>
        <source>My Layouts</source>
        <translation>Mes plans de coupe</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>The path to the measurments is already relative.</source>
        <translation type="vanished">Le chemin vers le tableau de mesure est déjà relatif.</translation>
    </message>
    <message>
        <source>The path to the measurments is already absolute.</source>
        <translation type="vanished">Le chemin vers le tableau de mesure est déjà absolu.</translation>
    </message>
</context>
<context>
    <name>QCommandLineParser</name>
    <message>
        <source>Displays version information.</source>
        <translation type="vanished">Affiche les informations de version.</translation>
    </message>
    <message>
        <source>Displays this help.</source>
        <translation type="vanished">Montrer cette aide.</translation>
    </message>
    <message>
        <source>Unknown option &apos;%1&apos;.</source>
        <translation type="vanished">Option inconnue &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Unknown options: %1.</source>
        <translation type="vanished">Options inconnues: &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Missing value after &apos;%1&apos;.</source>
        <translation type="vanished">Donnée manquante après &apos;%1&quot;.</translation>
    </message>
    <message>
        <source>Unexpected value after &apos;%1&apos;.</source>
        <translation type="vanished">Valeur inattendue après &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>[options]</source>
        <translation type="vanished">[options]</translation>
    </message>
    <message>
        <source>Usage: %1</source>
        <translation type="vanished">Utilisation : %1</translation>
    </message>
    <message>
        <source>Options:</source>
        <translation type="vanished">Options:</translation>
    </message>
    <message>
        <source>Arguments:</source>
        <translation type="vanished">Arguments:</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation>S&apos;appuie sur Qt %1 (%2, %3 bit)</translation>
    </message>
    <message>
        <source>VLayoutPassmark prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VLayoutPassmark : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VLayoutPassmark compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VLayoutPassmark : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VLayoutPlaceLabel prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VLayoutPlaceLabel : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VLayoutPlaceLabel compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VLayoutPlaceLabel : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VAbstractPieceData prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VAbstractPieceData : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VAbstractPieceData compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VAbstractPieceData : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VRawLayoutData prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VRawLayoutData : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VRawLayoutData compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VRawLayoutData : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VLayoutPiecePathData prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VLayoutPiecePathData : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VLayoutPiecePathData compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VLayoutPiecePathData : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>TextLine prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe TextLine : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>TextLine compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité TextLine : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VTextManager prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VTextManager : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VTextManager compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VTextManager : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>CustomSARecord prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe CustomSARecord : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>CustomSARecord compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité CustomSARecord : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
    <message>
        <source>VPieceNodeData prefix mismatch error: actualStreamHeader = 0x%1 and streamHeader = 0x%2</source>
        <translation>Erreur de correspondance de préfixe VPieceNodeData : actualStreamHeader = 0x%1 et streamHeader = 0x%2</translation>
    </message>
    <message>
        <source>VPieceNodeData compatibility error: actualClassVersion = %1 and classVersion = %2</source>
        <translation>Erreur de compatibilité VPieceNodeData : actualClassVersion = %1 et classVersion = %2</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Create new pattern piece to start working.</source>
        <translation type="vanished">créer un nouvel élément de patron pour commencer.</translation>
    </message>
    <message>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <source>inch</source>
        <translation>Pouce</translation>
    </message>
    <message>
        <source>Property</source>
        <extracomment>The text that appears in the first column header</extracomment>
        <translation>Propriété</translation>
    </message>
    <message>
        <source>Value</source>
        <extracomment>The text that appears in the second column header</extracomment>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <source>add node</source>
        <translation type="vanished">Ajouter un nœud</translation>
    </message>
    <message>
        <source>move detail</source>
        <translation>Déplacer la pièce</translation>
    </message>
    <message>
        <source>Changes applied.</source>
        <translation type="vanished">Changements appliqués.</translation>
    </message>
    <message>
        <source>Wrong tag name &apos;%1&apos;.</source>
        <translation>Mauvais nom de tag &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Can&apos;t convert toUInt parameter</source>
        <translation>Impossible de convertir vers un paramètre de type entier</translation>
    </message>
    <message>
        <source>Can&apos;t convert toBool parameter</source>
        <translation>Impossible de convertir vers un paramètre de type Booléen</translation>
    </message>
    <message>
        <source>Got empty parameter</source>
        <translation>Paramètre vide</translation>
    </message>
    <message>
        <source>Can&apos;t convert toDouble parameter</source>
        <translation>Conversion du paramètre impossible vers toDouble</translation>
    </message>
    <message>
        <source>Got wrong parameter id. Need only id &gt; 0.</source>
        <translation>Mauvais identifiant de paramètre. Nécessite seulement un id &gt; 0.</translation>
    </message>
    <message>
        <source>United detail</source>
        <translation>Pièces de patron fusionnées</translation>
    </message>
    <message>
        <source>Fabric</source>
        <translation>Tissu</translation>
    </message>
    <message>
        <source>Lining</source>
        <translation>Doublure</translation>
    </message>
    <message>
        <source>Interfacing</source>
        <translation>Interfaçage</translation>
    </message>
    <message>
        <source>Interlining</source>
        <translation>Interligne</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Coupure</translation>
    </message>
    <message>
        <source>on fold</source>
        <translation>Au pli</translation>
    </message>
    <message>
        <source>Visibility trigger contains error and will be ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User material number %1 was not defined in this pattern.</source>
        <translation>La matière numéro %1 n&apos;a pas été définie dans ce patron.</translation>
    </message>
    <message>
        <source>Error in internal path &apos;%1&apos;. There is no intersection of first point with cutting countour</source>
        <translation>Erreur dans le chemin interne &apos;%1&apos;. Il n&apos;y a pas d&apos;intersection entre le premier point et le contour de coupe</translation>
    </message>
    <message>
        <source>Error in internal path &apos;%1&apos;. There is no intersection of last point with cutting countour</source>
        <translation>Erreur dans le chemin interne &apos;%1&apos;. Il n&apos;y a pas d&apos;intersection entre le dernier point et le contour de coupe</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;. Seam allowance is empty.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. La marge de couture n&apos;est pas renseignée.</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;. Cannot find position for a notch.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. Impossible de déterminer la position du cran.</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;. Unable to fix a notch position.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. Impossible de corriger la position du cran.</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;. Notch collapse.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. Disparition du cran.</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;. Cannot find intersection.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. Impossible de trouver une intersection.</translation>
    </message>
    <message>
        <source>Cannot get tokens from formula &apos;%1&apos;. Parser error: %2.</source>
        <translation>Les champs saisis dans la formule &apos;%1&apos; sont erronés. Erreur de formule : %2.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Empty formula</source>
        <translation>Formule vide</translation>
    </message>
    <message>
        <source>Invalid result. Value is infinite or NaN. Please, check your calculations.</source>
        <translation>Résultat invalide. La valeur est infinie ou n&apos;est pas une valeur numérique. Veuillez vérifier vos calculs.</translation>
    </message>
    <message>
        <source>Value can&apos;t be 0</source>
        <translation>La valeur ne peut pas être égale à 0</translation>
    </message>
    <message>
        <source>Value can&apos;t be less than 0</source>
        <translation>La valeur ne peut pas être inférieure à 0</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation>Erreur d&apos;analyse : %1</translation>
    </message>
    <message>
        <source>Piece &apos;%1&apos; has invalid layout allowance. Please, check seam allowance to check how seam allowance behave.</source>
        <translation>La pièce &apos;%1&apos; a une valeur de marge de plan de coupe invalide. Merci de vérifier la marge de couture pour vérifier son comportement.</translation>
    </message>
    <message>
        <source>Piece &apos;%1&apos;. Seam allowance is not valid.</source>
        <translation>Pièce &apos;%1&apos;. La marge de couture est invalide.</translation>
    </message>
    <message>
        <source>Found null notch for point &apos;%1&apos; in piece &apos;%2&apos;. Length is less than minimal allowed.</source>
        <translation>Cran d&apos;assemblage nul pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;. La longueur est inférieure à la longueur minimale autorisée.</translation>
    </message>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos; with built in seam allowance. User must manually provide length.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos; avec la marge de couture intégrée. Merci de saisir une longueur manuelle.</translation>
    </message>
    <message>
        <source>Cannot calculate seam allowance before for point &apos;%1&apos;. Reason: %2.</source>
        <translation>Impossible de calculer la marge de couture avant le point &apos;%1&apos;. Motif : %2.</translation>
    </message>
    <message>
        <source>Cannot calculate seam allowance after for point &apos;%1&apos;. Reason: %2.</source>
        <translation>Impossible de calculer la marge de couture après le point &apos;%1&apos;. Motif : %2.</translation>
    </message>
    <message>
        <source>Cannot calculate seam allowance after for point &apos;%1&apos;. Reason: </source>
        <translation>Impossible de calculer la marge de couture après le point &apos;%1&apos;. Motif : </translation>
    </message>
    <message>
        <source>Cannot calculate passmark length for point &apos;%1&apos;. Reason: %2.</source>
        <translation>Impossible de calculer une longueur de cran d&apos;assemblage pour le point &apos;%1&apos;. Raison : %2.</translation>
    </message>
    <message>
        <source>Passmark &apos;%1&apos; is not part of piece &apos;%2&apos;.</source>
        <translation>Le cran d&apos;assemblage &apos;%1&apos; ne fait pas partie de la pièce &apos;%2&apos;.</translation>
    </message>
    <message>
        <source>Cannot cast tool with id &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty list of nodes for tool with id &apos;%1&apos;.</source>
        <translation>Listes de nœuds vide pour l&apos;outil avec l&apos;identifiant &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Invalid formula &apos;%1&apos; for tool with id &apos;%2&apos;. %3.</source>
        <translation>Formule invalide &apos;%1&apos; pour cet outil avec l&apos;identifiant &apos;%2&apos;. %3.</translation>
    </message>
    <message>
        <source>Can&apos;t convert toInt parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Piece name &apos;%1&apos; is not unique.</source>
        <translation>La pièce nommée &apos;%1&apos; n&apos;est pas unique.</translation>
    </message>
    <message>
        <source>Could not find the segment start.</source>
        <translation>Impossible de trouver le début du segment.</translation>
    </message>
    <message>
        <source>Could not find the segment end.</source>
        <translation>Impossible de trouver la fin du segment.</translation>
    </message>
    <message>
        <source>Segment is too short.</source>
        <translation>Le segment est trop court.</translation>
    </message>
    <message>
        <source>Error calculating segment for curve &apos;%1&apos;. %2</source>
        <translation>Erreur de calcul du segment pour la courbe &apos;%1&apos;. %2</translation>
    </message>
    <message>
        <source>Error in path &apos;%1&apos;. Calculating segment for curve &apos;%2&apos; has failed. %3</source>
        <translation>Erreur dans le chemin &apos;%1&apos;. Le calcul du segment pour la courbe &apos;%2&apos; a échoué. %3</translation>
    </message>
    <message>
        <source>Cannot prepare builtin passmark &apos;%1&apos; for piece &apos;%2&apos;. Passmark is empty.</source>
        <translation>Impossible de configurer le cran d&apos;assemblage &apos;%1&apos; dans la pièce &apos;%2&apos;. Le cran d&apos;assemblage est absent.</translation>
    </message>
    <message>
        <source>Cannot prepare builtin  passmark &apos;%1&apos; for piece &apos;%2&apos;. Passmark base line is empty.</source>
        <translation>Impossible de configurer le cran d&apos;assemblage &apos;%1&apos; dans la pièce &apos;%2&apos;. La ligne de base du cran d&apos;assemblage est absente.</translation>
    </message>
    <message>
        <source>Cannot prepare passmark &apos;%1&apos; for piece &apos;%2&apos;. Passmark base line is empty.</source>
        <translation>Impossible de configurer le cran d&apos;assemblage &apos;%1&apos; dans la pièce &apos;%2&apos;. La ligne de base du cran d&apos;assemblage est absente.</translation>
    </message>
    <message>
        <source>Cannot prepare passmark &apos;%1&apos; for piece &apos;%2&apos;. Passmark is empty.</source>
        <translation>Impossible de configurer le cran d&apos;assemblage &apos;%1&apos; dans la pièce &apos;%2&apos;. Le cran d&apos;assemblage est absent.</translation>
    </message>
    <message>
        <source>Cannot get tokens from formula &apos;%1&apos;. Formula error: %2.</source>
        <translation>Les champs saisis dans la formule &apos;%1&apos; sont erronés. Erreur de formule : %2.</translation>
    </message>
    <message>
        <source>Failed to prepare final measurement placeholder. Parser error at line %1: %2.</source>
        <translation>Champ de mesure finale invalide. Erreur d&apos;analyse ligne %1: %2.</translation>
    </message>
    <message>
        <source>No data for the height dimension.</source>
        <translation>Absence de données pour la dimension stature.</translation>
    </message>
    <message>
        <source>No data for the size dimension.</source>
        <translation>Absence de données pour la dimension taille commerciale.</translation>
    </message>
    <message>
        <source>No data for the hip dimension.</source>
        <translation>Absence de données pour la dimension tour de hanches.</translation>
    </message>
    <message>
        <source>No data for the waist dimension.</source>
        <translation>Absence de données pour la dimension tour de taille.</translation>
    </message>
    <message>
        <source>Piece &apos;%1&apos;. Grainline is not valid.</source>
        <translation>Piece &apos;%1&apos;. Droit fil non valide.</translation>
    </message>
    <message>
        <source>Invalid global value for a passmark length. Piece &apos;%1&apos;. Length is less than minimal allowed.</source>
        <translation>Valeur par défaut de longueur de cran d&apos;assemblage invalide. Pièce &apos;%1&apos;. La longueur est inférieure à la longueur minimale autorisée.</translation>
    </message>
    <message>
        <source>move pieces</source>
        <translation>déplacer les pièces</translation>
    </message>
    <message>
        <source>rotate pieces</source>
        <translation>Rotation des pièces</translation>
    </message>
    <message>
        <source>Cannot set printer page size</source>
        <translation>Impossible de définir la taille de page d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot set printer margins</source>
        <translation>Impossible de définir les marges d&apos;impression de l&apos;imprimante</translation>
    </message>
    <message>
        <source>Unable to cut curve &apos;%1&apos;. The curve is too short.</source>
        <translation>Impossible de segmenter la courbe &apos;%1&apos;. La longueur de la courbe est insuffisante.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
    <message>
        <source>Point &apos;%1&apos; repeats twice</source>
        <translation>Le point &apos;%1&apos; se répète deux fois</translation>
    </message>
    <message>
        <source>Points &apos;%1&apos; and &apos;%2&apos; have the same coordinates.</source>
        <translation>Les points &apos;%1&apos; et &apos;%2&apos; ont les mêmes coordonnées.</translation>
    </message>
    <message>
        <source>Leave only one copy of curve &apos;%1&apos;</source>
        <translation>Ne laisser qu&apos;une copie de la courbe &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Point &apos;%1&apos; does not lie on a curve &apos;%2&apos;</source>
        <translation>Le point &apos;%1&apos; n&apos;est pas situé sur la courbe &apos;%2&apos;</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <source>Confirm deletion</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <source>Do you really want to delete?</source>
        <translation>Voulez vous vraiment supprimer?</translation>
    </message>
    <message>
        <source>No data for the X dimension.</source>
        <translation>Pas de données pour la dimension X.</translation>
    </message>
    <message>
        <source>No data for the Y dimension.</source>
        <translation>Pas de données pour la dimension Y.</translation>
    </message>
    <message>
        <source>No data for the Z dimension.</source>
        <translation>Pas de données pour la dimension Z.</translation>
    </message>
    <message>
        <source>No data for the W dimension.</source>
        <translation>Pas de données pour la dimension W.</translation>
    </message>
    <message>
        <source>z value move piece</source>
        <translation>Valeur de déplacement Z de la pièce (vers avant ou arrière-plan)</translation>
    </message>
    <message>
        <source>z value move pieces</source>
        <translation>Valeur de déplacement Z de la pièce (vers avant ou arrière-plan)</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
</context>
<context>
    <name>QSaveFile</name>
    <message>
        <source>Existing file %1 is not writable</source>
        <translation type="vanished">Fichier existant %1 n&apos;est pas modifiable</translation>
    </message>
    <message>
        <source>Writing canceled by application</source>
        <translation type="vanished">L&apos;enregistrement a été annulé par le logiciel</translation>
    </message>
    <message>
        <source>Partial write. Partition full?</source>
        <translation type="vanished">Partiellement enregistré. La partition est pleine?</translation>
    </message>
</context>
<context>
    <name>QmuParser</name>
    <message>
        <source>too few arguments for function sum.</source>
        <comment>parser error message</comment>
        <translation>manque d&apos;arguments pour la fonction somme.</translation>
    </message>
    <message>
        <source>too few arguments for function min.</source>
        <comment>parser error message</comment>
        <translation>manque d&apos;argument pour la fonction soustraction.</translation>
    </message>
</context>
<context>
    <name>QmuParserErrorMsg</name>
    <message>
        <source>Unexpected token &quot;$TOK$&quot; found at position $POS$.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Symbole &quot;$TOK$&quot; inattendu à l&apos;emplacement $POS$.</translation>
    </message>
    <message>
        <source>Internal error</source>
        <comment>Math parser error messages.</comment>
        <translation>Erreur interne</translation>
    </message>
    <message>
        <source>Invalid function-, variable- or constant name: &quot;$TOK$&quot;.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>Nom invalide : fonction, variable, ou constante &quot;$TOK$&quot;.</translation>
    </message>
    <message>
        <source>Invalid binary operator identifier: &quot;$TOK$&quot;.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>L&apos;identifiant d&apos;opération binaire &quot;$TOK$&quot; est invalide.</translation>
    </message>
    <message>
        <source>Invalid infix operator identifier: &quot;$TOK$&quot;.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>L&apos;identifiant de l&apos;opérateur infixe &quot;$TOK$&quot; est invalide.</translation>
    </message>
    <message>
        <source>Invalid postfix operator identifier: &quot;$TOK$&quot;.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>L&apos;identifiant de l&apos;opérateur postfixe &quot;$TOK$&quot; est invalide.</translation>
    </message>
    <message>
        <source>Invalid pointer to callback function.</source>
        <comment>Math parser error messages.</comment>
        <translation>Le pointeur de fonction de rappel est invalide.</translation>
    </message>
    <message>
        <source>Expression is empty.</source>
        <comment>Math parser error messages.</comment>
        <translation>Champ vide.</translation>
    </message>
    <message>
        <source>Invalid pointer to variable.</source>
        <comment>Math parser error messages.</comment>
        <translation>Le pointeur de variable est invalide.</translation>
    </message>
    <message>
        <source>Unexpected operator &quot;$TOK$&quot; found at position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Opérateur &quot;$TOK$&quot; inattendu à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected end of expression at position $POS$</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>Fin inattendue d&apos;expression à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected argument separator at position $POS$</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>Séparateur d&apos;arguments inattendu à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected parenthesis &quot;$TOK$&quot; at position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Parenthèse &quot;$TOK$&quot; inattendue à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected function &quot;$TOK$&quot; at position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Fonction &quot;$TOK$&quot; inattendue à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected value &quot;$TOK$&quot; found at position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Valeur &quot;$TOK$&quot; inattendue à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Unexpected variable &quot;$TOK$&quot; found at position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Variable &quot;$TOK$&quot; inattendue à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Function arguments used without a function (position: $POS$)</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>Pas de fonction spécifiée pour les arguments (emplacement : $POS$)</translation>
    </message>
    <message>
        <source>Missing parenthesis</source>
        <comment>Math parser error messages.</comment>
        <translation>Oubli de parenthèse</translation>
    </message>
    <message>
        <source>Too many parameters for function &quot;$TOK$&quot; at expression position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Trop d&apos;arguments pour la fonction &quot;$TOK$&quot; à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Too few parameters for function &quot;$TOK$&quot; at expression position $POS$</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>Pas assez d&apos;arguments pour la fonction &quot;$TOK$&quot; à l&apos;emplacement $POS$</translation>
    </message>
    <message>
        <source>Divide by zero</source>
        <comment>Math parser error messages.</comment>
        <translation>Division par zéro</translation>
    </message>
    <message>
        <source>Domain error</source>
        <comment>Math parser error messages.</comment>
        <translation>Erreur de domaine</translation>
    </message>
    <message>
        <source>Name conflict</source>
        <comment>Math parser error messages.</comment>
        <translation>Conflit de nom</translation>
    </message>
    <message>
        <source>Invalid value for operator priority (must be greater or equal to zero).</source>
        <comment>Math parser error messages.</comment>
        <translation>La valeur de priorité de l&apos;opérateur est invalide. (Elle doit être supérieure ou égale à zéro).</translation>
    </message>
    <message>
        <source>user defined binary operator &quot;$TOK$&quot; conflicts with a built in operator.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>L&apos;opérateur binaire &quot;$TOK$&quot; défini par l&apos;utilisateur est en conflit avec un opérateur intrinsèque.</translation>
    </message>
    <message>
        <source>Unexpected string token found at position $POS$.</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>Chaîne de caractères inattendue à l&apos;emplacement $POS$.</translation>
    </message>
    <message>
        <source>Unterminated string starting at position $POS$.</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>La chaîne de caractères commençant à la position $POS$ n&apos;est pas terminée.</translation>
    </message>
    <message>
        <source>String function called with a non string type of argument.</source>
        <comment>Math parser error messages.</comment>
        <translation>Fonction de chaîne de caractères appelée pour un argument d&apos;un autre type.</translation>
    </message>
    <message>
        <source>String value used where a numerical argument is expected.</source>
        <comment>Math parser error messages.</comment>
        <translation>Un argument numérique est attendu ici, pas une chaîne de caractères.</translation>
    </message>
    <message>
        <source>No suitable overload for operator &quot;$TOK$&quot; at position $POS$.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot; and $POS$</comment>
        <translation>L&apos;opérateur &quot;$TOK$&quot; à la position $POS$ n&apos;est pas défini pour des arguments de ce type.</translation>
    </message>
    <message>
        <source>Function result is a string.</source>
        <comment>Math parser error messages.</comment>
        <translation>Le résultat de la fonction est une chaîne de caractère.</translation>
    </message>
    <message>
        <source>Parser error.</source>
        <comment>Math parser error messages.</comment>
        <translation>Erreur d&apos;analyse syntaxique.</translation>
    </message>
    <message>
        <source>Decimal separator is identic to function argument separator.</source>
        <comment>Math parser error messages.</comment>
        <translation>Le séparateur des décimales est le même que le séparateur des arguments de fonction.</translation>
    </message>
    <message>
        <source>The &quot;$TOK$&quot; operator must be preceeded by a closing bracket.</source>
        <comment>Math parser error messages. Left untouched &quot;$TOK$&quot;</comment>
        <translation>L&apos;opérateur &quot;$TOK$&quot; doit être précédé d&apos;un crochet fermant.</translation>
    </message>
    <message>
        <source>If-then-else operator is missing an else clause</source>
        <comment>Math parser error messages. Do not translate operator name.</comment>
        <translation>Il manque une condition &quot;sinon&quot; dans l&apos;opérateur si-alors-sinon</translation>
    </message>
    <message>
        <source>Misplaced colon at position $POS$</source>
        <comment>Math parser error messages. Left untouched $POS$</comment>
        <translation>Deux-points &quot; : &quot; mal placés à l&apos;emplacement $POS$</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <source>Black</source>
        <translation>Noir</translation>
    </message>
    <message>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <source>Dark red</source>
        <translation>Rouge foncé</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <source>Dark green</source>
        <translation>Vert foncé</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <source>Dark blue</source>
        <translation>Bleu foncé</translation>
    </message>
    <message>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <source>Dark cyan</source>
        <translation>Cyan foncé</translation>
    </message>
    <message>
        <source>Magenta</source>
        <translation>Magenta</translation>
    </message>
    <message>
        <source>Dark magenta</source>
        <translation>Magenta foncé</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <source>Dark yellow</source>
        <translation>Jaune foncé</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation>Gris</translation>
    </message>
    <message>
        <source>Dark gray</source>
        <translation>Gris foncé</translation>
    </message>
    <message>
        <source>Light gray</source>
        <translation>Gris clair</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
</context>
<context>
    <name>RemoveItemFromGroup</name>
    <message>
        <source>Remove item from group</source>
        <translation>Retirer l&apos;élément du groupe</translation>
    </message>
</context>
<context>
    <name>RenameBackgroundImage</name>
    <message>
        <source>rename background image</source>
        <translation>Renommer l&apos;image</translation>
    </message>
</context>
<context>
    <name>RenameGroup</name>
    <message>
        <source>rename group</source>
        <translation>renommer un groupe</translation>
    </message>
</context>
<context>
    <name>RenamePP</name>
    <message>
        <source>rename pattern piece</source>
        <translation>renommer l&apos;élément de patron</translation>
    </message>
</context>
<context>
    <name>ResetBackgroundImage</name>
    <message>
        <source>reset background image transformation</source>
        <translation>Annuler les modifications</translation>
    </message>
</context>
<context>
    <name>RotateBackgroundImage</name>
    <message>
        <source>rotate background image</source>
        <translation>faire pivoter l&apos;image</translation>
    </message>
</context>
<context>
    <name>RotationMoveLabel</name>
    <message>
        <source>move point label</source>
        <translation type="vanished">déplace l&apos;étiquette de point</translation>
    </message>
</context>
<context>
    <name>SaveDetailOptions</name>
    <message>
        <source>save detail option</source>
        <translation type="vanished">sauvegarder les options de la pièce de patron</translation>
    </message>
</context>
<context>
    <name>SavePieceOptions</name>
    <message>
        <source>save detail options</source>
        <translation>Enregistrer les options de la pièce du patron</translation>
    </message>
</context>
<context>
    <name>SavePiecePathOptions</name>
    <message>
        <source>save path options</source>
        <translation>Sauvegarde les options de tracé</translation>
    </message>
</context>
<context>
    <name>SavePlaceLabelOptions</name>
    <message>
        <source>save place label options</source>
        <translation>Enregistrer les options de placement d&apos;étiquette</translation>
    </message>
</context>
<context>
    <name>SaveToolOptions</name>
    <message>
        <source>save tool option</source>
        <translation>options d&apos;outil de Sauvegarde</translation>
    </message>
</context>
<context>
    <name>ScaleBackgroundImage</name>
    <message>
        <source>scale background image</source>
        <translation>modifier l&apos;échelle de l&apos;image</translation>
    </message>
</context>
<context>
    <name>ShowDoubleLabel</name>
    <message>
        <source>toggle the first dart label</source>
        <translation>Basculer vers l&apos;étiquette de la première pince</translation>
    </message>
    <message>
        <source>togggle the second dart label</source>
        <translation>Basculer vers l&apos;étiquette de la seconde pince</translation>
    </message>
</context>
<context>
    <name>ShowLabel</name>
    <message>
        <source>toggle label</source>
        <translation>Attacher l&apos;étiquette</translation>
    </message>
</context>
<context>
    <name>TMainWindow</name>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Select New for creation measurement file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Cliquez sur Nouveau pour créer un fichier de mesures vierge.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Calculated value</source>
        <translation>Valeur calculée</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation type="vanished">Formule</translation>
    </message>
    <message>
        <source>Base value</source>
        <translation type="vanished">Valeur de base</translation>
    </message>
    <message>
        <source>In sizes</source>
        <translation type="vanished">En tailles</translation>
    </message>
    <message>
        <source>In heights</source>
        <translation type="vanished">En statures</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Pièces</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afficher le calcul complet &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Base value:</source>
        <translation>Valeur de base :</translation>
    </message>
    <message>
        <source>In sizes:</source>
        <translation type="vanished">En tailles :</translation>
    </message>
    <message>
        <source>In heights:</source>
        <translation type="vanished">En statures :</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <source>Move measurement up</source>
        <translation>Déplacer la mesure vers le haut</translation>
    </message>
    <message>
        <source>Move measurement down</source>
        <translation>Déplacer la mesure vers le bas</translation>
    </message>
    <message>
        <source>Calculated value:</source>
        <translation>Valeur calculée:</translation>
    </message>
    <message>
        <source>Full name:</source>
        <translation>Nom complet:</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Informations</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <source>Measurement type</source>
        <translation>Type de mesures</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation>Chemin d&apos;accès:</translation>
    </message>
    <message>
        <source>Path to file</source>
        <translation type="vanished">Chemin d&apos;accès</translation>
    </message>
    <message>
        <source>Show in Explorer</source>
        <translation>Afficher dans l&apos;explorateur</translation>
    </message>
    <message>
        <source>Base size:</source>
        <translation type="vanished">Taille de base:</translation>
    </message>
    <message>
        <source>Base size value</source>
        <translation type="vanished">Valeur de taille de base</translation>
    </message>
    <message>
        <source>Base height:</source>
        <translation type="vanished">Stature de base:</translation>
    </message>
    <message>
        <source>Base height value</source>
        <translation type="vanished">Valeur de stature de base</translation>
    </message>
    <message>
        <source>Given name:</source>
        <translation type="vanished">Prénom:</translation>
    </message>
    <message>
        <source>Family name:</source>
        <translation type="vanished">Nom de famille:</translation>
    </message>
    <message>
        <source>Birth date:</source>
        <translation>Date de naissance:</translation>
    </message>
    <message>
        <source>yyyy-MM-dd</source>
        <translation type="vanished">aaaa-mm-jj</translation>
    </message>
    <message>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <source>Notes:</source>
        <translation>Notes:</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Fichier</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">Fenêtre</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Aide</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation>Mesures</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <source>Gradation</source>
        <translation>Gradation</translation>
    </message>
    <message>
        <source>Open individual ...</source>
        <translation type="vanished">Ouvrir individuelles ...</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Save As ...</source>
        <translation type="vanished">Enregistrer sous ...</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Quitter</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <source>About Tape</source>
        <translation type="vanished">&amp;A propos de Tape</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nouveau</translation>
    </message>
    <message>
        <source>Add known</source>
        <translation>Ajouter connue</translation>
    </message>
    <message>
        <source>Add custom</source>
        <translation>Ajouter personnalisée</translation>
    </message>
    <message>
        <source>Read only</source>
        <translation>Lecture seule</translation>
    </message>
    <message>
        <source>Open standard ...</source>
        <translation type="vanished">Ouvrir standard ...</translation>
    </message>
    <message>
        <source>Open template</source>
        <translation>Ouvrir modèle</translation>
    </message>
    <message>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
    <message>
        <source>Show information about all known measurement</source>
        <translation>Afficher les informations de toutes les mensurations connues</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>untitled %1</source>
        <translation>sans nom %1</translation>
    </message>
    <message>
        <source>This file already opened in another window.</source>
        <translation>Ce fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>Could not save file</source>
        <translation>Le fichier n&apos;a pas pu etre enregistré</translation>
    </message>
    <message>
        <source>measurements</source>
        <translation>mesures</translation>
    </message>
    <message>
        <source>Individual measurements (*.vit)</source>
        <translation type="vanished">Mesures individuelles (*.vit)</translation>
    </message>
    <message>
        <source>Standard measurements (*.vst)</source>
        <translation type="vanished">Mesures multi-tailles (*.vst)</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <source>&amp;New Window</source>
        <translation>&amp;Nouvelle Fenêtre</translation>
    </message>
    <message>
        <source>Edit measurement</source>
        <translation>Editer les mesures</translation>
    </message>
    <message>
        <source>M_%1</source>
        <translation type="vanished">M_%1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Empty field.</source>
        <translation>Champ vide.</translation>
    </message>
    <message>
        <source>Parser error: %1</source>
        <translation>Erreur analyseur : %1</translation>
    </message>
    <message>
        <source>Standard measurements</source>
        <translation type="vanished">Mesures standard</translation>
    </message>
    <message>
        <source>Height: </source>
        <translation type="vanished">Stature :</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="vanished">Taille:</translation>
    </message>
    <message>
        <source>Individual measurements</source>
        <translation>Mesures individuelles</translation>
    </message>
    <message>
        <source>untitled</source>
        <translation>sans nom</translation>
    </message>
    <message>
        <source>&lt;Empty&gt;</source>
        <translation type="vanished">&lt;Empty&gt;</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Modifications non enregistrées</translation>
    </message>
    <message>
        <source>Measurements have been modified.
Do you want to save your changes?</source>
        <translation>Les mesures ont été modifiées.
Voulez-vous enregistrer vos changements ?</translation>
    </message>
    <message>
        <source>Empty field</source>
        <translation>Champ vide</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <source>Import from a pattern</source>
        <translation>Importer depuis un patron</translation>
    </message>
    <message>
        <source>Pattern files (*.val)</source>
        <translation>Fichier de patron (*.val)</translation>
    </message>
    <message>
        <source>Pattern unit:</source>
        <translation>Unité du patron:</translation>
    </message>
    <message>
        <source>Find:</source>
        <translation type="vanished">Rechercher:</translation>
    </message>
    <message>
        <source>Find Previous</source>
        <translation type="vanished">Résultat précédent</translation>
    </message>
    <message>
        <source>Ctrl+Shift+G</source>
        <translation type="vanished">Ctrl+Shift+G</translation>
    </message>
    <message>
        <source>Find Next</source>
        <translation type="vanished">Résultat suivant</translation>
    </message>
    <message>
        <source>Ctrl+G</source>
        <translation type="vanished">Ctrl+G</translation>
    </message>
    <message>
        <source>Individual measurements (*.vit);;Standard measurements (*.vst);;All files (*.*)</source>
        <translation type="vanished">Mesures individuelles (*.vit);;Mesures multi-tailles (*.vst);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Standard measurements (*.vst);;Individual measurements (*.vit);;All files (*.*)</source>
        <translation type="vanished">Mesures multi-tailles (*.vst);;Mesures individuelles (*.vit);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Measurements (*.vst *.vit);;All files (*.*)</source>
        <translation type="vanished">Mesures (*.vst *.vit);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window.</source>
        <translation>Verrouillage impossible. Le fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window. Expect collissions when run 2 copies of the program.</source>
        <translation>Verrouillage impossible car le fichier est déjà ouvert dans une autre fenêtre. Ceci se produit généralement quand 2 copies du programme sont ouvertes en même temps.</translation>
    </message>
    <message>
        <source>File contains invalid known measurement(s).</source>
        <translation>Le fichier contient une ou des mensurations invalides (de type &apos;connue&apos;).</translation>
    </message>
    <message>
        <source>File has unknown format.</source>
        <translation>Format de fichier inconnu.</translation>
    </message>
    <message>
        <source>Full name</source>
        <translation type="vanished">Nom complet</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; doesn&apos;t exist!</source>
        <translation>Le fichier &apos;%1&apos; n&apos;existe pas !</translation>
    </message>
    <message>
        <source>The name of known measurement forbidden to change.</source>
        <translation>Il n&apos;est pas possible de modifier le nom de mesures de type &apos;connue&apos;.</translation>
    </message>
    <message>
        <source>Can&apos;t find measurement &apos;%1&apos;.</source>
        <translation>Impossible de trouver la mesure &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>The base value of known measurement forbidden to change.</source>
        <translation type="vanished">Il n&apos;est pas possible de modifier la valeur de base de mensurations connues.</translation>
    </message>
    <message>
        <source>The size increase value of known measurement forbidden to change.</source>
        <translation type="vanished">Il n&apos;est pas possible de modifier l&apos;intervalle d&apos;augmentation de taille de mesures connues.</translation>
    </message>
    <message>
        <source>The height increase value of known measurement forbidden to change.</source>
        <translation type="vanished">Il n&apos;est pas possible de modifier l&apos;intervalle d&apos;augmentation de stature de mesures connues.</translation>
    </message>
    <message>
        <source>The full name of known measurement forbidden to change.</source>
        <translation>Il n&apos;est pas possible de modifier le nom complet de mesures connues.</translation>
    </message>
    <message>
        <source>Name in formula</source>
        <translation type="vanished">Nom dans la formule</translation>
    </message>
    <message>
        <source>Function Wizard</source>
        <translation>Boîte de dialogue formule</translation>
    </message>
    <message>
        <source>Move measurement top</source>
        <translation>Déplacer la mesure vers le haut</translation>
    </message>
    <message>
        <source>Move measurement bottom</source>
        <translation>Déplacer la mesure vers le bas</translation>
    </message>
    <message>
        <source>Delete measurement</source>
        <translation>Supprimer la mesure</translation>
    </message>
    <message>
        <source>unknown</source>
        <comment>gender</comment>
        <translation>inconnu</translation>
    </message>
    <message>
        <source>male</source>
        <comment>gender</comment>
        <translation>homme</translation>
    </message>
    <message>
        <source>female</source>
        <comment>gender</comment>
        <translation>femme</translation>
    </message>
    <message>
        <source>Gender:</source>
        <translation>Genre:</translation>
    </message>
    <message>
        <source>PM system:</source>
        <translation>Méthode de patronage:</translation>
    </message>
    <message>
        <source>Create from existing ...</source>
        <translation type="vanished">Créer depuis individuelles existantes …</translation>
    </message>
    <message>
        <source>Create from existing file</source>
        <translation>Créer à partir d&apos;un fichier existant</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <source>Export standard measurements not supported.</source>
        <translation type="vanished">L&apos;export des mensurations multi-tailles n&apos;est pas pris en charge.</translation>
    </message>
    <message>
        <source>Measurement diagram</source>
        <translation>Diagramme des mesures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:340pt;&quot;&gt;?&lt;/span&gt;&lt;/p&gt;&lt;p align=\&quot;center\&quot;&gt;Unknown measurement&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:340pt;&quot;&gt;?&lt;/span&gt;&lt;/p&gt;&lt;p align=\&quot;center\&quot;&gt;Mesure inconnue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:340pt;&quot;&gt;?&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Unknown measurement&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:340pt;&quot;&gt;?&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Mesure inconnue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <source>File was not saved yet.</source>
        <translation>Fichier non sauvegardé.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Measurement&apos;s name in a formula</source>
        <translation>Nom de la mesure tel qu&apos;il apparaîtra dans une formule de calcul</translation>
    </message>
    <message>
        <source>Measurement&apos;s name in a formula.</source>
        <translation>Nom de la mesure tel qu&apos;il apparaîtra dans une formule de calcul.</translation>
    </message>
    <message>
        <source>Measurement&apos;s human-readable name.</source>
        <translation>Nom développé de la mesure.</translation>
    </message>
    <message>
        <source>Customer&apos;s name.</source>
        <translation type="vanished">Prénom du client.</translation>
    </message>
    <message>
        <source>Customer&apos;s family name.</source>
        <translation type="vanished">Nom de famille du client.</translation>
    </message>
    <message>
        <source>Customer&apos;s email address.</source>
        <translation type="vanished">Adresse email du client.</translation>
    </message>
    <message>
        <source>Save...</source>
        <translation type="vanished">Sauvegarder ...</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>Ne pas sauvegarder</translation>
    </message>
    <message>
        <source>Locking file</source>
        <translation type="vanished">Verrouiller le fichier (lecture seule)</translation>
    </message>
    <message>
        <source>This file already opened in another window. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Ce fichier est déjà ouvert dans une autre fenêtre. Ignorer pour continuer quand même (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Le fichier sous lecture seule ne peut être créé car vous n&apos;avez pas les permissions nécessaires. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation type="vanished">Une erreur inconnue s&apos;est produite, par exemple pour cause de partition pleine. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions.</source>
        <translation type="vanished">Impossible de créer le fichier en lecture seule, vous n&apos;avez pas les permissions nécessaires.</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file.</source>
        <translation type="vanished">Une erreur inconnue s&apos;est produite, il est possible qu&apos;une partition pleine empêche d&apos;écrire le fichier en lecture seule.</translation>
    </message>
    <message>
        <source>Export to CSV</source>
        <translation>Exporter sous format .csv</translation>
    </message>
    <message>
        <source>Comma-Separated Values (*.cvs)</source>
        <translation type="vanished">Tableur format texte (*.csv)</translation>
    </message>
    <message>
        <source>Invalid value</source>
        <translation type="vanished">Valeur non valide</translation>
    </message>
    <message>
        <source>Show in Finder</source>
        <translation>Afficher dans l&apos;explorateur</translation>
    </message>
    <message>
        <source>Comma-Separated Values</source>
        <translation>Tableur format texte</translation>
    </message>
    <message>
        <source>Customer&apos;s name</source>
        <translation>Nom du client</translation>
    </message>
    <message>
        <source>Customer&apos;s family name</source>
        <translation type="vanished">Nom de famille du client</translation>
    </message>
    <message>
        <source>Customer&apos;s email address</source>
        <translation>Adresse email du client</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation type="vanished">Stature:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="vanished">Taille :</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <source>Could not save the file</source>
        <translation>Le fichier n&apos;a pas pu être enregistré</translation>
    </message>
    <message>
        <source>read only</source>
        <translation>Lecture seule</translation>
    </message>
    <message>
        <source>Multisize measurements</source>
        <translation>Mensurations multi-tailles</translation>
    </message>
    <message>
        <source>Invalid result. Value is infinite or NaN. Please, check your calculations.</source>
        <translation>Résultat invalide. La valeur est infinie ou n&apos;est pas une valeur numérique. Veuillez vérifier vos calculs.</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vide</translation>
    </message>
    <message>
        <source>Customer name:</source>
        <translation>Nom du client :</translation>
    </message>
    <message>
        <source>Open multisize …</source>
        <translation>Ouvrir mesures multi-tailles …</translation>
    </message>
    <message>
        <source>Create from existing …</source>
        <translation>Créer depuis individuelles existantes …</translation>
    </message>
    <message>
        <source>Save…</source>
        <translation>Enregistrer …</translation>
    </message>
    <message>
        <source>Import from CSV</source>
        <translation>Importer d&apos;un fichier .csv</translation>
    </message>
    <message>
        <source>Individual measurements require at least 2 columns.</source>
        <translation>2 colonnes minimum doivent être remplies en mesures individuelles.</translation>
    </message>
    <message>
        <source>Error in row %1.</source>
        <translation>Erreur dans la ligne %1.</translation>
    </message>
    <message>
        <source>Multisize measurements require at least 4 columns.</source>
        <translation>4 colonnes minimum doivent être remplies en mesures multi-tailles.</translation>
    </message>
    <message>
        <source>Imported file must not contain the same name twice.</source>
        <translation>Le fichier importé ne doit pas contenir deux fois le même nom.</translation>
    </message>
    <message>
        <source>Cannot save settings. Access denied.</source>
        <translation>Impossible d&apos;enregistrer les réglages. Accès refusé.</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Window</source>
        <translation>&amp;Fenêtre</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Aid&amp;e</translation>
    </message>
    <message>
        <source>&amp;Measurements</source>
        <translation>&amp;Mesures</translation>
    </message>
    <message>
        <source>&amp;Open individual …</source>
        <translation>Ouvrir mesures individuelles …</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Save &amp;As …</source>
        <translation>S&amp;auvegarder sous …</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;About Tape</source>
        <translation>&amp;A propos de Tape</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>Cannot read settings from a malformed .INI file.</source>
        <translation>Lecture des réglages impossible, fichier .INI invalide.</translation>
    </message>
    <message>
        <source>Name</source>
        <comment>measurement column</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Full name</source>
        <comment>measurement column</comment>
        <translation>Nom complet</translation>
    </message>
    <message>
        <source>Calculated value</source>
        <comment>measurement column</comment>
        <translation>Valeur calculée</translation>
    </message>
    <message>
        <source>Formula</source>
        <comment>measurement column</comment>
        <translation>Formule</translation>
    </message>
    <message>
        <source>Base value</source>
        <comment>measurement column</comment>
        <translation>Valeur de base</translation>
    </message>
    <message>
        <source>Correction</source>
        <comment>measurement column</comment>
        <translation>Correction</translation>
    </message>
    <message>
        <source>Units:</source>
        <translation>Unités :</translation>
    </message>
    <message>
        <source>Shift B</source>
        <translation>Shift B</translation>
    </message>
    <message>
        <source>Shift C</source>
        <translation>Shift C</translation>
    </message>
    <message>
        <source>Correction:</source>
        <translation>Correction:</translation>
    </message>
    <message>
        <source>Dimension:</source>
        <translation>Mesure :</translation>
    </message>
    <message>
        <source>Select one of the dimensions to later use the measurement value in piece label</source>
        <translation>Sélectionnez l&apos;une des mesures pour pouvoir utiliser ultérieurement sa valeur dans une étiquette de mentions du patron</translation>
    </message>
    <message>
        <source>Base Values:</source>
        <translation>Valeurs de base :</translation>
    </message>
    <message>
        <source>Export to individual</source>
        <translation>Exporter comme mesures individuelles</translation>
    </message>
    <message>
        <source>Export to individual measurements</source>
        <translation>Exporter comme mesures individuelles</translation>
    </message>
    <message>
        <source>Use full circumference</source>
        <translation>Avec circonférence entière</translation>
    </message>
    <message>
        <source>Restrict second dimension</source>
        <translation>Moins de valeurs pour mesure Y</translation>
    </message>
    <message>
        <source>Restrict third dimension</source>
        <translation>Moins de valeurs pour mesure Z</translation>
    </message>
    <message>
        <source>Dimension labels</source>
        <translation>Libellés des valeurs</translation>
    </message>
    <message>
        <source>The table doesn&apos;t provide dimensions</source>
        <translation>La table de mesures ne fournit pas les dimensions nécessaires</translation>
    </message>
    <message>
        <source>Invalid base value for dimension A</source>
        <translation>Valeur de base invalide pour la mesure A</translation>
    </message>
    <message>
        <source>The table doesn&apos;t support dimension B</source>
        <translation>Cette table ne prend pas en charge la mesure B</translation>
    </message>
    <message>
        <source>Invalid base value for dimension B</source>
        <translation>Valeur de base invalide pour la mesure B</translation>
    </message>
    <message>
        <source>The table doesn&apos;t support dimension C</source>
        <translation>Cette table ne prend pas en charge la mesure C</translation>
    </message>
    <message>
        <source>Invalid base value for dimension C</source>
        <translation>Valeur de base invalide pour la mesure C</translation>
    </message>
    <message>
        <source>measurements.vit</source>
        <translation>mesures-individuelles.vit</translation>
    </message>
    <message>
        <source>Shift (%1):</source>
        <translation>Intervalle (%1):</translation>
    </message>
    <message>
        <source>%1 shift</source>
        <translation>%1 intervalle</translation>
    </message>
    <message>
        <source>Cannot convert base value to double in column 2.</source>
        <translation>Impossible de doubler la valeur de base de la colonne 2.</translation>
    </message>
    <message>
        <source>Cannot convert shift value to double in column %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Millimeters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Degrees</source>
        <translation>Degrés</translation>
    </message>
    <message>
        <source>Restrict first dimension</source>
        <translation>Moins de valeurs pour mesure X</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Select New to create a measurement file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Cliquez sur Nouveau pour créer un fichier de mesures vierge.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search history &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recherches précédentes &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+Down</source>
        <translation>Alt+Down</translation>
    </message>
    <message>
        <source>0 results</source>
        <translation>0 résultat trouvé</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match Case &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient au moins &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match words &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contient exactement &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Match with regular expressions &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Avec expression régulière &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use unicode properties &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;The meaning of the \w, \d, etc., character classes, as well as the meaning of their counterparts (\W, \D, etc.), is changed from matching ASCII characters only to matching any character with the corresponding Unicode property. For instance, \d is changed to match any character with the Unicode Nd (decimal digit) property; \w to match any character with either the Unicode L (letter) or N (digit) property, plus underscore, and so on. This option corresponds to the /u modifier in Perl regular expressions.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;utilisent les propriétés unicode &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;span style=&quot; color:#888a85;&quot;&gt;La signification de \w, \d, etc., appelés classes de caractères, aussi bien que la signification de leurs équivalents (\W, \D, etc.), est modifiée pour correspondre aux caractères ASCII de n&apos;importe quel caractère ayant la propriété Unicode correspondante. Par exemple, \d est modifié pour correspondre à n&apos;importe quel caractère ayant la propriété Unicode Nd (chiffre decimal ou decimal digit); \w pour correspondre à n&apos;importe quel caractère ayant soit la propriété Unicode L (lettre) ou N (chiffre), plus underscore, etc. Cette option correspond au modificateur /u dans les expressions régulières Perl.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Previous &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat précédent &lt;span style=&quot; color:#888a85;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Next %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Résultat suivant %1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <source>Add separator</source>
        <translation>Ajouter séparateur</translation>
    </message>
    <message>
        <source>Dimension custom names</source>
        <translation>Noms personnalisés dimensions</translation>
    </message>
    <message>
        <source>Measurement name is empty.</source>
        <translation>Le champ &apos;nom de la mesure&apos; n&apos;est pas renseigné.</translation>
    </message>
    <message>
        <source>Measurement &apos;%1&apos; doesn&apos;t match regex pattern.</source>
        <translation>La mesure &apos;%1&apos; ne correspond pas au système de codage de caractères.</translation>
    </message>
    <message>
        <source>Measurement &apos;%1&apos; already used in the file.</source>
        <translation>La mesure &apos;%1&apos; est déjà utilisée dans le fichier.</translation>
    </message>
    <message>
        <source>Measurement &apos;%1&apos; is not one of the known measurements.</source>
        <translation>La mesure &apos;%1&apos; n&apos;est pas de type &apos;connue&apos;.</translation>
    </message>
    <message>
        <source>Measurement &apos;%1&apos; already used in file.</source>
        <translation>La mesure &apos;%1&apos; est déjà utilisée dans le fichier.</translation>
    </message>
    <message>
        <source>Error in row %1. The measurement name is empty.</source>
        <translation>Erreur dans la ligne %1. Le nom de la mesure n&apos;est pas renseigné.</translation>
    </message>
    <message>
        <source>The measurement name is empty.</source>
        <translation>Le champ &apos;nom de la mesure&apos; n&apos;est pas renseigné.</translation>
    </message>
</context>
<context>
    <name>TabGrainline</name>
    <message>
        <source>Grainline visible</source>
        <translation>Droit fil visible</translation>
    </message>
    <message>
        <source>Rotation:</source>
        <translation>Rotation :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Montrer le calcul complet dans une boite de dialogue &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Center pin:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Top pin:</source>
        <translation>Epingle supérieure :</translation>
    </message>
    <message>
        <source>Bottom pin:</source>
        <translation>Épingle inférieure :</translation>
    </message>
    <message>
        <source>Arrows:</source>
        <translation>Flèches :</translation>
    </message>
</context>
<context>
    <name>TabLabels</name>
    <message>
        <source>Piece label data</source>
        <translation>Données de l&apos;étiquette</translation>
    </message>
    <message>
        <source>Letter:</source>
        <translation>Lettre :</translation>
    </message>
    <message>
        <source>Letter of pattern piece</source>
        <translation>Lettre de la pièce de patron</translation>
    </message>
    <message>
        <source>Placement:</source>
        <translation>Placement:</translation>
    </message>
    <message>
        <source>Labels</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant de composition de formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Montrer le calcul complet dans une boite de dialogue &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur:</translation>
    </message>
    <message>
        <source>Center pin:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Top left pin:</source>
        <translation>Épingle supérieure gauche :</translation>
    </message>
    <message>
        <source>Bottom right pin:</source>
        <translation>Épingle inférieure droite :</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Pattern label visible</source>
        <translation>Etiquette de patron visible</translation>
    </message>
    <message>
        <source>Label template:</source>
        <translation>Modèle d&apos;étiquette :</translation>
    </message>
    <message>
        <source>Edit template</source>
        <translation>Modifier le modèle</translation>
    </message>
    <message>
        <source>Label data</source>
        <translation>Données de l&apos;étiquette</translation>
    </message>
    <message>
        <source>Quantity:</source>
        <translation>Quantité :</translation>
    </message>
    <message>
        <source>on fold</source>
        <translation>Au pli</translation>
    </message>
    <message>
        <source>Annotation:</source>
        <translation>Annotation :</translation>
    </message>
    <message>
        <source>A text field to add comments in</source>
        <translation>Un champ de texte pour ajouter des commentaires</translation>
    </message>
    <message>
        <source>Orientation:</source>
        <translation>Orientation :</translation>
    </message>
    <message>
        <source>Rotation:</source>
        <translation>Rotation :</translation>
    </message>
    <message>
        <source>Tilt:</source>
        <translation>Basculer :</translation>
    </message>
    <message>
        <source>Fold position:</source>
        <translation>Position de la pliure :</translation>
    </message>
    <message>
        <source>Edit piece label template</source>
        <translation>Éditer le modèle d&apos;étiquette de pièce</translation>
    </message>
    <message>
        <source>Options to control position a detail label. &lt;b&gt;Not available if a detail label template is empty&lt;/b&gt;.</source>
        <translation>Options de réglage de la position d&apos;une étiquette de pièce. &lt;b&gt;Non réglables si le modèle d&apos;étiquette des mentions de la pièce est vide&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Options to control position a pattern label. &lt;b&gt;Not available if a pattern label template is empty&lt;/b&gt;.</source>
        <translation>Options de réglages de la position de l&apos;étiquette de patron. &lt;b&gt;Non réglables si le modèle d&apos;étiquette des mentions du patron est vide&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Pattern label data</source>
        <translation>Données du patron</translation>
    </message>
    <message>
        <source>Edit pattern label</source>
        <translation>Modifier l&apos;étiquette du patron</translation>
    </message>
    <message>
        <source>Pattern name:</source>
        <translation>Nom du patron :</translation>
    </message>
    <message>
        <source>Pattern number:</source>
        <translation>Numéro de patron :</translation>
    </message>
    <message>
        <source>Company/Designer name:</source>
        <translation>Nom de Société/Modéliste :</translation>
    </message>
    <message>
        <source>Customer name:</source>
        <translation>Nom du client :</translation>
    </message>
    <message>
        <source>Date format:</source>
        <translation>Format de date:</translation>
    </message>
    <message>
        <source>Time format:</source>
        <translation>Format d&apos;heure:</translation>
    </message>
    <message>
        <source>Customer birth date:</source>
        <translation>Date de naissance client :</translation>
    </message>
    <message>
        <source>yyyy-MM-dd</source>
        <translation>yyyy-MM-dd</translation>
    </message>
    <message>
        <source>Customer email:</source>
        <translation>Email client :</translation>
    </message>
    <message>
        <source>Materials</source>
        <translation>Matières</translation>
    </message>
    <message>
        <source>Pattern materials:</source>
        <translation>Matériaux du patron :</translation>
    </message>
    <message>
        <source>Manage list of pattern materials</source>
        <translation>Gérer la liste des matériaux du patron</translation>
    </message>
    <message>
        <source>Manage</source>
        <translation>Gérer</translation>
    </message>
    <message>
        <source>Piece label visible</source>
        <translation>Etiquette de pièce visible</translation>
    </message>
</context>
<context>
    <name>TabPassmarks</name>
    <message>
        <source>Passmark:</source>
        <translation>Cran d&apos;assemblage :</translation>
    </message>
    <message>
        <source>One line</source>
        <translation>Un trait</translation>
    </message>
    <message>
        <source>Two lines</source>
        <translation>Deux traits</translation>
    </message>
    <message>
        <source>Three lines</source>
        <translation>Trois traits</translation>
    </message>
    <message>
        <source>T mark</source>
        <translation>Marque T</translation>
    </message>
    <message>
        <source>V mark</source>
        <translation>Marque V</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Straightforward</source>
        <translation>Tout droit</translation>
    </message>
    <message>
        <source>Bisector</source>
        <translation>Bissectrice</translation>
    </message>
    <message>
        <source>Marks</source>
        <translation>Repères</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Intersection</source>
        <translation>Intersection</translation>
    </message>
    <message>
        <source>Show the second passmark on seam line</source>
        <translation>Afficher le second cran d&apos;assemblage sur la ligne de couture</translation>
    </message>
    <message>
        <source>This option has effect only if the second passmark on seam line enabled in global preferences. The option helps disable the second passmark for this passmark only.</source>
        <translation>Cette option n&apos;a d&apos;effet que si la présence d&apos;un second cran d&apos;assemblage sur la ligne de couture, est activée dans les préférences de Valentina. Cette option ne désactive la présence du cran d&apos;assemblage que pour ce second repère.</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark. Show only left passmark.</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage. N&apos;affiche que le cran d&apos;assemblage de gauche.</translation>
    </message>
    <message>
        <source>Intersection (only left)</source>
        <translation>Intersection (seulement à gauche)</translation>
    </message>
    <message>
        <source>Select if need designate the corner point as a passmark. Show only right passmark.</source>
        <translation>Sélectionner si vous avez besoin d&apos;étalonner le coin comme un cran d&apos;assemblage. N&apos;affiche que le cran d&apos;assemblage de droite.</translation>
    </message>
    <message>
        <source>Intersection (only right)</source>
        <translation>Intersection (seulement à droite)</translation>
    </message>
    <message>
        <source>Intersection 2</source>
        <translation>Intersection 2</translation>
    </message>
    <message>
        <source>Intersection 2 (only left)</source>
        <translation>Intersection 2 (gauche seulement)</translation>
    </message>
    <message>
        <source>Intersection 2 (only right)</source>
        <translation>Intersection 2 (droite seulement)</translation>
    </message>
    <message>
        <source>Acute angle that looks outside of piece</source>
        <translation>En forme d&apos;angle aigu oreinté vers l&apos;extérieur de la pièce</translation>
    </message>
    <message>
        <source>V mark 2</source>
        <translation>Marquage en V (n° 2)</translation>
    </message>
    <message>
        <source>Manual length</source>
        <translation>Longueur personnalisée</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant formule</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voir le calcul dans une boite de dialogue&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>U mark</source>
        <translation>En forme de U</translation>
    </message>
    <message>
        <source>Box mark</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <source>List of all defined passmarks. To define a passmark return to the Main Path tab, call context menu for a point item and make it a passmark.</source>
        <translation>Liste de tous les crans d&apos;assemblage définis. Pour définir un nouveau cran d&apos;assemblage, revenir à l&apos;onglet du contour principal, ouvrir le menu contextuel d&apos;un des points du contour, et le choisir en tant que cran d&apos;assemblage.</translation>
    </message>
    <message>
        <source>Acute angle that looks inside of piece</source>
        <translation>En forme d&apos;angle aigu orienté vers l&apos;intérieur de la pièce</translation>
    </message>
</context>
<context>
    <name>TabPaths</name>
    <message>
        <source>Main path</source>
        <translation>Contour principal</translation>
    </message>
    <message>
        <source>All objects in path should follow in clockwise direction.</source>
        <translation>Tous les objets du chemin doivent se suivre dans le sens horaire.</translation>
    </message>
    <message>
        <source>Forbid piece be mirrored in a layout.</source>
        <translation>Interdire le placement d&apos;une pièce en miroir dans un plan de coupe.</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation>Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Ready!</source>
        <translation>Prêt !</translation>
    </message>
    <message>
        <source>Seam allowance</source>
        <translation>Marge de couture</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Formula wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Calculation</source>
        <translation>Formule</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show full calculation in message box&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Montrer le calcul complet dans une boite de dialogue &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation>Nœuds</translation>
    </message>
    <message>
        <source>Node:</source>
        <translation>Nœud :</translation>
    </message>
    <message>
        <source>Before:</source>
        <translation>Avant :</translation>
    </message>
    <message>
        <source>Return to default width</source>
        <translation>Revenir à la largeur par défaut</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>After:</source>
        <translation>Après :</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Start point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>End point:</source>
        <translation>Dernier point :</translation>
    </message>
    <message>
        <source>Include as:</source>
        <translation>Inclure comme :</translation>
    </message>
    <message>
        <source>Internal paths</source>
        <translation>Chemins internes</translation>
    </message>
    <message>
        <source>The seam allowance is part of main path</source>
        <translation>La marge de couture fait partie du chemin principal</translation>
    </message>
    <message>
        <source>Built in</source>
        <translation>Image intégrée</translation>
    </message>
    <message>
        <source>Hide the main path if the seam allowance is enabled</source>
        <translation>Cacher le chemin principal si la marge de couture est activée</translation>
    </message>
    <message>
        <source>Hide main path</source>
        <translation>Cacher le chemin principal</translation>
    </message>
    <message>
        <source>Name of detail:</source>
        <translation>Nom de la pièce de patron :</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation>Pièce de patron</translation>
    </message>
    <message>
        <source>Name can&apos;t be empty</source>
        <translation>La valeur Nom ne peut pas être vide</translation>
    </message>
    <message>
        <source>Force piece to be always flipped in a layout.</source>
        <translation>Forcer la pièce à toujours être retournée dans un plan de coupe.</translation>
    </message>
    <message>
        <source>Force flipping</source>
        <translation>Forcer la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Move on top</source>
        <translation>Déplacer au premier plan</translation>
    </message>
    <message>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <source>Move on bottom</source>
        <translation>Déplacer vers l&apos;arrière plan</translation>
    </message>
    <message>
        <source>Priority:</source>
        <translation>Priorité :</translation>
    </message>
    <message>
        <source>Controls priority in layout. 0 - no priority.</source>
        <translation>Contrôle la priorité dans le plan de coupe. 0 - pas de priorité.</translation>
    </message>
    <message>
        <source>Automatic</source>
        <comment>seam allowance</comment>
        <translation>Automatique</translation>
    </message>
    <message>
        <source>Custom</source>
        <comment>seam allowance</comment>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <source>Piece</source>
        <translation>Pièce</translation>
    </message>
    <message>
        <source>Universally Unique IDentifier of piece. Used to identifier piece while updating manual layout. Left empty to generate new value.</source>
        <translation>Identifiant unique universel UUID de la pièce. Utilisé pour identifier la pièce lorsque le plan de coupe est mis à jour manuellement. Laisser vide pour générer une nouvelle valeur.</translation>
    </message>
    <message>
        <source>Gradation label:</source>
        <translation>Etiquette de gradation :</translation>
    </message>
    <message>
        <source>Insert…</source>
        <translation>Insérer…</translation>
    </message>
</context>
<context>
    <name>TabPlaceLabels</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
</context>
<context>
    <name>TapeConfigDialog</name>
    <message>
        <source>Apply</source>
        <translation type="vanished">Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="vanished">&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="vanished">&amp;Ok</translation>
    </message>
    <message>
        <source>Config Dialog</source>
        <translation type="vanished">Boîte de configuration</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">Configuration</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation type="vanished">Chemins d&apos;accès</translation>
    </message>
</context>
<context>
    <name>TapeConfigurationPage</name>
    <message>
        <source>Language</source>
        <translation type="vanished">Langue</translation>
    </message>
    <message>
        <source>GUI language</source>
        <translation type="vanished">Langue de l&apos;interface</translation>
    </message>
    <message>
        <source>Pattern making system</source>
        <translation type="vanished">Programme de réalisation de patrons</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation type="vanished">Auteur:</translation>
    </message>
    <message>
        <source>Book:</source>
        <translation type="vanished">Livre:</translation>
    </message>
    <message>
        <source>Decimal separator parts</source>
        <translation type="vanished">Séparateur de décimale</translation>
    </message>
    <message>
        <source>With OS options (%1)</source>
        <translation type="vanished">Utiliser les réglages par défaut (%1)</translation>
    </message>
    <message>
        <source>GUI language:</source>
        <translation type="vanished">Langue de l&apos;interface:</translation>
    </message>
    <message>
        <source>Decimal separator parts:</source>
        <translation type="vanished">Séparateur de décimale:</translation>
    </message>
    <message>
        <source>Pattern making system:</source>
        <translation type="vanished">Programme de réalisation de patrons:</translation>
    </message>
    <message>
        <source>Default height and size</source>
        <translation type="vanished">Taille et stature par défaut</translation>
    </message>
    <message>
        <source>Default height:</source>
        <translation type="vanished">Stature par défaut:</translation>
    </message>
    <message>
        <source>Default size:</source>
        <translation type="vanished">Taille par défaut:</translation>
    </message>
</context>
<context>
    <name>TapePathPage</name>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">Ouvrir un dossier</translation>
    </message>
    <message>
        <source>Path that use Valentina</source>
        <translation type="vanished">Dossier de travail de Valentina</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Défault</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Éditer</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">Chemin</translation>
    </message>
    <message>
        <source>Individual measurements</source>
        <translation type="vanished">Mesures individuelles</translation>
    </message>
    <message>
        <source>Standard measurements</source>
        <translation type="vanished">Mesures standard</translation>
    </message>
    <message>
        <source>Templates</source>
        <translation type="vanished">Modèles</translation>
    </message>
</context>
<context>
    <name>TapePreferencesConfigurationPage</name>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>GUI language:</source>
        <translation>Langue de l&apos;interface:</translation>
    </message>
    <message>
        <source>Decimal separator parts:</source>
        <translation>Séparateur de décimale:</translation>
    </message>
    <message>
        <source>Pattern making system</source>
        <translation>Programme de réalisation de patrons</translation>
    </message>
    <message>
        <source>Pattern making system:</source>
        <translation>Programme de réalisation de patrons:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <source>Book:</source>
        <translation>Source:</translation>
    </message>
    <message>
        <source>Measurements editing</source>
        <translation>Édition des mensurations</translation>
    </message>
    <message>
        <source>Reset warnings</source>
        <translation>Afficher à nouveau les messages d&apos;avertissement</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <source>The text appears under the icon (recommended for beginners).</source>
        <translation>Un libellé accompagne les icônes (recommandé pour les débutants).</translation>
    </message>
    <message>
        <source>With OS options</source>
        <translation>Celui du système</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Thème de l&apos;interface</translation>
    </message>
    <message>
        <source>Activate dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>dark mode</source>
        <translation>Mode nuit</translation>
    </message>
    <message>
        <source>Don&apos;t use the native file dialog</source>
        <translation>Ne pas utiliser la boîte de dialogue de fichier native</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <source>Automatically check for updates each time the application starts</source>
        <translation>Vérification des mises à jour automatique à chaque démarrage</translation>
    </message>
    <message>
        <source>Automatically check for updates</source>
        <translation>Vérification des mises à jour automatique</translation>
    </message>
</context>
<context>
    <name>TapePreferencesPathPage</name>
    <message>
        <source>Paths that Valentina uses</source>
        <translation>Chemins d&apos;accès que Valentina utilise</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ouvrir Dossier</translation>
    </message>
    <message>
        <source>My Individual Measurements</source>
        <translation>Mes Mensurations</translation>
    </message>
    <message>
        <source>My Multisize Measurements</source>
        <translation>Mes Mensurations Multi-tailles</translation>
    </message>
    <message>
        <source>My Templates</source>
        <translation>Mes Modèles</translation>
    </message>
    <message>
        <source>My Patterns</source>
        <translation>Mes patrons</translation>
    </message>
    <message>
        <source>My Layouts</source>
        <translation>Mes plans de coupe</translation>
    </message>
</context>
<context>
    <name>ToggleDetailInLayout</name>
    <message>
        <source>detail in layout list</source>
        <translation type="vanished">Liste des pièces de patron dans le plan de coupe</translation>
    </message>
</context>
<context>
    <name>TogglePieceForceForbidFlipping</name>
    <message>
        <source>piece flipping</source>
        <translation>Reproduction en miroir de l&apos;élément</translation>
    </message>
</context>
<context>
    <name>TogglePieceInLayout</name>
    <message>
        <source>detail in layout list</source>
        <translation>Liste des pièces de patron dans le plan de coupe</translation>
    </message>
</context>
<context>
    <name>Utils::CheckableMessageBox</name>
    <message>
        <source>Do not ask again</source>
        <translation>Ne plus demander</translation>
    </message>
    <message>
        <source>Do not &amp;ask again</source>
        <translation>Ne plus &amp;demander</translation>
    </message>
    <message>
        <source>Do not &amp;show again</source>
        <translation>Ne plus &amp;montrer</translation>
    </message>
</context>
<context>
    <name>VAbstartMeasurementDimension</name>
    <message>
        <source>Invalid min/max range</source>
        <translation>Plage minimum/maximum invalide</translation>
    </message>
    <message>
        <source>Invalid step</source>
        <translation>Pas invalide</translation>
    </message>
    <message>
        <source>Base value invalid</source>
        <translation>Valeur de base invalide</translation>
    </message>
    <message>
        <source>Height</source>
        <comment>dimension</comment>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>Size</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Waist</source>
        <comment>dimension</comment>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Hip</source>
        <comment>dimension</comment>
        <translation>Hanche</translation>
    </message>
    <message>
        <source>Chest full circumference</source>
        <comment>dimension</comment>
        <translation>Tour de poitrine</translation>
    </message>
    <message>
        <source>Chest half circumference</source>
        <comment>dimension</comment>
        <translation>Demi tour de poitrine</translation>
    </message>
    <message>
        <source>Waist full circumference</source>
        <comment>dimension</comment>
        <translation>Tour de taille</translation>
    </message>
    <message>
        <source>Waist half circumference</source>
        <comment>dimension</comment>
        <translation>Demi tour de taille</translation>
    </message>
    <message>
        <source>Hip full circumference</source>
        <comment>dimension</comment>
        <translation>Tour de hanches</translation>
    </message>
    <message>
        <source>Hip half circumference</source>
        <comment>dimension</comment>
        <translation>Demi tour de hanches</translation>
    </message>
</context>
<context>
    <name>VAbstractConverter</name>
    <message>
        <source>Couldn&apos;t get version information.</source>
        <translation type="vanished">Impossible d&apos;obtenir les informations de version.</translation>
    </message>
    <message>
        <source>Too many tags &lt;%1&gt; in file.</source>
        <translation type="vanished">Trop d&apos;étiquettes &lt;%1&gt; dans le fichier.</translation>
    </message>
    <message>
        <source>Version &quot;%1&quot; invalid.</source>
        <translation type="vanished">Version &quot;%1&quot; invalide.</translation>
    </message>
    <message>
        <source>Version &quot;0.0.0&quot; invalid.</source>
        <translation type="vanished">Version &quot;0.0.0&quot; invalide.</translation>
    </message>
    <message>
        <source>Invalid version. Minimum supported version is %1</source>
        <translation type="vanished">Version non valide. La version minimale supportée est  %1</translation>
    </message>
    <message>
        <source>Invalid version. Maximum supported version is %1</source>
        <translation type="vanished">Version non valide. La version maximale supportée est  %1</translation>
    </message>
    <message>
        <source>Error no unique id.</source>
        <translation type="vanished">Erreur : id non unique.</translation>
    </message>
    <message>
        <source>Could not change version.</source>
        <translation>La version n&apos;a pas pu être changée.</translation>
    </message>
    <message>
        <source>Error creating a backup file: %1.</source>
        <translation type="vanished">Erreur lors de la création du fichier de sauvegarde : %1.</translation>
    </message>
    <message>
        <source>Error creating a reserv copy: %1.</source>
        <translation>Erreur lors de la création d&apos;un copie de réserve: %1.</translation>
    </message>
    <message>
        <source>Unexpected version &quot;%1&quot;.</source>
        <translation>Version inattendue &quot;%1&quot;.</translation>
    </message>
    <message>
        <source>Error replacing a symlink by real file: %1.</source>
        <translation type="vanished">Erreur de remplacement d&apos;un lien symbolique par un fichier réel : %1.</translation>
    </message>
    <message>
        <source>Invalid version. Minimum supported format version is %1</source>
        <translation>Version invalide. Le format de version minimum supporté est %1</translation>
    </message>
    <message>
        <source>Invalid version. Maximum supported format version is %1</source>
        <translation>Version invalide. Le format de version maximum supporté est %1</translation>
    </message>
    <message>
        <source>Can&apos;t open file %1:
%2.</source>
        <translation>impossible d&apos;ouvrir le fichier %1:
%2.</translation>
    </message>
    <message>
        <source>Can&apos;t open schema file %1:
%2.</source>
        <translation>Erreur d&apos;ouverture du fichier de schéma %1: %2.</translation>
    </message>
    <message>
        <source>Could not load schema file &apos;%1&apos;.</source>
        <translation>Impossible de lire le schéma de fichier &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Validation error file %3 in line %1 column %2</source>
        <translation>Erreur de validation : fichier %3, ligne %1, colonne %2</translation>
    </message>
    <message>
        <source>Error opening a temp file: %1.</source>
        <translation>Erreur à l&apos;ouverture du fichier temporaire : %1.</translation>
    </message>
</context>
<context>
    <name>VAbstractCubicBezierPath</name>
    <message>
        <source>Can&apos;t cut this spline</source>
        <translation>Impossible de couper la ligne</translation>
    </message>
    <message>
        <source>Unable to cut curve &apos;%1&apos;. The curve is too short.</source>
        <translation>Impossible de segmenter la courbe &apos;%1&apos;. La longueur de la courbe est insuffisante.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
</context>
<context>
    <name>VAbstractLayoutDialog</name>
    <message>
        <source>Letter</source>
        <comment>Paper format</comment>
        <translation>Lettre</translation>
    </message>
    <message>
        <source>Legal</source>
        <comment>Paper format</comment>
        <translation>Légal</translation>
    </message>
    <message>
        <source>Tabloid</source>
        <comment>Paper format</comment>
        <translation>Tabloïd</translation>
    </message>
    <message>
        <source>Roll 24in</source>
        <comment>Paper format</comment>
        <translation>Traceur 24 pouces</translation>
    </message>
    <message>
        <source>Roll 30in</source>
        <comment>Paper format</comment>
        <translation>Traceur 30 pouces</translation>
    </message>
    <message>
        <source>Roll 36in</source>
        <comment>Paper format</comment>
        <translation>Traceur 36 pouces</translation>
    </message>
    <message>
        <source>Roll 42in</source>
        <comment>Paper format</comment>
        <translation>Traceur 42 pouces</translation>
    </message>
    <message>
        <source>Roll 44in</source>
        <comment>Paper format</comment>
        <translation>Traceur 44 pouces</translation>
    </message>
    <message>
        <source>Roll 48in</source>
        <comment>Paper format</comment>
        <translation>Traceur 44 pouces {48i?}</translation>
    </message>
    <message>
        <source>Roll 62in</source>
        <comment>Paper format</comment>
        <translation>Traceur 44 pouces {62i?}</translation>
    </message>
    <message>
        <source>Roll 72in</source>
        <comment>Paper format</comment>
        <translation>Traceur 44 pouces {72i?}</translation>
    </message>
    <message>
        <source>Custom</source>
        <comment>Paper format</comment>
        <translation>Personnalisé</translation>
    </message>
</context>
<context>
    <name>VAbstractMainWindow</name>
    <message>
        <source>Confirm format rewriting</source>
        <translation>Veuillez confirmer la réécriture du format</translation>
    </message>
    <message>
        <source>This file is using previous format version v%1. The current is v%2. Saving the file with this app version will update the format version for this file. This may prevent you from be able to open the file with older app versions. Do you really want to continue?</source>
        <translation>Ce fichier a été créé sous la version v%1. Vous l&apos;ouvrez maintenant avec la version v%2. En sauvergardant le fichier sous la version actuelle, vous ne pourrez plus l&apos;ouvrir sous une version plus ancienne. Voulez-vous continuer quand même ?</translation>
    </message>
    <message>
        <source>Comma-Separated Values</source>
        <translation>Tableur format texte</translation>
    </message>
    <message>
        <source>values</source>
        <translation>valeurs</translation>
    </message>
    <message>
        <source>Export to CSV</source>
        <translation>Exporter sous format .csv</translation>
    </message>
    <message>
        <source>Cannot set permissions for %1 to writable.</source>
        <translation>Impossible d&apos;attribuer les droits en écriture sur %1.</translation>
    </message>
    <message>
        <source>Could not save the file.</source>
        <translation>Impossible de sauvegarder le fichier.</translation>
    </message>
    <message>
        <source>Locking file</source>
        <translation>Verrouiller le fichier (lecture seule)</translation>
    </message>
    <message>
        <source>This file already opened in another window. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Ce fichier est déjà ouvert dans une autre fenêtre. Ignorer pour continuer quand même (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Le fichier sous lecture seule ne peut être créé car vous n&apos;avez pas les permissions nécessaires. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Une erreur inconnue s&apos;est produite, par exemple pour cause de partition pleine. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>This file already opened in another window.</source>
        <translation>Ce fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions.</source>
        <translation>Impossible de créer le fichier en lecture seule, vous n&apos;avez pas les permissions nécessaires.</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file.</source>
        <translation>Une erreur inconnue s&apos;est produite, il est possible qu&apos;une partition pleine empêche d&apos;écrire le fichier en lecture seule.</translation>
    </message>
    <message>
        <source>The file has no write permissions.</source>
        <translation>Le fichier est en lecture seule.</translation>
    </message>
    <message>
        <source>Do you want to change the permissions?</source>
        <translation>Voulez-vous modifier les permissions ?</translation>
    </message>
</context>
<context>
    <name>VAbstractNode</name>
    <message>
        <source>This id (%1) is not unique.</source>
        <translation>Cet identifiant (%1) n&apos;est pas unique.</translation>
    </message>
</context>
<context>
    <name>VAbstractOperation</name>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>operation options</source>
        <translation>options d&apos;opération</translation>
    </message>
    <message>
        <source>delete operation</source>
        <translation>annuler l&apos;opération</translation>
    </message>
    <message>
        <source>Visibility group</source>
        <translation>Groupe de visibilité</translation>
    </message>
    <message>
        <source>default</source>
        <translation>défaut</translation>
    </message>
</context>
<context>
    <name>VAbstractPattern</name>
    <message>
        <source>Can&apos;t find tool in table.</source>
        <translation>L&apos;outil n&apos;a pas été trouvé dans la table.</translation>
    </message>
    <message>
        <source>Error creating or updating group</source>
        <translation>Erreur lors de la création ou la mise à jour du groupe</translation>
    </message>
    <message>
        <source>New group</source>
        <translation>Nouveau groupe</translation>
    </message>
    <message>
        <source>measurement</source>
        <translation>mesures</translation>
    </message>
</context>
<context>
    <name>VAbstractPieceData</name>
    <message>
        <source>Detail</source>
        <translation>Pièce de patron</translation>
    </message>
</context>
<context>
    <name>VAbstractSpline</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VAbstractTool</name>
    <message>
        <source>black</source>
        <translation>noir</translation>
    </message>
    <message>
        <source>green</source>
        <translation>vert</translation>
    </message>
    <message>
        <source>blue</source>
        <translation>bleu</translation>
    </message>
    <message>
        <source>dark red</source>
        <translation>rouge foncé</translation>
    </message>
    <message>
        <source>dark green</source>
        <translation>vert foncé</translation>
    </message>
    <message>
        <source>dark blue</source>
        <translation>bleu foncé</translation>
    </message>
    <message>
        <source>yellow</source>
        <translation>jaune</translation>
    </message>
    <message>
        <source>Confirm deletion</source>
        <translation type="vanished">Confirmer la suppression</translation>
    </message>
    <message>
        <source>Do you really want to delete?</source>
        <translation type="vanished">Voulez vous vraiment supprimer?</translation>
    </message>
    <message>
        <source>light salmon</source>
        <translation>saumon clair</translation>
    </message>
    <message>
        <source>golden rod</source>
        <translation type="vanished">jaune d&apos;or</translation>
    </message>
    <message>
        <source>orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <source>deep pink</source>
        <translation>rose foncé</translation>
    </message>
    <message>
        <source>violet</source>
        <translation>violet</translation>
    </message>
    <message>
        <source>dark violet</source>
        <translation>violet foncé</translation>
    </message>
    <message>
        <source>medium sea green</source>
        <translation>vert de mer moyen</translation>
    </message>
    <message>
        <source>lime</source>
        <translation>citron</translation>
    </message>
    <message>
        <source>deep sky blue</source>
        <translation>bleu ciel</translation>
    </message>
    <message>
        <source>corn flower blue</source>
        <translation>bleu centaurée</translation>
    </message>
    <message>
        <source>Edit wrong formula</source>
        <translation>Editer la formule erronée</translation>
    </message>
    <message>
        <source>goldenrod</source>
        <translation>jaune d&apos;or</translation>
    </message>
</context>
<context>
    <name>VApplication</name>
    <message>
        <source>Error parsing file. Program will be terminated.</source>
        <translation>Erreur d&apos;interprétation du fichier. Fin du programme.</translation>
    </message>
    <message>
        <source>Error bad id. Program will be terminated.</source>
        <translation>Erreur d&apos;identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Error can&apos;t convert value. Program will be terminated.</source>
        <translation>Erreur : valeur non convertissable. Fin du programme.</translation>
    </message>
    <message>
        <source>Error empty parameter. Program will be terminated.</source>
        <translation>Erreur : paramètre vide. Fin du programme.</translation>
    </message>
    <message>
        <source>Error wrong id. Program will be terminated.</source>
        <translation>Erreur : mauvais identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Something&apos;s wrong!!</source>
        <translation>Quel que chose ne va pas!!</translation>
    </message>
    <message>
        <source>Parser error: %1. Program will be terminated.</source>
        <translation>Erreur dans l&apos;interprétation: %1 va quitter.</translation>
    </message>
    <message>
        <source>Exception thrown: %1. Program will be terminated.</source>
        <translation>Erreur : %1. Fin du programme.</translation>
    </message>
    <message>
        <source>Invalid notch.</source>
        <translation>Cran invalide.</translation>
    </message>
    <message>
        <source>Formula warning: %1. Program will be terminated.</source>
        <translation>Problème de formule : %1. Le programme va s&apos;arrêter.</translation>
    </message>
</context>
<context>
    <name>VArc</name>
    <message>
        <source>Unable to cut curve &apos;%1&apos;. The curve is too short.</source>
        <translation>Impossible de segmenter la courbe &apos;%1&apos;. La longueur de la courbe est insuffisante.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too small. Optimize it to minimal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe insuffisante. Paramétrez une valeur au moins égale au minimum requis.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment (%2) is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. La partie segmentée (%2) est de longueur trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
    <message>
        <source>Curve &apos;%1&apos;. Length of a cut segment is too big. Optimize it to maximal value.</source>
        <translation>Courbe &apos;%1&apos;. Valeur de segmentation de la courbe trop élevée. Paramétrez une valeur au plus égale au maximum possible.</translation>
    </message>
</context>
<context>
    <name>VBackgroundImageItem</name>
    <message>
        <source>Hold</source>
        <translation>Ancrer</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <source>Show in Finder</source>
        <translation>Révéler dans le finder</translation>
    </message>
    <message>
        <source>Show in Explorer</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <source>Save as …</source>
        <translation>Sauvegarder sous …</translation>
    </message>
    <message>
        <source>Reset transformation</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
</context>
<context>
    <name>VBackgroundPatternImage</name>
    <message>
        <source>Unexpected mime type: %1</source>
        <translation>Type mime inattendu : %1</translation>
    </message>
    <message>
        <source>Couldn&apos;t read the image. Error: %1</source>
        <translation>Impossible de lire l&apos;image. Erreur : %1</translation>
    </message>
    <message>
        <source>No data.</source>
        <translation>Aucune donnée.</translation>
    </message>
    <message>
        <source>Invalid id.</source>
        <translation>Identifiant invalide.</translation>
    </message>
    <message>
        <source>Content type is empty.</source>
        <translation>Le type de contenu est vide.</translation>
    </message>
    <message>
        <source>Not image.</source>
        <translation>Ceci n&apos;est pas une image.</translation>
    </message>
    <message>
        <source>Content type mismatch.</source>
        <translation>Le type de contenu ne correspond pas.</translation>
    </message>
</context>
<context>
    <name>VBank</name>
    <message>
        <source>Error of preparing data for layout: Detail &apos;%1&apos; square &lt;= 0</source>
        <translation>Erreur rencontrée lors de la préparation des données pour l&apos;édition du plan de coupe : la taille de la pièce &apos;%1&apos; est &lt;= 0</translation>
    </message>
    <message>
        <source>Error of preparing data for layout: Layout paper sheet &lt;= 0</source>
        <translation>Erreur rencontrée lors de la préparation des données pour l&apos;édition du plan de coupe : la taille de la feuille est  &lt;= 0</translation>
    </message>
    <message>
        <source>Error of preparing data for layout: List of details is empty</source>
        <translation>Erreur rencontrée lors de la préparation des données pour l&apos;édition du plan de coupe : la liste des pièces à intégrer est vide</translation>
    </message>
</context>
<context>
    <name>VComboBoxDelegate</name>
    <message>
        <source>Select material</source>
        <translation>Sélectionner le tissus</translation>
    </message>
</context>
<context>
    <name>VCommandLine</name>
    <message>
        <source>Path to custom measure file (export mode).</source>
        <translation>Chemin d&apos;accès au fichier de mesures personnalisées (mode export).</translation>
    </message>
    <message>
        <source>The measure file</source>
        <translation>Le fichier de mesure</translation>
    </message>
    <message>
        <source>Number corresponding to output format (default = 0, export mode): </source>
        <translation type="vanished">Numéro correspondant au format de sortie (défaut=0, mode export):</translation>
    </message>
    <message>
        <source>Format number</source>
        <translation>Numéro de format</translation>
    </message>
    <message>
        <source>Number corresponding to page template (default = 0, export mode): </source>
        <translation type="vanished">Numéro correspondant au modèle de page (par défaut = 0, mode export) :</translation>
    </message>
    <message>
        <source>Template number</source>
        <translation>Numéro de modèle</translation>
    </message>
    <message>
        <source>The page width</source>
        <translation>La largeur de page</translation>
    </message>
    <message>
        <source>The measure unit</source>
        <translation>L&apos;unité de mesure</translation>
    </message>
    <message>
        <source>Rotation in degrees (one of predefined). Default (or 0) is no-rotate (export mode).</source>
        <translation type="vanished">Rotation en degrés (un des prédéfinis). Défaut (ou 0) égal pas de rotation (mode export).</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Auto crop unused length (export mode).</source>
        <translation>Rogner automatiquement la longueur non utilisée (mode export).</translation>
    </message>
    <message>
        <source>Unite pages if possible (export mode).</source>
        <translation type="vanished">Page unique si possible (mode export)</translation>
    </message>
    <message>
        <source>Save length of the sheet if set. (export mode).</source>
        <translation type="vanished">Mémoriser la longueur de la feuille si paramétrée. (mode export)</translation>
    </message>
    <message>
        <source>Layout units (as paper&apos;s one except px, export mode).</source>
        <translation type="vanished">Dimensions du plan de coupe (égales à celles du papier hors px, mode export).</translation>
    </message>
    <message>
        <source>The unit</source>
        <translation>L&apos;unité</translation>
    </message>
    <message>
        <source>Shift layout length measured in layout units (export mode).</source>
        <translation type="vanished">Mesure de décalage de dimensions du plan de coupe (mode export)</translation>
    </message>
    <message>
        <source>Shift length</source>
        <translation type="vanished">Longueur de décalage</translation>
    </message>
    <message>
        <source>Gap width x2, measured in layout units. (export mode).</source>
        <translation type="vanished">Largeur d&apos;espacement x2, en unités du plan de coupe. (mode export)</translation>
    </message>
    <message>
        <source>The gap width</source>
        <translation>Largeur d&apos;espacement</translation>
    </message>
    <message>
        <source>Sets layout groupping (export mode): </source>
        <translation type="vanished">Réglages de groupement du plan de coupe (mode export):</translation>
    </message>
    <message>
        <source>Grouping type</source>
        <translation>Type de groupes</translation>
    </message>
    <message>
        <source>Cannot use pageformat and page explicit size/units together.</source>
        <translation type="vanished">On ne peut utiliser un format de page et une longueur/largeur explicite.</translation>
    </message>
    <message>
        <source>Page height, width, units must be used all 3 at once.</source>
        <translation>Hauteur, largeur de page et unités doivent être utilisés ensembles.</translation>
    </message>
    <message>
        <source>Invalid rotation value. That must be one of predefined values.</source>
        <translation type="vanished">Valeur de rotation invalide. Utilisez les valeurs prédéfinies.</translation>
    </message>
    <message>
        <source>Unknown page templated selected.</source>
        <translation>Modèle de page inconnu.</translation>
    </message>
    <message>
        <source>Unsupported paper units.</source>
        <translation>Dimensions de papier non supportées.</translation>
    </message>
    <message>
        <source>Unsupported layout units.</source>
        <translation>Dimensions du plan de coupe non supportées.</translation>
    </message>
    <message>
        <source>Export options can be used with single input file only.</source>
        <translation>Les options d&apos;export ne peuvent être utilisé qu&apos;avec un fichier d&apos;entrée simple.</translation>
    </message>
    <message>
        <source>Run the program in a test mode. The program this mode load a single pattern file and silently quit without showing the main window. The key have priority before key &apos;%1&apos;.</source>
        <translation type="vanished">Lance le logiciel en mode test. Dans ce mode, le logiciel charge un fichier de patron simple puis quitte sans afficher la fenêtre principale. La clé a priorité sur la clé &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Test option can be used with single input file only.</source>
        <translation>Les options de test ne peuvent être utilisé qu&apos;avec un fichier d&apos;entrée simple.</translation>
    </message>
    <message>
        <source>The base filename of exported layout files. Use it to enable console export mode.</source>
        <translation>Le nom d&apos;export du fichier de plan de coupe. À n&apos;utiliser que dans le cas d&apos;un mode export console.</translation>
    </message>
    <message>
        <source>The base filename of layout files</source>
        <translation>Le nom de fichier du plan de coupe</translation>
    </message>
    <message>
        <source>The path to output destination folder.</source>
        <translation type="vanished">Emplacement du dossier de destination.</translation>
    </message>
    <message>
        <source>The destination folder</source>
        <translation>Le dossier de destination</translation>
    </message>
    <message>
        <source>Set size value a pattern file, that was opened with standard measurements (export mode). Valid values: %1cm.</source>
        <translation type="vanished">Règle la valeur de taille d&apos;un patron qui a été ouvert avec des mensurations standard (mode export). Valeur correcte : %1cm.</translation>
    </message>
    <message>
        <source>The size value</source>
        <translation type="vanished">Valeur de taille de base</translation>
    </message>
    <message>
        <source>Set height value a pattern file, that was opened with standard measurements (export mode). Valid values: %1cm.</source>
        <translation type="vanished">Règle la valeur de stature d&apos;un patron qui a été ouvert avec des mensurations standard (mode export). Valeur correcte : %1cm.</translation>
    </message>
    <message>
        <source>The height value</source>
        <translation type="vanished">Valeur de hauteur de base</translation>
    </message>
    <message>
        <source>Page width in current units like 12.0 (cannot be used with &quot;%1&quot;, export mode).</source>
        <translation>Largeur de page en unité courante, ex. 12.0 (ne peut pas être utilisé avec &quot;%1&quot;, mode exportation).</translation>
    </message>
    <message>
        <source>Page height in current units like 12.0 (cannot be used with &quot;%1&quot;, export mode).</source>
        <translation>Hauteur de page en unité courante, ex. 12.0 (ne peut pas être utilisé avec &quot;%1&quot;, mode exportation).</translation>
    </message>
    <message>
        <source>Page height/width measure units (cannot be used with &quot;%1&quot;, export mode): </source>
        <translation type="vanished">Unité de mesure de hauteur/largeur de page (&quot;%1&quot; ne peut pas être utilisé, mode exportation):</translation>
    </message>
    <message>
        <source>Invalid gradation size value.</source>
        <translation type="vanished">Valeur de gradation incorrecte.</translation>
    </message>
    <message>
        <source>Invalid gradation height value.</source>
        <translation type="vanished">Valeur de stature incorrecte.</translation>
    </message>
    <message>
        <source>Pattern making program.</source>
        <translation>Programme de réalisation de patrons.</translation>
    </message>
    <message>
        <source>Pattern file.</source>
        <translation>Fichier Patron.</translation>
    </message>
    <message>
        <source>Ignore margins printing (export mode). Set all margins to 0.</source>
        <translation type="vanished">Ignorer les marges d&apos;impression (mode export). Règle les marge à 0.</translation>
    </message>
    <message>
        <source>Page left margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found.</source>
        <translation>Marge gauche de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé.</translation>
    </message>
    <message>
        <source>Page right margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found.</source>
        <translation>Marge droite de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé.</translation>
    </message>
    <message>
        <source>Page top margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found.</source>
        <translation>Marge du haut de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé.</translation>
    </message>
    <message>
        <source>Page bottom margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found.</source>
        <translation>Marge du bas de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé.</translation>
    </message>
    <message>
        <source>Shift length must be used together with shift units.</source>
        <translation type="vanished">La longueur de décalage doit être spécifiée dans l&apos;unité de  décalage</translation>
    </message>
    <message>
        <source>Gap width must be used together with shift units.</source>
        <translation>Largeur d&apos;espacement devant être utilisé avec les largeurs de décalage.</translation>
    </message>
    <message>
        <source>Left margin must be used together with page units.</source>
        <translation>la marge gauche doit être spécifiée dans l&apos;unité de la page.</translation>
    </message>
    <message>
        <source>Right margin must be used together with page units.</source>
        <translation>la marge droite doit être spécifiée dans l&apos;unité de la page.</translation>
    </message>
    <message>
        <source>Top margin must be used together with page units.</source>
        <translation>la marge du haut doit être spécifiée dans l&apos;unité de la page.</translation>
    </message>
    <message>
        <source>Bottom margin must be used together with page units.</source>
        <translation>la marge du bas doit être spécifiée dans l&apos;unité de la page.</translation>
    </message>
    <message>
        <source>The path to output destination folder. By default the directory at which the application was started.</source>
        <translation>Emplacement du dossier de destination. Par défaut, le répertoire est celui de l&apos;application.</translation>
    </message>
    <message>
        <source>Page height/width measure units (cannot be used with &quot;%1&quot;, export mode). Valid values: %2.</source>
        <translation type="vanished">Unité de mesure des hauteur/largeur de la page (&quot;%1&quot; ne peut pas être utilisé, mode exportation). Valeur valide: %2.</translation>
    </message>
    <message>
        <source>Ignore margins printing (export mode). Disable value keys: &quot;%1&quot;, &quot;%2&quot;, &quot;%3&quot;, &quot;%4&quot;. Set all margins to 0.</source>
        <translation type="vanished">Ignorer les marges d&apos;impression (mode export). Inactive les clés de valeur : &quot;%1&quot;, &quot;%2&quot;, &quot;%3&quot;, &quot;%4&quot;. Règle les marge à 0.</translation>
    </message>
    <message>
        <source>Page left margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found. Value will be ignored if key &quot;%1&quot; is used.</source>
        <translation type="vanished">Marge gauche de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé. La valeur sera ignorée si &quot;%1&quot; est utilisé.</translation>
    </message>
    <message>
        <source>Page right margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found. Value will be ignored if key &quot;%1&quot; is used.</source>
        <translation type="vanished">Marge droite de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé. La valeur sera ignorée si &quot;%1&quot; est utilisé.</translation>
    </message>
    <message>
        <source>Page top margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found. Value will be ignored if key &quot;%1&quot; is used.</source>
        <translation type="vanished">Marge du haut de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé. La valeur sera ignorée si &quot;%1&quot; est utilisé.</translation>
    </message>
    <message>
        <source>Page bottom margin in current units like 3.0 (export mode). If not set will be used value from default printer. Or 0 if none printers was found. Value will be ignored if key &quot;%1&quot; is used.</source>
        <translation type="vanished">Marge du bas de la page en unité courante, ex. 3.0 (mode export).   Si non renseigné, la valeur par défaut de l&apos;imprimante sera utilisé. Ou 0 si aucune imprimante n&apos;a été trouvé. La valeur sera ignorée si &quot;%1&quot; est utilisé.</translation>
    </message>
    <message>
        <source>Rotation in degrees (one of predefined, export mode). Default value is 180. 0 is no-rotate. Valid values: %1. Each value show how many times details will be rotated. For example 180 mean two times (360/180=2) by 180 degree.</source>
        <translation type="vanished">Rotation en degrés (prédéfini en mode export). La valeur par défaut est de 180 degrés. 0 degré si aucune rotation. Valeurs possibles : %1. Chaque valeur indique combien de fois la pièce de patron fera l&apos;objet d&apos;une rotation. Par exemple, 180 signifie 2 rotations (360/180=2) de 180 degrés.</translation>
    </message>
    <message>
        <source>Unite pages if possible (export mode). Maximum value limited by QImage that supports only a maximum of 32768x32768 px images.</source>
        <translation>Page unique si possible (mode export). La valeur maximum est limité par QImage qui ne supporte que des images d&apos;un maximun 32768x32768 px.</translation>
    </message>
    <message>
        <source>Save length of the sheet if set (export mode). The option tells the program to use as much as possible width of sheet. Quality of a layout can be worse when this option was used.</source>
        <translation>Sauvegarder la longueur de la feuille si réglée (mode export). Cette option demande au logiciel d&apos;utiliser la feuille au maximum en largeur. La qualité du plan de coupe peut être moindre si cette option est choisie.</translation>
    </message>
    <message>
        <source>Shift layout length measured in layout units (export mode). The option show how many points along edge will be used in creating a layout.</source>
        <translation type="vanished">Décalage mesuré en fonction des dimensions du plan de coupe (mode export). Cette option affiche le nombre de point, le long d&apos;une arête, qui seront utilisé pour créer le plan de coupe.</translation>
    </message>
    <message>
        <source>The layout gap width x2, measured in layout units (export mode). Set distance between details and a detail and a sheet.</source>
        <translation>La largeur d&apos;espacement entre pièces x2, mesurée en fonction des dimensions du plan de coupe (mode export). Réglage de la distance entre les pièces de patron, ainsi qu&apos;une pièce de patron et le bord de la feuille.</translation>
    </message>
    <message>
        <source>Sets layout groupping cases (export mode): %1.</source>
        <translation>Réglage des cas de regroupement de plan de coupe (mode export): %1.</translation>
    </message>
    <message>
        <source>Run the program in a test mode. The program in this mode loads a single pattern file and silently quit without showing the main window. The key have priority before key &apos;%1&apos;.</source>
        <translation>Lance le logiciel en mode test. Dans ce mode, le logiciel charge un fichier de patron simple puis quitte sans afficher la fenêtre principale. La clé a priorité sur la clé &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Shift/Offset layout length measured in layout units (export mode). The option show how many points along edge will be used in creating a layout.</source>
        <translation type="vanished">Décalage mesuré en fonction des dimensions du plan de coupe (mode export). Cette option affiche le nombre de point, le long d&apos;une arête, qui seront utilisé pour créer le plan de coupe.</translation>
    </message>
    <message>
        <source>Shift/Offset length</source>
        <translation type="vanished">Longueur de décalage</translation>
    </message>
    <message>
        <source>Shift/Offset length must be used together with shift units.</source>
        <translation type="vanished">la longueur de décalage doit être spécifiée dans l&apos;unité de décalage.</translation>
    </message>
    <message>
        <source>Number corresponding to output format (default = 0, export mode):</source>
        <translation>Numéro correspondant au format de sortie (défaut=0, mode export) :</translation>
    </message>
    <message>
        <source>Number corresponding to page template (default = 0, export mode):</source>
        <translation type="vanished">Numéro correspondant au modèle de page (défaut=0, mode export) :</translation>
    </message>
    <message>
        <source>Disable high dpi scaling. Call this option if has problem with scaling (by default scaling enabled). Alternatively you can use the %1 environment variable.</source>
        <translation>Désactiver la mise à l&apos;échelle haute précision. Choisissez cette option si vous avez un problème avec la mise à l&apos;échelle (activée par défaut). Sinon vous pouvez aussi utiliser la variable d’environnement %1.</translation>
    </message>
    <message>
        <source>Export dxf in binary form.</source>
        <translation>Export du fichier dxf en binaire.</translation>
    </message>
    <message>
        <source>Export text as paths.</source>
        <translation>Exporter les textes en chemins.</translation>
    </message>
    <message>
        <source>Export only details. Export details as they positioned in the details mode. Any layout related options will be ignored.</source>
        <translation>Export des pièces uniquement, positionnées comme elles le sont en mode Pièces. Ne tient pas compte des options du plan de coupe.</translation>
    </message>
    <message>
        <source>Export only details that match a piece name regex.</source>
        <translation>Exporter uniquement les pièces dont le nom correspond à une expression régulière de pièce.</translation>
    </message>
    <message>
        <source>The name regex</source>
        <translation>Expression régulière du nom</translation>
    </message>
    <message>
        <source>Export to csv with header. By default disabled.</source>
        <translation>Export en csv avec en-tête. Désactivé par défaut.</translation>
    </message>
    <message>
        <source>Specify codec that will be used to save data. List of supported codecs provided by Qt. Default value depend from system. On Windows, the codec will be based on a system locale. On Unix systems, the codec will might fall back to using the iconv library if no builtin codec for the locale can be found. Valid values for this installation:</source>
        <translation>Spécifier le codec qui sera utilisé pour enregistrer les données. La liste des codecs supportés est fournie par Qt. Les valeurs par défaut dépendent du système. Sur Windows, le codec est basé sur une locale système. Sur les systèmes Unix, le codec peut se rabattre sur l&apos;utilisation d&apos;une bibliothèque iconv, si aucun codec intégré pour la locale n&apos;est trouvé. Valeurs valides pour cette installation :</translation>
    </message>
    <message>
        <source>Codec name</source>
        <translation>Nom du code</translation>
    </message>
    <message>
        <source>Specify csv separator character. Default value is &apos;%1&apos;. Valid characters:</source>
        <translation>Spécifier le caractère séparateur du fichier csv. Par défaut la valeur est &apos;%1&apos;. Caractères valides :</translation>
    </message>
    <message>
        <source>Separator character</source>
        <translation>Caractère séparateur</translation>
    </message>
    <message>
        <source>Calling this command enable exporting final measurements. Specify path to csv file with final measurements. The path must contain path to directory and name of file. It can be absolute or relatetive. In case of relative path will be used current working directory to calc a destination path.</source>
        <translation>Cette commande permet d&apos;exporter les mesures finales. Indiquez un chemin d&apos;accès pour le fichier CSV contenant les mesures à exporter. Ce chemin doit spécifier le dossier dans lequel le fichier sera enregistré, ainsi que le nom du fichier. Il peut être absolu ou relatif. Le chemin relatif pointera vers le dossier dans lequel votre patron actuel est enregistré.</translation>
    </message>
    <message>
        <source>Path to csv file</source>
        <translation>Chemin d&apos;accès du fichier csv</translation>
    </message>
    <message>
        <source>Number corresponding to layout page template (default = 0, export mode):</source>
        <translation>Numéro correspondant au modèle de page du plan de coupe (défaut=0, mode export) :</translation>
    </message>
    <message>
        <source>Number corresponding to tiled pdf page template (default = 0, export mode with tiled pdf format):</source>
        <translation>Numéro caractérisant le mode de page pdf empilé (par défaut = 0, mode export au format pdf empilé) :</translation>
    </message>
    <message>
        <source>Tiled page left margin in current units like 3.0 (export mode). If not set will be used default value 1 cm.</source>
        <translation>Marge gauche de la page empilée en unité courante sous la forme &apos;3.0&apos; (mode export). Si non spécifiée, la valeur de 1 cm sera utilisée par défaut.</translation>
    </message>
    <message>
        <source>The left margin</source>
        <translation>La marge de gauche</translation>
    </message>
    <message>
        <source>Tiled page right margin in current units like 3.0 (export mode). If not set will be used default value 1 cm.</source>
        <translation>Marge droite de la page empilée en unité courante sous la forme &apos;3.0&apos; (mode export). Si non spécifiée, la valeur de 1 cm sera utilisée par défaut.</translation>
    </message>
    <message>
        <source>The right margin</source>
        <translation>La marge de droite</translation>
    </message>
    <message>
        <source>Tiled page top margin in current units like 3.0 (export mode). If not set will be used value default value 1 cm.</source>
        <translation>Marge haut de la page empilée en unité courante sous la forme &apos;3.0&apos; (mode export). Si non spécifiée, la valeur de 1 cm sera utilisée par défaut.</translation>
    </message>
    <message>
        <source>The top margin</source>
        <translation>La marge du haut</translation>
    </message>
    <message>
        <source>Tiled page bottom margin in current units like 3.0 (export mode). If not set will be used value default value 1 cm.</source>
        <translation>Marge bas de la page empilée en unité courante sous la forme &apos;3.0&apos; (mode export). Si non spécifiée, la valeur de 1 cm sera utilisée par défaut.</translation>
    </message>
    <message>
        <source>The bottom margin</source>
        <translation>La marge du bas</translation>
    </message>
    <message>
        <source>Set tiled page orienatation to landscape (export mode). Default value if not set portrait.</source>
        <translation>Définir l&apos;orientation de la page en mode empilé, au format paysage (mode export). Format portrait par défaut si orientation non configurée.</translation>
    </message>
    <message>
        <source>Cannot use pageformat and page explicit size together.</source>
        <translation>Impossible d&apos;utiliser les fonctions format de page et taille spécifique de page ensemble.</translation>
    </message>
    <message>
        <source>Tiled left margin must be used together with page units.</source>
        <translation>La marge gauche en empilement doit être spécifiée dans la même unité que celle de la page.</translation>
    </message>
    <message>
        <source>Tiled right margin must be used together with page units.</source>
        <translation>La marge droite en empilement doit être spécifiée dans la même unité que celle de la page.</translation>
    </message>
    <message>
        <source>Tiled top margin must be used together with page units.</source>
        <translation>La marge haut en empilement doit être spécifiée dans la même unité que celle de la page.</translation>
    </message>
    <message>
        <source>Tiled bottom margin must be used together with page units.</source>
        <translation>La marge bas en empilement doit être spécifiée dans la même unité que celle de la page.</translation>
    </message>
    <message>
        <source>Page measure units (export mode). Valid values: %1.</source>
        <translation>Unités de mesure de la page (mode export). Valeurs valides : %1.</translation>
    </message>
    <message>
        <source>Use this option to override user material defined in pattern. The value must be in form &lt;number&gt;@&lt;user matrial name&gt;. The number should be in range from 1 to %1. For example, 1@Fabric2. The key can be used multiple times. Has no effect in GUI mode.</source>
        <translation>Use this option to override user material defined in pattern. The value must be in form &lt;number&gt;@&lt;user matrial name&gt;. The number should be in range from 1 to %1. For example, 1@Fabric2. The key can be used multiple times. Has no effect in GUI mode.</translation>
    </message>
    <message>
        <source>User material</source>
        <translation>Tissu de l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Make all parsing warnings into errors. Have effect only in console mode. Use to force Valentina to immediately terminate if a pattern contains a parsing warning.</source>
        <translation>Prend toutes les alertes d&apos;analyses comme des erreurs. N&apos;a un effet qu&apos;en mode console. Utilisé pour forcer Valentina à s&apos;arrêter immédiatement si un patron contient des erreurs d&apos;analyse.</translation>
    </message>
    <message>
        <source>Invalid user material &apos;%1&apos;. Separator is missing.</source>
        <translation>Le tissu &apos;%1&apos; de l&apos;utilisateur est invalide : séparateur manquant.</translation>
    </message>
    <message>
        <source>Invalid user material &apos;%1&apos;. Wrong material number.</source>
        <translation>Le tissu &apos;%1&apos; de l&apos;utilisateur est invalide : numéro de tissu erroné.</translation>
    </message>
    <message>
        <source>Switch page template orientation to landscape (export mode). This option has effect only for one of predefined page templates.</source>
        <translation>Passer le modèle de page en mode paysage (mode export). Ce réglage n&apos;aura d&apos;effet que pour l&apos;un des modèles pré-définis.</translation>
    </message>
    <message>
        <source>The page height</source>
        <translation>La hauteur de page</translation>
    </message>
    <message>
        <source>Order detail to follow grainline direction (export mode).</source>
        <translation>Tenir compte de la direction du droit-fil dans le placement des pièces (mode export).</translation>
    </message>
    <message>
        <source>&lt;Time&gt; in minutes given for the algorithm to find best layout. Time must be in range from 1 minute to 60 minutes. Default value 1 minute.</source>
        <translation>&lt;Time&gt; en minutes données à l&apos;algorithme pour trouver le meilleur plan de coupe. La durée doit être comprise entre 1 et 60 minutes. Valeur par défaut 1 minute.</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <source>Time must be in range from 1 minute to 60 minutes.</source>
        <translation>Le temps indiqué doit être compris entre 1 et 60 minutes.</translation>
    </message>
    <message>
        <source>Follow manual priority over priority by square (export mode).</source>
        <translation>Privilégier la priorité manuelle (mode export).</translation>
    </message>
    <message>
        <source>Nest quantity copies of each piece (export mode).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set layout efficiency &lt;coefficient&gt;. Layout efficiency coefficient is the ratio of the area occupied by the pieces to the bounding rect of all pieces. If nesting reaches required level the process stops. If value is 0 no check will be made. Coefficient must be in range from 0 to 100. Default value 0.</source>
        <translation>Configurez le coefficient d&apos;efficacité du plan de coupe &lt;coefficient&gt;.Ce coefficient constitue le ratio de l&apos;espace occupé par les pièces par rapport au rectangle englobant toutes les pièces. Quand le ratio configuré est atteint, la création est automatiquement validée même si la durée minimale de création configurée n&apos;est pas écoulée.  Si le ratio est configuré à 0, le programme ne cherchera pas à disposer les pièces en fonction de ce critère. Le coefficient doit être compris entre 0 et 100. Valeur par défaut 0.</translation>
    </message>
    <message>
        <source>Coefficient must be in range from 0 to 100.</source>
        <translation>Le coefficient doit être compris entre 0 et 100.</translation>
    </message>
    <message>
        <source>Auto crop unused width (export mode).</source>
        <translation>Rogner automatiquement la largeur non utilisée (mode export).</translation>
    </message>
    <message>
        <source>Set horizontal scale factor from 0.01 to 3.0 (default = 1.0, export mode).</source>
        <translation>Configurer l&apos;échelle horizontale de 0.01 à 3.0 (par défaut = 1.0,  mode export).</translation>
    </message>
    <message>
        <source>Horizontal scale</source>
        <translation>Echelle horizontale</translation>
    </message>
    <message>
        <source>Set vertical scale factor from 0.01 to 3.0 (default = 1.0, export mode).</source>
        <translation>Configurer l&apos;échelle verticale de 0.01 à 3.0 (par défaut = 1.0,  mode export).</translation>
    </message>
    <message>
        <source>Vertical scale</source>
        <translation>Echelle verticale</translation>
    </message>
    <message>
        <source>Prefer one sheet layout solution (export mode).</source>
        <translation>Préférer le plan de coupe sur une seule feuille (mode export).</translation>
    </message>
    <message>
        <source>Invalid dimension A value.</source>
        <translation>Valeur de la dimension A non valide.</translation>
    </message>
    <message>
        <source>Invalid dimension B value.</source>
        <translation>Valeur de la dimension B non valide.</translation>
    </message>
    <message>
        <source>Invalid dimension C value.</source>
        <translation>Valeur de la dimension C non valide.</translation>
    </message>
    <message>
        <source>Set base for dimension A in the multisize measurements units (export mode).</source>
        <translation>Configurer la valeur de base de la dimention A dans l&apos;unité des mensurations multi-tailles (mode export).</translation>
    </message>
    <message>
        <source>The dimension A base</source>
        <translation>Valeur de base pour la dimension A</translation>
    </message>
    <message>
        <source>Set base for dimension B in the multisize measurements units (export mode).</source>
        <translation>Configurer la valeur de base de la dimention B dans l&apos;unité des mensurations multi-tailles (mode export).</translation>
    </message>
    <message>
        <source>The dimension B base</source>
        <translation>Valeur de base pour la dimension B</translation>
    </message>
    <message>
        <source>Set base for dimension C in the multisize measurements units (export mode).</source>
        <translation>Configurer la valeur de base de la dimention C dans l&apos;unité des mensurations multi-tailles (mode export).</translation>
    </message>
    <message>
        <source>The dimension C base</source>
        <translation>Valeur de base pour la dimension C</translation>
    </message>
    <message>
        <source>The filename of exported layout file. Use it to enable console export mode.</source>
        <translation>Nom de fichier du plan de coupe exporté. A utiliser pour activer le mode export en console.</translation>
    </message>
    <message>
        <source>The filename of layout file</source>
        <translation>Nom de fichier du plan de coupe</translation>
    </message>
    <message>
        <source>The raw layout data file</source>
        <translation>Fichier de données brutes de plan de coupe</translation>
    </message>
    <message>
        <source>Number corresponding to output format (default = 0, export mode): &lt;not defined&gt;</source>
        <translation>Numéro correspondant au format de sortie (par défaut = 0, mode export) : &lt;not defined&gt;</translation>
    </message>
    <message>
        <source>Number corresponding to tiled pdf page template (default = 0, export mode with tiled pdf format): &lt;not defined&gt;</source>
        <translation>Numéro correspondant au modèle de page de PDF empilé (par défaut = 0, mode export sous format PDF empilé) : &lt;not defined&gt;</translation>
    </message>
    <message>
        <source>Run the program in a test mode. The program in this mode loads a single layout file and silently quit without showing the main window. The key have priority before key &apos;%1&apos;.</source>
        <translation>Lance le logiciel en mode test. Dans ce mode, le logiciel charge uniquement un fichier de patron puis quitte sans afficher la fenêtre principale. La clé a priorité sur la clé &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Invalid page height value.</source>
        <translation>Valeur de hauteur de page invalide.</translation>
    </message>
    <message>
        <source>Invalid page width value.</source>
        <translation>Valeur de largeur de page invalide.</translation>
    </message>
    <message>
        <source>Invalid gap width.</source>
        <translation>Valeur d&apos;espacement non valide.</translation>
    </message>
    <message>
        <source>Invalid layout page left margin.</source>
        <translation>Marge de gauche de la page du plan de coupe invalide.</translation>
    </message>
    <message>
        <source>Invalid layout page right margin.</source>
        <translation>Marge de droite de la page du plan de coupe invalide.</translation>
    </message>
    <message>
        <source>Invalid layout page top margin.</source>
        <translation>Marge du haut de la page du plan de coupe invalide.</translation>
    </message>
    <message>
        <source>Invalid layout page bottom margin.</source>
        <translation>Marge du bas de la page du plan de coupe invalide.</translation>
    </message>
    <message>
        <source>Invalid tiled page left margin.</source>
        <translation>Marge gauche de la page empilée invalide.</translation>
    </message>
    <message>
        <source>Invalid tiled page right margin.</source>
        <translation>Marge droite de la page empilée invalide.</translation>
    </message>
    <message>
        <source>Invalid tiled page top margin.</source>
        <translation>Marge haut de la page empilée invalide.</translation>
    </message>
    <message>
        <source>Invalid tiled page bottom margin.</source>
        <translation>Marge bas de la page empilée invalide.</translation>
    </message>
    <message>
        <source>Layout units (as paper&apos;s one except px, export mode). Default units cm.</source>
        <translation>Unités du plan de coupe (mode export : identiques à celles du papier, hors px). Unité par défaut en &quot;cm&quot;.</translation>
    </message>
    <message>
        <source>Load pattern pieces from the raw layout data file.</source>
        <translation>Charger les éléments de patron depuis le fichier de données brutes du plan de coupe.</translation>
    </message>
    <message>
        <source>Ignore printer margins (export mode). Use if need full paper space. In case of later printing you must account for the margins themselves.</source>
        <translation>Ignorer les marges d&apos;impression (mode export). A choisir si besoin d&apos;utiliser toute la surface de la feuille. En cas d&apos;impression ultérieure, ne pas oublier de prendre en compte les marges.</translation>
    </message>
</context>
<context>
    <name>VCommonSettings</name>
    <message>
        <source>measurements</source>
        <translation>mesures</translation>
    </message>
    <message>
        <source>individual</source>
        <translation>Individuel</translation>
    </message>
    <message>
        <source>multisize</source>
        <translation>multi-tailles</translation>
    </message>
    <message>
        <source>templates</source>
        <translation>modèles</translation>
    </message>
    <message>
        <source>label templates</source>
        <translation>modèles d&apos;étiquettes</translation>
    </message>
    <message>
        <source>patterns</source>
        <translation>patrons</translation>
    </message>
    <message>
        <source>manual layouts</source>
        <translation>plans de coupe manuels</translation>
    </message>
</context>
<context>
    <name>VContainer</name>
    <message>
        <source>Can&apos;t find object</source>
        <translation>Objet non trouvé</translation>
    </message>
    <message>
        <source>Can&apos;t cast object</source>
        <translation>Ne peut convertir l&apos;objet</translation>
    </message>
    <message>
        <source>Can&apos;t find object. Type mismatch.</source>
        <translation>impossible de trouver l&apos;objet. Le type ne correspond pas.</translation>
    </message>
    <message>
        <source>Number of free id exhausted.</source>
        <translation>Le nombre de ID libre est épuisé.</translation>
    </message>
    <message>
        <source>Can&apos;t create a curve with type &apos;%1&apos;</source>
        <translation>Impossible de créer une courbe de type &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t cast object.</source>
        <translation>Ne peut convertir l&apos;objet.</translation>
    </message>
</context>
<context>
    <name>VCubicBezierPath</name>
    <message>
        <source>Not enough points to create the spline.</source>
        <translation>Pas assez de point pour créer la spline.</translation>
    </message>
    <message>
        <source>This spline does not exist.</source>
        <translation>Cette spline n&apos;existe pas.</translation>
    </message>
</context>
<context>
    <name>VDomDocument</name>
    <message>
        <source>Can&apos;t convert toUInt parameter</source>
        <translation type="vanished">Conversion du paramètre impossible vers toUInt</translation>
    </message>
    <message>
        <source>Can&apos;t convert toBool parameter</source>
        <translation type="vanished">Conversion du paramètre impossible vers toBool</translation>
    </message>
    <message>
        <source>Got empty parameter</source>
        <translation type="vanished">Paramètre vide</translation>
    </message>
    <message>
        <source>Can&apos;t convert toDouble parameter</source>
        <translation type="vanished">Conversion du paramètre impossible vers toDouble</translation>
    </message>
    <message>
        <source>Can&apos;t open file %1:
%2.</source>
        <translation>impossible d&apos;ouvrir le fichier %1:
%2.</translation>
    </message>
    <message>
        <source>Can&apos;t open schema file %1:
%2.</source>
        <translation type="vanished">Erreur d&apos;ouverture du fichier de schéma %1: %2.</translation>
    </message>
    <message>
        <source>Could not load schema file.</source>
        <translation type="vanished">Erreur de chargement du fichier de schéma.</translation>
    </message>
    <message>
        <source>Validation error file %3 in line %1 column %2</source>
        <translation type="vanished">Erreur de validation : fichier %3, ligne %1, colonne %2</translation>
    </message>
    <message>
        <source>Parsing error file %3 in line %1 column %2</source>
        <translation>Erreur d&apos;interprétation : fichier %3, ligne %1, colonne %2</translation>
    </message>
    <message>
        <source>Couldn&apos;t get node</source>
        <translation>Noeud inaccessible</translation>
    </message>
    <message>
        <source>Got wrong parameter id. Need only id &gt; 0.</source>
        <translation type="vanished">Mauvais id . Seul les id &gt; 0 sont autorisés.</translation>
    </message>
    <message>
        <source>This id is not unique.</source>
        <translation type="vanished">Cet identifiant n&apos;est pas unique.</translation>
    </message>
    <message>
        <source>Could not copy temp file to document file</source>
        <translation type="vanished">La copie du fichier temporaire vers le document a échoué</translation>
    </message>
    <message>
        <source>Could not remove document file</source>
        <translation type="vanished">Impossible de supprimer le document</translation>
    </message>
    <message>
        <source>Could not load schema file &apos;%1&apos;.</source>
        <translation type="vanished">Impossible de lire le schéma de fichier &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Fail to write Canonical XML.</source>
        <translation>Echec d&apos;écriture du XML Canonique.</translation>
    </message>
    <message>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
    <message>
        <source>Not unique id (%1)</source>
        <translation>Pas un identifiant unique (%1)</translation>
    </message>
    <message>
        <source>This id (%1) is not unique.</source>
        <translation>Cet identifiant (%1) n&apos;est pas unique.</translation>
    </message>
    <message>
        <source>Couldn&apos;t get version information.</source>
        <translation>Impossible d&apos;obtenir les informations de version.</translation>
    </message>
    <message>
        <source>Too many tags &lt;%1&gt; in file.</source>
        <translation>Trop d&apos;étiquettes &lt;%1&gt; dans le fichier.</translation>
    </message>
    <message>
        <source>Version &quot;%1&quot; invalid.</source>
        <translation>Version &quot;%1&quot; invalide.</translation>
    </message>
    <message>
        <source>Version &quot;0.0.0&quot; invalid.</source>
        <translation>Version &quot;0.0.0&quot; invalide.</translation>
    </message>
</context>
<context>
    <name>VDrawTool</name>
    <message>
        <source>Edit wrong formula</source>
        <translation type="vanished">Editer la formule erronée</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>This id (%1) is not unique.</source>
        <translation>Cet identifiant (%1) n&apos;est pas unique.</translation>
    </message>
    <message>
        <source>Add to group</source>
        <translation>Ajouter au groupe</translation>
    </message>
    <message>
        <source>Remove from group</source>
        <translation>Retirer du groupe</translation>
    </message>
    <message>
        <source>Show label</source>
        <translation>Montrer l&apos;étiquette</translation>
    </message>
    <message>
        <source>Restore label position</source>
        <translation>Restaurer la position de l&apos;étiquette</translation>
    </message>
</context>
<context>
    <name>VException</name>
    <message>
        <source>Exception: %1</source>
        <translation>Exception : (%1)</translation>
    </message>
</context>
<context>
    <name>VFormula</name>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Data container is empty</source>
        <translation>Le conteneur de données est vide</translation>
    </message>
    <message>
        <source>Math parser error: %1</source>
        <translation>Erreur d&apos;analyse de Math : %1</translation>
    </message>
    <message>
        <source>Result is infinite</source>
        <translation>Le résultat est infini</translation>
    </message>
    <message>
        <source>Result is NaN</source>
        <translation>Le résultat n&apos;est pas un nombre (NAN)</translation>
    </message>
    <message>
        <source>Result is zero</source>
        <translation>Le résultat est zéro</translation>
    </message>
    <message>
        <source>Result less than zero</source>
        <translation>Résultat inférieur à zéro</translation>
    </message>
    <message>
        <source>Formula is empty</source>
        <translation>La formule est vide</translation>
    </message>
    <message>
        <source>Not evaluated</source>
        <translation>Non évalué</translation>
    </message>
</context>
<context>
    <name>VFormulaData</name>
    <message>
        <source>Not evaluated</source>
        <translation>Non évalué</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Formula is empty</source>
        <translation>La formule est vide</translation>
    </message>
</context>
<context>
    <name>VFormulaProperty</name>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation>Formule</translation>
    </message>
</context>
<context>
    <name>VLayoutConverter</name>
    <message>
        <source>Could not change version.</source>
        <translation>Impossible de modifier la version.</translation>
    </message>
</context>
<context>
    <name>VLayoutExporter</name>
    <message>
        <source>Can&apos;t save file &apos;%1&apos;. Error: %2.</source>
        <translation>Impossible d&apos;enregistrer le fichier &apos;%1&apos;. Erreur : %2.</translation>
    </message>
    <message>
        <source>Cannot set printer page size</source>
        <translation>Impossible de définir la taille de page d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot set printer margins</source>
        <translation>Impossible de définir les marges d&apos;impression</translation>
    </message>
    <message>
        <source>Can&apos;t open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Export raw layout data failed. %1.</source>
        <translation>Echec de l&apos;export au format données brutes du plan de coupe. %1.</translation>
    </message>
    <message>
        <source>Creating file &apos;%1&apos; failed! %2</source>
        <translation>La création du fichier &apos;%1&apos; a échoué ! %2</translation>
    </message>
    <message>
        <source>Can&apos;t create an AAMA dxf file.</source>
        <translation>Impossible de créer un fichier au format AAMA-DXF.</translation>
    </message>
    <message>
        <source>Can&apos;t create an ASTM dxf file.</source>
        <translation>Impossible de créer un fichier au format ASTM-DXF.</translation>
    </message>
    <message>
        <source>(flat) files</source>
        <translation>fichiers plats (CSV)</translation>
    </message>
    <message>
        <source>files</source>
        <translation>fichiers</translation>
    </message>
    <message>
        <source>Image files</source>
        <translation>Format image</translation>
    </message>
    <message>
        <source>tiled</source>
        <translation>empilé</translation>
    </message>
    <message>
        <source>Numerical control</source>
        <translation>Contrôle numérique</translation>
    </message>
    <message>
        <source>Raw Layout Data</source>
        <translation>Données de plan de coupe brutes</translation>
    </message>
    <message>
        <source>Can&apos;t create a flat dxf file.</source>
        <translation>Impossible de créer un fichier au format DXF.</translation>
    </message>
</context>
<context>
    <name>VLayoutPiece</name>
    <message>
        <source>Piece %1 doesn&apos;t have shape.</source>
        <translation>La pièce %1 n&apos;a pas de forme.</translation>
    </message>
</context>
<context>
    <name>VMeasurements</name>
    <message>
        <source>Can&apos;t find measurement &apos;%1&apos;</source>
        <translation>Impossible de trouver la mesure &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The measurement name is empty!</source>
        <translation>Le nom de mesure est vide !</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>Hip</source>
        <translation>Hanche</translation>
    </message>
    <message>
        <source>Waist</source>
        <translation>Taille</translation>
    </message>
</context>
<context>
    <name>VNodePoint</name>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>In layout</source>
        <translation>Dans isolation</translation>
    </message>
    <message>
        <source>Show label</source>
        <translation>Montrer l&apos;étiquette</translation>
    </message>
    <message>
        <source>Exclude</source>
        <translation>Exclure</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation>Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Force flipping</source>
        <translation>Forcer la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>by length</source>
        <translation>Par longueur</translation>
    </message>
    <message>
        <source>by points intersetions</source>
        <translation>Par points d&apos;intersections</translation>
    </message>
    <message>
        <source>by first edge symmetry</source>
        <translation>par symétrie du premier côté</translation>
    </message>
    <message>
        <source>by second edge symmetry</source>
        <translation>par symétrie du second côté</translation>
    </message>
    <message>
        <source>by first edge right angle</source>
        <translation>par l&apos;angle droit du premier côté</translation>
    </message>
    <message>
        <source>by second edge right angle</source>
        <translation>par l&apos;angle droit du second côté</translation>
    </message>
    <message>
        <source>Passmark</source>
        <translation>Cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Seam allowance angle</source>
        <translation>Angle de la marge de couture</translation>
    </message>
    <message>
        <source>Passmark angle</source>
        <translation>Angle du cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>Straightforward</source>
        <translation>Tout droit</translation>
    </message>
    <message>
        <source>Bisector</source>
        <translation>Bissectrice</translation>
    </message>
    <message>
        <source>Intersection</source>
        <translation>Intersection</translation>
    </message>
    <message>
        <source>Intersection (only left)</source>
        <translation>Intersection (seulement à gauche)</translation>
    </message>
    <message>
        <source>Intersection (only right)</source>
        <translation>Intersection (seulement à droite)</translation>
    </message>
    <message>
        <source>Intersection 2</source>
        <translation>Intersection 2</translation>
    </message>
    <message>
        <source>Intersection 2 (only left)</source>
        <translation>Intersection 2 (gauche seulement)</translation>
    </message>
    <message>
        <source>Intersection 2 (only right)</source>
        <translation>Intersection 2 (droite seulement)</translation>
    </message>
    <message>
        <source>Passmark mark</source>
        <translation>Marquage du cran d&apos;assemblage</translation>
    </message>
    <message>
        <source>One line</source>
        <translation>Un trait</translation>
    </message>
    <message>
        <source>Two lines</source>
        <translation>Deux traits</translation>
    </message>
    <message>
        <source>Three lines</source>
        <translation>Trois traits</translation>
    </message>
    <message>
        <source>T mark</source>
        <translation>Marque T</translation>
    </message>
    <message>
        <source>V mark</source>
        <translation>Marque V</translation>
    </message>
    <message>
        <source>V mark 2</source>
        <translation>Marquage en V (n° 2)</translation>
    </message>
    <message>
        <source>U mark</source>
        <translation>En forme de U</translation>
    </message>
    <message>
        <source>Box mark</source>
        <translation>Rectangle</translation>
    </message>
</context>
<context>
    <name>VPApplication</name>
    <message>
        <source>Error parsing file. Program will be terminated.</source>
        <translation>Erreur d&apos;interprétation du fichier. Fin du programme.</translation>
    </message>
    <message>
        <source>Error bad id. Program will be terminated.</source>
        <translation>Erreur d&apos;identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Error can&apos;t convert value. Program will be terminated.</source>
        <translation>Erreur : valeur non convertissable. Fin du programme.</translation>
    </message>
    <message>
        <source>Error empty parameter. Program will be terminated.</source>
        <translation>Erreur : paramètre vide. Fin du programme.</translation>
    </message>
    <message>
        <source>Error wrong id. Program will be terminated.</source>
        <translation>Erreur : mauvais identifiant. Fin du programme.</translation>
    </message>
    <message>
        <source>Something&apos;s wrong!!</source>
        <translation>Quel que chose ne va pas!!</translation>
    </message>
    <message>
        <source>Exception thrown: %1. Program will be terminated.</source>
        <translation>Erreur d&apos;exception : %1. Fin du programme.</translation>
    </message>
    <message>
        <source>Can&apos;t begin to listen for incoming connections on name &apos;%1&apos;</source>
        <translation>Ne peut être à l&apos;écoute des connexions entrantes de %1</translation>
    </message>
    <message>
        <source>Please, provide one input file.</source>
        <translation>Merci de choisir un fichier.</translation>
    </message>
    <message>
        <source>Export mode doesn&apos;t support opening several files.</source>
        <translation>Impossible d&apos;ouvrir plusieurs fichiers simultanément en mode export.</translation>
    </message>
    <message>
        <source>Import raw layout data does not support opening several layout files.</source>
        <translation>Impossible d&apos;ouvrir plusieurs fichiers simultanément quand import de données brutes de plan de coupe.</translation>
    </message>
</context>
<context>
    <name>VPCarrousel</name>
    <message>
        <source>Unplaced pieces</source>
        <translation>Pièces non placées</translation>
    </message>
    <message>
        <source>Pieces of</source>
        <translation>Pièces associées</translation>
    </message>
</context>
<context>
    <name>VPCarrouselPieceList</name>
    <message>
        <source>Move to Sheet</source>
        <translation>Aller à la feuille</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Remove from Sheet</source>
        <translation>Retirer de la feuille</translation>
    </message>
    <message>
        <source>Move to</source>
        <translation>Déplacer vers</translation>
    </message>
</context>
<context>
    <name>VPDialogAbout</name>
    <message>
        <source>About Puzzle</source>
        <translation>A propos du puzzle</translation>
    </message>
    <message>
        <source>Puzzle version</source>
        <translation>Version puzzle</translation>
    </message>
    <message>
        <source>Build revision:</source>
        <translation>N° de version :</translation>
    </message>
    <message>
        <source>This program is part of Valentina project.</source>
        <translation>Ce programme fait partie du projet Valentina.</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation>Vérifier les Mises à Jour</translation>
    </message>
    <message>
        <source>Cannot open your default browser</source>
        <translation>Impossible d&apos;ouvrir votre navigateur par défaut</translation>
    </message>
    <message>
        <source>Build revision: %1</source>
        <translation>N° de version : %1</translation>
    </message>
    <message>
        <source>Built on %1 at %2</source>
        <translation>Compilé le %1 à %2</translation>
    </message>
    <message>
        <source>Web site : %1</source>
        <translation>Site web : %1</translation>
    </message>
</context>
<context>
    <name>VPE::VBoolProperty</name>
    <message>
        <source>True</source>
        <translation type="vanished">Vrai</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Faux</translation>
    </message>
</context>
<context>
    <name>VPE::VFileEditWidget</name>
    <message>
        <source>Directory</source>
        <translation>Dossier</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation>Ouvrir fichier</translation>
    </message>
</context>
<context>
    <name>VPGraphicsPiece</name>
    <message>
        <source>Move to</source>
        <translation>Déplacer vers</translation>
    </message>
    <message>
        <source>Remove from Sheet</source>
        <translation>Supprimer de la feuille</translation>
    </message>
</context>
<context>
    <name>VPLayout</name>
    <message>
        <source>Sheet %1</source>
        <translation>Feuille %1</translation>
    </message>
</context>
<context>
    <name>VPLayoutFileReader</name>
    <message>
        <source>Piece</source>
        <translation>Pièce</translation>
    </message>
    <message>
        <source>Got empty attribute &apos;%1&apos;</source>
        <translation>Attribut &apos;%1&apos; manquant</translation>
    </message>
    <message>
        <source>Piece %1 invalid.</source>
        <translation>Pièce %1 non valide.</translation>
    </message>
    <message>
        <source>Error in line %1. Seam allowance is empty.</source>
        <translation>Erreur ligne %1. La marge de couture est manquante.</translation>
    </message>
    <message>
        <source>Error in line %1. Grainline is empty.</source>
        <translation>Erreur ligne %1. Le droit-fil est manquant.</translation>
    </message>
    <message>
        <source>Error in line %1. Internal path shape is empty.</source>
        <translation>Erreur dans la ligne %1. Le chemin interne est vide.</translation>
    </message>
    <message>
        <source>Error in line %1. Marker shape is empty.</source>
        <translation>Erreur ligne %1. La forme du repère est manquante.</translation>
    </message>
    <message>
        <source>Unexpected tag %1 in line %2</source>
        <translation>Tag %1 inattendu à ligne %2</translation>
    </message>
</context>
<context>
    <name>VPMainGraphicsView</name>
    <message>
        <source>Restore transformation origin</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <source>Remove sheet</source>
        <translation>Supprimer la feuille</translation>
    </message>
</context>
<context>
    <name>VPMainWindow</name>
    <message>
        <source>Puzzle</source>
        <translation>Puzzle</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Window</source>
        <translation>&amp;Fenêtre</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Aid&amp;e</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <source>Piece Carrousel</source>
        <translation>Carrousel des pièces</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>Current piece properties</source>
        <translation>Propriétés de la pièce sélectionnée</translation>
    </message>
    <message>
        <source>Current piece</source>
        <translation>Pièce sélectionnée</translation>
    </message>
    <message>
        <source>Infos</source>
        <translation>Infos</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>Seamline</source>
        <translation>Ligne de couture</translation>
    </message>
    <message>
        <source>Show Seamline</source>
        <translation>Afficher la ligne de couture</translation>
    </message>
    <message>
        <source>Geometry</source>
        <translation>Géométrie</translation>
    </message>
    <message>
        <source>Mirror piece</source>
        <translation>Pièce en miroir</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle :</translation>
    </message>
    <message>
        <source>Placement</source>
        <translation>Placement</translation>
    </message>
    <message>
        <source>No piece selected</source>
        <translation>Aucune pièce sélectionnée</translation>
    </message>
    <message>
        <source>Sheet properties</source>
        <translation>Propriétés de la feuille</translation>
    </message>
    <message>
        <source>Current sheet</source>
        <translation>Feuille en cours</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Remove unused length</source>
        <translation>Rogner la longueur non utilisée</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Right:</source>
        <translation>Droite :</translation>
    </message>
    <message>
        <source>Top:</source>
        <translation>Haut :</translation>
    </message>
    <message>
        <source>Left:</source>
        <translation>Gauche :</translation>
    </message>
    <message>
        <source>Bottom:</source>
        <translation>Bas :</translation>
    </message>
    <message>
        <source>Placement Grid</source>
        <translation>Grille de placement</translation>
    </message>
    <message>
        <source>Show Grid</source>
        <translation>Afficher la grille</translation>
    </message>
    <message>
        <source>Column width</source>
        <translation>Largeur de colonne</translation>
    </message>
    <message>
        <source>Row height</source>
        <translation>Hauteur de rangée</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Contrôles</translation>
    </message>
    <message>
        <source>Follow grainline</source>
        <translation>Respecter le droit-fil</translation>
    </message>
    <message>
        <source>Sticky edges</source>
        <translation>Forcer l&apos;espacement minimum entre pièces</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <source>Export Sheet</source>
        <translation>Exporter la feuille</translation>
    </message>
    <message>
        <source>Tiles properties</source>
        <translation>Propriétés de l&apos;empilement</translation>
    </message>
    <message>
        <source>Tiled Pdf Export</source>
        <translation>Export au format PDF empilé</translation>
    </message>
    <message>
        <source>Show Tiles on sheet</source>
        <translation>Afficher la vue empilée sur la feuille</translation>
    </message>
    <message>
        <source>Layout properties</source>
        <translation>Propriétés du plan de coupe</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Plan de coupe</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Warning superposition of pieces</source>
        <translation>Avertir de la superposition des pièces</translation>
    </message>
    <message>
        <source>Warning pieces out of bound</source>
        <translation>Avertir quand le plan excède la zone imprimable de la feuille</translation>
    </message>
    <message>
        <source>zoom</source>
        <translation>zoom</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Save &amp;As</source>
        <translation>S&amp;auvegarder sous</translation>
    </message>
    <message>
        <source>&amp;Import Raw Layout Data</source>
        <translation>Importer données brutes de plan de coupe (*rld)</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <source>About &amp;Puzzle</source>
        <translation>A propos de &amp;Puzzle</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <source>Zoom 1:1</source>
        <translation>Zoom 1:1</translation>
    </message>
    <message>
        <source>Zoom fit best</source>
        <translation>Zoom optimal</translation>
    </message>
    <message>
        <source>Zoom sheet</source>
        <translation>Zoomer sur la feuille</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; doesn&apos;t exist!</source>
        <translation>Le fichier &apos;%1&apos; n&apos;existe pas !</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>Fail to create layout.</source>
        <translation>Impossible de créer le plan de coupe.</translation>
    </message>
    <message>
        <source>Could not extract data from file &apos;%1&apos;. %2</source>
        <translation>Impossible d&apos;extraire les données du fichier &apos;%1&apos;. %2</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation>Echelle :</translation>
    </message>
    <message>
        <source>untitled %1.vlt</source>
        <translation>sans nom %1.vlt</translation>
    </message>
    <message>
        <source>untitled.vlt</source>
        <translation>sans-nom.vlt</translation>
    </message>
    <message>
        <source>read only</source>
        <translation>Lecture seule</translation>
    </message>
    <message>
        <source>Cannot read settings from a malformed .INI file.</source>
        <translation>Lecture des réglages impossible, fichier .INI invalide.</translation>
    </message>
    <message>
        <source>Cannot save settings. Access denied.</source>
        <translation>Impossible d&apos;enregistrer les réglages. Accès refusé.</translation>
    </message>
    <message>
        <source>Failed to open file, is it writable?</source>
        <translation>Impossible d&apos;ouvrir le fichier, avez-vous les droits d&apos;écriture ?</translation>
    </message>
    <message>
        <source>&amp;New Window</source>
        <translation>&amp;Nouvelle Fenêtre</translation>
    </message>
    <message>
        <source>Layout files</source>
        <translation>Fichiers du plan de coupe</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <source>Could not save the file</source>
        <translation>Impossible de sauvegarder le fichier</translation>
    </message>
    <message>
        <source>layout</source>
        <translation>plan de coupe</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window.</source>
        <translation>Verrouillage impossible. Le fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>Could not save file</source>
        <translation>Impossible de sauvegarder le fichier</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window. Expect collissions when run 2 copies of the program.</source>
        <translation>Verrouillage impossible car le fichier est déjà ouvert dans une autre fenêtre. Ceci se produit généralement quand 2 copies du programme sont ouvertes en même temps.</translation>
    </message>
    <message>
        <source>Raw Layout files</source>
        <translation>Fichiers de données brutes de plan de coupe</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <source>&amp;Sheet</source>
        <translation>Fe&amp;uille</translation>
    </message>
    <message>
        <source>&amp;Layout</source>
        <translation>P&amp;lan de coupe</translation>
    </message>
    <message>
        <source>Watermark</source>
        <translation>Filigrane</translation>
    </message>
    <message>
        <source>Gradation id:</source>
        <translation>Id gradation :</translation>
    </message>
    <message>
        <source>Transformation</source>
        <translation>Transformation</translation>
    </message>
    <message>
        <source>Translate</source>
        <comment>Translate piece</comment>
        <translation>Déplacement</translation>
    </message>
    <message>
        <source>Horizontal:</source>
        <translation>Horizontal :</translation>
    </message>
    <message>
        <source>Vertical:</source>
        <translation>Vertical :</translation>
    </message>
    <message>
        <source>Relative translation</source>
        <translation>Déplacement relatif</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Apply to each piece separately</source>
        <translation>Appliquer séparément à chaque pièce</translation>
    </message>
    <message>
        <source>Unit:</source>
        <translation>Unité :</translation>
    </message>
    <message>
        <source>Templates:</source>
        <translation>Modèles :</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Grainline orientation:</source>
        <translation>Sens du droit-fil :</translation>
    </message>
    <message>
        <source>Force the grainline orientation to always be horizontal</source>
        <translation>Forcer droit-fil toujours horizontal</translation>
    </message>
    <message>
        <source>Force the grainline orientation to always be vertical</source>
        <translation>Forcer droit-fil toujours vertical</translation>
    </message>
    <message>
        <source>Ignore margins</source>
        <translation>Ignorer les marges</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <source>Show watermark preview</source>
        <translation>Afficher aperçu du filigrane</translation>
    </message>
    <message>
        <source>Show watermark</source>
        <translation>Afficher le filigrane</translation>
    </message>
    <message>
        <source>Print tiles scheme</source>
        <translation>Imprimer aussi le plan général de l&apos;empilement</translation>
    </message>
    <message>
        <source>Pieces gap:</source>
        <translation>Espacement entre pièces :</translation>
    </message>
    <message>
        <source>Export Layout</source>
        <translation>Exporter le plan de coupe</translation>
    </message>
    <message>
        <source>Undo commands</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Add Sheet</source>
        <translation>Ajouter une feuille</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <source>Print preview</source>
        <translation>Aperçu avant impression</translation>
    </message>
    <message>
        <source>Print tiled</source>
        <translation>Impression empilée</translation>
    </message>
    <message>
        <source>Print preview tiled</source>
        <translation>Imprimer l&apos;aperçu empilé</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <source>Create or edit a watermark</source>
        <translation>Créer ou éditer un filigrane</translation>
    </message>
    <message>
        <source>Edit current</source>
        <translation>Editer le filigrane chargé</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Retirer</translation>
    </message>
    <message>
        <source>Unable to read a layout file. %1</source>
        <translation>Impossible de lire le fichier %1</translation>
    </message>
    <message>
        <source>Piece %1 invalid.</source>
        <translation>Pièce %1 non valide.</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Rétablir</translation>
    </message>
    <message>
        <source>Pixels</source>
        <translation>Pixels</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>Layout has been modified.
Do you want to save your changes?</source>
        <translation>Le plan de coupe a été modifié.
Voulez-vous enregistrer les modifications ?</translation>
    </message>
    <message>
        <source>Save…</source>
        <translation>Enregistrer …</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>Ne pas sauvegarder</translation>
    </message>
    <message>
        <source>Can&apos;t create a path</source>
        <translation>Impossible de créer l&apos;emplacement</translation>
    </message>
    <message>
        <source>Can&apos;t open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Failed in flushing page to disk, disk full?</source>
        <translation>Echec d&apos;envoi de la page sur le disque, disque plein ?</translation>
    </message>
    <message>
        <source>The layout is invalid.</source>
        <translation>Plan de coupe non valide.</translation>
    </message>
    <message>
        <source>The layout is invalid. Piece out of bound. Do you want to continue export?</source>
        <translation>Le plan de coupe est non valide. Zone imprimable excédée. Voulez-vous poursuivre quand même ?</translation>
    </message>
    <message>
        <source>The layout is invalid. Pieces superposition. Do you want to continue export?</source>
        <translation>Le plan de coupe est non valide, les pièces se superposent. Voulez-vous poursuivre quand même ?</translation>
    </message>
    <message>
        <source>Sheet %1</source>
        <translation>Feuille %1</translation>
    </message>
    <message>
        <source>translate pieces</source>
        <translation>Déplacement des pièces</translation>
    </message>
    <message>
        <source>rotate pieces</source>
        <translation>Rotation des pièces</translation>
    </message>
    <message>
        <source>For printing multipages document all sheet should have the same size.</source>
        <translation>Pour imprimer un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>Print error</source>
        <translation>Erreur d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot proceed because there are no available printers in your system.</source>
        <translation>Impossible de poursuivre, aucune imprimante système détectée.</translation>
    </message>
    <message>
        <source>Watermark files</source>
        <translation>Fichiers filigranes</translation>
    </message>
    <message>
        <source>Unable to get sheet page settings</source>
        <translation>Impossible d&apos;analyser les réglages de la feuille</translation>
    </message>
    <message>
        <source>Show tile number</source>
        <translation>Afficher les numéros des pages</translation>
    </message>
    <message>
        <source>Z value</source>
        <translation>Valeur Z</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>En arrière-plan</translation>
    </message>
    <message>
        <source>Down</source>
        <translation>Arrière +</translation>
    </message>
    <message>
        <source>Up</source>
        <translation>Avant +</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>En avant-plan</translation>
    </message>
</context>
<context>
    <name>VPTileFactory</name>
    <message>
        <source>Grid ( %1 , %2 )</source>
        <translation>Grille ( %1 , %2 )</translation>
    </message>
    <message>
        <source>Page %1 of %2</source>
        <translation>Page %1 sur %2</translation>
    </message>
    <message>
        <source>cm</source>
        <comment>unit</comment>
        <translation>cm</translation>
    </message>
    <message>
        <source>in</source>
        <comment>unit</comment>
        <translation>pouces</translation>
    </message>
</context>
<context>
    <name>VPUndoAddSheet</name>
    <message>
        <source>add sheet</source>
        <translation>Ajouter une feuille</translation>
    </message>
</context>
<context>
    <name>VPUndoMovePieceOnSheet</name>
    <message>
        <source>move piece on sheet</source>
        <translation>déplacer la pièce sur la feuille</translation>
    </message>
</context>
<context>
    <name>VPUndoOriginMove</name>
    <message>
        <source>move transformation origin</source>
        <translation>Déplacer le point d&apos;origine</translation>
    </message>
</context>
<context>
    <name>VPUndoPieceMove</name>
    <message>
        <source>move piece</source>
        <translation>déplacer la pièce</translation>
    </message>
</context>
<context>
    <name>VPUndoPieceRotate</name>
    <message>
        <source>rotate piece</source>
        <translation>Rotation de la pièce</translation>
    </message>
</context>
<context>
    <name>VPUndoRemoveSheet</name>
    <message>
        <source>add sheet</source>
        <translation>Ajouter une feuille</translation>
    </message>
</context>
<context>
    <name>VPattern</name>
    <message>
        <source>Error no unique id.</source>
        <translation type="vanished">Erreur : id non unique.</translation>
    </message>
    <message>
        <source>Error parsing file.</source>
        <translation>Erreur d&apos;interprétation de fichier.</translation>
    </message>
    <message>
        <source>Error can&apos;t convert value.</source>
        <translation>Erreur : valeur non convertissable.</translation>
    </message>
    <message>
        <source>Error empty parameter.</source>
        <translation>Erreur : paramètre vide.</translation>
    </message>
    <message>
        <source>Error wrong id.</source>
        <translation>Erreur : id erroné.</translation>
    </message>
    <message>
        <source>Error parsing file (std::bad_alloc).</source>
        <translation>Erreur d&apos;interprétation de fichier. (std::bad_alloc).</translation>
    </message>
    <message>
        <source>Error creating or updating detail</source>
        <translation>Erreur lors de la création ou de la mise à jour de la pièce de patron</translation>
    </message>
    <message>
        <source>Error creating or updating single point</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un point seul</translation>
    </message>
    <message>
        <source>Error creating or updating point of end line</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un point de fin de ligne</translation>
    </message>
    <message>
        <source>Error creating or updating point along line</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un point de la ligne</translation>
    </message>
    <message>
        <source>Error creating or updating point of shoulder</source>
        <translation>Erreur lors de la création ou mise à jour du point d&apos;épaule</translation>
    </message>
    <message>
        <source>Error creating or updating point of normal</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un point de la normale</translation>
    </message>
    <message>
        <source>Error creating or updating point of bisector</source>
        <translation>Erreur lors de la création ou mise à jour du point de bisection</translation>
    </message>
    <message>
        <source>Error creating or updating point of lineintersection</source>
        <translation type="vanished">Impossible de créer ou mettre à jour le point d&apos;intersection de 2 lignes</translation>
    </message>
    <message>
        <source>Error creating or updating point of contact</source>
        <translation>Erreur lors de la création ou mise à jour du point de contact</translation>
    </message>
    <message>
        <source>Error creating or updating modeling point</source>
        <translation>Erreur lors de la création ou mise à jour du point de modélisation</translation>
    </message>
    <message>
        <source>Error creating or updating height</source>
        <translation>Erreur lors de la création ou mise à jour de la taille</translation>
    </message>
    <message>
        <source>Error creating or updating triangle</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un triangle</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection</source>
        <translation>Erreur lors de la création ou mise à jour du point d&apos;intersection</translation>
    </message>
    <message>
        <source>Error creating or updating cut spline point</source>
        <translation>Erreur lors de la création ou mise à jour du point de découpe de crannelure</translation>
    </message>
    <message>
        <source>Error creating or updating cut spline path point</source>
        <translation>Erreur lors de la création ou mise à jour de la trajectoire de la spline</translation>
    </message>
    <message>
        <source>Error creating or updating cut arc point</source>
        <translation>Erreur lors de la création ou de la mise à jour d&apos;un point de découpe d&apos;arc</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection line and axis</source>
        <translation>Erreur lors de la création ou de la mise à jour du point d&apos;intersection ligne/axe</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection curve and axis</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection entre courbe et axe</translation>
    </message>
    <message>
        <source>Error creating or updating line</source>
        <translation>Erreur lors de la création ou de la mise à jour de la ligne</translation>
    </message>
    <message>
        <source>Error creating or updating simple curve</source>
        <translation>Erreur lors de la création ou de la mise à jour de la courbe</translation>
    </message>
    <message>
        <source>Error creating or updating curve path</source>
        <translation>Erreur lors de la création ou de la mise à jour du chemin de la courbe</translation>
    </message>
    <message>
        <source>Error creating or updating modeling simple curve</source>
        <translation>Erreur lors de la création ou de la mise à jour de la courbe</translation>
    </message>
    <message>
        <source>Error creating or updating modeling curve path</source>
        <translation>Erreur lors de la création ou de la mise à jour de la trajectoire de la courbe</translation>
    </message>
    <message>
        <source>Error creating or updating simple arc</source>
        <translation>Erreur lors de la création ou mise à jour d&apos;un arc simple</translation>
    </message>
    <message>
        <source>Error creating or updating modeling arc</source>
        <translation>Erreur lors de la création ou mise à jour d&apos;un arc modelé</translation>
    </message>
    <message>
        <source>Error creating or updating union details</source>
        <translation>Erreur lors de la création ou de la mise à jour de la fusion de pièces de patron</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection arcs</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection des arcs</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection circles</source>
        <translation>Impossible de créer ou de mettre à jour le point d&apos;intersection des cercles</translation>
    </message>
    <message>
        <source>Error creating or updating point from circle and tangent</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection entre cercle et tangente</translation>
    </message>
    <message>
        <source>Error creating or updating point from arc and tangent</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection entre arc et tangente</translation>
    </message>
    <message>
        <source>Error creating or updating true darts</source>
        <translation>Impossible de créer ou mettre à jour une pince automatique</translation>
    </message>
    <message>
        <source>Wrong tag name &apos;%1&apos;.</source>
        <translation>Mauvais nom de tag &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Unknown point type &apos;%1&apos;.</source>
        <translation>Point de type inconnu &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Unknown spline type &apos;%1&apos;.</source>
        <translation>Spline inconnue de type &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Unknown arc type &apos;%1&apos;.</source>
        <translation>Courbe inconnue de type &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Unknown tools type &apos;%1&apos;.</source>
        <translation>Outil inconnu de type &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Error not unique id.</source>
        <translation>Erreur, Id. non unique.</translation>
    </message>
    <message>
        <source>Error creating or updating point of intersection curves</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection de courbes</translation>
    </message>
    <message>
        <source>Error creating or updating simple interactive spline</source>
        <translation>Erreur lors de la création ou de la mise à jour de la spline interactive simple</translation>
    </message>
    <message>
        <source>Error creating or updating interactive spline path</source>
        <translation>Erreur lors de la création ou mise à jour du point de découpe de la trajectoire de la spline interactive</translation>
    </message>
    <message>
        <source>Error creating or updating cubic bezier curve</source>
        <translation>Erreur lors de la création ou de la mise à jour de la courbe cubique de bezier</translation>
    </message>
    <message>
        <source>Error creating or updating cubic bezier path curve</source>
        <translation>Erreur lors de la création ou de la mise à jour de la trajectoire de la courbe de bezier</translation>
    </message>
    <message>
        <source>Error creating or updating operation of rotation</source>
        <translation>Erreur lors de la création ou mise à jour de l&apos;opération de rotation</translation>
    </message>
    <message>
        <source>Unknown operation type &apos;%1&apos;.</source>
        <translation>Opération inconnue de type &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Error creating or updating operation of flipping by line</source>
        <translation>Erreur lors de la création ou la mise à jour de l&apos;opération de reproduction d&apos;objets en miroir par rapport à une ligne</translation>
    </message>
    <message>
        <source>Error creating or updating operation of flipping by axis</source>
        <translation>Erreur de création ou de mise à jour de l&apos;opération de reproduction d&apos;objets en miroir par rapport à un axe</translation>
    </message>
    <message>
        <source>Error creating or updating operation of moving</source>
        <translation>Erreur de création ou de mise à jour de l&apos;opération de déplacement</translation>
    </message>
    <message>
        <source>Error creating or updating point of line intersection</source>
        <translation>Impossible de créer ou mettre à jour le point d&apos;intersection de 2 lignes</translation>
    </message>
    <message>
        <source>Error creating or updating simple elliptical arc</source>
        <translation>Erreur de création ou de mise à jour d&apos;arc de cercle elliptique simple</translation>
    </message>
    <message>
        <source>Unknown elliptical arc type &apos;%1&apos;.</source>
        <translation>Type d&apos;arc de cercle elliptique inconnu &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Error creating or updating modeling elliptical arc</source>
        <translation>Erreur de création ou de mise à jour d&apos;arc élliptique</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation>Pièce de patron</translation>
    </message>
    <message>
        <source>Unnamed path</source>
        <translation>Chemin sans nom</translation>
    </message>
    <message>
        <source>Error creating or updating a piece path</source>
        <translation>Erreur de création ou de mise à jour de chemin de pièce de patron</translation>
    </message>
    <message>
        <source>Error creating or updating pin point</source>
        <translation>Erreur de création ou de mise à jour de point d&apos;épingle</translation>
    </message>
    <message>
        <source>Piece path doesn&apos;t contain nodes</source>
        <translation>Le chemin de la pièce ne contient pas de nœuds</translation>
    </message>
    <message>
        <source>Error creating or updating place label</source>
        <translation>Erreur de création ou de mise à jour de repère d&apos;assemblage</translation>
    </message>
    <message>
        <source>Can&apos;t find increment &apos;%1&apos;</source>
        <translation>Impossible de trouver l&apos;incrément &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>VPatternConverter</name>
    <message>
        <source>Unexpected version &quot;%1&quot;.</source>
        <translation type="vanished">Version &quot;%1&quot; inattendue. </translation>
    </message>
    <message>
        <source>Error restoring backup file: %1.</source>
        <translation type="vanished">Erreur de restauration du fichier de sauvegarde : %1.</translation>
    </message>
    <message>
        <source>Error no unique id.</source>
        <translation>Erreur : id non unique.</translation>
    </message>
</context>
<context>
    <name>VPatternImage</name>
    <message>
        <source>Unexpected mime type: %1</source>
        <translation>Type mime inattendu : %1</translation>
    </message>
    <message>
        <source>Couldn&apos;t read the image. Error: %1</source>
        <translation>Impossible de lire l&apos;image. Erreur : %1</translation>
    </message>
    <message>
        <source>No data.</source>
        <translation>Aucune donnée.</translation>
    </message>
    <message>
        <source>Content type is empty.</source>
        <translation>Le type de contenu est vide.</translation>
    </message>
    <message>
        <source>Not image.</source>
        <translation>Ceci n&apos;est pas une image.</translation>
    </message>
    <message>
        <source>Content type mismatch.</source>
        <translation>Le type de contenu ne correspond pas.</translation>
    </message>
</context>
<context>
    <name>VPatternRecipe</name>
    <message>
        <source>Invalid tag %1</source>
        <translation>Tag %1 non valide</translation>
    </message>
    <message>
        <source>Can&apos;t find element by id &apos;%1&apos;</source>
        <translation>Impossible de trouver l&apos;élément avec l&apos;identifiant &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t create history record for the tool.</source>
        <translation>Impossible de créer l&apos;historique pour cet outil.</translation>
    </message>
    <message>
        <source>Reading final measurements error.</source>
        <translation>Erreur de lecture des mesures finales.</translation>
    </message>
    <message>
        <source>Value for final measurtement &apos;%1&apos; is infinite or NaN. Please, check your calculations.</source>
        <translation>La valeur de la mesure finale &apos;%1&apos; est infinie ou n&apos;est pas une valeur numérique. Veuillez vérifier vos calculs.</translation>
    </message>
    <message>
        <source>Unable to create record for final measurement &apos;%1&apos;. Error: %2</source>
        <translation>Impossible d&apos;enregistrer la mesure finale &apos;%1&apos;. Erreur: %2</translation>
    </message>
</context>
<context>
    <name>VPiece</name>
    <message>
        <source>Cannot calculate a notch for point &apos;%1&apos; in piece &apos;%2&apos;.</source>
        <translation>Impossible de calculer un cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce &apos;%2&apos;.</translation>
    </message>
    <message>
        <source>Notch for point &apos;%1&apos; in piece &apos;%2&apos; will be disabled. Manual length is less than allowed value.</source>
        <translation>Le cran d&apos;assemblage pour le point &apos;%1&apos; dans la pièce&apos;%2&apos; va être désactivé. La longueur manuelle spécifiée est inférieure à la longueur autorisée.</translation>
    </message>
    <message>
        <source>Main path of piece %1</source>
        <translation>Contour principal de la pièce %1</translation>
    </message>
</context>
<context>
    <name>VPoster</name>
    <message>
        <source>Grid ( %1 , %2 )</source>
        <translation>Grille ( %1 , %2 )</translation>
    </message>
    <message>
        <source>Page %1 of %2</source>
        <translation>Page %1 sur %2</translation>
    </message>
    <message>
        <source>Sheet %1 of %2</source>
        <translation>Feuille %1 sur %2</translation>
    </message>
    <message>
        <source>Not supported file suffix &apos;%1&apos;</source>
        <translation>Suffixe de fichier &apos;%1&apos; non pris en charge</translation>
    </message>
    <message>
        <source>cm</source>
        <comment>unit</comment>
        <translation>cm</translation>
    </message>
    <message>
        <source>in</source>
        <comment>unit</comment>
        <translation>pouces</translation>
    </message>
    <message>
        <source>Cannot open the watermark image.</source>
        <translation>Impossible d&apos;ouvrir le filigrane.</translation>
    </message>
</context>
<context>
    <name>VPrintLayout</name>
    <message>
        <source>For printing multipages document all sheet should have the same size.</source>
        <translation>Pour imprimer un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>For previewing multipage document all sheet should have the same size.</source>
        <translation>Pour générer un aperçu avant impression d&apos;un document multipages, toutes les feuilles doivent être de la même taille.</translation>
    </message>
    <message>
        <source>Pages will be cropped because they do not fit printer paper size.</source>
        <translation>Les pages vont être tronquées, ne correspondent pas à la taille du papier de l&apos;imprimante.</translation>
    </message>
    <message>
        <source>Print error</source>
        <translation>Erreur d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot proceed because there are no available printers in your system.</source>
        <translation>Impossible de poursuivre, aucune imprimante système détectée.</translation>
    </message>
    <message>
        <source>Failed to open file, is it writable?</source>
        <translation>Impossible d&apos;ouvrir le fichier, avez-vous les droits d&apos;écriture ?</translation>
    </message>
    <message>
        <source>Failed in flushing page to disk, disk full?</source>
        <translation>Echec d&apos;envoi de la page sur le disque, disque plein ?</translation>
    </message>
    <message>
        <source>Cannot set printer margins</source>
        <translation>Impossible de définir les marges d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot set custom printer page size</source>
        <translation>Impossible de définir la taille de page d&apos;impression personnalisée</translation>
    </message>
    <message>
        <source>Cannot set printer page size</source>
        <translation>Impossible de définir la taille de page d&apos;impression</translation>
    </message>
    <message>
        <source>Cannot set printer tiled page size</source>
        <translation>Impossible de définir la taille d&apos;impression de la page empilée</translation>
    </message>
    <message>
        <source>unamed</source>
        <translation>sans nom</translation>
    </message>
    <message>
        <source>File error.

%1

%2</source>
        <translation>Erreur de fichier.

%1

%2</translation>
    </message>
    <message>
        <source>The layout is stale.</source>
        <translation>Le plan de coupe est figée.</translation>
    </message>
    <message>
        <source>The layout was not updated since last pattern modification. Do you want to continue?</source>
        <translation>Le plan de coupe n&apos;a pas été mis à jour depuis la dernière modification du patron. Voulez-vous poursuivre quand même ?</translation>
    </message>
</context>
<context>
    <name>VRawLayout</name>
    <message>
        <source>VRawLayout::ReadFile() failed. Raw layout format prefix mismatch error.</source>
        <translation>VRawLayout::ReadFile() échec. Préfixe du format .rld erroné.</translation>
    </message>
    <message>
        <source>VRawLayout::ReadFile() failed.
Raw layout format compatibility error: actualFileVersion = %1 and fileVersion = %2</source>
        <translation>VRawLayout::ReadFile() échec.
Erreur de compatibilité du format .rld: actualFileVersion = %1 alors que fileVersion = %2</translation>
    </message>
</context>
<context>
    <name>VSplinePath</name>
    <message>
        <source>Not enough points to create the spline.</source>
        <translation>Pas assez de points pour créer la courbe.</translation>
    </message>
    <message>
        <source>This spline does not exist.</source>
        <translation>La courbe n&apos;existe pas.</translation>
    </message>
    <message>
        <source>Can&apos;t cut spline path with one point</source>
        <translation type="vanished">Impossible de couper la courbe avec un point</translation>
    </message>
</context>
<context>
    <name>VTableSearch</name>
    <message>
        <source>Match case</source>
        <translation>Correspondance à la casse</translation>
    </message>
    <message>
        <source>Words</source>
        <translation>Mots</translation>
    </message>
    <message>
        <source>Regex</source>
        <translation>Expression régulière</translation>
    </message>
    <message>
        <source>Match case and words</source>
        <translation>Correspondance à la casse et aux mots</translation>
    </message>
    <message>
        <source>Match case and regex</source>
        <translation>Correspondance à la casse et à l&apos;expression régulière</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>VTextManager</name>
    <message>
        <source>Cut %1 on %2%3</source>
        <translation type="vanished">Couper %1 de %2%3</translation>
    </message>
    <message>
        <source> on Fold</source>
        <translation type="vanished">Au pli</translation>
    </message>
    <message>
        <source>on Fold</source>
        <translation type="vanished">Au pli</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Couper</translation>
    </message>
    <message>
        <source>cut</source>
        <translation type="vanished">Couper</translation>
    </message>
    <message>
        <source>on fold</source>
        <translation type="vanished">au pli</translation>
    </message>
</context>
<context>
    <name>VToolAlongLine</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolArc</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>Start angle</source>
        <translation>Angle de départ</translation>
    </message>
    <message>
        <source>End angle</source>
        <translation>Angle de fin</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolArcWithLength</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>Start angle</source>
        <translation>Angle de départ</translation>
    </message>
    <message>
        <source>End angle</source>
        <translation>Angle de fin</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolBasePoint</name>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolCurveIntersectAxis</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. There is no intersection with curve &apos;%2&apos; and axis through point &apos;%3&apos; with angle %4°</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Il n&apos;y a pas de point d&apos;intersection entre la courbe &apos;%2&apos; et l&apos;axe passant par le point &apos;%3&apos; avec la valeur angulaire de  %4° spécifiée</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Segment 1</source>
        <translation>Segment 1</translation>
    </message>
    <message>
        <source>Segment 2</source>
        <translation>Segment 2</translation>
    </message>
</context>
<context>
    <name>VToolCutArc</name>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>start angle</source>
        <translation>angle de départ</translation>
    </message>
    <message>
        <source>end angle</source>
        <translation>angle de fin</translation>
    </message>
    <message>
        <source>radius</source>
        <translation>rayon</translation>
    </message>
    <message>
        <source>label</source>
        <translation>étiquette</translation>
    </message>
</context>
<context>
    <name>VToolCutSpline</name>
    <message>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <source>length</source>
        <translation>longueur</translation>
    </message>
    <message>
        <source>label</source>
        <translation>étiquette</translation>
    </message>
</context>
<context>
    <name>VToolCutSplinePath</name>
    <message>
        <source>Curve</source>
        <translation>Courbe</translation>
    </message>
    <message>
        <source>length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>label</source>
        <translation>étiquette</translation>
    </message>
</context>
<context>
    <name>VToolDetail</name>
    <message>
        <source>Options</source>
        <translation type="vanished">Options</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Supprimer</translation>
    </message>
    <message>
        <source>move pattern piece label</source>
        <translation type="vanished">déplace l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>resize pattern piece label</source>
        <translation type="vanished">redimensionne l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>rotate pattern piece label</source>
        <translation type="vanished">tourne l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>move pattern info label</source>
        <translation type="vanished">déplace l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>resize pattern info label</source>
        <translation type="vanished">redimensionne l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>rotate pattern info label</source>
        <translation type="vanished">tourne l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>In layout</source>
        <translation type="vanished">Dans isolation</translation>
    </message>
    <message>
        <source>move grainline</source>
        <translation type="vanished">déplacer le droit-fil</translation>
    </message>
    <message>
        <source>resize grainline</source>
        <translation type="vanished">redimensionner le droit-fil</translation>
    </message>
    <message>
        <source>rotate grainline</source>
        <translation type="vanished">Pivoter le droit fil</translation>
    </message>
</context>
<context>
    <name>VToolDoublePoint</name>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolEllipticalArc</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>Start angle</source>
        <translation>Angle de départ</translation>
    </message>
    <message>
        <source>End angle</source>
        <translation>Angle de fin</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
</context>
<context>
    <name>VToolFlippingByAxis</name>
    <message>
        <source>Origin point</source>
        <translation>Point d&apos;origine</translation>
    </message>
    <message>
        <source>Flipping by axis</source>
        <translation>Symétrie axiale</translation>
    </message>
</context>
<context>
    <name>VToolFlippingByLine</name>
    <message>
        <source>First line point</source>
        <translation>Point de la première ligne</translation>
    </message>
    <message>
        <source>Second line point</source>
        <translation>Point de la seconde ligne</translation>
    </message>
    <message>
        <source>flipping by line</source>
        <translation>Reproduction en miroir par rapport à une ligne</translation>
    </message>
</context>
<context>
    <name>VToolHeight</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolLine</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
</context>
<context>
    <name>VToolLineIntersect</name>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Error calculating point &apos;%1&apos;. Lines (%2;%3) and (%4;%5) have no point of intersection</source>
        <translation>Impossible de calculer la position du point &apos;%1&apos;. Les lignes (%2;%3) et (%4;%5) n&apos;ont pas de point d&apos;intersection</translation>
    </message>
</context>
<context>
    <name>VToolLineIntersectAxis</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Error calculating point &apos;%1&apos;. Line (%2;%3) doesn&apos;t have intersection with axis through point &apos;%4&apos; and angle %5°</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Aucun point d&apos;intersection entre la ligne (%2;%3) et l&apos;axe passant par le point &apos;%4&apos; avec l&apos;angle de %5° spécifié</translation>
    </message>
</context>
<context>
    <name>VToolLinePoint</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolMove</name>
    <message>
        <source>Rotation angle</source>
        <translation>Angle de rotation</translation>
    </message>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Center point</source>
        <translation>Point central</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Rotation origin point</source>
        <translation>Point d&apos;origine de la rotation</translation>
    </message>
    <message>
        <source>move</source>
        <translation>déplacer</translation>
    </message>
</context>
<context>
    <name>VToolOptionsPropertyBrowser</name>
    <message>
        <source>Base point</source>
        <translation>Point de départ</translation>
    </message>
    <message>
        <source>Point label</source>
        <translation type="vanished">Nom du point</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>Point at distance and angle</source>
        <translation>Point à distance et angle</translation>
    </message>
    <message>
        <source>Line type</source>
        <translation type="vanished">Type de ligne</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Point at distance along line</source>
        <translation>Point à distance dans l&apos;axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Arc</source>
        <translation>Arc</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>First angle</source>
        <translation type="vanished">Premier angle</translation>
    </message>
    <message>
        <source>Second angle</source>
        <translation type="vanished">Deuxième angle</translation>
    </message>
    <message>
        <source>Point along bisector</source>
        <translation>Point sur bissectrice</translation>
    </message>
    <message>
        <source>Cut arc tool</source>
        <translation>Outil Coupe Arc</translation>
    </message>
    <message>
        <source>Tool for segmenting a curve</source>
        <translation>Outil pour segmenter une courbe</translation>
    </message>
    <message>
        <source>Tool segment a pathed curve</source>
        <translation>Outil segmentation de la trajectoire de la courbe</translation>
    </message>
    <message>
        <source>Perpendicular point along line</source>
        <translation>Point perpendiculaire sur axe d&apos;un segment</translation>
    </message>
    <message>
        <source>Line between points</source>
        <translation>Ligne entre 2 points</translation>
    </message>
    <message>
        <source>Point at line intersection</source>
        <translation>Point à l&apos;intersection de 2 lignes</translation>
    </message>
    <message>
        <source>Point along perpendicular</source>
        <translation>Point sur perpendiculaire</translation>
    </message>
    <message>
        <source>Additional angle degrees</source>
        <translation type="vanished">Degrés d&apos;angle supplémentaires</translation>
    </message>
    <message>
        <source>Point at intersection of arc and line</source>
        <translation type="vanished">Point à l&apos;intersection d&apos;un arc et d&apos;une ligne</translation>
    </message>
    <message>
        <source>Tool to make point from x &amp; y of two other points</source>
        <translation>Créer un nouveau point à partir des coordonnées x &amp; y de deux points différents</translation>
    </message>
    <message>
        <source>Special point on shoulder</source>
        <translation>Point épaule</translation>
    </message>
    <message>
        <source>Curve tool</source>
        <translation>Outil courbe</translation>
    </message>
    <message>
        <source>Curve factor</source>
        <translation type="vanished">Facteur de courbe</translation>
    </message>
    <message>
        <source>Tool for path curve</source>
        <translation>Outil la trajectoire de la courbe</translation>
    </message>
    <message>
        <source>Tool triangle</source>
        <translation>Outil Triangle</translation>
    </message>
    <message>
        <source>Point intersection line and axis</source>
        <translation>Point à l&apos;intersection d&apos;une ligne et d&apos;un axe</translation>
    </message>
    <message>
        <source>Line color</source>
        <translation type="vanished">Couleur de la ligne</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Couleur</translation>
    </message>
    <message>
        <source>Point intersection curve and axis</source>
        <translation>Point d&apos;intersection d&apos;une courbe et d&apos;un axe</translation>
    </message>
    <message>
        <source>First point</source>
        <translation>Premier point</translation>
    </message>
    <message>
        <source>Second point</source>
        <translation>Deuxième point</translation>
    </message>
    <message>
        <source>Arc with given length</source>
        <translation>Arc avec une longueur donnée</translation>
    </message>
    <message>
        <source>True darts</source>
        <translation>Pince automatique</translation>
    </message>
    <message>
        <source>Point 1 label</source>
        <translation type="vanished">Nom du point 1</translation>
    </message>
    <message>
        <source>Point 2 label</source>
        <translation type="vanished">Nom du point 2</translation>
    </message>
    <message>
        <source>Tool to make point from intersection two arcs</source>
        <translation>Outil de création d&apos;un point à l&apos;intersection de deux arcs</translation>
    </message>
    <message>
        <source>Take</source>
        <translation type="vanished">Prendre</translation>
    </message>
    <message>
        <source>Tool to make point from intersection two circles</source>
        <translation>Outil de création d&apos;un point à l&apos;intersection de deux cercles</translation>
    </message>
    <message>
        <source>First circle radius</source>
        <translation type="vanished">Rayon du premier cercle</translation>
    </message>
    <message>
        <source>Second circle radius</source>
        <translation type="vanished">Rayon du deuxième cercle</translation>
    </message>
    <message>
        <source>Tool to make point from circle and tangent</source>
        <translation>Outil de création d&apos;un point à l&apos;intersection d&apos;un cercle et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Circle radius</source>
        <translation type="vanished">Rayon du cercle</translation>
    </message>
    <message>
        <source>Tool to make point from arc and tangent</source>
        <translation>Outil de création d&apos;un point à l&apos;intersection d&apos;un arc et d&apos;une tangente</translation>
    </message>
    <message>
        <source>Highest point</source>
        <translation>Point le plus haut</translation>
    </message>
    <message>
        <source>Lowest point</source>
        <translation>Point le plus bas</translation>
    </message>
    <message>
        <source>Leftmost point</source>
        <translation>Point le plus à gauche</translation>
    </message>
    <message>
        <source>Rightmost point</source>
        <translation>Point le plus à droite</translation>
    </message>
    <message>
        <source>Tool to make point from intersection two curves</source>
        <translation>Outil pour créer un point d&apos;intersection entre deux courbes</translation>
    </message>
    <message>
        <source>Vertical correction</source>
        <translation type="vanished">Correction verticale</translation>
    </message>
    <message>
        <source>Horizontal correction</source>
        <translation type="vanished">Correction horizontale</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>C1: angle</source>
        <translation type="vanished">C1: angle</translation>
    </message>
    <message>
        <source>C1: length</source>
        <translation type="vanished">C1: longueur</translation>
    </message>
    <message>
        <source>C2: angle</source>
        <translation type="vanished">C2: angle</translation>
    </message>
    <message>
        <source>C2: length</source>
        <translation type="vanished">C2: longueur</translation>
    </message>
    <message>
        <source>Cubic bezier curve</source>
        <translation>Courbe de Bezier Cubique</translation>
    </message>
    <message>
        <source>Tool cubic bezier curve</source>
        <translation>Outil courbe de bezier</translation>
    </message>
    <message>
        <source>Tool rotation</source>
        <translation>Outil rotation</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation type="vanished">Suffixe</translation>
    </message>
    <message>
        <source>Vertical axis</source>
        <translation>Axes verticaux</translation>
    </message>
    <message>
        <source>Horizontal axis</source>
        <translation>Axes horizontaux</translation>
    </message>
    <message>
        <source>Tool move</source>
        <translation>Outil main</translation>
    </message>
    <message>
        <source>Tool flipping by line</source>
        <translation>Outil de reproduction en miroir par rapport à une ligne</translation>
    </message>
    <message>
        <source>Tool flipping by axis</source>
        <translation>Outil de reproduction en miroir par rapport à un axe</translation>
    </message>
    <message>
        <source>Axis type</source>
        <translation type="vanished">Type d&apos;axe</translation>
    </message>
    <message>
        <source>Elliptical arc</source>
        <translation>Arc elliptique</translation>
    </message>
    <message>
        <source>Point label:</source>
        <translation>Nom du point :</translation>
    </message>
    <message>
        <source>Position:</source>
        <translation>Position :</translation>
    </message>
    <message>
        <source>Base point:</source>
        <translation>Point de base :</translation>
    </message>
    <message>
        <source>Line type:</source>
        <translation>Type de ligne :</translation>
    </message>
    <message>
        <source>Line color:</source>
        <translation>Couleur de la ligne :</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>First point:</source>
        <translation>Premier point :</translation>
    </message>
    <message>
        <source>Second point:</source>
        <translation>Deuxième point :</translation>
    </message>
    <message>
        <source>Center point:</source>
        <translation>Point central :</translation>
    </message>
    <message>
        <source>Radius:</source>
        <translation>Rayon :</translation>
    </message>
    <message>
        <source>First angle:</source>
        <translation>Premier angle :</translation>
    </message>
    <message>
        <source>Second angle:</source>
        <translation>Second angle :</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Third point:</source>
        <translation>Troisième point :</translation>
    </message>
    <message>
        <source>Point 1 label:</source>
        <translation>Etiquette du point 1 :</translation>
    </message>
    <message>
        <source>Point 2 label:</source>
        <translation>Etiquette du point 2 :</translation>
    </message>
    <message>
        <source>First base point:</source>
        <translation>Premier point de base :</translation>
    </message>
    <message>
        <source>Second base point:</source>
        <translation>Second point de base :</translation>
    </message>
    <message>
        <source>First dart point:</source>
        <translation>Premier point de la pince :</translation>
    </message>
    <message>
        <source>Arc:</source>
        <translation>Arc :</translation>
    </message>
    <message>
        <source>Curve:</source>
        <translation>Courbe :</translation>
    </message>
    <message>
        <source>First line point:</source>
        <translation>Premier point de la ligne :</translation>
    </message>
    <message>
        <source>Second line point:</source>
        <translation>Second point de la ligne :</translation>
    </message>
    <message>
        <source>First line (first point):</source>
        <translation>Première ligne (premier point) :</translation>
    </message>
    <message>
        <source>First line (second point):</source>
        <translation>Première ligne (second point) :</translation>
    </message>
    <message>
        <source>Second line (first point):</source>
        <translation>Deuxième ligne (premier point) :</translation>
    </message>
    <message>
        <source>Second line (second point):</source>
        <translation>Deuxième ligne (second point) :</translation>
    </message>
    <message>
        <source>Additional angle degrees:</source>
        <translation>Degrés supplémentaires de l&apos;angle:</translation>
    </message>
    <message>
        <source>Center of arc:</source>
        <translation>Centre de l&apos;arc :</translation>
    </message>
    <message>
        <source>Top of the line:</source>
        <translation>Début de la ligne :</translation>
    </message>
    <message>
        <source>End of the line:</source>
        <translation>Fin de la ligne :</translation>
    </message>
    <message>
        <source>X: vertical point:</source>
        <translation>X : Point vertical :</translation>
    </message>
    <message>
        <source>Y: horizontal point:</source>
        <translation>Y : Point horizontal :</translation>
    </message>
    <message>
        <source>First arc:</source>
        <translation>Premier arc de cercle :</translation>
    </message>
    <message>
        <source>Second arc:</source>
        <translation>Second arc de cercle :</translation>
    </message>
    <message>
        <source>Take:</source>
        <translation>Prendre :</translation>
    </message>
    <message>
        <source>Center of the first circle:</source>
        <translation>Centre du premier cercle :</translation>
    </message>
    <message>
        <source>Center of the second circle:</source>
        <translation>Centre du second cercle :</translation>
    </message>
    <message>
        <source>First circle radius:</source>
        <translation>Rayon du premier cercle :</translation>
    </message>
    <message>
        <source>Second circle radius:</source>
        <translation>Rayon du second cercle :</translation>
    </message>
    <message>
        <source>First curve:</source>
        <translation>Première courbe :</translation>
    </message>
    <message>
        <source>Second curve:</source>
        <translation>Deuxième courbe :</translation>
    </message>
    <message>
        <source>Vertical correction:</source>
        <translation>Correction verticale :</translation>
    </message>
    <message>
        <source>Horizontal correction:</source>
        <translation>Correction horizontale :</translation>
    </message>
    <message>
        <source>Center of the circle:</source>
        <translation>Centre du cercle :</translation>
    </message>
    <message>
        <source>Tangent point:</source>
        <translation>Point tangent :</translation>
    </message>
    <message>
        <source>Circle radius:</source>
        <translation>Rayon du cercle :</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>C1: angle:</source>
        <translation>C1 : angle :</translation>
    </message>
    <message>
        <source>C1: length:</source>
        <translation>C1 : longueur :</translation>
    </message>
    <message>
        <source>C2: angle:</source>
        <translation>C2 : angle :</translation>
    </message>
    <message>
        <source>C2: length:</source>
        <translation>C2 : longueur :</translation>
    </message>
    <message>
        <source>First point of axis:</source>
        <translation>Premier point de l&apos;axe :</translation>
    </message>
    <message>
        <source>Second point of axis:</source>
        <translation>Deuxième point de l&apos;axe :</translation>
    </message>
    <message>
        <source>Axis point:</source>
        <translation>Point de l&apos;axe :</translation>
    </message>
    <message>
        <source>Suffix:</source>
        <translation>Suffixe :</translation>
    </message>
    <message>
        <source>Origin point:</source>
        <translation>Point d&apos;origine :</translation>
    </message>
    <message>
        <source>Axis type:</source>
        <translation>Type d&apos;axe :</translation>
    </message>
    <message>
        <source>Rotation angle:</source>
        <translation>Angle de rotation :</translation>
    </message>
    <message>
        <source>Fourth point:</source>
        <translation>Quatrième point :</translation>
    </message>
    <message>
        <source>Pen style:</source>
        <translation>Style de stylo:</translation>
    </message>
    <message>
        <source>Approximation scale:</source>
        <translation>Approximation d&apos;échelle :</translation>
    </message>
    <message>
        <source>Rotation origin point:</source>
        <translation>Point d&apos;origine de la rotation:</translation>
    </message>
    <message>
        <source>Notes:</source>
        <translation>Notes:</translation>
    </message>
    <message>
        <source>Alias:</source>
        <translation>Alias :</translation>
    </message>
    <message>
        <source>Alias1:</source>
        <translation>Alias1:</translation>
    </message>
    <message>
        <source>Alias2:</source>
        <translation>Alias2 :</translation>
    </message>
    <message>
        <source>Point of intersection circle and segment</source>
        <translation>Point à l&apos;intersection d&apos;un cercle et d&apos;un segment</translation>
    </message>
    <message>
        <source>Background image</source>
        <translation>Image en arrière-plan</translation>
    </message>
    <message>
        <source>Hold:</source>
        <translation>Ancrer :</translation>
    </message>
    <message>
        <source>Visible:</source>
        <translation>Visible :</translation>
    </message>
    <message>
        <source>Opacity:</source>
        <translation>Opacité :</translation>
    </message>
</context>
<context>
    <name>VToolPointFromArcAndTangent</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Tangent to arc &apos;%2&apos; from point &apos;%3&apos; cannot be found</source>
        <translation>Erreur de calcul du point &apos;%1&apos;. La tangente à l&apos;arc &apos;%2&apos; du point &apos;%3&apos; ne peut être trouvée</translation>
    </message>
</context>
<context>
    <name>VToolPointFromCircleAndTangent</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Tangent to circle with center &apos;%2&apos; and radius &apos;%3&apos; from point &apos;%4&apos; cannot be found</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Il n&apos;existe pas de tangente au cercle de centre &apos;%2&apos; et de rayon &apos;%3&apos;, depuis le point &apos;%4&apos;</translation>
    </message>
</context>
<context>
    <name>VToolPointOfContact</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Error calculating point &apos;%1&apos;. Circle with center &apos;%2&apos; and radius &apos;%3&apos; doesn&apos;t have intersection with line (%4;%5)</source>
        <translation>Erreur de calcul du point &apos;%1&apos;. Le cercle de centre &apos;%2&apos; et de rayon &apos;%3&apos; ne croise pas la ligne (%4;%5)</translation>
    </message>
</context>
<context>
    <name>VToolPointOfIntersectionArcs</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Arcs &apos;%2&apos; and &apos;%3&apos; have no point of intersection</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Les arcs &apos;%2&apos; et &apos;%3&apos; n&apos;ont pas de point d&apos;intersection</translation>
    </message>
</context>
<context>
    <name>VToolPointOfIntersectionCircles</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Circles with centers in points &apos;%2&apos; and &apos;%3&apos; have no point of intersection</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Les cercles &apos;%2&apos; et &apos;%3&apos; n&apos;ont pas de point d&apos;intersection</translation>
    </message>
</context>
<context>
    <name>VToolPointOfIntersectionCurves</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Curves &apos;%2&apos; and &apos;%3&apos; have no point of intersection</source>
        <translation>Impossible de calculer l&apos;emplacement du point &apos;%1&apos;. Les courbes &apos;%2&apos; et &apos;%3&apos; n&apos;ont pas de point d&apos;intersection</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <source>Curve 1 segment 1</source>
        <translation>Courbe 1 segment 1</translation>
    </message>
    <message>
        <source>Curve 1 segment 2</source>
        <translation>Courbe 1 segment 2</translation>
    </message>
    <message>
        <source>Curve 2 segment 1</source>
        <translation>Courbe 2 segment 1</translation>
    </message>
    <message>
        <source>Curve 2 segment 2</source>
        <translation>Courbe 2 segment 2</translation>
    </message>
</context>
<context>
    <name>VToolRotation</name>
    <message>
        <source>Origin point</source>
        <translation>Point d&apos;origine</translation>
    </message>
    <message>
        <source>Rotation angle</source>
        <translation>Angle de rotation</translation>
    </message>
    <message>
        <source>rotate</source>
        <translation>tourner</translation>
    </message>
</context>
<context>
    <name>VToolSeamAllowance</name>
    <message>
        <source>Current seam allowance</source>
        <translation>Marge de couture actuelle</translation>
    </message>
    <message>
        <source>move pattern piece label</source>
        <translation>Déplacer l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>resize pattern piece label</source>
        <translation>Redimensionner l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>rotate pattern piece label</source>
        <translation>Pivoter l&apos;étiquette de l&apos;élément de patron</translation>
    </message>
    <message>
        <source>move pattern info label</source>
        <translation>Déplacer l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>resize pattern info label</source>
        <translation>Redimensionner l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>rotate pattern info label</source>
        <translation>Pivoter l&apos;étiquette d&apos;information du patron</translation>
    </message>
    <message>
        <source>move grainline</source>
        <translation>déplacer le droit-fil</translation>
    </message>
    <message>
        <source>resize grainline</source>
        <translation>redimensionner le droit-fil</translation>
    </message>
    <message>
        <source>rotate grainline</source>
        <translation>Pivoter le droit fil</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>In layout</source>
        <translation>Dans isolation</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>apply save detail options</source>
        <translation>Appliquer les modifications</translation>
    </message>
    <message>
        <source>multi deletion</source>
        <translation>Suppression multiple</translation>
    </message>
    <message>
        <source>Forbid flipping</source>
        <translation>Interdire la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>Force flipping</source>
        <translation>Forcer la reproduction en miroir des objets</translation>
    </message>
    <message>
        <source>This id (%1) is not unique.</source>
        <translation>Cet identifiant (%1) n&apos;est pas unique.</translation>
    </message>
    <message>
        <source>Tool was used after deleting.</source>
        <translation>L&apos;outil a été utilisé après suppression.</translation>
    </message>
</context>
<context>
    <name>VToolShoulderPoint</name>
    <message>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
</context>
<context>
    <name>VToolTriangle</name>
    <message>
        <source>Error calculating point &apos;%1&apos;. Point of intersection cannot be found</source>
        <translation>Erreur de calcul du point &apos;%1&apos;. Le point d&apos;intersection n&apos;a pas été trouvé</translation>
    </message>
</context>
<context>
    <name>VToolUnionDetails</name>
    <message>
        <source>union details</source>
        <translation>Fusion de pièces de patron</translation>
    </message>
    <message>
        <source>United detail</source>
        <translation type="vanished">Pièces de patron fusionnées</translation>
    </message>
    <message>
        <source>This id (%1) is not unique.</source>
        <translation>Cet identifiant (%1) n&apos;est pas unique.</translation>
    </message>
</context>
<context>
    <name>VTranslateVars</name>
    <message>
        <source>Bunka</source>
        <comment>System name</comment>
        <translation>Bunka</translation>
    </message>
    <message>
        <source>Bunka Fashion College</source>
        <comment>Author name</comment>
        <translation>Bunka Fashion College</translation>
    </message>
    <message>
        <source>Fundamentals of Garment Design</source>
        <comment>Book name</comment>
        <translation>Fundamentals of Garment Design</translation>
    </message>
    <message>
        <source>Barnfield and Richard</source>
        <comment>System name</comment>
        <translation>Barnfield and Richard</translation>
    </message>
    <message>
        <source>Jo Barnfield and Andrew Richards</source>
        <comment>Author name</comment>
        <translation>Jo Barnfield and Andrew Richards</translation>
    </message>
    <message>
        <source>Pattern Making Primer</source>
        <comment>Book name</comment>
        <translation>Pattern Making Primer</translation>
    </message>
    <message>
        <source>Friendship/Women</source>
        <comment>System name</comment>
        <translation>Friendship/Women</translation>
    </message>
    <message>
        <source>Elizabeth Friendship</source>
        <comment>Author name</comment>
        <translation>Elizabeth Friendship</translation>
    </message>
    <message>
        <source>Creating Historical Clothes - Pattern Cutting from the 16th to the 19th Centuries</source>
        <comment>Book name</comment>
        <translation>Creating Historical Clothes - Pattern Cutting from the 16th to the 19th Centuries</translation>
    </message>
    <message>
        <source>Morris, K.</source>
        <comment>System name</comment>
        <translation>Morris, K.</translation>
    </message>
    <message>
        <source>Karen Morris</source>
        <comment>Author name</comment>
        <translation>Karen Morris</translation>
    </message>
    <message>
        <source>Sewing Lingerie that Fits</source>
        <comment>Book name</comment>
        <translation>Sewing Lingerie that Fits</translation>
    </message>
    <message>
        <source>Castro</source>
        <comment>System name</comment>
        <translation>Castro</translation>
    </message>
    <message>
        <source>Lucia Mors de Castro</source>
        <comment>Author name</comment>
        <translation>Lucia Mors de Castro</translation>
    </message>
    <message>
        <source>Patternmaking in Practic</source>
        <comment>Book name</comment>
        <translation>Patternmaking in Practic</translation>
    </message>
    <message>
        <source>Kim &amp; Uh</source>
        <comment>System name</comment>
        <translation>Kim &amp; Uh</translation>
    </message>
    <message>
        <source>Injoo Kim and Mykyung Uh</source>
        <comment>Author name</comment>
        <translation>Injoo Kim and Mykyung Uh</translation>
    </message>
    <message>
        <source>Apparel Making in Fashion Design</source>
        <comment>Book name</comment>
        <translation>Apparel Making in Fashion Design</translation>
    </message>
    <message>
        <source>Waugh</source>
        <comment>System name</comment>
        <translation>Waugh</translation>
    </message>
    <message>
        <source>Norah Waugh</source>
        <comment>Author name</comment>
        <translation>Norah Waugh</translation>
    </message>
    <message>
        <source>Corsets and Crinolines</source>
        <comment>Book name</comment>
        <translation>Corsets and Crinolines</translation>
    </message>
    <message>
        <source>Grimble</source>
        <comment>System name</comment>
        <translation>Grimble</translation>
    </message>
    <message>
        <source>Frances Grimble</source>
        <comment>Author name</comment>
        <translation>Frances Grimble</translation>
    </message>
    <message>
        <source>Fashions of the Gilded Age</source>
        <comment>Book name</comment>
        <translation>Fashions of the Gilded Age</translation>
    </message>
    <message>
        <source>Thornton&apos;s International System</source>
        <comment>System name</comment>
        <translation>Thornton&apos;s International System</translation>
    </message>
    <message>
        <source>ed. R. L. Shep</source>
        <comment>Author name</comment>
        <translation>ed. R. L. Shep</translation>
    </message>
    <message>
        <source>The Great War: Styles and Patterns of the 1910s</source>
        <comment>Book name</comment>
        <translation>The Great War: Styles and Patterns of the 1910s</translation>
    </message>
    <message>
        <source>Hillhouse &amp; Mansfield</source>
        <comment>System name</comment>
        <translation>Hillhouse &amp; Mansfield</translation>
    </message>
    <message>
        <source>Marion S. Hillhouse and Evelyn A. Mansfield</source>
        <comment>Author name</comment>
        <translation>Marion S. Hillhouse and Evelyn A. Mansfield</translation>
    </message>
    <message>
        <source>Dress Design: Draping and Flat Pattern Making</source>
        <comment>Book name</comment>
        <translation>Dress Design: Draping and Flat Pattern Making</translation>
    </message>
    <message>
        <source>Pivnick</source>
        <comment>System name</comment>
        <translation>Pivnick</translation>
    </message>
    <message>
        <source>Esther Kaplan Pivnick</source>
        <comment>Author name</comment>
        <translation>Esther Kaplan Pivnick</translation>
    </message>
    <message>
        <source>How to Design Beautiful Clothes: Designing and Pattern Making</source>
        <comment>Book name</comment>
        <translation>How to Design Beautiful Clothes: Designing and Pattern Making</translation>
    </message>
    <message>
        <source>Minister &amp; Son</source>
        <comment>System name</comment>
        <translation>Minister &amp; Son</translation>
    </message>
    <message>
        <source>Edward Minister &amp; Son, ed. R. L. Shep</source>
        <comment>Author name</comment>
        <translation>Edward Minister &amp; Son, ed. R. L. Shep</translation>
    </message>
    <message>
        <source>The Complete Guide to Practical Cutting (1853)</source>
        <comment>Book name</comment>
        <translation>The Complete Guide to Practical Cutting (1853)</translation>
    </message>
    <message>
        <source>Strickland</source>
        <comment>System name</comment>
        <translation>Strickland</translation>
    </message>
    <message>
        <source>Gertrude Strickland</source>
        <comment>Author name</comment>
        <translation>Gertrude Strickland</translation>
    </message>
    <message>
        <source>A Tailoring Manual</source>
        <comment>Book name</comment>
        <translation>A Tailoring Manual</translation>
    </message>
    <message>
        <source>Loh &amp; Lewis</source>
        <comment>System name</comment>
        <translation>Loh &amp; Lewis</translation>
    </message>
    <message>
        <source>May Loh and Diehl Lewis</source>
        <comment>Author name</comment>
        <translation>May Loh and Diehl Lewis</translation>
    </message>
    <message>
        <source>Patternless Fashion Design</source>
        <comment>Book name</comment>
        <translation>Patternless Fashion Design</translation>
    </message>
    <message>
        <source>Morris, F. R.</source>
        <comment>System name</comment>
        <translation>Morris, F. R.</translation>
    </message>
    <message>
        <source>F. R. Morris</source>
        <comment>Author name</comment>
        <translation>F. R. Morris</translation>
    </message>
    <message>
        <source>Ladies Garment Cutting and Making</source>
        <comment>Book name</comment>
        <translation>Ladies Garment Cutting and Making</translation>
    </message>
    <message>
        <source>Mason</source>
        <comment>System name</comment>
        <translation>Mason</translation>
    </message>
    <message>
        <source>Gertrude Mason</source>
        <comment>Author name</comment>
        <translation>Gertrude Mason</translation>
    </message>
    <message>
        <source>Gertrude Mason&apos;s Patternmaking Book</source>
        <comment>Book name</comment>
        <translation>Gertrude Mason&apos;s Patternmaking Book</translation>
    </message>
    <message>
        <source>Kimata</source>
        <comment>System name</comment>
        <translation>Kimata</translation>
    </message>
    <message>
        <source>K. Kimata</source>
        <comment>Author name</comment>
        <translation>K. Kimata</translation>
    </message>
    <message>
        <source>K.Kimata&apos;s Simplified Drafting Book for Dressmaking</source>
        <comment>Book name</comment>
        <translation>K.Kimata&apos;s Simplified Drafting Book for Dressmaking</translation>
    </message>
    <message>
        <source>Master Designer</source>
        <comment>System name</comment>
        <translation>Master Designer</translation>
    </message>
    <message>
        <source>The Master Designer (Chicago, IL)</source>
        <comment>Author name</comment>
        <translation>The Master Designer (Chicago, IL)</translation>
    </message>
    <message>
        <source>Master Designer&apos;s System of Designing, Cutting and Grading</source>
        <comment>Book name</comment>
        <translation>Master Designer&apos;s System of Designing, Cutting and Grading</translation>
    </message>
    <message>
        <source>Kopp</source>
        <comment>System name</comment>
        <translation>Kopp</translation>
    </message>
    <message>
        <source>Ernestine Kopp, Vittorina Rolfo, Beatrice Zelin, Lee Gross</source>
        <comment>Author name</comment>
        <translation>Ernestine Kopp, Vittorina Rolfo, Beatrice Zelin, Lee Gross</translation>
    </message>
    <message>
        <source>How to Draft Basic Patterns</source>
        <comment>Book name</comment>
        <translation>How to Draft Basic Patterns</translation>
    </message>
    <message>
        <source>Ekern</source>
        <comment>System name</comment>
        <translation>Ekern</translation>
    </message>
    <message>
        <source>Doris Ekern</source>
        <comment>Author name</comment>
        <translation>Doris Ekern</translation>
    </message>
    <message>
        <source>Slacks Cut-to-Fit for Your Figure</source>
        <comment>Book name</comment>
        <translation>Slacks Cut-to-Fit for Your Figure</translation>
    </message>
    <message>
        <source>Doyle</source>
        <comment>System name</comment>
        <translation>Doyle</translation>
    </message>
    <message>
        <source>Sarah J. Doyle</source>
        <comment>Author name</comment>
        <translation>Sarah J. Doyle</translation>
    </message>
    <message>
        <source>Sarah&apos;s Key to Pattern Drafting</source>
        <comment>Book name</comment>
        <translation>Sarah&apos;s Key to Pattern Drafting</translation>
    </message>
    <message>
        <source>Shelton</source>
        <comment>System name</comment>
        <translation>Shelton</translation>
    </message>
    <message>
        <source>Karla J. Shelton</source>
        <comment>Author name</comment>
        <translation>Karla J. Shelton</translation>
    </message>
    <message>
        <source>Design and Sew Jeans</source>
        <comment>Book name</comment>
        <translation>Design and Sew Jeans</translation>
    </message>
    <message>
        <source>Lady Boutique</source>
        <comment>System name</comment>
        <translation>Lady Boutique</translation>
    </message>
    <message>
        <source>Lady Boutique</source>
        <comment>Author name</comment>
        <translation>Lady Boutique</translation>
    </message>
    <message>
        <source>Lady Boutique magazine (Japan)</source>
        <comment>Book name</comment>
        <translation>Lady Boutique magazine (Japan)</translation>
    </message>
    <message>
        <source>Rohr</source>
        <comment>System name</comment>
        <translation>Rohr</translation>
    </message>
    <message>
        <source>M. Rohr</source>
        <comment>Author name</comment>
        <translation>M. Rohr</translation>
    </message>
    <message>
        <source>Pattern Drafting and Grading: Women&apos;s nd Misses&apos; Garment Design</source>
        <comment>Book name</comment>
        <translation>Pattern Drafting and Grading: Women&apos;s nd Misses&apos; Garment Design</translation>
    </message>
    <message>
        <source>Moore</source>
        <comment>System name</comment>
        <translation>Moore</translation>
    </message>
    <message>
        <source>Dorothy Moore</source>
        <comment>Author name</comment>
        <translation>Dorothy Moore</translation>
    </message>
    <message>
        <source>Dorothy Moore&apos;s Pattern Drafting and Dressmaking</source>
        <comment>Book name</comment>
        <translation>Dorothy Moore&apos;s Pattern Drafting and Dressmaking</translation>
    </message>
    <message>
        <source>Abling</source>
        <comment>System name</comment>
        <translation>Abling</translation>
    </message>
    <message>
        <source>Bina Abling</source>
        <comment>Author name</comment>
        <translation>Bina Abling</translation>
    </message>
    <message>
        <source>Integrating Draping, Drafting and Drawing</source>
        <comment>Book name</comment>
        <translation>Integrating Draping, Drafting and Drawing</translation>
    </message>
    <message>
        <source>Fukomoto</source>
        <comment>System name</comment>
        <translation>Fukomoto</translation>
    </message>
    <message>
        <source>Sue S. Fukomoto</source>
        <comment>Author name</comment>
        <translation>Sue S. Fukomoto</translation>
    </message>
    <message>
        <source>Scientific Pattern Drafting as taught at Style Center School of Costume Design, Dressmaking and Millinery</source>
        <comment>Book name</comment>
        <translation>Scientific Pattern Drafting as taught at Style Center School of Costume Design, Dressmaking and Millinery</translation>
    </message>
    <message>
        <source>Dressmaking International</source>
        <comment>System name</comment>
        <translation>Dressmaking International</translation>
    </message>
    <message>
        <source>Dressmaking International</source>
        <comment>Author name</comment>
        <translation>Dressmaking International</translation>
    </message>
    <message>
        <source>Dressmaking International magazine (Japan)</source>
        <comment>Book name</comment>
        <translation>Dressmaking International magazine (Japan)</translation>
    </message>
    <message>
        <source>Erwin</source>
        <comment>System name</comment>
        <translation>Erwin</translation>
    </message>
    <message>
        <source>Mabel D. Erwin</source>
        <comment>Author name</comment>
        <translation>Mabel D. Erwin</translation>
    </message>
    <message>
        <source>Practical Dress Design</source>
        <comment>Book name</comment>
        <translation>Practical Dress Design</translation>
    </message>
    <message>
        <source>Gough</source>
        <comment>System name</comment>
        <translation>Gough</translation>
    </message>
    <message>
        <source>E. L. G. Gough</source>
        <comment>Author name</comment>
        <translation>E. L. G. Gough</translation>
    </message>
    <message>
        <source>Principles of Garment Cutting</source>
        <comment>Book name</comment>
        <translation>Principles of Garment Cutting</translation>
    </message>
    <message>
        <source>Allemong</source>
        <comment>System name</comment>
        <translation>Allemong</translation>
    </message>
    <message>
        <source>Elizabeth M. Allemong</source>
        <comment>Author name</comment>
        <translation>Elizabeth M. Allemong</translation>
    </message>
    <message>
        <source>European Cut</source>
        <comment>Book name</comment>
        <translation>European Cut</translation>
    </message>
    <message>
        <source>McCunn</source>
        <comment>System name</comment>
        <translation>McCunn</translation>
    </message>
    <message>
        <source>Donald H. McCunn</source>
        <comment>Author name</comment>
        <translation>Donald H. McCunn</translation>
    </message>
    <message>
        <source>How to Make Your Own Sewing Patterns</source>
        <comment>Book name</comment>
        <translation>How to Make Your Own Sewing Patterns</translation>
    </message>
    <message>
        <source>Zarapkar</source>
        <comment>System name</comment>
        <translation>Zarapkar</translation>
    </message>
    <message>
        <source>Shri K. R. Zarapkar and Shri Arvind K. Zarapkar</source>
        <comment>Author name</comment>
        <translation>Shri K. R. Zarapkar and Shri Arvind K. Zarapkar</translation>
    </message>
    <message>
        <source>Zarapkar System of Cutting</source>
        <comment>Book name</comment>
        <translation>Zarapkar System of Cutting</translation>
    </message>
    <message>
        <source>Kunick</source>
        <comment>System name</comment>
        <translation>Kunick</translation>
    </message>
    <message>
        <source>Philip Kunick</source>
        <comment>Author name</comment>
        <translation>Philip Kunick</translation>
    </message>
    <message>
        <source>Sizing, Pattern Construction and Grading for Women&apos;s and Children&apos;s Garments</source>
        <comment>Book name</comment>
        <translation>Sizing, Pattern Construction and Grading for Women&apos;s and Children&apos;s Garments</translation>
    </message>
    <message>
        <source>Handford</source>
        <comment>System name</comment>
        <translation>Handford</translation>
    </message>
    <message>
        <source>Jack Handford</source>
        <comment>Author name</comment>
        <translation>Jack Handford</translation>
    </message>
    <message>
        <source>Professional Patternmaking for Designers: Women&apos;s Wear, Men&apos;s Casual Wear</source>
        <comment>Book name</comment>
        <translation>Professional Patternmaking for Designers: Women&apos;s Wear, Men&apos;s Casual Wear</translation>
    </message>
    <message>
        <source>Davis</source>
        <comment>System name</comment>
        <translation>Davis</translation>
    </message>
    <message>
        <source>R. I. Davis</source>
        <comment>Author name</comment>
        <translation>R. I. Davis</translation>
    </message>
    <message>
        <source>Men&apos;s 17th &amp; 18th Century Costume, Cut &amp; Fashion</source>
        <comment>Book name</comment>
        <translation>Men&apos;s 17th &amp; 18th Century Costume, Cut &amp; Fashion</translation>
    </message>
    <message>
        <source>MacLochlainn</source>
        <comment>System name</comment>
        <translation>MacLochlainn</translation>
    </message>
    <message>
        <source>Jason MacLochlainn</source>
        <comment>Author name</comment>
        <translation>Jason MacLochlainn</translation>
    </message>
    <message>
        <source>The Victorian Tailor: An Introduction to Period Tailoring</source>
        <comment>Book name</comment>
        <translation>The Victorian Tailor: An Introduction to Period Tailoring</translation>
    </message>
    <message>
        <source>Joseph-Armstrong</source>
        <comment>System name</comment>
        <translation>Joseph-Armstrong</translation>
    </message>
    <message>
        <source>Helen Joseph-Armstrong</source>
        <comment>Author name</comment>
        <translation>Helen Joseph-Armstrong</translation>
    </message>
    <message>
        <source>Patternmaking for Fashion Design</source>
        <comment>Book name</comment>
        <translation>Patternmaking for Fashion Design</translation>
    </message>
    <message>
        <source>Supreme System</source>
        <comment>System name</comment>
        <translation>Supreme System</translation>
    </message>
    <message>
        <source>Frederick T. Croonberg</source>
        <comment>Author name</comment>
        <translation>Frederick T. Croonberg</translation>
    </message>
    <message>
        <source>The Blue Book of Men&apos;s Tailoring, Grand Edition of Supreme System for Producing Mens Garments (1907)</source>
        <comment>Book name</comment>
        <translation>The Blue Book of Men&apos;s Tailoring, Grand Edition of Supreme System for Producing Mens Garments (1907)</translation>
    </message>
    <message>
        <source>Sugino</source>
        <comment>System name</comment>
        <translation>Sugino</translation>
    </message>
    <message>
        <source>Dressmaking</source>
        <comment>Author name</comment>
        <translation>Dressmaking</translation>
    </message>
    <message>
        <source>Pattern Drafting Vols. I, II, III (Japan)</source>
        <comment>Book name</comment>
        <translation>Pattern Drafting Vols. I, II, III (Japan)</translation>
    </message>
    <message>
        <source>Centre Point System</source>
        <comment>System name</comment>
        <translation>Centre Point System</translation>
    </message>
    <message>
        <source>Louis Devere</source>
        <comment>Author name</comment>
        <translation>Louis Devere</translation>
    </message>
    <message>
        <source>The Handbook of Practical Cutting on the Centre Point System</source>
        <comment>Book name</comment>
        <translation>The Handbook of Practical Cutting on the Centre Point System</translation>
    </message>
    <message>
        <source>Aldrich/Men</source>
        <comment>System name</comment>
        <translation>Aldrich/Men</translation>
    </message>
    <message>
        <source>Winifred Aldrich</source>
        <comment>Author name</comment>
        <translation>Winifred Aldrich</translation>
    </message>
    <message>
        <source>Metric Pattern Cutting for Menswear</source>
        <comment>Book name</comment>
        <translation>Metric Pattern Cutting for Menswear</translation>
    </message>
    <message>
        <source>Aldrich/Women</source>
        <comment>System name</comment>
        <translation>Aldrich/Women</translation>
    </message>
    <message>
        <source>Metric Pattern Cutting for Women&apos;s Wear</source>
        <comment>Book name</comment>
        <translation>Metric Pattern Cutting for Women&apos;s Wear</translation>
    </message>
    <message>
        <source>Kershaw</source>
        <comment>System name</comment>
        <translation>Kershaw</translation>
    </message>
    <message>
        <source>Gareth Kershaw</source>
        <comment>Author name</comment>
        <translation>Gareth Kershaw</translation>
    </message>
    <message>
        <source>Patternmaking for Menswear</source>
        <comment>Book name</comment>
        <translation>Patternmaking for Menswear</translation>
    </message>
    <message>
        <source>Gilewska</source>
        <comment>System name</comment>
        <translation>Gilewska</translation>
    </message>
    <message>
        <source>Teresa Gilewska</source>
        <comment>Author name</comment>
        <translation>Teresa Gilewska</translation>
    </message>
    <message>
        <source>Pattern-Drafting for Fashion: The Basics</source>
        <comment>Book name</comment>
        <translation>Pattern-Drafting for Fashion: The Basics</translation>
    </message>
    <message>
        <source>Lo</source>
        <comment>System name</comment>
        <translation>Lo</translation>
    </message>
    <message>
        <source>Dennic Chunman Lo</source>
        <comment>Author name</comment>
        <translation>Dennic Chunman Lo</translation>
    </message>
    <message>
        <source>Pattern Cutting</source>
        <comment>Book name</comment>
        <translation>Pattern Cutting</translation>
    </message>
    <message>
        <source>Bray</source>
        <comment>System name</comment>
        <translation>Bray</translation>
    </message>
    <message>
        <source>Natalie Bray</source>
        <comment>Author name</comment>
        <translation>Natalie Bray</translation>
    </message>
    <message>
        <source>Dress Pattern Designing: The Basic Principles of Cut and Fit</source>
        <comment>Book name</comment>
        <translation>Dress Pattern Designing: The Basic Principles of Cut and Fit</translation>
    </message>
    <message>
        <source>Knowles/Men</source>
        <comment>System name</comment>
        <translation>Knowles/Men</translation>
    </message>
    <message>
        <source>Lori A. Knowles</source>
        <comment>Author name</comment>
        <translation>Lori A. Knowles</translation>
    </message>
    <message>
        <source>The Practical Guide to Patternmaking for Fashion Designers: Menswear</source>
        <comment>Book name</comment>
        <translation>The Practical Guide to Patternmaking for Fashion Designers: Menswear</translation>
    </message>
    <message>
        <source>Friendship/Men</source>
        <comment>System name</comment>
        <translation>Friendship/Men</translation>
    </message>
    <message>
        <source>Pattern Cutting for Men&apos;s Costume</source>
        <comment>Book name</comment>
        <translation>Pattern Cutting for Men&apos;s Costume</translation>
    </message>
    <message>
        <source>Brown</source>
        <comment>System name</comment>
        <translation>Brown</translation>
    </message>
    <message>
        <source>P. Clement Brown</source>
        <comment>Author name</comment>
        <translation>P. Clement Brown</translation>
    </message>
    <message>
        <source>Art in Dress</source>
        <comment>Book name</comment>
        <translation>Art in Dress</translation>
    </message>
    <message>
        <source>Mitchell</source>
        <comment>System name</comment>
        <translation>Mitchell</translation>
    </message>
    <message>
        <source>Jno. J. Mitchell</source>
        <comment>Author name</comment>
        <translation>Jno. J. Mitchell</translation>
    </message>
    <message>
        <source>&quot;Standard&quot; Work on Cutting (Men&apos;s Garments) 1886: The Art and Science of Garment Cutting</source>
        <comment>Book name</comment>
        <translation>&quot;Standard&quot; Work on Cutting (Men&apos;s Garments) 1886: The Art and Science of Garment Cutting</translation>
    </message>
    <message>
        <source>GOST 17917-86</source>
        <comment>System name</comment>
        <translation>GOST 17917-86</translation>
    </message>
    <message>
        <source>Ministry of consumer industry of the USSR</source>
        <comment>Author name</comment>
        <translation>Ministry of consumer industry of the USSR</translation>
    </message>
    <message>
        <source>Standard figure boys</source>
        <comment>Book name</comment>
        <translation>Standard figure boys</translation>
    </message>
    <message>
        <source>Eddy</source>
        <comment>System name</comment>
        <translation>Eddy</translation>
    </message>
    <message>
        <source>Josephine F. Eddy and Elizabeth C. B. Wiley</source>
        <comment>Author name</comment>
        <translation>Josephine F. Eddy and Elizabeth C. B. Wiley</translation>
    </message>
    <message>
        <source>Pattern and Dress Design</source>
        <comment>Book name</comment>
        <translation>Pattern and Dress Design</translation>
    </message>
    <message>
        <source>Knowles/Women</source>
        <comment>System name</comment>
        <translation>Knowles / Femmes</translation>
    </message>
    <message>
        <source>Practical Guide to Patternmaking for Fashion Designers: Juniors, Misses, and Women</source>
        <comment>Book name</comment>
        <translation>Practical Guide to Patternmaking for Fashion Designers: Juniors, Misses, and Women</translation>
    </message>
    <message>
        <source>American Garment Cutter</source>
        <comment>System name</comment>
        <translation>American Garment Cutter</translation>
    </message>
    <message>
        <source>None</source>
        <comment>System name</comment>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>Valentina team</source>
        <comment>Author name</comment>
        <translation>L&apos;équipe de Valentina</translation>
    </message>
    <message>
        <source>Valentina&apos;s internal standard</source>
        <comment>Book name</comment>
        <translation>Standard interne à Valentina</translation>
    </message>
    <message>
        <source>Line_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Line_</translation>
    </message>
    <message>
        <source>AngleLine_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Angleligne_</translation>
    </message>
    <message>
        <source>Arc_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Arc_</translation>
    </message>
    <message>
        <source>Spl_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Spl_</translation>
    </message>
    <message>
        <source>SplPath</source>
        <comment>Do not add symbol _ to the end of name</comment>
        <translation type="vanished">SplPath</translation>
    </message>
    <message>
        <source>RadiusArc_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">RadiusArc_</translation>
    </message>
    <message>
        <source>Angle1Arc_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Angle1Arc_</translation>
    </message>
    <message>
        <source>Angle2Arc_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Angle2Arc_</translation>
    </message>
    <message>
        <source>Angle1Spl_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Angle1Spl_</translation>
    </message>
    <message>
        <source>Angle2Spl_</source>
        <comment>Left symbol _ in name</comment>
        <translation type="vanished">Angle2Spl_</translation>
    </message>
    <message>
        <source>Angle1SplPath</source>
        <comment>Do not add symbol _ to the end of name</comment>
        <translation type="vanished">Angle1SplPath</translation>
    </message>
    <message>
        <source>Angle2SplPath</source>
        <comment>Do not add symbol _ to the end of name</comment>
        <translation type="vanished">Angle2SplPath</translation>
    </message>
    <message>
        <source>sin</source>
        <comment>sine function</comment>
        <translation type="vanished">sin</translation>
    </message>
    <message>
        <source>cos</source>
        <comment>cosine function</comment>
        <translation type="vanished">cos</translation>
    </message>
    <message>
        <source>tan</source>
        <comment>tangens function</comment>
        <translation type="vanished">tan</translation>
    </message>
    <message>
        <source>asin</source>
        <comment>arcus sine function</comment>
        <translation type="vanished">asin</translation>
    </message>
    <message>
        <source>acos</source>
        <comment>arcus cosine function</comment>
        <translation type="vanished">acos</translation>
    </message>
    <message>
        <source>atan</source>
        <comment>arcus tangens function</comment>
        <translation type="vanished">atan</translation>
    </message>
    <message>
        <source>sinh</source>
        <comment>hyperbolic sine function</comment>
        <translation>sinh</translation>
    </message>
    <message>
        <source>cosh</source>
        <comment>hyperbolic cosine</comment>
        <translation>cosh</translation>
    </message>
    <message>
        <source>tanh</source>
        <comment>hyperbolic tangens function</comment>
        <translation>tanh</translation>
    </message>
    <message>
        <source>asinh</source>
        <comment>hyperbolic arcus sine function</comment>
        <translation>asinh</translation>
    </message>
    <message>
        <source>acosh</source>
        <comment>hyperbolic arcus tangens function</comment>
        <translation type="vanished">acosh</translation>
    </message>
    <message>
        <source>atanh</source>
        <comment>hyperbolic arcur tangens function</comment>
        <translation>atanh</translation>
    </message>
    <message>
        <source>log2</source>
        <comment>logarithm to the base 2</comment>
        <translation>log2</translation>
    </message>
    <message>
        <source>log10</source>
        <comment>logarithm to the base 10</comment>
        <translation>log10</translation>
    </message>
    <message>
        <source>log</source>
        <comment>logarithm to the base 10</comment>
        <translation>log</translation>
    </message>
    <message>
        <source>ln</source>
        <comment>logarithm to base e (2.71828...)</comment>
        <translation>ln</translation>
    </message>
    <message>
        <source>exp</source>
        <comment>e raised to the power of x</comment>
        <translation>exp</translation>
    </message>
    <message>
        <source>sqrt</source>
        <comment>square root of a value</comment>
        <translation>sqrt</translation>
    </message>
    <message>
        <source>sign</source>
        <comment>sign function -1 if x&lt;0; 1 if x&gt;0</comment>
        <translation>sign</translation>
    </message>
    <message>
        <source>rint</source>
        <comment>round to nearest integer</comment>
        <translation>rint</translation>
    </message>
    <message>
        <source>abs</source>
        <comment>absolute value</comment>
        <translation>abs</translation>
    </message>
    <message>
        <source>min</source>
        <comment>min of all arguments</comment>
        <translation>min</translation>
    </message>
    <message>
        <source>max</source>
        <comment>max of all arguments</comment>
        <translation>max</translation>
    </message>
    <message>
        <source>sum</source>
        <comment>sum of all arguments</comment>
        <translation>sum</translation>
    </message>
    <message>
        <source>avg</source>
        <comment>mean value of all arguments</comment>
        <translation>avg</translation>
    </message>
    <message>
        <source>fmod</source>
        <comment>Returns the floating-point remainder of numer/denom (rounded towards zero)</comment>
        <translation>fmod</translation>
    </message>
    <message>
        <source>cm</source>
        <comment>centimeter</comment>
        <translation type="vanished">cm</translation>
    </message>
    <message>
        <source>mm</source>
        <comment>millimeter</comment>
        <translation type="vanished">mm</translation>
    </message>
    <message>
        <source>in</source>
        <comment>inch</comment>
        <translation type="vanished">in</translation>
    </message>
    <message>
        <source>Line_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Ligne_</translation>
    </message>
    <message>
        <source>AngleLine_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>AngleLine_</translation>
    </message>
    <message>
        <source>Arc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Arc_</translation>
    </message>
    <message>
        <source>Spl_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Spl_</translation>
    </message>
    <message>
        <source>SplPath</source>
        <comment>Do not add symbol _ to the end of the name</comment>
        <translation>SplPath</translation>
    </message>
    <message>
        <source>RadiusArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>RadiusArc_</translation>
    </message>
    <message>
        <source>Angle1Arc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle1Arc_</translation>
    </message>
    <message>
        <source>Angle2Arc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle2Arc_</translation>
    </message>
    <message>
        <source>Angle1Spl_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle1Spl_</translation>
    </message>
    <message>
        <source>Angle2Spl_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle2Spl_</translation>
    </message>
    <message>
        <source>Angle1SplPath</source>
        <comment>Do not add symbol _ to the end of the name</comment>
        <translation>Angle1SplPath</translation>
    </message>
    <message>
        <source>Angle2SplPath</source>
        <comment>Do not add symbol _ to the end of the name</comment>
        <translation>Angle2SplPath</translation>
    </message>
    <message>
        <source>Seg_</source>
        <comment>Segment. Left symbol _ in the name</comment>
        <translation>Seg_</translation>
    </message>
    <message>
        <source>CurrentLength</source>
        <comment>Do not add space between words</comment>
        <translation>CurrentLength</translation>
    </message>
    <message>
        <source>acosh</source>
        <comment>hyperbolic arcus cosine function</comment>
        <translation>acosh</translation>
    </message>
    <message>
        <source>size</source>
        <comment>placeholder</comment>
        <translation type="vanished">taille</translation>
    </message>
    <message>
        <source>height</source>
        <comment>placeholder</comment>
        <translation type="vanished">stature</translation>
    </message>
    <message>
        <source>C1LengthSpl_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>C1LengthSpl_</translation>
    </message>
    <message>
        <source>C2LengthSpl_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>C2LengthSpl_</translation>
    </message>
    <message>
        <source>C1LengthSplPath</source>
        <comment>Do not add symbol _ to the end of the name</comment>
        <translation>C1LengthSplPath</translation>
    </message>
    <message>
        <source>C2LengthSplPath</source>
        <comment>Do not add symbol _ to the end of the name</comment>
        <translation>C2LengthSplPath</translation>
    </message>
    <message>
        <source>CurrentSeamAllowance</source>
        <comment>Do not add space between words</comment>
        <translation>CurrentSeamAllowance</translation>
    </message>
    <message>
        <source>degTorad</source>
        <comment>converts degrees to radian</comment>
        <translation>degTorad</translation>
    </message>
    <message>
        <source>radTodeg</source>
        <comment>converts radian to degrees</comment>
        <translation>radTodeg</translation>
    </message>
    <message>
        <source>sin</source>
        <comment>sine function working with radians</comment>
        <translation>sin</translation>
    </message>
    <message>
        <source>cos</source>
        <comment>cosine function working with radians</comment>
        <translation>cos</translation>
    </message>
    <message>
        <source>tan</source>
        <comment>tangens function working with radians</comment>
        <translation>tan</translation>
    </message>
    <message>
        <source>asin</source>
        <comment>arcus sine function working with radians</comment>
        <translation>asin</translation>
    </message>
    <message>
        <source>acos</source>
        <comment>arcus cosine function working with radians</comment>
        <translation>acos</translation>
    </message>
    <message>
        <source>atan</source>
        <comment>arcus tangens function working with radians</comment>
        <translation>atan</translation>
    </message>
    <message>
        <source>sinD</source>
        <comment>sine function working with degrees</comment>
        <translation>sinD</translation>
    </message>
    <message>
        <source>cosD</source>
        <comment>cosine function working with degrees</comment>
        <translation>cosD</translation>
    </message>
    <message>
        <source>tanD</source>
        <comment>tangens function working with degrees</comment>
        <translation>tanD</translation>
    </message>
    <message>
        <source>asinD</source>
        <comment>arcus sine function working with degrees</comment>
        <translation>asinD</translation>
    </message>
    <message>
        <source>acosD</source>
        <comment>arcus cosine function working with degrees</comment>
        <translation>acosD</translation>
    </message>
    <message>
        <source>atanD</source>
        <comment>arcus tangens function working with degrees</comment>
        <translation>atanD</translation>
    </message>
    <message>
        <source>M_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>M_</translation>
    </message>
    <message>
        <source>Increment_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Increment_</translation>
    </message>
    <message>
        <source>ElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>ElArc_</translation>
    </message>
    <message>
        <source>Radius1ElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Radius1ElArc_</translation>
    </message>
    <message>
        <source>Radius2ElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Radius2ElArc_</translation>
    </message>
    <message>
        <source>Angle1ElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle1ElArc_</translation>
    </message>
    <message>
        <source>Angle2ElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>Angle2ElArc_</translation>
    </message>
    <message>
        <source>r2cm</source>
        <comment>round to up to 1 decimal</comment>
        <translation>r2cm</translation>
    </message>
    <message>
        <source>csrCm</source>
        <comment>cut, split and rotate modeling operation. Takes cm units.</comment>
        <translation>csrCm</translation>
    </message>
    <message>
        <source>csrInch</source>
        <comment>cut, split and rotate modeling operation. Takes inch units.</comment>
        <translation>csrInch</translation>
    </message>
    <message>
        <source>RotationElArc_</source>
        <comment>Left symbol _ in the name</comment>
        <translation>RotationElArc_</translation>
    </message>
    <message>
        <source>converts radian to degrees</source>
        <comment>function radTodeg</comment>
        <translation>convertit les radians en degrés</translation>
    </message>
    <message>
        <source>sine function working with radians</source>
        <comment>function sin</comment>
        <translation>fonction sinus en radians</translation>
    </message>
    <message>
        <source>cosine function working with radians</source>
        <comment>function cos</comment>
        <translation>fonction cosinus en degrés</translation>
    </message>
    <message>
        <source>tangens function working with radians</source>
        <comment>function tan</comment>
        <translation>fonction tangente en radians</translation>
    </message>
    <message>
        <source>arcus sine function working with radians</source>
        <comment>function asin</comment>
        <translation>fonction arc sinus en radians</translation>
    </message>
    <message>
        <source>arcus cosine function working with radians</source>
        <comment>function acos</comment>
        <translation>fonction arc cosinus en radians</translation>
    </message>
    <message>
        <source>arcus tangens function working with radians</source>
        <comment>function atan</comment>
        <translation>fonction arc tangente en radians</translation>
    </message>
    <message>
        <source>hyperbolic sine function</source>
        <comment>function sinh</comment>
        <translation>sinus hyperbolique</translation>
    </message>
    <message>
        <source>hyperbolic cosine</source>
        <comment>function cosh</comment>
        <translation>cosinus hyperbolique</translation>
    </message>
    <message>
        <source>hyperbolic tangens function</source>
        <comment>function tanh</comment>
        <translation>tangente hyperbolique</translation>
    </message>
    <message>
        <source>hyperbolic arcus sine function</source>
        <comment>function asinh</comment>
        <translation>fonction arc sinus hyperbolique</translation>
    </message>
    <message>
        <source>hyperbolic arcus cosine function</source>
        <comment>function acosh</comment>
        <translation>fonction arc cosinus hyperbolique</translation>
    </message>
    <message>
        <source>hyperbolic arcur tangens function</source>
        <comment>function atanh</comment>
        <translation>fonction arc tangente hyperbolique</translation>
    </message>
    <message>
        <source>sine function working with degrees</source>
        <comment>function sinD</comment>
        <translation>fonction sinus en degrés</translation>
    </message>
    <message>
        <source>cosine function working with degrees</source>
        <comment>function cosD</comment>
        <translation>fonction cosinus en degrés</translation>
    </message>
    <message>
        <source>tangens function working with degrees</source>
        <comment>function tanD</comment>
        <translation>fonction tangente en degrés</translation>
    </message>
    <message>
        <source>arcus sine function working with degrees</source>
        <comment>function asinD</comment>
        <translation>fonction arc sinus en degrés</translation>
    </message>
    <message>
        <source>arcus cosine function working with degrees</source>
        <comment>function acosD</comment>
        <translation>fonction arc cosinus en degrés</translation>
    </message>
    <message>
        <source>arcus tangens function working with degrees</source>
        <comment>function atanD</comment>
        <translation>fonction arc tangente en degrés</translation>
    </message>
    <message>
        <source>logarithm to the base 2</source>
        <comment>function log2</comment>
        <translation>logarithme base 2</translation>
    </message>
    <message>
        <source>logarithm to the base 10</source>
        <comment>function log10</comment>
        <translation>logarithme base 10</translation>
    </message>
    <message>
        <source>logarithm to the base 10</source>
        <comment>function log</comment>
        <translation>logarithme base 10</translation>
    </message>
    <message>
        <source>logarithm to base e (2.71828...)</source>
        <comment>function ln</comment>
        <translation>logarithme e (2.71828...)</translation>
    </message>
    <message>
        <source>e raised to the power of x</source>
        <comment>function exp</comment>
        <translation>valeur e à la puissance x</translation>
    </message>
    <message>
        <source>square root of a value</source>
        <comment>function sqrt</comment>
        <translation>racine carrée</translation>
    </message>
    <message>
        <source>sign function -1 if x&lt;0; 1 if x&gt;0</source>
        <comment>function sign</comment>
        <translation>extrait le signe d&apos;un argument ; renvoie -1 si x&lt;0; 1 si x&gt;0</translation>
    </message>
    <message>
        <source>round to nearest integer</source>
        <comment>function rint</comment>
        <translation>arrondi à 2 décimales</translation>
    </message>
    <message>
        <source>round to up to 1 decimal</source>
        <comment>function r2cm</comment>
        <translation>arrondi à 1 décimale</translation>
    </message>
    <message>
        <source>cut, split and rotate modeling operation. Takes cm units.</source>
        <comment>function csrCm</comment>
        <translation>transformation découpe, sépare et fait pivoter. En cms.</translation>
    </message>
    <message>
        <source>cut, split and rotate modeling operation. Takes inch units.</source>
        <comment>function csrInch</comment>
        <translation>transformation découpe, sépare et fait pivoter. En pouces.</translation>
    </message>
    <message>
        <source>absolute value</source>
        <comment>function abs</comment>
        <translation>valeur absolue</translation>
    </message>
    <message>
        <source>min of all arguments</source>
        <comment>function min</comment>
        <translation>retourne la valeur min de tous les arguments</translation>
    </message>
    <message>
        <source>max of all arguments</source>
        <comment>function max</comment>
        <translation>retourne la valeur max de tous les arguments</translation>
    </message>
    <message>
        <source>sum of all arguments</source>
        <comment>function sum</comment>
        <translation>somme de tous les arguments</translation>
    </message>
    <message>
        <source>mean value of all arguments</source>
        <comment>function avg</comment>
        <translation>moyenne de tous les arguments</translation>
    </message>
    <message>
        <source>Returns the floating-point remainder of numer/denom (rounded towards zero)</source>
        <comment>function fmod</comment>
        <translation>retourne le reste à la virgule flottante d&apos;une division</translation>
    </message>
    <message>
        <source>warning</source>
        <comment>Calculation warning</comment>
        <translation>warning</translation>
    </message>
    <message>
        <source>Show a warning in calculations</source>
        <comment>function warning</comment>
        <translation>affiche un message d&apos;avertissement lorsque le résultat d&apos;un calcul est retourné</translation>
    </message>
    <message>
        <source>converts degrees to radian</source>
        <comment>function degTorad</comment>
        <translation>convertit les degrés en radians</translation>
    </message>
</context>
<context>
    <name>VVITConverter</name>
    <message>
        <source>Unexpected version &quot;%1&quot;.</source>
        <translation type="vanished">Version &quot;%1&quot; inattendue. </translation>
    </message>
    <message>
        <source>Error restoring backup file: %1.</source>
        <translation type="vanished">Erreur de restauration du fichier de sauvegarde: %1.</translation>
    </message>
</context>
<context>
    <name>VVSTConverter</name>
    <message>
        <source>Unexpected version &quot;%1&quot;.</source>
        <translation type="vanished">Version &quot;%1&quot; inattendue. </translation>
    </message>
    <message>
        <source>Error restoring backup file: %1.</source>
        <translation type="vanished">Erreur de restauration du fichier de sauvegarde: %1.</translation>
    </message>
</context>
<context>
    <name>VValentinaSettings</name>
    <message>
        <source>layouts</source>
        <translation>plans de coupe</translation>
    </message>
</context>
<context>
    <name>VWidgetBackgroundImages</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Transformation</source>
        <translation>Transformation</translation>
    </message>
    <message>
        <source>Translate</source>
        <comment>Translate piece</comment>
        <translation>Déplacer</translation>
    </message>
    <message>
        <source>Horizontal:</source>
        <translation>horizontal :</translation>
    </message>
    <message>
        <source>Vertical:</source>
        <translation>vertical :</translation>
    </message>
    <message>
        <source>Relative translation</source>
        <translation>Déplacement relatif</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Stature:</translation>
    </message>
    <message>
        <source>Scale proportionally</source>
        <translation>Echelle de proportionnalité</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Angle:</translation>
    </message>
    <message>
        <source>Z Value</source>
        <translation>Valeur Z</translation>
    </message>
    <message>
        <source>Background image</source>
        <translation>Image en arrière-plan</translation>
    </message>
    <message>
        <source>Hold</source>
        <translation>Ancrer</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <source>Reset transformation</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Hold All</source>
        <translation>Ancrer tout</translation>
    </message>
    <message>
        <source>Unhold All</source>
        <translation>Désancrer tout</translation>
    </message>
    <message>
        <source>Hide All</source>
        <translation>Tout cacher</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation>Tout montrer</translation>
    </message>
    <message>
        <source>Pixels</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Millimiters</source>
        <translation>Millimètres</translation>
    </message>
    <message>
        <source>Centimeters</source>
        <translation>Centimètres</translation>
    </message>
    <message>
        <source>Inches</source>
        <translation>Pouces</translation>
    </message>
</context>
<context>
    <name>VWidgetDetails</name>
    <message>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <source>Unnamed</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>Tout selectionner</translation>
    </message>
    <message>
        <source>Select none</source>
        <translation>Sélectionner aucun</translation>
    </message>
    <message>
        <source>select all details</source>
        <translation>Selectionner toutes les pièces</translation>
    </message>
    <message>
        <source>select none details</source>
        <translation>Ne sélectionner aucune pièce</translation>
    </message>
    <message>
        <source>Invert selection</source>
        <translation>Inverser la sélection</translation>
    </message>
    <message>
        <source>invert selection</source>
        <translation>inverser la sélection</translation>
    </message>
    <message>
        <source>Hide not in layout</source>
        <translation>Afficher dans le plan de coupe</translation>
    </message>
    <message>
        <source>Piece options</source>
        <translation>Options de pièce</translation>
    </message>
    <message>
        <source>Delete piece</source>
        <translation>Supprimer la pièce</translation>
    </message>
    <message>
        <source>Cannot find piece by id &apos;%1&apos;</source>
        <translation>Impossible de trouver la pièce avec l&apos;identifiant &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>VWidgetGroups</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Formulaire</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Renommer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Montrer</translation>
    </message>
    <message>
        <source>Hide All</source>
        <translation>Tout cacher</translation>
    </message>
    <message>
        <source>Show All</source>
        <translation>Tout montrer</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation>Marqueurs:</translation>
    </message>
    <message>
        <source>Separate each tag with comma.</source>
        <translation>Séparez chaque balise par une virgule.</translation>
    </message>
    <message>
        <source>Filter by tags</source>
        <translation>Filtrer par étiquettes</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Categories: %1.</source>
        <translation>Catégories : %1.</translation>
    </message>
</context>
<context>
    <name>VisToolAlongLine</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolArc</name>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2, first angle = %3°; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the first angle, &lt;b&gt;%4&lt;/b&gt; - sticking angle, &lt;b&gt;%5&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2, premier angle = %3°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du premier angle, &lt;b&gt;%4&lt;/b&gt; - angle magnétique, &lt;b&gt;%5&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2, first angle = %3°, second angle = %4°; &lt;b&gt;Mouse click&lt;/b&gt; - finish creating, &lt;b&gt;%5&lt;/b&gt; - sticking angle, &lt;b&gt;%6&lt;/b&gt; - sticking end, &lt;b&gt;%7&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2, angle 1 = %3°, angle 2 = %4°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la création, &lt;b&gt;%5&lt;/b&gt; - angle magnétique, &lt;b&gt;%6&lt;/b&gt; - extrêmité magnétique, &lt;b&gt;%7&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolArcWithLength</name>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2, first angle = %3°; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the first angle, &lt;b&gt;%4&lt;/b&gt; - sticking angle, &lt;b&gt;%5&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2, premier angle = %3°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du premier angle, &lt;b&gt;%4&lt;/b&gt; - angle magnétique, &lt;b&gt;%5&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Arc&lt;/b&gt;: radius = %1%2, first angle = %3°, arc length = %4%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish creating, &lt;b&gt;%5&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc&lt;/b&gt;: rayon = %1%2, angle 1 = %3°, longueur de l&apos;arc = %4%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la création, &lt;b&gt;%5&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolBisector</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolCubicBezierPath</name>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select seven or more points</source>
        <translation>&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionner au moins 7 points</translation>
    </message>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select seven or more points, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionner au moins 7 points, &lt;b&gt;Taper Entrée&lt;/b&gt; - Terminez</translation>
    </message>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select more points for complete segment</source>
        <translation>&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionner plus de points pour compléter</translation>
    </message>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select seven or more points, &lt;b&gt;%1&lt;/b&gt; - finish creation</source>
        <translation>&lt;b&gt;Courbe cubique complexe&lt;/b&gt;: sélectionnez au moins 7 points, &lt;b&gt;%1&lt;/b&gt; - terminez</translation>
    </message>
</context>
<context>
    <name>VisToolCurveIntersectAxis</name>
    <message>
        <source>&lt;b&gt;Intersection curve and axis&lt;/b&gt;: angle = %1°; &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Intersection courbe et axe&lt;/b&gt; : angle = %1°; &lt;b&gt;Déplacer&lt;/b&gt; - Angle magnétique, &lt;b&gt;Entrée&lt;/b&gt; - valider la création</translation>
    </message>
    <message>
        <source>&lt;b&gt;Intersection curve and axis&lt;/b&gt;: angle = %1°; &lt;b&gt;%2&lt;/b&gt; - sticking angle, &lt;b&gt;%3&lt;/b&gt; - finish creation</source>
        <translation>&lt;b&gt;Intersection courbe et axe&lt;/b&gt;: angle = %1°; &lt;b&gt;%2&lt;/b&gt; - angle magnétique, &lt;b&gt;%3&lt;/b&gt; - validez la création du point</translation>
    </message>
</context>
<context>
    <name>VisToolCutArc</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolCutSpline</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolCutSplinePath</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolEllipticalArc</name>
    <message>
        <source>&lt;b&gt;Elliptical arc&lt;/b&gt;: radius1 = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the first radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc elliptique&lt;/b&gt;: rayon1 = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du premier rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Elliptical arc&lt;/b&gt;: radius1 = %1%2, radius2 = %3%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the second radius, &lt;b&gt;%4&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc elliptique&lt;/b&gt;: rayon1 = %1%2, rayon2 = %3%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du second rayon, &lt;b&gt;%4&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Elliptical arc&lt;/b&gt;: radius1 = %1%2, radius2 = %3%2, angle1 = %4°; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the second radius, &lt;b&gt;%5&lt;/b&gt; - sticking angle, &lt;b&gt;%6&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc elliptique&lt;/b&gt;: rayon1 = %1%2, rayon2 = %3%2, angle1 = %4°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du second rayon, &lt;b&gt;%5&lt;/b&gt; - angle magnétique, &lt;b&gt;%6&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Elliptical arc&lt;/b&gt;: radius1 = %1%2, radius2 = %3%2, angle1 = %4°, angle2 = %5°; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the second radius, &lt;b&gt;%6&lt;/b&gt; - sticking angle, &lt;b&gt;%7&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc elliptique&lt;/b&gt;: rayon1 = %1%2, rayon2 = %3%2, angle1 = %4°, angle2 = %5°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du second rayon, &lt;b&gt;%6&lt;/b&gt; - angle magnétique, &lt;b&gt;%7&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>&lt;b&gt;Elliptical arc&lt;/b&gt;: radius1 = %1%2, radius2 = %3%2, angle1 = %4°, angle2 = %5°, rotation = %6°; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the second radius, &lt;b&gt;%7&lt;/b&gt; - sticking angle, &lt;b&gt;%8&lt;/b&gt; - skip</source>
        <translation>&lt;b&gt;Arc elliptique&lt;/b&gt;: rayon1 = %1%2, rayon2 = %3%2, angle1 = %4°, angle2 = %5°, rotation = %6°; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du second rayon, &lt;b&gt;%7&lt;/b&gt; - angle magnétique, &lt;b&gt;%8&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolEndLine</name>
    <message>
        <source>&lt;b&gt;Point at distance and angle&lt;/b&gt;: angle = %1°; &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Point à distance et angle&lt;/b&gt; : angle = %1°; &lt;b&gt;Déplacer&lt;/b&gt; - Angle magnétique, &lt;b&gt;Entrée&lt;/b&gt; - valider la création</translation>
    </message>
    <message>
        <source>&lt;b&gt;Point at distance and angle&lt;/b&gt;: angle = %1°, length = %2%3; &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Point à la distance et l&apos;angle&lt;/b&gt;: angle = %1°, distance = %2%3; &lt;b&gt;Touche shift&lt;/b&gt; - angle magnétique, &lt;b&gt;Entrée&lt;/b&gt; - Validez la création</translation>
    </message>
    <message>
        <source>&lt;b&gt;Point at distance and angle&lt;/b&gt;: angle = %1°, length = %2%3; &lt;b&gt;%4&lt;/b&gt; - sticking angle, &lt;b&gt;%5&lt;/b&gt; - finish creation</source>
        <translation>&lt;b&gt;Point à distance et angle&lt;/b&gt;: angle = %1°, longueur = %2%3; &lt;b&gt;%4&lt;/b&gt; - angle magnétique, &lt;b&gt;%5&lt;/b&gt; - achevez la création du point</translation>
    </message>
</context>
<context>
    <name>VisToolLineIntersectAxis</name>
    <message>
        <source>&lt;b&gt;Intersection line and axis&lt;/b&gt;: angle = %1°; &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Intersection ligne et axe&lt;/b&gt;: angle = %1°; &lt;b&gt;Touche Shift&lt;/b&gt; - Angle magnétique, &lt;b&gt;Entrée&lt;/b&gt; - Valider la création</translation>
    </message>
    <message>
        <source>&lt;b&gt;Intersection line and axis&lt;/b&gt;: angle = %1°; &lt;b&gt;%2&lt;/b&gt; - sticking angle, &lt;b&gt;%3&lt;/b&gt; - finish creation</source>
        <translation>&lt;b&gt;Intersection ligne et axe&lt;/b&gt;: angle = %1°; &lt;b&gt;%2&lt;/b&gt; - angle magnétique, &lt;b&gt;%3&lt;/b&gt; - Validez la création du point</translation>
    </message>
</context>
<context>
    <name>VisToolMove</name>
    <message>
        <source>Length = %1%2, angle = %3°, &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Mouse click&lt;/b&gt; - finish creation</source>
        <translation type="vanished">Longueur = %1%2, angle = %3°, &lt;b&gt;Touche shift&lt;/b&gt; - angle magnétique, &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - finir la création</translation>
    </message>
    <message>
        <source>Length = %1%2, angle = %3°, &lt;b&gt;%4&lt;/b&gt; - sticking angle, &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting a position</source>
        <translation>Longueur = %1%2, angle = %3°, &lt;b&gt;%4&lt;/b&gt; - angle magnétique, &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - position sélectionnée</translation>
    </message>
    <message>
        <source>Length = %1%2, angle = %3°, rotation angle = %4°, &lt;b&gt;%5&lt;/b&gt; - sticking angle, &lt;b&gt;%6&lt;/b&gt; - change rotation origin point, &lt;b&gt;Mouse click&lt;/b&gt; - finish creating</source>
        <translation>Longueur = %1%2, angle = %3°, angle de rotation = %4°, &lt;b&gt;%5&lt;/b&gt; - angle magnétique, &lt;b&gt;%6&lt;/b&gt; - modifier le point de rotation d&apos;origine, &lt;b&gt;Clic souris&lt;/b&gt; - finir création</translation>
    </message>
</context>
<context>
    <name>VisToolNormal</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolPointFromCircleAndTangent</name>
    <message>
        <source>Radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolPointOfContact</name>
    <message>
        <source>Radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolPointOfIntersectionCircles</name>
    <message>
        <source>Radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the second radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du second rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
    <message>
        <source>Radius = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the first radius, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Rayon = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection du premier rayon, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolRotation</name>
    <message>
        <source>Rotating angle = %1°, &lt;b&gt;Shift&lt;/b&gt; - sticking angle, &lt;b&gt;Mouse click&lt;/b&gt; - finish creation</source>
        <translation type="vanished">Angle de rotation = %1°, &lt;b&gt;Touche Shift&lt;/b&gt; - angle magnétique, &lt;b&gt;Clic souris&lt;/b&gt; - finir la création</translation>
    </message>
    <message>
        <source>Rotating angle = %1°, &lt;b&gt;%2&lt;/b&gt; - sticking angle, &lt;b&gt;Mouse click&lt;/b&gt; - finish creation</source>
        <translation>Angle de rotation = %1°, &lt;b&gt;%2&lt;/b&gt; - angle magnétique, &lt;b&gt;Clic souris&lt;/b&gt; - finir la création</translation>
    </message>
</context>
<context>
    <name>VisToolShoulderPoint</name>
    <message>
        <source>Length = %1%2; &lt;b&gt;Mouse click&lt;/b&gt; - finish selecting the length, &lt;b&gt;%3&lt;/b&gt; - skip</source>
        <translation>Longueur = %1%2; &lt;b&gt;Cliquez avec la souris&lt;/b&gt; - achevez la sélection de la longueur, &lt;b&gt;%3&lt;/b&gt; - terminé</translation>
    </message>
</context>
<context>
    <name>VisToolSpline</name>
    <message>
        <source>Use &lt;b&gt;Shift&lt;/b&gt; for sticking angle!</source>
        <translation type="vanished">Utilisez la touche &lt;b&gt;Shift&lt;/b&gt; pour contraindre la valeur de l&apos;angle à un multiple de 45° !</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;%1&lt;/b&gt; for sticking angle!</source>
        <translation>Utilisez &lt;b&gt;%1&lt;/b&gt; pour configurer un angle contraint !</translation>
    </message>
</context>
<context>
    <name>VisToolSplinePath</name>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select three or more points</source>
        <translation>&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionnez au moins 3 points</translation>
    </message>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select three or more points, &lt;b&gt;Enter&lt;/b&gt; - finish creation</source>
        <translation type="vanished">&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionnez au moins 3 points, &lt;b&gt;Tapez Entrée&lt;/b&gt; - Terminez</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;Shift&lt;/b&gt; for sticking angle!</source>
        <translation type="vanished">Utilisez la touche &lt;b&gt;Shift&lt;/b&gt; pour contraindre la valeur de l&apos;angle à un multiple de 45° !</translation>
    </message>
    <message>
        <source>&lt;b&gt;Curved path&lt;/b&gt;: select three or more points, &lt;b&gt;%1&lt;/b&gt; - finish creation</source>
        <translation>&lt;b&gt;Courbe cubique complexe&lt;/b&gt; : sélectionnez au moins 3 points, &lt;b&gt;%1&lt;/b&gt; - terminez</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;%1&lt;/b&gt; for sticking angle!</source>
        <translation>Utilisez &lt;b&gt;%1&lt;/b&gt; pour configurer un angle contraint !</translation>
    </message>
</context>
<context>
    <name>WatermarkWindow</name>
    <message>
        <source>Watermark</source>
        <translation>Filigrane</translation>
    </message>
    <message>
        <source>Opacity:</source>
        <translation>Opacité :</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Text:</source>
        <translation>Texte :</translation>
    </message>
    <message>
        <source>watermark text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Rotation:</source>
        <translation>Rotation :</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>Police de caractère :</translation>
    </message>
    <message>
        <source>The quick brown fox jumps over the lazy dog</source>
        <extracomment>Use native text to test a font options</extracomment>
        <translation>Remplacez ce texte</translation>
    </message>
    <message>
        <source>Edit font</source>
        <translation>Choisir la police</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation>Chemin d&apos;accès :</translation>
    </message>
    <message>
        <source>path to image</source>
        <translation>chemin d&apos;accès à l&apos;image</translation>
    </message>
    <message>
        <source>Browse…</source>
        <translation>Parcourir …</translation>
    </message>
    <message>
        <source>Gray color</source>
        <translation>Nuances de gris</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>Operations</source>
        <translation>Opérations</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Save &amp;As…</source>
        <translation>Enregistrer sous…</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <source>Watermark image</source>
        <translation>Image</translation>
    </message>
    <message>
        <source>File error.</source>
        <translation>Erreur de fichier.</translation>
    </message>
    <message>
        <source>Watermark files</source>
        <translation>Fichiers filigranes</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <source>watermark</source>
        <translation>Filigrane</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window.</source>
        <translation>Verrouillage impossible. Le fichier est déjà ouvert dans une autre fenêtre.</translation>
    </message>
    <message>
        <source>Could not save file</source>
        <translation>Impossible de sauvegarder le fichier</translation>
    </message>
    <message>
        <source>Failed to lock. This file already opened in another window. Expect collissions when run 2 copies of the program.</source>
        <translation>Verrouillage impossible car le fichier est déjà ouvert dans une autre fenêtre. Ceci se produit généralement quand 2 copies du programme sont ouvertes en même temps.</translation>
    </message>
    <message>
        <source>The document has no write permissions.</source>
        <translation>Vous n&apos;avez pas les droits d&apos;écriture sur ce document.</translation>
    </message>
    <message>
        <source>Cannot set permissions for %1 to writable.</source>
        <translation>Impossible d&apos;attribuer les droits en écriture sur %1.</translation>
    </message>
    <message>
        <source>Could not save the file.</source>
        <translation>Impossible de sauvegarder le fichier.</translation>
    </message>
    <message>
        <source>Could not save the file</source>
        <translation>Le fichier n&apos;a pas pu etre enregistré</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>The watermark has been modified.
Do you want to save your changes?</source>
        <translation>Le filigrane a été modifié.
Voulez-vous sauvegarder vos modifications ?</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>Ne pas sauvegarder</translation>
    </message>
    <message>
        <source>read only</source>
        <translation>Lecture seule</translation>
    </message>
    <message>
        <source>untitled.vwm</source>
        <translation>sansTitre.vwm</translation>
    </message>
    <message>
        <source>Confirm format rewriting</source>
        <translation>Veuillez confirmer la réécriture du format</translation>
    </message>
    <message>
        <source>This file is using previous format version v%1. The current is v%2. Saving the file with this app version will update the format version for this file. This may prevent you from be able to open the file with older app versions. Do you really want to continue?</source>
        <translation>Ce fichier a été créé sous la version v%1. Vous l&apos;ouvrez maintenant avec la version v%2. En sauvergardant le fichier sous la version actuelle, vous ne pourrez plus l&apos;ouvrir sous une version plus ancienne. Voulez-vous continuer quand même ?</translation>
    </message>
    <message>
        <source>File saved</source>
        <translation>Fichier sauvegardé</translation>
    </message>
    <message>
        <source>Locking file</source>
        <translation>Verrouiller le fichier (lecture seule)</translation>
    </message>
    <message>
        <source>This file already opened in another window. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Ce fichier est déjà ouvert dans une autre fenêtre. Ignorer pour continuer quand même (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>The lock file could not be created, for lack of permissions. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Le fichier sous lecture seule ne peut être créé car vous n&apos;avez pas les permissions nécessaires. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>Unknown error happened, for instance a full partition prevented writing out the lock file. Ignore if you want to continue (not recommended, can cause a data corruption).</source>
        <translation>Une erreur inconnue s&apos;est produite, par exemple pour cause de partition pleine. Ignorer pour continuer (déconseillé, peut entraîner une corruption de données).</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <source>Black</source>
        <comment>color</comment>
        <translation>Noir</translation>
    </message>
    <message>
        <source>Red</source>
        <comment>color</comment>
        <translation>Rouge</translation>
    </message>
    <message>
        <source>Dark red</source>
        <comment>color</comment>
        <translation>Rouge foncé</translation>
    </message>
    <message>
        <source>Green</source>
        <comment>color</comment>
        <translation>Vert</translation>
    </message>
    <message>
        <source>Dark green</source>
        <comment>color</comment>
        <translation>Vert foncé</translation>
    </message>
    <message>
        <source>Blue</source>
        <comment>color</comment>
        <translation>Bleu</translation>
    </message>
    <message>
        <source>Dark blue</source>
        <comment>color</comment>
        <translation>Bleu foncé</translation>
    </message>
    <message>
        <source>Cyan</source>
        <comment>color</comment>
        <translation>Cyan</translation>
    </message>
    <message>
        <source>Dark cyan</source>
        <comment>color</comment>
        <translation>Cyan foncé</translation>
    </message>
    <message>
        <source>Magenta</source>
        <comment>color</comment>
        <translation>Magenta</translation>
    </message>
    <message>
        <source>Dark magenta</source>
        <comment>color</comment>
        <translation>Magenta foncé</translation>
    </message>
    <message>
        <source>Yellow</source>
        <comment>color</comment>
        <translation>Jaune</translation>
    </message>
    <message>
        <source>Dark yellow</source>
        <comment>color</comment>
        <translation>Jaune foncé</translation>
    </message>
    <message>
        <source>Gray</source>
        <comment>color</comment>
        <translation>Gris</translation>
    </message>
    <message>
        <source>Dark gray</source>
        <comment>color</comment>
        <translation>Gris foncé</translation>
    </message>
    <message>
        <source>Light gray</source>
        <comment>color</comment>
        <translation>Gris clair</translation>
    </message>
    <message>
        <source>Do you want to change the permissions?</source>
        <translation>Voulez-vous modifier les permissions ?</translation>
    </message>
</context>
<context>
    <name>ZValueMoveBackgroundImage</name>
    <message>
        <source>z value move a background image</source>
        <translation>déplacer l&apos;image d&apos;une valeur Z</translation>
    </message>
</context>
<context>
    <name>mNoisyHandler</name>
    <message>
        <source>DEBUG:</source>
        <translation>DEBUGAGE:</translation>
    </message>
    <message>
        <source>WARNING:</source>
        <translation>AVERTISSEMENT:</translation>
    </message>
    <message>
        <source>CRITICAL:</source>
        <translation>CRITIQUE:</translation>
    </message>
    <message>
        <source>FATAL:</source>
        <translation>FATAL:</translation>
    </message>
    <message>
        <source>INFO:</source>
        <translation>INFO:</translation>
    </message>
    <message>
        <source>Warning.</source>
        <translation type="vanished">Avertissement.</translation>
    </message>
    <message>
        <source>Critical error.</source>
        <translation type="vanished">Erreur critique.</translation>
    </message>
    <message>
        <source>Fatal error.</source>
        <translation type="vanished">Erreur fatale.</translation>
    </message>
    <message>
        <source>Information.</source>
        <translation type="vanished">Information.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>Critical error</source>
        <translation>Erreur critique</translation>
    </message>
    <message>
        <source>Fatal error</source>
        <translation>Erreur fatale</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
</context>
<context>
    <name>vNoisyHandler</name>
    <message>
        <source>DEBUG:</source>
        <translation>DEBUGAGE:</translation>
    </message>
    <message>
        <source>WARNING:</source>
        <translation>AVERTISSEMENT:</translation>
    </message>
    <message>
        <source>CRITICAL:</source>
        <translation>CRITIQUE:</translation>
    </message>
    <message>
        <source>FATAL:</source>
        <translation>FATAL:</translation>
    </message>
    <message>
        <source>INFO:</source>
        <translation>INFO:</translation>
    </message>
    <message>
        <source>Warning.</source>
        <translation type="vanished">Avertissement.</translation>
    </message>
    <message>
        <source>Critical error.</source>
        <translation type="vanished">Erreur critique.</translation>
    </message>
    <message>
        <source>Fatal error.</source>
        <translation type="vanished">Erreur fatale.</translation>
    </message>
    <message>
        <source>Information.</source>
        <translation type="vanished">Information.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>Critical error</source>
        <translation>Erreur critique</translation>
    </message>
    <message>
        <source>Fatal error</source>
        <translation>Erreur fatale</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
</context>
</TS>
